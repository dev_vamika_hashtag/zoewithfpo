//
//  ViewController.swift
//  ZoeBlueprint
//
//  Created by Reetesh Bajpai on 03/06/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit
import Alamofire


class ViewController: BaseViewController {
    
    @IBOutlet weak var lblLogIn: UILabel!    //1
    @IBOutlet weak var lblNewToZoe: UILabel!    //8
    @IBOutlet weak var btnClickHere: UIButton!
    @IBOutlet weak var btnRegisterHere: UIButton!   //9
    @IBOutlet weak var lblForgotPassword: UILabel!
    @IBOutlet weak var lblUserName: UILabel!   //2
    @IBOutlet weak var lblPassword: UILabel!     //3
    @IBOutlet weak var txtUserName: UITextField!   //4
    @IBOutlet weak var txtPassword: UITextField!     //5
    @IBOutlet weak var btnLogin: UIButton!    //7
    @IBOutlet weak var btnShowPassword: UIButton!   //6
    
    
    override func viewDidLayoutSubviews() {
        
    }
    struct Connectivity {
        static let sharedInstance = NetworkReachabilityManager()!
        static var isConnectedToInternet:Bool {
            return self.sharedInstance.isReachable
        }
    }
    func addUnderLineToField(color:UIColor)  {
        txtUserName.setUnderLineOfColor(color: color)
        txtPassword.setUnderLineOfColor(color: color)        
    }
    func showIntroScreen(){
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let objIntro  = storyboard.instantiateViewController(withIdentifier: "introscreen") as! IntroScreenViewController
        self.present(objIntro, animated: false, completion: nil)
        
    }
    @objc func methodOfReceivedNotificationforLogout(notification: Notification) {
    }
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationforLogout(notification:)), name: Notification.Name("Removetabbar"), object: nil)
        
        super.viewDidLoad()
        if !(UserDefaults.standard.bool(forKey: "introshown")){
            
            self.showIntroScreen()
            UserDefaults.standard.set(true, forKey: "introshown")
        }
        
        self.checkForLAreadyLogin()
        
        // Do any additional setup after loading the view.
        addTextFieldEventHandling()
        customizeUIElements()
        self.txtUserName.delegate = self
        self.txtPassword.delegate = self
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil )
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0
            {
                self.view.frame.origin.y -= 10.0
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        super.setNavigationBarHidden(toHide: true)
        Global.setUpViewWithTheme(ViewController: self)
        addUnderLineToField(color: APP_BLACK_COLOR)
    }
    
    func customizeUIElements()  {
        btnLogin.layer.cornerRadius = 4
    }
    
    func addTextFieldEventHandling()  {
        txtUserName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @IBAction func showPasswordButtonTapped(_ sender: Any) {
        self.txtPassword.isSecureTextEntry = !self.txtPassword.isSecureTextEntry
        switch self.txtPassword.isSecureTextEntry {
        case true:
            btnShowPassword.setImage(UIImage(named: "eye_close_icon"), for: .normal)
            break
            
        case false:
            btnShowPassword.setImage(UIImage(named: "eye_open_icon"), for: .normal)
            break
        }
    }
    @IBAction func loginButtonTapped(_ sender: Any) {
        guard let userName = txtUserName.text, isValidUserName(text: userName) else {
            highlightTextFieldForError(textField: txtUserName, label: lblUserName, placeHolder: NSLocalizedString("Invalid User Name", comment: ""))
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please enter valid email", comment: ""), preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if (txtPassword.text!.isEmpty){
            
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please enter password", comment: ""), preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if Connectivity.isConnectedToInternet {
            
            callForLogin()
            
        } else {
            
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No internet connection", comment: ""), preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func registerButtonTapped(_ sender: Any) {
    }
    @IBAction func forgetPasswordButtonTapped(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let obj = sb.instantiateViewController(withIdentifier: "forgetPass") as! ForgetPassword
        self.present(obj, animated: true)
        
    }
    func addViewController(viewController:UIViewController)  {
        viewController.willMove(toParent: self)
        self.view.addSubview(viewController.view)
        self.addChild(viewController)
        viewController.didMove(toParent: self)
    }
    func highlightTextFieldForError(textField:UITextField,label:UILabel, placeHolder:String) {
        textField.textColor = .red
        label.textColor = .red
        textField.layer.borderWidth = 1.0
        textField.layer.borderColor = UIColor.red.cgColor
    }
}

extension ViewController:UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let textFieldText = textField.text {
            if textField == txtUserName {
                //textField.textColor = .red
                if isValidUserName(text: textFieldText) {
                    textField.textColor = APP_BLACK_COLOR
                }
            }
            if textField == txtPassword {
                if isValidPassword(text: textFieldText) {
                    textField.textColor = APP_BLACK_COLOR
                }
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtUserName {
            if isValidUserName(text: textField.text!) {
                textField.textColor = APP_BLACK_COLOR
                textField.layer.borderWidth = 0.0
            }else{
                textField.textColor = .red
                textField.layer.borderWidth = 1.0
                lblUserName.textColor = .red
                textField.layer.borderColor = UIColor.red.cgColor
            }
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtUserName {
            textField.textColor = APP_BLACK_COLOR
            lblUserName.textColor = APP_BLACK_COLOR
            textField.layer.borderWidth = 0.0
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func isValidUserName(text:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: text)
    }
    
    func isValidPassword(text:String) -> Bool {
        return false
        
    }
    
    func callForLogin(){
        ActivityLoaderView.startAnimating()
        var userInfo =  [String:String]()
        userInfo[LoginServiceStrings.keyUserName] = txtUserName.text
        userInfo[LoginServiceStrings.keyPassword] = txtPassword.text
        
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.autheticateUserForLoginService(userData: userInfo) { (response, isSuccess) in
            if isSuccess {
                if let JSON = response as? [String: Any] {
                    let message = JSON["res_status"] as! String
                    //print(message)
                    if(message == "200"){
                        //********
                        
                        //print("LOGIN SuCCEssfull");
                        let loginTypeDict = JSON["res_data"] as? NSDictionary
                        //print(loginTypeDict!)
                        let loginTypeStr = loginTypeDict?["user_type"] as? String
                        let strUserStatus = loginTypeDict?["user_status"] as? String
                        if (strUserStatus == "10"){
                            if(loginTypeStr == "CSO" || loginTypeStr == "FPO"){
                                print("Login to \(loginTypeStr)")
                                print(strUserStatus!)
                                if  let tabBarController = UIStoryboard.init(name: "Main", bundle:Bundle.main).instantiateViewController(withIdentifier: "tabbar") as? UITabBarController {
                                    tabBarController.delegate = self as UITabBarControllerDelegate
                                    let loginData:Dictionary<String, Any> = (loginTypeDict as? Dictionary<String, Any>)!
                                    
                                    let encodedData = NSKeyedArchiver.archivedData(withRootObject: loginData)
                                    UserDefaults.standard.set(encodedData, forKey: UserDefaultKeys.key_LoggedInUserData)
                                    if loginData["user_timezone"] as? String == nil{
                                        UserDefaults.standard.set("EST", forKey: UserDefaultKeys.key_userTimeZone)
                                    }else{
                                        UserDefaults.standard.set(loginData["user_timezone"] as! String, forKey: UserDefaultKeys.key_userTimeZone)
                                    }
                                   
                                    
                                    for viewController in tabBarController.viewControllers! {
                                        if let dashboardVC = viewController as? CSODashboardViewController {
                                            tabBarController.selectedViewController = dashboardVC
                                        }
                                    }
                                    self.present(tabBarController, animated: true, completion: nil)
                                }
                                
                            }else if(loginTypeStr == "VOL" || loginTypeStr == "EMP"){
                                print("Login to VOL")
                                
                                print(strUserStatus!)
                                if let zipCode = loginTypeDict?["user_zipcode"] as? String{
                                    print("ZipCode= \(zipCode)")
                                }else{
                                    let loginData:Dictionary<String, Any> = (loginTypeDict as? Dictionary<String, Any>)!
                                    
                                    let encodedData = NSKeyedArchiver.archivedData(withRootObject: loginData)
                                    UserDefaults.standard.set(encodedData, forKey: UserDefaultKeys.key_LoggedInUserData)
                                    
                                    let sampleStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                    let csoReg  = sampleStoryBoard.instantiateViewController(withIdentifier: "volreg") as! VolRegistration
                                    csoReg.userTypeName = loginTypeDict?["user_type"] as? String
                                    csoReg.screen = "EDIT VIEW"
                                    self.present(csoReg, animated: true, completion: nil)
                                    return
                                }
                                if  let tabBarController = UIStoryboard.init(name: "Main", bundle:Bundle.main).instantiateViewController(withIdentifier: "tab") as? UITabBarController {
                                    tabBarController.delegate = self as UITabBarControllerDelegate
                                    let loginData:Dictionary<String, Any> = (loginTypeDict as? Dictionary<String, Any>)!
                                    
                                    let encodedData = NSKeyedArchiver.archivedData(withRootObject: loginData)
                                    UserDefaults.standard.set(encodedData, forKey: UserDefaultKeys.key_LoggedInUserData)
                                    if let usertimeZone = loginData["user_timezone"] as? String {
                                        UserDefaults.standard.set(usertimeZone, forKey: UserDefaultKeys.key_userTimeZone)
                                    }
                                    
                                    
                                    for viewController in tabBarController.viewControllers! {
                                        if let dashboardVol = viewController as? NewVolunteerDashboard {
                                            tabBarController.selectedViewController = dashboardVol
                                        }
                                    }
                                    tabBarController.modalPresentationStyle = .fullScreen
                                    self.present(tabBarController, animated: true, completion: nil)
                                }
                            }
                        }else if (strUserStatus == "1"){
                            if (loginTypeStr == "CSO"){
                                print("GO TO CSO second Stage")
                                
                                let sampleStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                let csoReg  = sampleStoryBoard.instantiateViewController(withIdentifier: "CSORegistrationViewController") as! CSORegistration
                                csoReg.loadStage = "1"
                                csoReg.regStage1data = loginTypeDict as? Dictionary<String, Any>
                                self.present(csoReg, animated: true, completion: nil)
                                
                                
                            }else if ( loginTypeStr == "FPO"){
                                print("GO TO FPO second Stage")
                                
                                let sampleStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                let csoReg  = sampleStoryBoard.instantiateViewController(withIdentifier: "FPORegistrationViewController") as! FPORegistrationViewController
                                csoReg.editProfileDetails = loginTypeDict as? Dictionary<String, Any>
                                self.present(csoReg, animated: true, completion: nil)
                                
                                
                            }else if (loginTypeStr == "VOL" || loginTypeStr == "EMP"){
                                print("GO TO VOL OTP PAGE")
                                
                                let sb = UIStoryboard(name: "Main", bundle: nil)
                                let phone_otp = sb.instantiateViewController(withIdentifier: "phoneotp") as! PhoneOtp
                                
                                phone_otp.phoneotp =  loginTypeDict?["phone_valid"] as? String
                                phone_otp.user_id = loginTypeDict?["user_id"] as? String
                                phone_otp.user_type = loginTypeStr
                                phone_otp.modalPresentationStyle = .fullScreen
                                self.present(phone_otp, animated: true)
                            }
                        }else if (strUserStatus == "20" && loginTypeStr == "EMP"){
                            if let _  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as? Data {
                                UserDefaults.standard.removeObject(forKey: UserDefaultKeys.key_LoggedInUserData)
                            }
                            self.removeImage(itemName: "profilepic", fileExtension: "jpg")
                             
                            
                            let loginData:Dictionary<String, Any> = (loginTypeDict as? Dictionary<String, Any>)!
                            let mutableDic = NSMutableDictionary(dictionary: loginData)
                            mutableDic["user_type"] = "VOL"
                            
                            let encodedData = NSKeyedArchiver.archivedData(withRootObject: mutableDic)
                            UserDefaults.standard.set(encodedData, forKey: UserDefaultKeys.key_LoggedInUserData)
                            if let usertimeZone = loginData["user_timezone"] as? String {
                                UserDefaults.standard.set(usertimeZone, forKey: UserDefaultKeys.key_userTimeZone)
                            }
                            
                            if  let tabBarController = UIStoryboard.init(name: "Main", bundle:Bundle.main).instantiateViewController(withIdentifier: "tab") as? UITabBarController {
                                tabBarController.delegate = self as UITabBarControllerDelegate
                                for viewController in tabBarController.viewControllers! {
                                    if let dashboardVol = viewController as? NewVolunteerDashboard {
                                        tabBarController.selectedViewController = dashboardVol
                                    }
                                }
                                tabBarController.modalPresentationStyle = .fullScreen
                                self.present(tabBarController, animated: true, completion: nil)
                            }
                        }else if (strUserStatus == "2"){
                            print("GO TO CSO Third Stage")
                            
                            let sampleStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let csoReg  = sampleStoryBoard.instantiateViewController(withIdentifier: "CSORegistrationViewController") as! CSORegistration
                            csoReg.loadStage = "2"
                            csoReg.regStage2data = loginTypeDict as? Dictionary<String, Any>
                            csoReg.modalPresentationStyle = .fullScreen
                            self.present(csoReg, animated: true, completion: nil)
                            
                        }else if (strUserStatus == "3"){
                            print("GO TO CSO OTP PAGE")
                            
                            let sb = UIStoryboard(name: "Main", bundle: nil)
                            let phone_otp = sb.instantiateViewController(withIdentifier: "phoneotp") as! PhoneOtp
                            
                            phone_otp.phoneotp =  loginTypeDict?["phone_valid"] as? String
                            phone_otp.user_id = loginTypeDict?["user_id"] as? String
                            phone_otp.user_type = loginTypeStr
                            phone_otp.modalPresentationStyle = .fullScreen
                            self.present(phone_otp, animated: true)
                        }else if (strUserStatus == "30"){
                            
                            let alertMessage = JSON["res_message"] as! String
                            AlertManager.shared.showAlertWith(title: NSLocalizedString("Your account is in verification process.", comment: ""), message: "")
                        }
                        //*******************
                        
                    }else{
                        let alertMessage = JSON["res_message"] as! String
                        AlertManager.shared.showAlertWith(title: NSLocalizedString("Login Failed!", comment: ""), message: alertMessage)
                    }
                    ActivityLoaderView.stopAnimating()
                } else {
                    ActivityLoaderView.stopAnimating()
                    AlertManager.shared.showAlertWith(title: NSLocalizedString("Error Occured!", comment: ""), message: NSLocalizedString("Please try again.", comment: ""))
                }
            }else{
                AlertManager.shared.showAlertWith(title: NSLocalizedString("Login Failed!", comment: ""), message: "")
            }
        }
    }
    
    func removeImage(itemName:String, fileExtension: String) {
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        guard let dirPath = paths.first else {
            return
        }
        let filePath = "\(dirPath)/\(itemName).\(fileExtension)"
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch let error as NSError {
            print(error.debugDescription)
        }}
}

extension ViewController:UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let csoDasboardVC = viewController as? CSODashboardViewController {
            removeAllOtherViewsOfVC(viewcontroller: csoDasboardVC)
            return true
        }
        if let csoEventVC = viewController as? CSOEventsViewController {
            removeAllOtherViewsOfVC(viewcontroller: csoEventVC)
            return true
        }
        if let CSOStudents = viewController as? volunteerSeeFollowers {
            removeAllOtherViewsOfVC(viewcontroller: CSOStudents)
            return true
        }
        if let csoMessageVC = viewController as? CSOMessagingViewController {
            removeAllOtherViewsOfVC(viewcontroller: csoMessageVC)
            return true
        }
        if let MessageVCs = viewController as? LockerViewController {
            removeAllOtherViewsOfVC(viewcontroller: MessageVCs)
            return true
        }
        return true
    }
    
    func removeAllOtherViewsOfVC(viewcontroller:UIViewController)  {
        
        for vc in viewcontroller.children {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
        
    }
    
    func checkForLAreadyLogin(){
        
        if let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data? {
            let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
            
            let usertype = userIDData["user_type"] as! String
            
            if(usertype == "CSO" || usertype == "FPO"){
                print("Login to CSO")
                
                if  let tabBarController = UIStoryboard.init(name: "Main", bundle:Bundle.main).instantiateViewController(withIdentifier: "tabbar") as? UITabBarController {
                    tabBarController.delegate = self as UITabBarControllerDelegate
                    
                    for viewController in tabBarController.viewControllers! {
                        if let dashboardVC = viewController as? CSODashboardViewController {
                            tabBarController.selectedViewController = dashboardVC
                        }
                    }
                    self.present(tabBarController, animated: false, completion: nil)
                }
                
            }else if(usertype == "VOL" || usertype == "EMP"){
                print("Login to VOL")
                
                if let zipCode = userIDData["user_zipcode"] as? String{
                    print("ZipCode= \(zipCode)")
                }else{
                    let sampleStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let csoReg  = sampleStoryBoard.instantiateViewController(withIdentifier: "volreg") as! VolRegistration
                    csoReg.userTypeName = usertype
                    csoReg.screen = "EDIT VIEW"
                    self.present(csoReg, animated: true, completion: nil)
                    return
                }
                
                if  let tabBarController = UIStoryboard.init(name: "Main", bundle:Bundle.main).instantiateViewController(withIdentifier: "tab") as? UITabBarController {
                    tabBarController.delegate = self as UITabBarControllerDelegate
                    for viewController in tabBarController.viewControllers! {
                        if let dashboardVol = viewController as? NewVolunteerDashboard {
                            tabBarController.selectedViewController = dashboardVol
                        }
                    }
                    tabBarController.modalPresentationStyle = .fullScreen
                    self.present(tabBarController, animated: false, completion: nil)
                }
            }
            
        }
    }
}
