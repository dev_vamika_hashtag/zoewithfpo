//
//  VolRegistration.swift
//  ZoeBlue//print
//
//  Created by iOS Training on 26/11/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit

class VolRegistration: UIViewController,UITextFieldDelegate {
    
    var ProfileSet:String?
    @IBOutlet weak var back_button: UIButton!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    
    @IBOutlet weak var lblBasicInformation: UILabel!
    @IBOutlet var bannerHeight: NSLayoutConstraint!
    @IBOutlet weak var bannerimage: UIImageView!
    
    @IBOutlet var confirmPhoneNumberTF: UITextField!
    @IBOutlet var confirmEmailTF: UITextField!
    @IBOutlet weak var imageGender: UIImageView!
    @IBOutlet var referralCodeTF: UITextField!
    @IBOutlet weak var imageCountry: UIImageView!
    
    @IBOutlet weak var imageState: UIImageView!
    
    @IBOutlet weak var viewSetProfile: UIView!
    @IBOutlet weak var lblZoeBlueprint: UILabel!
    @IBOutlet weak var volview: UIView!
    @IBOutlet weak var scroll_view: UIScrollView!
    
    @IBOutlet weak var passwordStackView: UIStackView!
    @IBOutlet weak var confirmPasswordStackView: UIStackView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var volEmail: UITextField!
    
    @IBOutlet weak var volPassword: UITextField!
    
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var volFirstName: UITextField!
    
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblPrivate: UILabel!
    
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var btnPrivatePressed: UIButton!
    @IBOutlet weak var lblPublic: UILabel!
    @IBOutlet weak var lblSetProfile: UILabel!
    @IBOutlet weak var volLastName: UITextField!
    
    @IBOutlet weak var newCalender: UIImageView!
    @IBOutlet weak var profileDropDown: UIImageView!
    @IBOutlet weak var genderDropDown: UIImageView!
    @IBOutlet weak var stateDropDown: UIImageView!
    @IBOutlet weak var countryDropDown: UIImageView!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var cityStreetStackView: UIStackView!
    
    @IBOutlet weak var btnPublicPressed: UIButton!
    @IBOutlet weak var volPhoneNumber: UITextField!
    
    @IBOutlet weak var volStreet: UITextField!
    
    @IBOutlet weak var volCity: UITextField!
    
    @IBOutlet weak var VolDOB: UIButton!
    var screen:String?
    @IBOutlet weak var volCountry: UIButton!
    @IBOutlet weak var volState: UIButton!
    var password_secure_eye:Bool?
    @IBOutlet weak var volZipCode: UITextField!
    
    @IBOutlet weak var VolGender: UIButton!
    @IBOutlet weak var volAge: UITextField!
    @IBOutlet weak var volParent: UITextField!
    @IBOutlet weak var volAnotherNum: UITextField!
    @IBOutlet weak var volEducationCalendar: UITextField!
    @IBOutlet weak var volGenderTF: UITextField!
    var volDOB1:String?
    var user_gender:String?
    var user_countryID:String?
    var user_stateID:String?
    var user_status:String = ""
    var boolShowBackAlert = false
    
    
    @IBOutlet weak var volPasswordEyeButton: UIButton!
    @IBOutlet weak var volConfirmPassword: NSLayoutConstraint!
    
    @IBOutlet weak var volPasswordStack: NSLayoutConstraint!
    @IBOutlet weak var volconfirmPasswordEyeButton: UIButton!
    
    @IBOutlet weak var volconfirmpassword: UITextField!
    
    @IBOutlet weak var btnRegisterUpdate: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    
    
    @IBOutlet weak var selectProfiletype: UIButton!
    
    var Name : String!
    var userTypeName:String?
    //    @IBOutlet weak var lblEditSetPro: UILabel!
    //    @IBOutlet weak var lblEditPrivate: UILabel!
    //    @IBOutlet weak var lblEditPublic: UILabel!
    //
    //    @IBOutlet weak var btnEditPublicPressed: UIButton!
    //
    //    @IBOutlet weak var btnEditPrivatePressed: UIButton!
    //
    //    @IBOutlet weak var editbtnReset: UIButton!
    //
    //    @IBOutlet weak var editbtnUpdate: UIButton!
    
    override func viewDidLayoutSubviews() {
        VolGender.setDropDownImagWithInset()
        volState.setDropDownImagWithInset()
        volCountry.setDropDownImagWithInset()
        addUnderLineToField(color: APP_BLACK_COLOR)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshViewInViewDidLoad()
    }
    
    func refreshViewInViewDidLoad(){
        self.volAnotherNum.delegate = self
        confirmPhoneNumberTF.delegate = self
        bannerHeight.constant = 150
        
        
        if(self.screen == "EDIT VIEW"){
            self.volPasswordEyeButton.isHidden = true
            self.volconfirmPasswordEyeButton.isHidden = true
            self.back_button.isHidden = true
            self.notificationBtn.isHidden = false
            self.menuButton.isHidden = false
            lblBasicInformation.backgroundColor = .clear
            self.getCoverImageForRank()
        }else{
            self.ProfileSet = "10"
            self.back_button.isHidden = false
            self.menuButton.isHidden = true
            self.notificationBtn.isHidden = true
//            bannerHeight.constant = 89
//            lblBasicInformation.backgroundColor = APP_BANNER_BLUE_COLOR
//            self.bannerimage.image =  nil
            lblBasicInformation.backgroundColor = .clear
            self.bannerimage.image =  UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
        }
        volZipCode.delegate = self
        
        self.btnPrivatePressed.setImage(UIImage(named: "checked_icon"), for: .normal)
        self.btnPublicPressed.setImage(UIImage(named: "uncheked_icon"), for: .normal)
        scroll_view.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)
        
        volPassword.isSecureTextEntry = true
        volconfirmpassword.isSecureTextEntry = true
        volEmail.delegate = self
        volFirstName.delegate = self
        volLastName.delegate = self
        volStreet.delegate = self
        volPhoneNumber.delegate = self
        volCity.delegate = self
        
        volPassword.delegate = self
        volconfirmpassword.delegate = self
    }
    func refreshViewInViewWillAppear(){
        Global.setUpViewWithTheme(ViewController: self)
        profile_pic()
        
        
        if(self.screen == "EDIT VIEW"){
            boolShowBackAlert = true
            self.btnRegisterUpdate.setTitle(NSLocalizedString("Update", comment: ""), for: .normal)
            
            
            let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
            let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
            let params = userIDData["user_id"] as! String
            let obj = ServiceHandlers()
            obj.editProfile(user_id: params){(responce,isSuccess) in
                if isSuccess{
                    let data = responce as! Dictionary<String,Any>
                    // //print(data)
                    self.user_status = (data["vol_status"] as? String)!
                    self.volEmail.text = data["user_email"] as? String
                    self.confirmEmailTF.text = data["user_email"] as? String
                    self.confirmPhoneNumberTF.text = self.formattedNumber(number:(data["user_phone"] as? String)!)

//                    self.volEmail.isEnabled = false
//                    self.volEmail.textColor = .darkGray
                    if self.userTypeName == "EMP"{
                        self.referralCodeTF.text = data["fpo_referral_code"] as? String
                        self.referralCodeTF.isUserInteractionEnabled = false
                    }
                    self.volFirstName.text = data["user_f_name"] as? String
                    self.volLastName.text = data["user_l_name"] as? String
                    self.volStreet.text = data["user_address"] as? String
                    
                    self.volAge.text = data["age_on"] as? String
                    if self.volAge.text == ""{
                        let now = Date()
                        let calendar = Calendar.current
                        let dateFormate = DateFormatter()
                        dateFormate.dateFormat = "MM-dd-yyyy"
                        if let date = dateFormate.date(from: (data["user_dob"] as? String)!) {
                            let ageComponents = calendar.dateComponents([.year, .month, .day], from: date, to: now)
                            self.volAge.text = "\(ageComponents.year!) year \(ageComponents.month!) month \(ageComponents.day!) days"
                            self.volAge.isEnabled = false
                            self.volAge.alpha = 0.5
                        }
                        
                    }
                    self.volEducationCalendar.text = data["school_grade"] as? String
                    self.volParent.text = data["parent_guardian_name"] as? String
                    self.volAnotherNum.text = self.formattedNumber(number:(data["number_for_text"] as? String)!)
                    self.volAge.isEnabled = false
                    self.volAge.alpha = 0.5
                    
                    self.volPhoneNumber.text = self.formattedNumber(number:(data["user_phone"] as? String)!)
//                    self.volPhoneNumber.isEnabled = false
//
//                    self.volPhoneNumber.textColor = .darkGray
                    self.volCity.text = data["user_city"] as? String
                    
                    self.volZipCode.text = data["user_zipcode"] as? String
                    if self.volZipCode.text == ""{
                        self.menuButton.isHidden = true
                        self.notificationBtn.isHidden = true
                    }else{
                        self.menuButton.isHidden = false
                        self.notificationBtn.isHidden = false
                    }
                    self.volState.setTitle(data["user_state_name"] as? String, for: .normal)
                    
                    self.user_stateID = data["user_state"] as? String
                    self.volCountry.setTitle((data["user_country_name"] as! String), for: .normal)
                    self.user_countryID = data["user_country"] as? String
                    self.VolDOB.setTitle(data["user_dob"] as? String, for: .normal)
                    self.volDOB1 = data["user_dob"] as? String
                    let strusergen = data["user_gender"] as? String
                    if strusergen == "M"{
                        self.VolGender.setTitle("Male", for: .normal)
                        self.user_gender = "M"
                    }
                    else if strusergen == "F" {
                        self.VolGender.setTitle("Female", for: .normal)
                        self.user_gender = "F"
                    }
                    else if strusergen == "O"{
                        self.VolGender.setTitle("Others", for: .normal)
                        self.user_gender = "O"
                        
                    }
                    
                    self.mainStackView.removeArrangedSubview(self.passwordStackView)
                    self.mainStackView.removeArrangedSubview(self.confirmPasswordStackView)
                    self.passwordStackView.isHidden = true
                    self.confirmPasswordStackView.isHidden = true
                    self.view.layoutIfNeeded()
                    
                    
                    
                    
                    self.ProfileSet = data["vol_status"] as? String
                    print(self.ProfileSet)
                    
                    if (self.ProfileSet == "10"){
                        
                        self.btnPrivatePressed.setImage(UIImage(named: "checked_icon"), for: .normal)
                        self.btnPublicPressed.setImage(UIImage(named: "uncheked_icon"), for: .normal)
                        
                    }else if (self.ProfileSet == "20"){
                        
                        self.btnPrivatePressed.setImage(UIImage(named: "uncheked_icon"), for: .normal)
                        self.btnPublicPressed.setImage(UIImage(named: "checked_icon"), for: .normal)
                    }
                    
                }
            }
            
            
        }else{
            self.resetButton.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
            self.user_countryID = "1"
            
            self.back_button.isHidden = false
            if self.userTypeName == "EMP"{
                self.selectProfiletype.setTitle("Employee", for: .normal)
                self.referralCodeTF.isHidden = false
            }else{
                self.selectProfiletype.setTitle("Volunteer", for: .normal)
                self.referralCodeTF.isHidden = true
            }
        }
    }
    func refreshViewInViewDidAppear(){
        if(self.screen != "EDIT VIEW"){
            if self.userTypeName == "VOL" || self.userTypeName  == nil{
                self.referralCodeTF.isHidden = true
                self.cityStreetStackView.isHidden = true
                self.stateDropDown.isHidden = true
                self.countryDropDown.isHidden = true
                self.stateTF.isHidden = true
                self.countryTF.isHidden = true
                self.volCity.isHidden = true
                self.volParent.isHidden = true
                self.volAnotherNum.isHidden = true
                self.volState.isHidden = true
                self.volStreet.isHidden = true
                self.volZipCode.isHidden = true
                self.volCountry.isHidden = true
                self.volEducationCalendar.isHidden = true
                self.volGenderTF.isHidden = true
                self.VolGender.isHidden = true
                self.genderDropDown.isHidden = true
            }else{
                self.referralCodeTF.isHidden = false
                self.cityStreetStackView.isHidden = false
                self.stateDropDown.isHidden = false
                self.countryDropDown.isHidden = false
                self.stateTF.isHidden = false
                self.countryTF.isHidden = false
                self.volCity.isHidden = false
                self.volParent.isHidden = false
                self.volAnotherNum.isHidden = false
                self.volState.isHidden = false
                self.volStreet.isHidden = false
                self.volZipCode.isHidden = false
                self.volCountry.isHidden = false
                self.volEducationCalendar.isHidden = false
                self.volGenderTF.isHidden = false
                self.VolGender.isHidden = false
                self.genderDropDown.isHidden = false
            }
        
        }else{
            self.selectProfiletype.isHidden = true
            self.profileDropDown.isHidden = true
        }
        if self.userTypeName == "EMP"{
            self.referralCodeTF.isHidden = false
            self.volGenderTF.placeholder = "Gender*"
        }else{
            self.referralCodeTF.isHidden = true
            self.volGenderTF.placeholder = "Gender"
            
        }
    }
    func refreshViewAfterChangingUserType(type: String){
        self.userTypeName = type
        self.refreshViewInViewDidAppear()
    }
    func profile_pic()  {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profileImage.image = image
                self.profileImage.layer.borderWidth = 1
                self.profileImage.layer.masksToBounds = false
                self.profileImage.layer.borderColor = APP_BLACK_COLOR.cgColor
                self.profileImage.layer.cornerRadius = self.profileImage.frame.height/2
                self.profileImage.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.refreshViewInViewWillAppear()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.refreshViewInViewDidAppear()
    }
    @IBAction func onClickNotificationButton(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    func getCoverImageForRank(){
        var strImageNameCover = "cover_cloud.jpg"
        
        if let decoded  = UserDefaults.standard.object(forKey: "VolData") as? Data, let volData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? Dictionary<String, Any>, let userAvgRank = volData["user_avg_rank"] as? String {
            
            
            let floatUserAverageRank = Float(userAvgRank)!
            
            
            if ((floatUserAverageRank >= 0) && (floatUserAverageRank <= 20)){
                strImageNameCover = "cover_riseandshine.jpg"
            }else if ((floatUserAverageRank > 20) && (floatUserAverageRank <= 40)){
                strImageNameCover = "cover_cake.jpg"
            }else if ((floatUserAverageRank > 40) && (floatUserAverageRank <= 60)){
                strImageNameCover = "cover_cool.jpg"
            }else if ((floatUserAverageRank > 60) && (floatUserAverageRank <= 80)){
                strImageNameCover = "cover_truck.jpg"
            }else if (floatUserAverageRank > 80 ){
                strImageNameCover = "cover_cloud.jpg"
            }
            self.bannerimage.image = UIImage(named:strImageNameCover)
        }
    }
    func addUnderLineToField(color:UIColor)  {
        confirmEmailTF.setUnderLineOfColor(color: color)
        confirmPhoneNumberTF.setUnderLineOfColor(color: color)
        volEmail.setUnderLineOfColor(color: color)
        volFirstName.setUnderLineOfColor(color: color)
        volLastName.setUnderLineOfColor(color: color)
        volPhoneNumber.setUnderLineOfColor(color: color)
        volStreet.setUnderLineOfColor(color: color)
        volCity.setUnderLineOfColor(color: color)
        volZipCode.setUnderLineOfColor(color: color)
        volPassword.setUnderLineOfColor(color: color)
        volconfirmpassword.setUnderLineOfColor(color: color)
        referralCodeTF.setUnderLineOfColor(color: color)
        volAge.setUnderLineOfColor(color: color)
        volParent.setUnderLineOfColor(color: color)
        volEducationCalendar.setUnderLineOfColor(color: color)
        volAnotherNum.setUnderLineOfColor(color: color)
        
    }
    
    
    
    @IBAction func editPublic(_ sender: Any) {
        //  let defaults = UserDefaults.standard.string(forKey: APP_THEME)
        //if defaults == "Light Mode"{
        self.btnPrivatePressed.setImage(UIImage(named: "uncheked_icon"), for: .normal)
        self.btnPublicPressed.setImage(UIImage(named: "checked_icon"), for: .normal)
        
        //}else if defaults == "Dark Mode" {
        
        //  self.btnPrivatePressed.setImage(UIImage(named: "lightuncheked_icon"), for: .normal)
        // self.btnPublicPressed.setImage(UIImage(named: "lightchecked_icon"), for: .normal)
        //}
        
        self.ProfileSet = "20"
        print("Public clicked")
    }
    
    
    @IBAction func editPrivate(_ sender: Any) {
        
        self.btnPrivatePressed.setImage(UIImage(named: "checked_icon"), for: .normal)
        self.btnPublicPressed.setImage(UIImage(named: "uncheked_icon"), for: .normal)
        
        self.ProfileSet = "10"
        print("Private clicked")
        
    }
    
    @IBAction func editReset(_ sender: Any) {
        
        if(self.screen == "EDIT VIEW"){
            
        }else{
            self.volEmail.text = ""
            self.volFirstName.text = ""
            self.volLastName.text = ""
            self.volPhoneNumber.text = ""
            self.volStreet.text = ""
            self.volCity.text = ""
        
            self.volState.setTitle("Select Sate", for: .normal)
            self.user_stateID = ""
            self.volZipCode.text = ""
            self.volCountry.setTitle("Select Country", for: .normal)
            self.user_countryID = "1"
            self.VolDOB.setTitle("Select Date of Birth", for: .normal)
            self.volDOB1 = ""
            self.volPassword.text = ""
            self.volconfirmpassword.text = ""
            self.volPasswordEyeButton.setImage(UIImage(named: "eye_close_icon"), for: .normal)
            self.volconfirmPasswordEyeButton.setImage(UIImage(named: "eye_close_icon"), for: .normal)
        }
    }
    
    @IBAction func editUpdate(_ sender: Any) {
        
        if(self.screen == "EDIT VIEW"){
            if (validate2()){
                let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
                let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
                let user_id = userIDData["user_id"] as! String
                
                
                var params2 = [
                    "user_id":user_id,
                    "user_type":userTypeName == "EMP" ? "EMP" : "VOL",
                    "user_device":UIDevice.current.identifierForVendor!.uuidString,
                    "user_f_name":self.volFirstName.text as! String,
                    "user_l_name":self.volLastName.text as! String,
                    "user_country":self.user_countryID,
                    "user_state":self.user_stateID,
                    "user_city":self.volCity.text as! String,
                    "user_zipcode":self.volZipCode.text as! String,
                    "user_address": self.volStreet.text as! String,
                    "user_dob":self.volDOB1,
                    "user_gender":self.user_gender,
                    "phoneNumber":self.volPhoneNumber.text as! String,
                    "emailaddress":self.volEmail.text as! String,
                    "parent_guardian_name": self.volParent.text!,
                    "number_for_text": self.volAnotherNum.text!,
                    "school_grade": self.volEducationCalendar.text!,
                    "age": self.volAge.text!,
                    "vol_status":self.user_status
                ]
                
                let servicehandler = ServiceHandlers()
                servicehandler.csoeditProfileStep1(data: params2){(responce,isSuccess) in
                    if isSuccess{
                        // goto dashboard
                        self.performSegueToReturnBack()
                        //self.dismiss(animated: true, completion: nil)
                        
                    }else{
                        
                        let alert = UIAlertController(title: "Error Occured!", message: NSLocalizedString("Please try again!", comment: ""), preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true)
                    }
                }
                
            }
            
        }else{
            if (validate()){
                
                // //print(self.user_gender)
                let params = [
                    "user_type":userTypeName == "EMP" ? "EMP" : "VOL",
                    "user_device":UIDevice.current.identifierForVendor!.uuidString,
                    "school_id":"",
                    "user_f_name":self.volFirstName.text as! String,
                    "user_l_name":self.volLastName.text as! String,
                    "user_email":self.volEmail.text as! String,
                    "user_phone":self.volPhoneNumber.text as! String,
                    "user_country":self.user_countryID,
                    "user_state":self.user_stateID,
                    "user_city":self.volCity.text as! String,
                    "user_zipcode":self.volZipCode.text as! String,
                    "user_address":self.volStreet.text as! String,
                    "user_dob":self.volDOB1,
                    "user_gender":self.user_gender,
                    "parent_guardian_name": self.volParent.text!,
                    "number_for_text": self.volAnotherNum.text!,
                    "school_grade": self.volEducationCalendar.text!,
                    "age": self.volAge.text!,
                    "vol_status":self.user_status,
                    "user_pass":self.volPassword.text as! String] as [String : Any]
                let serivehandlers = ServiceHandlers()
                serivehandlers.csoRegistrationStage1(data: params){(responce,isSuccess) in
                    if isSuccess {
                        let sb = UIStoryboard(name: "Main", bundle: nil)
                        let phone_otp = sb.instantiateViewController(withIdentifier: "phoneotp") as! PhoneOtp
                        let data = responce as! Dictionary<String,Any>
                        phone_otp.phoneotp =  data["phone_otp"] as? String
                        phone_otp.user_id =  data["user_id"] as? String
                        phone_otp.user_type = self.userTypeName == "EMP" ? "EMP" : "VOL"
                        self.present(phone_otp, animated: true)
                        
                    }else{
                        let msg = responce as? String
                        let alert = UIAlertController(title: "Alert!", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true)
                    }
                    
                }
                
            }
        }
    }
    
    
    
    @IBAction func btnPrivate(_ sender: Any) {
        
        self.btnPrivatePressed.setImage(UIImage(named: "checked_icon"), for: .normal)
        self.btnPublicPressed.setImage(UIImage(named: "uncheked_icon"), for: .normal)
        
        self.ProfileSet = "10"
        print("Private clicked")
        
    }
    
    @IBAction func btnPublic(_ sender: Any) {
        
        self.btnPrivatePressed.setImage(UIImage(named: "uncheked_icon"), for: .normal)
        self.btnPublicPressed.setImage(UIImage(named: "checked_icon"), for: .normal)
        
        self.ProfileSet = "20"
        print("Public clicked")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.volPhoneNumber || textField == self.volAnotherNum || textField == self.confirmPhoneNumberTF{
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = newString.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne {
                formattedString.append("1 ")
                index += 1
            }
            if (length - index) > 3 {
                let areaCode = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("(%@)", areaCode)
                index += 3
            }
            if length - index > 3 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", prefix)
                index += 3
            }
            
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
            
        }
        if(textField == volZipCode){
            
            guard let textFieldText = textField.text,
                  let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 5
            
        }
        return true
    }
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XXX)XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0
            {
                self.volview.frame.origin.y -= 82.0
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    @IBAction func volPasswordEyeButtonFunction(_ sender: Any) {
        
        if(self.password_secure_eye == true){
            volPasswordEyeButton.setImage(UIImage(named: "eye_open_icon"), for: .normal)
            self.password_secure_eye = false
            volPassword.isSecureTextEntry = false
        }else{
            volPasswordEyeButton.setImage(UIImage(named: "eye_close_icon"), for: .normal)
            self.password_secure_eye = true
            volPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func volConfirmPasswordEyeButtonFunction(_ sender: Any) {
        if(self.password_secure_eye == true){
            volconfirmPasswordEyeButton.setImage(UIImage(named: "eye_open_icon"), for: .normal)
            self.password_secure_eye = false
            volconfirmpassword.isSecureTextEntry = false
        }else{
            volconfirmPasswordEyeButton.setImage(UIImage(named: "eye_close_icon"), for: .normal)
            self.password_secure_eye = true
            volconfirmpassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func volSelectGender(_ sender: Any) {
        let contents = ["Female","Male"]
        showPopoverForView(view: sender, contents: contents)
    }
    @IBAction func selectuserTyep(_ sender: Any) {
        let contents = ["Volunteer","Employee"]
        showPopoverForView(view: sender, contents: contents)
    }
    
    @IBAction func volSelectCountry(_ sender: Any) {
        let utility = Utility()
        utility.fetchCountryList{ (eventTypeList, isValueFetched) in
            if let list = eventTypeList {
                self.showPopoverForView(view: sender, contents: list)
            }
        }
    }
    
    
    @IBAction func volSelectDateOfBirth(_ sender: Any) {
        view.endEditing(true)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let date = Calendar.current.date(byAdding: .year, value: 1, to: Date())!
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let endDate = formatter.string(from: date)
        let edate = formatter.date(from: endDate)
        
        var dateComponent = DateComponents()
        let yearsToAdd = -200
        dateComponent.year = yearsToAdd
        let startDate = Calendar.current.date(byAdding: dateComponent, to: date)
        
        
        let now = Date()
        let calendar = Calendar.current
        let dateSelectionPicker = DateSelectionViewController(startDate: startDate, endDate:  edate)
        dateSelectionPicker.view.frame = self.view.frame
        dateSelectionPicker.view.layoutIfNeeded()
        dateSelectionPicker.captureSelectDateValue(sender, inMode: .date) { (selectedDate) in
            let formatter = DateFormatter()
            // formatter.dateFormat = "dd-MMM-yyyy"
            formatter.dateFormat = "MM-dd-yyyy"
            //08-22-2019
            let dateString = formatter.string(from: selectedDate)
            self.volDOB1 = dateString
            (sender as AnyObject).setTitle(dateString, for:.normal)
            (sender as AnyObject).setImage(nil, for: .normal)
            let ageComponents = calendar.dateComponents([.year, .month, .day], from: selectedDate, to: now)
            self.volAge.text = "\(ageComponents.year!) year \(ageComponents.month!) month \(ageComponents.day!) days"
            self.volAge.isEnabled = false
            self.volAge.alpha = 0.5
        }
        addViewController(viewController: dateSelectionPicker)
    }
    
    func addViewController(viewController:UIViewController)  {
        viewController.willMove(toParent: self)
        self.view.addSubview(viewController.view)
        self.addChild(viewController)
        viewController.didMove(toParent: self)
    }
    @IBAction func volSelectState(_ sender: Any) {
        let utility = Utility()
        utility.fetchStateList{ (eventTypeList, isValueFetched) in
            if let list = eventTypeList {
                self.showPopoverForView(view: sender, contents: list)
            }
        }
    }
    
    @IBAction func back_button_function(_ sender: Any) {
        
        if boolShowBackAlert{
            let alert = UIAlertController(title: NSLocalizedString("CONFIRM EXIT?", comment: ""), message: NSLocalizedString("Do you want to discard changes?", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel, handler: { _ in
                //Cancel Action
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("YES", comment: ""),
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            //Sign out action
                                            self.performSegueToReturnBack()
                                          }))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.performSegueToReturnBack()
        }
    }
    
    @IBAction func volReset(_ sender: Any) {
        if(self.screen == "EDIT VIEW"){
            ActivityLoaderView.startAnimating()
            let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
            let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
            let params = userIDData["user_id"] as! String
            let obj = ServiceHandlers()
            obj.editProfile(user_id: params){(responce,isSuccess) in
                if isSuccess{
                    let data = responce as! Dictionary<String,Any>
                    // //print(data)
                    self.user_status = (data["vol_status"] as? String)!
                    self.volEmail.text = data["user_email"] as? String
                    self.confirmEmailTF.text = data["user_email"] as? String
                    self.confirmPhoneNumberTF.text = self.formattedNumber(number:(data["user_phone"] as? String)!)
//                    self.volEmail.isEnabled = false
//                    self.volEmail.textColor = .darkGray
                    self.volFirstName.text = data["user_f_name"] as? String
                    self.volLastName.text = data["user_l_name"] as? String
                    self.volStreet.text = data["user_address"] as? String
                    
                    self.volAge.text = data["age_on"] as? String
                    if self.volAge.text == ""{
                        let now = Date()
                        let calendar = Calendar.current
                        let dateFormate = DateFormatter()
                        dateFormate.dateFormat = "MM-dd-yyyy"
                        if let date = dateFormate.date(from: (data["user_dob"] as? String)!) {
                            let ageComponents = calendar.dateComponents([.year, .month, .day], from: date, to: now)
                            self.volAge.text = "\(ageComponents.year!) year \(ageComponents.month!) month \(ageComponents.day!) days"
                            self.volAge.isEnabled = false
                            self.volAge.alpha = 0.5
                        }
                        
                    }
                    self.volEducationCalendar.text = data["school_grade"] as? String
                    self.volParent.text = data["parent_guardian_name"] as? String
                    self.volAnotherNum.text = self.formattedNumber(number:(data["number_for_text"] as? String)!)
                    self.volAge.isEnabled = false
                    self.volAge.alpha = 0.5
                    
                    self.volPhoneNumber.text = self.formattedNumber(number:(data["user_phone"] as? String)!)
//                    self.volPhoneNumber.isEnabled = false
//
//                    self.volPhoneNumber.textColor = .darkGray
                    self.volCity.text = data["user_city"] as? String
                    
                    self.volZipCode.text = data["user_zipcode"] as? String
                    
                    self.volState.setTitle(data["user_state_name"] as? String, for: .normal)
                    
                   
                    self.user_stateID = data["user_state"] as? String
                    self.volCountry.setTitle((data["user_country_name"] as! String), for: .normal)
                    self.user_countryID = data["user_country"] as? String
                    self.VolDOB.setTitle(data["user_dob"] as? String, for: .normal)
                    self.volDOB1 = data["user_dob"] as? String
                    let strusergen = data["user_gender"] as? String
                    if strusergen == "M"{
                        self.VolGender.setTitle("Male", for: .normal)
                        self.user_gender = "M"
                    }
                    else if strusergen == "F" {
                        self.VolGender.setTitle("Female", for: .normal)
                        self.user_gender = "F"
                    }
                    else if strusergen == "O"{
                        self.VolGender.setTitle("Others", for: .normal)
                        self.user_gender = "O"
                        
                    }
                    self.mainStackView.removeArrangedSubview(self.passwordStackView)
                    self.mainStackView.removeArrangedSubview(self.confirmPasswordStackView)
                    self.passwordStackView.isHidden = true
                    self.confirmPasswordStackView.isHidden = true
                    //                        self.passwordStackView.removeFromSuperview()
                    //                        self.confirmPasswordStackView.removeFromSuperview()
                    self.view.layoutIfNeeded()
                    self.ProfileSet = data["vol_status"] as? String
                    print(self.ProfileSet)
                    
                    if (self.ProfileSet == "10"){
                        
                        self.btnPrivatePressed.setImage(UIImage(named: "checked_icon"), for: .normal)
                        self.btnPublicPressed.setImage(UIImage(named: "uncheked_icon"), for: .normal)
                        
                    }else if (self.ProfileSet == "20"){
                        
                        self.btnPrivatePressed.setImage(UIImage(named: "uncheked_icon"), for: .normal)
                        self.btnPublicPressed.setImage(UIImage(named: "checked_icon"), for: .normal)
                    }
                    
                }
                ActivityLoaderView.stopAnimating()
            }
            
            
        }else{
            self.volEmail.text = ""
            self.volFirstName.text = ""
            self.volLastName.text = ""
            self.volPhoneNumber.text = ""
            self.volStreet.text = ""
            self.volCity.text = ""
            self.volState.setTitle("Select Sate", for: .normal)
            self.user_stateID = ""
            self.volZipCode.text = ""
            self.volCountry.setTitle("Select Country", for: .normal)
            self.user_countryID = "1"
            self.VolDOB.setTitle("Select Date of Birth", for: .normal)
            self.volDOB1 = ""
            self.volPassword.text = ""
            self.volconfirmpassword.text = ""
            self.volPasswordEyeButton.setImage(UIImage(named: "eye_close_icon"), for: .normal)
            self.volconfirmPasswordEyeButton.setImage(UIImage(named: "eye_close_icon"), for: .normal)
        }
        
    }
    
    @IBAction func volSubmit(_ sender: Any) {
        if(self.screen == "EDIT VIEW"){
            if (validate2()){
                let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
                let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
                let user_id = userIDData["user_id"] as! String
                
                
                var params2 = [String : Any]()
                if userTypeName == "VOL" || userTypeName == nil{
                    params2 =   [
                        "user_id":user_id,
                        "user_type":"VOL",
                        "user_device":UIDevice.current.identifierForVendor!.uuidString,
                        "user_f_name":self.volFirstName.text as! String,
                        "user_l_name":self.volLastName.text as! String,
                        "user_country":self.user_countryID,
                        "user_state":self.user_stateID,
                        "user_city":self.volCity.text as! String,
                        "user_zipcode":self.volZipCode.text as! String,
                        "user_address": self.volStreet.text as! String,
                        "user_dob":self.volDOB1,
                        "user_gender":self.user_gender,
                        "phoneNumber":self.volPhoneNumber.text as! String,
                        "emailaddress":self.volEmail.text as! String,
                        "parent_guardian_name": self.volParent.text!,
                        "number_for_text": self.volAnotherNum.text!,
                        "school_grade": self.volEducationCalendar.text!,
                        "age": self.volAge.text!,
                        "vol_status":self.ProfileSet,
                        "user_email":self.volEmail.text as! String,
                        "user_phone":self.volPhoneNumber.text as! String,
                    ]
                }else{
                    params2 =   [
                        "user_id":user_id,
                        "fpo_referral_code":referralCodeTF.text!,
                        "user_type":"EMP",
                        "user_device":UIDevice.current.identifierForVendor!.uuidString,
                        "user_f_name":self.volFirstName.text as! String,
                        "user_l_name":self.volLastName.text as! String,
                        "user_country":self.user_countryID,
                        "user_state":self.user_stateID,
                        "user_city":self.volCity.text as! String,
                        "user_zipcode":self.volZipCode.text as! String,
                        "user_address": self.volStreet.text as! String,
                        "user_dob":self.volDOB1,
                        "user_gender":self.user_gender,
                        "phoneNumber":self.volPhoneNumber.text as! String,
                        "emailaddress":self.volEmail.text as! String,
                        "parent_guardian_name": self.volParent.text!,
                        "number_for_text": self.volAnotherNum.text!,
                        "school_grade": self.volEducationCalendar.text!,
                        "age": self.volAge.text!,
                        "vol_status":self.ProfileSet,
                        "user_email":self.volEmail.text as! String,
                        "user_phone":self.volPhoneNumber.text as! String,
                    ]
                }
                
                let servicehandler = ServiceHandlers()
                servicehandler.csoeditProfileStep1(data: params2){(responce,isSuccess) in
                    if isSuccess{
                        // goto dashboard
                        if self.screen == "EDIT VIEW"{
                            let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Profile Updated Successfully!", comment: ""), preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) in
                                self.performSegueToReturnBack()
                            }))
                            self.present(alert, animated: true)
                        }else{
                            self.performSegueToReturnBack()
                        }
                        //                        self.dismiss(animated: true, completion: nil)
                        
                    }else{
                        if responce == nil{
                        let alert = UIAlertController(title: "Error Occured!", message: NSLocalizedString("Please try again!", comment: ""), preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true)
                        }else{
                            let data = responce as! Dictionary<String,Any>
                            if (((data["res_message"] as? String)?.contains("EMAILEXIST")) != nil){
                            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Email id already exits!", comment: ""), preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true)
                            }else if (((data["res_message"] as? String)?.contains("PHONEEXIST")) != nil){
                                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Phone is already exits!", comment: ""), preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                                self.present(alert, animated: true)
                                }
                            else{
                        //
                            }
                        }
                    }
                }
                
            }
            
        }else{
            if (validate()){
                
                // //print(self.user_gender)
                var params = [String : Any]()
                if userTypeName == "VOL" || userTypeName == nil{
                    params =   [
                        "user_type":userTypeName == "EMP" ? "EMP" : "VOL",
                        "user_device":UIDevice.current.identifierForVendor!.uuidString,
                        "school_id":"",
                        "user_f_name": self.volFirstName.text!,
                        "user_l_name": self.volLastName.text!,
                        "user_email": self.volEmail.text!,
                        "user_phone": self.volPhoneNumber.text!,
                        "user_country": self.user_countryID,
                        "user_state": self.user_stateID,
                        "user_city": self.volCity.text!,
                        "user_zipcode": self.volZipCode.text!,
                        "user_address": self.volStreet.text!,
                        "user_dob": self.volDOB1,
                        "user_gender": self.user_gender,
                        "parent_guardian_name": self.volParent.text!,
                        "number_for_text": self.volAnotherNum.text!,
                        "school_grade": self.volEducationCalendar.text!,
                        "age": self.volAge.text!,
                        "vol_status":self.ProfileSet,
                        "user_pass":  self.volPassword.text!]
                }else{
                    
                    params =   [
                        "fpo_referral_code":referralCodeTF.text!,
                        "user_type":userTypeName == "EMP" ? "EMP" : "VOL",
                        "user_device":UIDevice.current.identifierForVendor!.uuidString,
                        "school_id":"",
                        "user_f_name": self.volFirstName.text!,
                        "user_l_name": self.volLastName.text!,
                        "user_email": self.volEmail.text!,
                        "user_phone": self.volPhoneNumber.text!,
                        "user_country": self.user_countryID,
                        "user_state": self.user_stateID,
                        "user_city": self.volCity.text!,
                        "user_zipcode": self.volZipCode.text!,
                        "user_address": self.volStreet.text!,
                        "user_dob": self.volDOB1,
                        "user_gender": self.user_gender,
                        "parent_guardian_name": self.volParent.text!,
                        "number_for_text": self.volAnotherNum.text!,
                        "school_grade": self.volEducationCalendar.text!,
                        "age": self.volAge.text!,
                        "vol_status":self.ProfileSet,
                        "user_pass":  self.volPassword.text!]
                }
                let serivehandlers = ServiceHandlers()
                serivehandlers.csoRegistrationStage1(data: params){(responce,isSuccess) in
                    if isSuccess {
                        let sb = UIStoryboard(name: "Main", bundle: nil)
                        let phone_otp = sb.instantiateViewController(withIdentifier: "phoneotp") as! PhoneOtp
                        let data = responce as! Dictionary<String,Any>
                        phone_otp.phoneotp =  data["phone_otp"] as? String
                        phone_otp.user_id =  data["user_id"] as? String
                        phone_otp.user_type = self.userTypeName == "EMP" ? "EMP" : "VOL"
                        self.present(phone_otp, animated: true)
                        
                    }else{
                        let msg = responce as? String
                        let alert = UIAlertController(title: "Alert!", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true)
                    }
                    
                }
                
            }
        }
    }
    func isValidUserName(text:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        print(emailTest.evaluate(with: text))
        return emailTest.evaluate(with: text)
    }
    func isValidPhoneNumber(text: String)-> Bool{
        
        if text.count == 13
        {
            return true
            
        }else{
            return false
        }
        return true
    }
    
    func validate() -> Bool {
        if referralCodeTF.text == "" && self.userTypeName == "EMP"{
        let alert = UIAlertController(title: "Alert!", message: "Please enter FPO Referral Code!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
        self.present(alert, animated: true)
        return false
    }
       else if(self.volEmail.text == ""){
            
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Email is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        }else if !(self.isValidUserName(text: self.volEmail.text!) ){
            
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Email invalid!", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
            
        }else if !(self.volEmail.text == self.confirmEmailTF.text){
            
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please enter correct confirmation email.", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
            
        }else if(self.volFirstName.text == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("First name is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        }else if(self.volLastName.text == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Last name is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        }else if(self.volPhoneNumber.text == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Direct Dial is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        }else if !(self.isValidPhoneNumber(text:self.volPhoneNumber.text!)){
            
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Invalid Direct Dial", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
            
        }else if !(self.volPhoneNumber.text == self.confirmPhoneNumberTF.text){
            
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please enter correct confirmation Direct Dial.", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
            
        }
        else if(self.user_stateID == nil && userTypeName == "EMP"){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("State not selected", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else if(self.user_countryID == nil && userTypeName == "EMP"){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Country not selected", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else if((self.volDOB1 == "") || (self.volDOB1 == nil)){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Date of birth is empty.", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        }else if (datefromString(strDate: self.volDOB1!).timeIntervalSinceNow.sign == .plus) {
            // date is in future
            let alert = UIAlertController(title: "Alert!", message:"Date of birth is not valid", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        }
        else if (self.volAnotherNum.text != "" ){
            if !(Global.isValidPhoneNumber(text: self.volAnotherNum.text!)){
                let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Number we call/text invalid!", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true)
                return false
            }
            
            
            
        }
        
        else if(self.user_gender == "" || self.user_gender == nil) &&  (userTypeName == "EMP"){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Gender not selected", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if(self.volPassword.text == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Password is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        }else  if !(self.validatePassword(password: self.volPassword.text!) )
        {
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Password must be at least 8 characters 1 uppercase 1 lowercase and 1 number", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        else if(self.volconfirmpassword.text == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Confirm password is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        }else if !(self.volconfirmpassword.text == self.volPassword.text){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Confirm password and password not same", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
            
        }
        return true
    }
    func datefromString(strDate: String)->Date{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let dateToCheck = dateFormatter.date(from: strDate)!
        return dateToCheck
    }
    func validatePassword(password: String) -> Bool
    {
        let regularExpression = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}"
        
        let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        
        return passwordValidation.evaluate(with: password)
    }
    func validate2() -> Bool {
        if referralCodeTF.text == "" && self.userTypeName == "EMP"{
        let alert = UIAlertController(title: "Alert!", message: "Please enter FPO Referral Code!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
        self.present(alert, animated: true)
        return false
    }
       else if(self.volFirstName.text == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("First name is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        }else if(self.volLastName.text == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Last Name is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        }else if(self.volDOB1 == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Date of birth is empty.", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
            
        }
        else if(self.user_gender == "" || self.user_gender == nil) &&  (userTypeName == "EMP"){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Gender not selected", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if(self.user_countryID == nil && userTypeName == "EMP"){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Country not selected", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        return true
    }
    
    fileprivate func showPopoverForView(view:Any, contents:Any) {
        let controller = DropDownItemsTable(contents)
        let senderButton = view as! UIButton
        controller.showPopoverInDestinationVC(destination: self, sourceView: view as! UIView) { (selectedValue) in
            if let selectVal = selectedValue as? String {
                if selectVal == "Volunteer" ||  selectVal == "Employee"{
                    let type = selectVal == "Volunteer" ? "VOL" : "EMP"
                    self.refreshViewAfterChangingUserType(type: type)
                }else{
                self.user_gender = String(selectVal.prefix(1))
                }
                senderButton.setTitle(selectVal, for: .normal)
                senderButton.setImage(nil, for: .normal)
            } else if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetCountryServiceStrings.keyCountryName] as? String {
                self.user_countryID = selectVal[GetCountryServiceStrings.keyCountryId] as! String
                senderButton.setTitle(title, for: .normal)
                senderButton.setImage(nil, for: .normal)
            }  else if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetStateServiceStrings.keyStateName] as? String {
                self.user_stateID = selectVal[GetStateServiceStrings.keyStateId] as! String
                senderButton.setTitle(title, for: .normal)
                senderButton.setImage(nil, for: .normal)
            }
            
            
        }
    }
}
