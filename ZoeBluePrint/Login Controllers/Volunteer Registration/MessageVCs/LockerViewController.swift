//
//  LockerViewController.swift
//  ZoeBlue//print
//
//  Created by HashTag Labs on 11/09/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit
import Alamofire

class LockerViewController: UIViewController   {
    
    
    let data = ["https://images5.alphacoders.com/581/581655.jpg"]
    
    
    var upcomingEvents = [[String:Any]]()
    
    
    
    @IBOutlet weak var InternalContentTable: UITableView!
    var DocumentContents: Dictionary<String, Any>?
    
    let documentInteractionConroller = UIDocumentInteractionController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Global.setUpViewWithTheme(ViewController: self)
    }
    @IBAction func LocalGalleryButton(_ sender: Any) {
        
    }
}


extension LockerViewController {
    
    func share(url: URL) {
        documentInteractionConroller.url = url
        documentInteractionConroller.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionConroller.name = url.localizedName ?? url.lastPathComponent
        documentInteractionConroller.presentPreview(animated: true)
    }
    
    func storeAndShare(withURLString: String) {
        guard let url = URL(string: withURLString) else { return }
        /// START YOUR ACTIVITY INDICATOR HERE
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else { return }
            let tmpURL = FileManager.default.temporaryDirectory
                .appendingPathComponent(response?.suggestedFilename ?? "fileName.jpg")
            do {
                try data.write(to: tmpURL)
            } catch {
                //print(error)
            }
            DispatchQueue.main.async {
                /// STOP YOUR ACTIVITY INDICATOR HERE
                self.share(url: tmpURL)
            }
        }.resume()
    }
}


extension LockerViewController: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}
extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}

extension LockerViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LockerTableViewCell", for: indexPath) as! LockerTableViewCell
        let eventData = upcomingEvents[indexPath.row]
        // //print(eventData)
        cell.DocLabel.text = eventData["DocumentLabel"] as? String
        cell.DocImage.image = eventData["Image"] as? UIImage
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return data.count
        
        
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        if (editingStyle == UITableViewCell.EditingStyle.delete)
        {
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
    
    
    
}


