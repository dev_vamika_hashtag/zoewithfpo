//
//  EmailNotificationViewController.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 24/03/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit

class EmailNotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var emialTableView: UITableView!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var bannerImageView: UIImageView!
    @IBOutlet var NotificationBtn: UIButton!
    var notiData:Array<Any>?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emialTableView.register(UINib.init(nibName: "EmailNotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "EmailNotificationTableViewCell")
        self.emialTableView.delegate = self
        self.emialTableView.dataSource = self
        self.emialTableView.tableFooterView = UIView()
        self.getBannerImage()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getNotificationData()
        profile_pic()
    }
    func profile_pic()  {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profileImageView.image = image
                self.profileImageView.layer.borderWidth = 1
                self.profileImageView.layer.masksToBounds = false
                self.profileImageView.layer.borderColor = APP_BLACK_COLOR.cgColor
                self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
                self.profileImageView.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
        
    }
    func getNotificationData(){
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let org_id = userIDData["org_id"] as! String
        
        let serviceHandler = ServiceHandlers()
        serviceHandler.getEmailNotification(userData: org_id){(responce,isSucess) in
            if isSucess{
                self.notiData = (responce as! Array<Any>)
                self.emialTableView.reloadData()
            }else{
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    func getBannerImage(){
            self.bannerImageView.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
    }
    @IBAction func onClickNotificationButton(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notiData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:EmailNotificationTableViewCell = self.emialTableView.dequeueReusableCell(withIdentifier: "EmailNotificationTableViewCell") as! EmailNotificationTableViewCell
        let dic =  self.notiData![indexPath.row] as! NSDictionary
        cell.detailTextView.textContainer.lineFragmentPadding = 0
        cell.activeButton.tag = indexPath.row
        if dic["notification_status"] as? String == "10"{
            cell.activeButton.isSelected = true
        }else{
            cell.activeButton.isSelected = false
        }
        let font = "<style>" +
                        "html *" +
                        "{" +
                        "font-size: 11pt !important;" +
                        "color: #000000 !important;" +
                        "font-family: Roboto-Regular, Roboto !important;" +
                    "}</style>%@"
//        let font = "<font face='Roboto-Regular' font-size='14'>%@"
        cell.detailTextView.attributedText = (String(format: font, dic["email_template_primary"] as! String)).htmlToAttributedString
        cell.titleLbl.text = (dic["email_heading"] as! String)
        cell.activeButton.addTarget(self, action: #selector(activeButtonClicked(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dic =  self.notiData![indexPath.row] as! NSDictionary
        let textView = UITextView()
        let font = "<style>" +
                        "html *" +
                        "{" +
                        "font-size: 12pt !important;" +
                        "color: #000000 !important;" +
                        "font-family: Roboto-Regular, Roboto !important;" +
                    "}</style>%@"
        textView.attributedText = (String(format: font, dic["email_template_primary"] as! String)).htmlToAttributedString
        
        print(textView.contentSize.height)
        return 150.0 + textView.contentSize.height;
    }
    @objc func activeButtonClicked(sender:UIButton)
    {
        print(sender.tag)
        let dic =  self.notiData![sender.tag] as! NSDictionary
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let org_id = userIDData["org_id"] as! String
        var param = [String:Any]()
        var mesgStr = ""
        if dic["status"] as? String == "10"{
            //diactivate
            mesgStr = "Are you sure want to inactive notification?"
            param = ["org_id":org_id,
                         "notification_id":dic["id"] as? String,
                         "type":"inactive"]
        }else{
            //activate
            mesgStr = "Are you sure want to active notification?"
            param = ["org_id":org_id,
                         "notification_id":dic["id"] as? String,
                         "type":"active"]
        }
        //"Are you sure want to inactive notification?"
        let alert = UIAlertController(title: "Alert!", message: mesgStr, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("YES", comment: ""), style: UIAlertAction.Style.default, handler: { (action) in
            
            let serviceHandler = ServiceHandlers()
            serviceHandler.activateOrDiactivateEmailTemplate(params: param){(responce,isSucess) in
                if isSucess{
                    self.getNotificationData()
                }else{
                
                }
                
            }
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("NO", comment: ""), style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

