//
//  EmailNotificationTableViewCell.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 24/03/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit

class EmailNotificationTableViewCell: UITableViewCell {
    @IBOutlet var activeButton: UIButton!
    @IBOutlet var detailTextView: UITextView!
    @IBOutlet var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
