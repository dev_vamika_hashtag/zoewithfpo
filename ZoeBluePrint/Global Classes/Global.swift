//
//  Global.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 04/02/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit
import CoreLocation
let APP_GREEN_COLOR = UIColor.init(red: 27/255.0, green: 145/255.0, blue: 138/255.0, alpha: 1.0)
let APPLE_LANGUAGE_KEY = "AppleLanguages"
let CURRENT_LANG = "CURRENT_LANG"
let APP_THEME = "ChangeTheme"
let APP_WHITE_COLOR = UIColor.white//UIColor(named: "White_color")
let APP_BLACK_COLOR = UIColor.black//UIColor(named: "black_color")
let APP_BANNER_BLUE_COLOR = UIColor.init(red: 27/255.0, green: 31/255.0, blue: 64/255.0, alpha: 1.0)
class Global: NSObject {
    static func dottedBorderWith(view: UIView){
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = view.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: view.bounds).cgPath
        view.layer.addSublayer(yourViewBorder)
    }
    static func updatePlaceHolderName(textField: UITextField , nameString: String){
        textField.attributedPlaceholder = NSAttributedString(string: nameString,attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
    }
    static func localToUTC(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = Calendar.current
        dateFormatter.timeZone = TimeZone.current
        
        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatter.dateFormat = "hh:mm:ss"
            
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    static func utcToLocal(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "h:mm a"
            
            return dateFormatter.string(from: date)
        }
        return nil
    }
    static func utcToLocalDate(dateStr: String, timeZone: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        
        dateFormatter.timeZone = TimeZone(abbreviation: timeZone)
        
        if let date = dateFormatter.date(from: dateStr) {
            //            dateFormatter.timeZone = TimeZone.current
            dateFormatter.timeStyle = .none
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            return date
        }
        return nil
    }
    static func localToUTCDate(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        dateFormatter.calendar = Calendar.current
        dateFormatter.timeZone = TimeZone.current
        
        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            
            return dateFormatter.string(from: date)
        }
        return nil
    }
    static func compareBothDate(date1:Date, date2:Date) -> Bool {
        let order = NSCalendar.current.isDate(date1 as Date, equalTo: date2 as Date, toGranularity:.day)
        if order {
            return true
        }else{
            return false
        }
    }
    static func tabBarDarkAndLightMode(ViewController: UIViewController) {
        let theme = UserDefaults.standard.value(forKey: APP_THEME)
        if theme as! String == "Light Mode"{
            ViewController.tabBarController?.tabBar.barTintColor = .white
            UITabBar.appearance().barTintColor = .white
            UITabBar.appearance().tintColor = .black
            
        }else{
            
            ViewController.tabBarController?.tabBar.barTintColor = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
            UITabBar.appearance().barTintColor = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
            UITabBar.appearance().tintColor = .white
        }
        
    }
    static func setUpViewWithTheme(ViewController: UIViewController){
        self.tabBarDarkAndLightMode(ViewController: ViewController)
        self.ShowViewsLightAndDarkMode(ViewController: ViewController)
    }
    static func ShowViewsLightAndDarkMode(ViewController: UIViewController){
        let theme = UserDefaults.standard.value(forKey: APP_THEME)
        if theme as! String == "Light Mode"{
            if #available(iOS 13.0, *) {
                ViewController.overrideUserInterfaceStyle = .light
            } else {
                // Fallback on earlier versions
            }
        }else{
            if #available(iOS 13.0, *) {
                ViewController.overrideUserInterfaceStyle = .dark
            } else {
                // Fallback on earlier versions
            }
            
        }
        
    }
    static func getWeekDays(startDate: Date) -> Int {
        
        let calendar = NSCalendar.current
        let date1 = calendar.startOfDay(for:startDate)
        //        let date2 = calendar.startOfDay(for:endDate)
        return calendar.component(.weekday, from: date1)
    }
    static func getDatesBetweenDates(startDate: String, endDate: String) -> [Date]{
        var strUserTimezone = "EST"
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        if let timeZone = userIDData["user_timezone"] {
            strUserTimezone = timeZone as? String ?? "EST"
        }else{
            strUserTimezone = "EST"
        }
        
        let date1  = Global.utcToLocalDate(dateStr: startDate, timeZone: strUserTimezone)!
        let date2  = Global.utcToLocalDate(dateStr: endDate, timeZone: strUserTimezone)!
        
        if date1 > date2 {return [Date]() }
        var tempDate = date1
        var array = [tempDate]
        while tempDate < date2 {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        return array
    }
    static func fixFontWithInWidth(button : UIButton){
        button.titleLabel?.numberOfLines = 1
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.lineBreakMode = .byClipping
    }
    static func setTabBarFont(){
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 12)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 12)!], for: .selected)
    }
    
    static func isValidUserName(text:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        print(emailTest.evaluate(with: text))
        return emailTest.evaluate(with: text)
    }
    static func isValidPhoneNumber(text: String)-> Bool{
        if text.count == 13
        {
            return true
            
        }
        return false
    }
    static func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    static func convertTimeIn24HoursFormate(strTime:String)-> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let datestart = dateFormatter.date(from: strTime)
        if datestart == nil{
            return ""
        }
        dateFormatter.dateFormat = "HH:mm"
        let dateIn24 = dateFormatter.string(from: datestart!)
        return dateIn24
    }
    static func convertTimeIn12HoursFormate(strTime:String)-> String{
        let timeArr = strTime.components(separatedBy: " ")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let datestart = dateFormatter.date(from: timeArr[1])
        if datestart == nil{
            return ""
        }
        dateFormatter.dateFormat = "h:mm a"
        let dateIn24 = dateFormatter.string(from: datestart!)
        return dateIn24
    }
//    static func getTimeDifferenceBetweenTimes(sTime: String, eTime : String)-> String{
//           let formatter = DateFormatter()
//           formatter.dateFormat = "h:mma"
//           let date1 = formatter.date(from: sTime)!
//           let date2 = formatter.date(from: eTime)!
//           let elapsedTime = date2.timeIntervalSince(date1)
//           // convert from seconds to hours, rounding down to the nearest hour
//           let hours = floor(elapsedTime / 60 / 60)
////           let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)
////           print("\(Int(hours)) hr and \(Int(minutes)) min")
//        return "\(hours)"
//    }
    static func getTimeDifferenceBetweenTimes(sTime: String, eTime: String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "h:mma"

        guard let time1 = timeformatter.date(from: sTime),
            let time2 = timeformatter.date(from: eTime) else { return "" }

        //You can directly use from here if you have two dates

        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
//        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
//        let intervalInt = Int(interval)
        return "\(Float(hour))"//"\(intervalInt < 0 ? "-" : "+") \(Float(hour)) Hours \(Float(minute)) Minutes"
    }
}
