//
//  CSOTabViewController.swift
//  ZoeBluePrint
//
//  Created by Rishi Chaurasia on 20/10/20.
//  Copyright © 2020 Reetesh Bajpai. All rights reserved.
//

import UIKit

class CSOTabViewController: UITabBarController, UITabBarControllerDelegate {

    
    
    @IBOutlet var tabBarView: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        Global.setTabBarFont()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Global.setUpViewWithTheme(ViewController: self)
    }

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if let controllers =  self.viewControllers {
            if controllers.count > selectedIndex{
                if let navController = controllers[selectedIndex] as? UINavigationController {
                    navController.popToRootViewController(animated: false)
                }
            }
        }
        
    }


}
