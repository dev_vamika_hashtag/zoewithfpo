//
//  VolUpgradeProfileViewController.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 11/05/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit

class VolUpgradeProfileViewController: UIViewController {
    @IBOutlet weak var employeeBtn: UIButton!
    @IBOutlet weak var volunteerBtn: UIButton!
    @IBOutlet weak var chooseFpoBtn: UIButton!
    @IBOutlet weak var fpoReferralCodeTF: UITextField!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var profilePic: UIImageView!
    var fpoOptionData = NSMutableArray()
    var fpoListData = NSArray()
    var fpoId = String()
    var userId = String()
    var userType = String()
    var selectedIndex : Int?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        userType = userIDData["user_type"] as! String
        userId = userIDData["user_id"] as! String
        self.getEmployeeData()
        if userType == "VOL"{
            self.volunteerBtn.isSelected = true
        }else{
            self.fillFpoData()
            self.employeeBtn.isSelected = true
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getCoverImageForRank()
        self.profile_pic()
        
    }
    func getCoverImageForRank(){
        
        var strImageNameCover = "cover_cloud.jpg"
        
        if let decoded  = UserDefaults.standard.object(forKey: "VolData") as? Data, let volData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? Dictionary<String, Any>, let userAvgRank = volData["user_avg_rank"] as? String
        //print(volData)
        {
            
            let floatUserAverageRank = Float(userAvgRank)!
            if ((floatUserAverageRank >= 0) && (floatUserAverageRank <= 20)){
                strImageNameCover = "cover_riseandshine.jpg"
            }else if ((floatUserAverageRank > 20) && (floatUserAverageRank <= 40)){
                strImageNameCover = "cover_cake.jpg"
            }else if ((floatUserAverageRank > 40) && (floatUserAverageRank <= 60)){
                strImageNameCover = "cover_cool.jpg"
            }else if ((floatUserAverageRank > 60) && (floatUserAverageRank <= 80)){
                strImageNameCover = "cover_truck.jpg"
            }else if (floatUserAverageRank > 80 ){
                strImageNameCover = "cover_cloud.jpg"
            }
            
            
        }
        self.bannerImage.image = UIImage(named:strImageNameCover)
    }
    @IBAction func choosedFpoClicked(_ sender: Any) {
//        if userType == "EMP"{
//            selectedIndex = self.fpoOptionData.index(of: self.chooseFpoBtn.titleLabel?.text as Any)
//        }
        self.showPopoverForView(view: self.chooseFpoBtn!, contents: self.fpoOptionData)
    }
    fileprivate func showPopoverForView(view:UIButton, contents:Any) {
        let controller = DropDownItemsTable(contents)
        
        controller.showPopoverInDestinationVC(destination: self, sourceView: view as! UIView) { (selectedValue) in
            
            if let selectVal = selectedValue as? String {
                view.setTitle(selectVal, for: .normal)
                //fpoCode
                let resultPredicate = NSPredicate(format: "fpo_name contains[c] %@", selectVal)
                let data = self.fpoListData.filtered(using: resultPredicate) as NSArray
                let dic = data[0] as! NSDictionary
                var referralCode = String()
                if self.userType == "VOL"{
                    referralCode = dic["referral_code"] as? String ?? ""
                }else{
                    referralCode = dic["fpo_referral_code"] as? String ?? ""
                    if dic["referral_code"] != nil && referralCode == ""{
                        referralCode = dic["referral_code"] as? String ?? ""
                    }
                }
                print(data)
                self.fpoReferralCodeTF.text = referralCode
                self.fpoId = dic["id"] as? String ?? ""
            }
        }
        
    }
    
    func fillFpoData(){
        self.fpoListData = NSMutableArray()
        let param = ["user_id": self.userId]
        let serviceHandler = ServiceHandlers()
        serviceHandler.getEmployeeFPOData(params: param){(responce,isSuccess) in
            if isSuccess{
                let data = responce as! [[String:String]]
//                self.fpoListData = filteredDetails as NSArray
                for data in data{
                    let dic = data as? NSDictionary
//                    self.fpoOptionData.add(dic!["fpo_name"] ?? "")
                    self.chooseFpoBtn.setTitle(dic!["fpo_name"] as? String, for: .normal)
                    self.fpoReferralCodeTF.text = dic!["fpo_referral_code"] as? String
                    self.fpoId = dic!["id"] as? String ?? ""
                }
//                print(self.fpoOptionData)
            }else{
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    func getEmployeeData(){
        self.fpoListData = NSMutableArray()
        let serviceHandler = ServiceHandlers()
        serviceHandler.getAllFPOListData(){(responce,isSuccess) in
            if isSuccess{
                let data = responce as! [[String:String]]
                let filteredDetails = data.filter { !$0.values.contains("") }
                self.fpoListData = filteredDetails as NSArray
                for data in self.fpoListData{
                    let dic = data as? NSDictionary
                    self.fpoOptionData.add(dic!["fpo_name"] ?? "")
                }
                print(self.fpoOptionData)
            }else{
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    @IBAction func employeePressed(_ sender: Any) {
        self.employeeBtn.isSelected = true
        self.volunteerBtn.isSelected = false
    }
    @IBAction func volunteerPressed(_ sender: Any) {
        self.employeeBtn.isSelected = false
        self.volunteerBtn.isSelected = true
    }
    @IBAction func notificationPressed(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    @IBAction func savePressed(_ sender: Any) {
        if userType == "EMP" && !employeeBtn.isSelected{
            upgradeUserStatusData()
        }else{
        if !volunteerBtn.isSelected && !employeeBtn.isSelected{
            return
            
        }
        if fpoReferralCodeTF.text == ""{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Enter Referral code!", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        if chooseFpoBtn.titleLabel?.text == "Select FPO"{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Select FPO!", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        upgradeData()
        }
    }
    
    func upgradeUserStatusData(){
        
        let param = ["status":"30", "row_id":self.fpoId]
            let serviceHandler = ServiceHandlers()
            serviceHandler.changeEmployeeRequestStatus(params: param){(responce,isSuccess) in
                if isSuccess{
                    if let _  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as? Data {
                        UserDefaults.standard.removeObject(forKey: UserDefaultKeys.key_LoggedInUserData)
                    }
                    self.removeImage(itemName: "profilepic", fileExtension: "jpg")
                    UIApplication.shared.keyWindow?.rootViewController = self.storyboard!.instantiateViewController(withIdentifier: "login")
                }else{
                   
                }
            }
    }
    func removeImage(itemName:String, fileExtension: String) {
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        guard let dirPath = paths.first else {
            return
        }
        let filePath = "\(dirPath)/\(itemName).\(fileExtension)"
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch let error as NSError {
            print(error.debugDescription)
        }}
    func upgradeData(){
        let serviceHandler = ServiceHandlers()
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userId = userIDData["user_id"] as! String
        let userType = userIDData["user_type"] as! String
        let userName = String(format: "%@ %@", userIDData["user_f_name"] as! String, userIDData["user_l_name"] as! String)
        let param = ["user_id":userId,"user_type":userType,"user_name":userName,"profile_type":self.volunteerBtn.isSelected ? "VOL" : "EMP","fpo_code":self.fpoReferralCodeTF.text!,"referral_code":self.fpoReferralCodeTF.text!]
        serviceHandler.upgardeVolData(params:param){(responce,isSuccess) in
            if isSuccess{
                let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Request sent successfully!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                let alert = UIAlertController(title: "Alert!", message: responce as? String, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    func profile_pic()  {
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let params = userIDData["user_id"] as! String
        let serivehandler = ServiceHandlers()
        serivehandler.editProfile(user_id: params){(responce,isSuccess) in
            if isSuccess{
                let data = responce as! Dictionary<String,Any>
                let string_url = data["user_profile_pic"] as! String
                if let url = URL(string: string_url){
                    
                    do {
                        DispatchQueue.global().async {
                            let imageData = try? Data(contentsOf: url) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                            self.saveImageInDocsDir(dataImage: imageData!)
                            DispatchQueue.main.async {
                                self.profilePic.image = UIImage(data: imageData!)
                                self.profilePic.layer.borderWidth = 1
                                self.profilePic.layer.masksToBounds = false
                                self.profilePic.layer.borderColor = UIColor.black.cgColor
                                self.profilePic.layer.cornerRadius = self.profilePic.frame.height/2
                                self.profilePic.clipsToBounds = true
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func saveImageInDocsDir(dataImage: Data ) {
        
        //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        
        if (dataImage != nil) {
            // get the documents directory url
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            // choose a name for your image
            let fileName = "profilepic.jpg"
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            do {
                // writes the image data to disk
                try dataImage.write(to: fileURL, options: Data.WritingOptions.atomic)
                print("file saved")
                print(fileURL)
            } catch {
                print("error saving file:", error)
            }
            
        }
    }
}
