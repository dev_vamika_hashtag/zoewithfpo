//
//  NewVolunteerDashboard.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 29/06/20.
//  Copyright © 2020 Reetesh Bajpai. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import LanguageManager_iOS
class NewVolunteerDashboard: UIViewController,UITabBarDelegate,UITabBarControllerDelegate,CLLocationManagerDelegate{
    
    
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    
    @IBOutlet weak var btnEvents: UILabel!
    @IBOutlet weak var btnLocker: UILabel!
    @IBOutlet weak var btnTargets: UILabel!
    @IBOutlet weak var btnMessage: UILabel!
    @IBOutlet weak var btnCSOArea: UILabel!
    @IBOutlet weak var second_counter: UILabel!
    @IBOutlet weak var Minute_counter: UILabel!
    @IBOutlet weak var Hours_value: UILabel!
    @IBOutlet weak var Days_counter: UILabel!
    @IBOutlet weak var counterMessageLbl: UILabel!
    
    @IBOutlet weak var imageEvent: UIImageView!
    @IBOutlet weak var imageLocker: UIImageView!
    @IBOutlet weak var imageTarget: UIImageView!
    @IBOutlet weak var imageMessage: UIImageView!
    @IBOutlet weak var imageArea: UIImageView!
    @IBOutlet weak var Days_counter_value: UILabel!
    
    @IBOutlet weak var Hours_counter_value: UILabel!
    
    @IBOutlet weak var Minute_counter_value: UILabel!
    
    @IBOutlet weak var Seconds_counter_Value: UILabel!
    //    var isStartUpadtingLocation = false
    var releaseDate: NSDate?
    var countdownTimer = Timer()
    var locationTimer = Timer()
    var zipcode = ""
    var volunteerEvent : [[String:Any]]!
    var locationManager: CLLocationManager!
    var boolShoOrg = false
    var isDataLoading = false
    var objOrganization : OrganizationViewController!
    var volunteerEventlist : [[String:Any]]?
    static let geoCoder = CLGeocoder()
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.delegate = self
        // Do any additional setup after loading the view.
        
        objOrganization = self.storyboard!.instantiateViewController(withIdentifier: "organization") as? OrganizationViewController
        
        
        let tabbar = UITabBarController()
        tabbar.delegate = self
        
    }
    func callData()  {
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userID = userIDData["user_id"] as! String
        //print(userID)
        let servicehandler = ServiceHandlers()
        
        servicehandler.VolunteerEventList(userData: userID ) { (responce, isSuccess) in
            if isSuccess {
                let data = responce as! Dictionary<String,Any>
                self.volunteerEventlist = data["event_data"] as? [[String : Any]]
                //print(data)
                self.configureLocationTimer()
                self.configureCountDown()
                
                self.zipcode = (data["user_zipcode"] as? String)!
                //print(self.zipcode)
                self.getCoverImageForRank()
                
            }
        }
        
    }
    func updateViewsWithLanguage(){
        
    }
    @objc func StartupdateLocation() {
        var strUserTimezone = "EST"
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        if let timeZone = userIDData["user_timezone"] {
            strUserTimezone = timeZone as? String ?? "EST"
        }else{
            strUserTimezone = "EST"
        }
        let end_time = self.volunteerEventlist!.first!["shift_end_time"] as? String
        let currentTime = self.getCurrentShortTime(timeZone: strUserTimezone)
        let f = DateFormatter()
        f.dateFormat = "h:mma"
        f.timeZone = NSTimeZone(name: strUserTimezone) as TimeZone?
        let istime =  (f.date(from: currentTime)! >= f.date(from: end_time!)! )
        if istime{
            locationTimer.invalidate()
            return
        }
        //        isStartUpadtingLocation = true
//        if !self.isLocationAccessEnabled(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startUpdatingLocation()
//        }
    }
    func isLocationAccessEnabled()-> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("location No access")
                
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                print("location Access")
                return true
            }
        } else {
            print("Location services not enabled")
            return false
        }
    }
    func utcToLocal(dateStr: String) -> String? {
        
        return dateStr
    }
    fileprivate func configureLocationTimer() {
        if(self.volunteerEventlist != nil){
            var strUserTimezone = "EST"
            let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
            let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
            if let timeZone = userIDData["user_timezone"] {
                strUserTimezone = timeZone as? String ?? "EST"
            }else{
                strUserTimezone = "EST"
            }
            let shift_date = (self.volunteerEventlist!.first!["shift_date"] as? String)!
            let actual_date = Global.utcToLocalDate(dateStr: shift_date, timeZone: strUserTimezone)!
            let start_time = self.volunteerEventlist!.first!["shift_start_time"] as? String
            let end_time = self.volunteerEventlist!.first!["shift_end_time"] as? String
            let todayDate = self.getCurrentShortDate()
            
            let dateIsToday = Global.compareBothDate(date1: actual_date, date2: todayDate)
            if dateIsToday   {
                print("Both dates are same")
                let f = DateFormatter()
                f.dateFormat = "h:mma"
                //                f.dateStyle = .none
                f.timeZone = NSTimeZone(name: strUserTimezone) as TimeZone?
                //                f.date(from: start_time!)
                //                f.date(from: end_time!)
                let currentTime = self.getCurrentShortTime(timeZone: strUserTimezone)
                let istime = (f.date(from: currentTime)! >= f.date(from: start_time!)! ) &&  (f.date(from: currentTime)! <= f.date(from: end_time!)! )// true
                print("is time")
                print(istime)
                if istime{
                    self.alwaysAuthorization()
                    locationTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(StartupdateLocation), userInfo: nil, repeats: true)
                    
                    
                }
            }
        }
    }
    func findDateDiff(time1Str: String, time2Str: String) -> TimeInterval {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "h:mm a"
        guard let time1 = timeformatter.date(from: time1Str),
              let time2 = timeformatter.date(from: time2Str) else { return 0 }
        let interval = time2.timeIntervalSince(time1)
        return interval
    }
    func getCurrentShortDate() -> Date {
        let todaysDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //        let DateInFormat = dateFormatter.string(from: todaysDate as Date)
        return todaysDate
    }
    func getCurrentShortTime(timeZone : String) -> String {
        let currentTime = Date()
        let dateFormatter = DateFormatter()
        //        dateFormatter.dateStyle = .none
        dateFormatter.timeZone = TimeZone(abbreviation: timeZone)
        dateFormatter.dateFormat = "h:mma"
        //        let DateInFormat = dateFormatter.string(from: todaysDate as Date)
        return dateFormatter.string(from: currentTime)
    }
    // Creating Countdown Timer:
    fileprivate func configureCountDown() {
        if(self.volunteerEventlist != nil){
            var strUserTimezone = "EST"
//            let decoded  = UserDefaults.standasrd.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
//            let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
//            userIDData["user_timezone"]
            if let timeZone = self.volunteerEventlist!.first!["user_timezone_hours"]  {
                strUserTimezone = timeZone as? String ?? "EST"
            }else{
                strUserTimezone = "EST"
            }
            let shift_date = self.volunteerEventlist!.first!["shift_date"] as? String
            let shift_time = self.volunteerEventlist!.first!["shift_start_time_timer"] as? String
            
            let shift_date_time = self.utcToLocal(dateStr: shift_date! + " " + shift_time!)
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
            dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
            dateFormatter.timeZone = NSTimeZone(abbreviation: strUserTimezone) as TimeZone?
            
            
            guard let date = dateFormatter.date(from: shift_date_time!) else { return  }
            
            let releaseDateString =  dateFormatter.string(from: date)
            let releaseDateFormatter = DateFormatter()
            releaseDateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
            releaseDateFormatter.timeZone = NSTimeZone(abbreviation: strUserTimezone) as TimeZone?
            releaseDate = releaseDateFormatter.date(from: releaseDateString) as NSDate?
            countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        }else{
            self.volunteerEventlist = []
        }
    }
    
    @objc func updateTime() {
        
        let currentDate = Date()
        let calendar = Calendar.current
        ////print(releaseDate!)
        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: (releaseDate! as Date))
        let strday = "\(diffDateComponents.day ?? 00)"
        if  (strday.count<2){
            Days_counter_value.text = "0"+strday
        }else{
            Days_counter_value.text = strday
        }
        
        // For Hours:
        let strHours = "\(diffDateComponents.hour ?? 00)"
        if(strHours.count < 2){
            Hours_counter_value.text = "0" + strHours
        }else{
            Hours_counter_value.text = strHours
        }
        
        //For Minutes:
        let strMinutes = "\(diffDateComponents.minute ?? 00)"
        if(strMinutes.count < 2){
            Minute_counter_value.text = "0" + strMinutes
        }else{
            Minute_counter_value.text = strMinutes
        }
        
        // For Seconds:
        let strSeconds = "\(diffDateComponents.second ?? 00)"
        if(strSeconds.count < 2){
            Seconds_counter_Value.text = "0" + strSeconds
            
        }else{
            Seconds_counter_Value.text = strSeconds
        }
        
        if let val = Seconds_counter_Value.text {
            let secondvalue:Int = Int(val) ?? 0
            if secondvalue < 0 {
                Days_counter_value.text = "00"
                Hours_counter_value.text = "00"
                Minute_counter_value.text = "00"
                Seconds_counter_Value.text = "00"
                
            }
        }
        if releaseDate != nil {
            if let val = Seconds_counter_Value.text{
                if Int(val) == 0 && Int(Minute_counter_value.text ?? "") == 0{
                    if locationTimer.isValid || locationTimer == nil {self.configureLocationTimer()}
                }
            }
        }
        
        
    }
    func getCoverImageForRank(){
        
        var strImageNameCover = "cover_cloud.jpg"
        
        if let decoded  = UserDefaults.standard.object(forKey: "VolData") as? Data, let volData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? Dictionary<String, Any>, let userAvgRank = volData["user_avg_rank"] as? String
        //print(volData)
        {
            
            let floatUserAverageRank = Float(userAvgRank)!
            if ((floatUserAverageRank >= 0) && (floatUserAverageRank <= 20)){
                strImageNameCover = "cover_riseandshine.jpg"
            }else if ((floatUserAverageRank > 20) && (floatUserAverageRank <= 40)){
                strImageNameCover = "cover_cake.jpg"
            }else if ((floatUserAverageRank > 40) && (floatUserAverageRank <= 60)){
                strImageNameCover = "cover_cool.jpg"
            }else if ((floatUserAverageRank > 60) && (floatUserAverageRank <= 80)){
                strImageNameCover = "cover_truck.jpg"
            }else if (floatUserAverageRank > 80 ){
                strImageNameCover = "cover_cloud.jpg"
            }
            
            
        }
        self.image.image = UIImage(named:strImageNameCover)
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Global.setUpViewWithTheme(ViewController: self)
        self.callData()
        locationManager = CLLocationManager.init()
//            && CLLocationManager.authorizationStatus() != .authorizedAlways
        if !self.isLocationAccessEnabled()  {
            locationManager.requestAlwaysAuthorization()
        }
        
        self.profile_pic()
        
        if (!boolShoOrg){
            
            
            for vc in self.children {
                vc.willMove(toParent: nil)
                vc.view.removeFromSuperview()
                vc.removeFromParent()
            }
        }
        boolShoOrg = false
        updateViewsWithLanguage()
    }
    
    func profile_pic()  {
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let params = userIDData["user_id"] as! String
        let serivehandler = ServiceHandlers()
        serivehandler.editProfile(user_id: params){(responce,isSuccess) in
            if isSuccess{
                let data = responce as! Dictionary<String,Any>
                let string_url = data["user_profile_pic"] as! String
                if let url = URL(string: string_url){
                    
                    do {
                        DispatchQueue.global().async {
                            let imageData = try? Data(contentsOf: url) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                            if imageData != nil{
                            self.saveImageInDocsDir(dataImage: imageData!)
                            
                            DispatchQueue.main.async {
                                self.profilePic.image = UIImage(data: imageData!)
                                self.profilePic.layer.borderWidth = 1
                                self.profilePic.layer.masksToBounds = false
                                self.profilePic.layer.borderColor = UIColor.black.cgColor
                                self.profilePic.layer.cornerRadius = self.profilePic.frame.height/2
                                self.profilePic.clipsToBounds = true
                            }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func saveImageInDocsDir(dataImage: Data ) {
        
        //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        
        if (dataImage != nil) {
            // get the documents directory url
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            // choose a name for your image
            let fileName = "profilepic.jpg"
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            do {
                // writes the image data to disk
                try dataImage.write(to: fileURL, options: Data.WritingOptions.atomic)
                print("file saved")
                print(fileURL)
            } catch {
                print("error saving file:", error)
            }
            
        }
    }
    
    
    
    
    @IBAction func notificationBellTapped(_ sender: Any) {
        
        Utility.showNotificationScreen(navController: self.navigationController)
        
    }
    
    @IBAction func eventButton(_ sender: Any) {
        
        if let navController = self.tabBarController?.viewControllers?[1] as? UINavigationController, let firstVC = navController.viewControllers.first, let eventVC = firstVC as? VolunteerEventsViewController
        {
            eventVC.strFromScreen = "DASHBOARD"
            eventVC.strPostalCode = nil
            self.tabBarController?.selectedIndex = 1
        }
        
        
        
    }
    
    
    @IBAction func lockerButton(_ sender: Any) {
        self.tabBarController?.selectedIndex = 2
    }
    
    
    @IBAction func teargetButton(_ sender: Any) {
        self.tabBarController?.selectedIndex = 3
    }
    
    @IBAction func messageButton(_ sender: Any) {
        self.tabBarController?.selectedIndex = 4
    }
    
    
    @IBAction func csoAreaButton(_ sender: Any) {
        
        
        
        
        if let navController = self.tabBarController?.viewControllers?[1] as? UINavigationController, let firstVC = navController.viewControllers.first, let eventVC = firstVC as? VolunteerEventsViewController
        {
            eventVC.strFromScreen = "DASHBOARD"
            eventVC.strPostalCode = self.zipcode
            self.tabBarController?.selectedIndex = 1
        }
        
        
    }
    
    
    func getNearByeventData(){
        
        let vc = self.tabBarController?.viewControllers?[1] as! VolunteerEventsViewController
        vc.strFromScreen = "DASHBOARD"
        self.tabBarController?.selectedIndex = 1
        
    }
    func alwaysAuthorization(){
//        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .authorizedAlways {
//            locationManager.requestAlwaysAuthorization()
//        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if volunteerEventlist != nil && !isDataLoading{
            
            var strUserTimezone = "EST"
            let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
            let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
            if let timeZone = userIDData["user_timezone"] {
                strUserTimezone = timeZone as? String ?? "EST"
            }else{
                strUserTimezone = "EST"
            }
            let date = NSDate()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            //            let resultDate = dateFormatter.date(from: date)
            
            let releaseDateString =  dateFormatter.string(from: date as Date)
            let releaseDateFormatter = DateFormatter()
            releaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            releaseDateFormatter.timeZone = NSTimeZone(abbreviation: strUserTimezone) as TimeZone?
            
            let result = releaseDateFormatter.date(from: releaseDateString) as NSDate?
            let resultDate = releaseDateFormatter.string(from: result as! Date)
            let location = locations.last! as CLLocation
            print("======================LOCATION=======================")
            print(location.coordinate.latitude)
            print(location.coordinate.longitude)
            print(volunteerEventlist as Any)
            
            let dic = NSMutableDictionary.init()
            dic["user_id"] = userIDData["user_id"] as! String
            dic["event_id"] = volunteerEventlist?.first!["event_id"]
            dic["shift_id"] = volunteerEventlist?.first!["shift_id"]
            dic["map_id"] = volunteerEventlist?.first!["map_id"]
            dic["log_track_time"] = "\(resultDate)"
            dic["log_latitude"] = location.coordinate.latitude
            dic["log_longitude"] = location.coordinate.longitude
            let currentLocation = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let eventLocation = CLLocation(latitude: location.coordinate.latitude + 20, longitude: location.coordinate.longitude + 20)
            //CLLocation(latitude: CLLocationDegrees(Double(volunteerEventlist?.first!["event_latitude"] as! String)!), longitude: CLLocationDegrees(Double(volunteerEventlist?.first!["event_longitude"] as! String)!))
            let distance = currentLocation.distance(from: eventLocation) / 1000
            //in km
            print(String(format: "The distance to my buddy is %.01fkm", distance))
            if distance > 2 {
                print("YOU ARE OUT")
                dic["log_inout"] = "O"
            }
            else{
                print("YOU ARE IN")
                dic["log_inout"] = "I"
            }
            print("==============tracking======data==================")
            print(dic)
            
            
            let servicehandler = ServiceHandlers()
            isDataLoading = true
            servicehandler.sendTrackLocationToServer(trackData: dic as! Dictionary<String, Any> ) { (response, isSuccess) in
                if isSuccess {
                    self.isDataLoading = false
                    print(response as Any)
                }
            }
            // let cityCoords = CLLocation(latitude: newLat, longitude: newLon)
            //self.getAdressName(coords: location)
        }
    }
    
    func getAdressName(coords: CLLocation) {
        
        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
            if error != nil {
                print("Hay un error")
                let alertController = UIAlertController(title: "Error Occured!", message: "Postal Code not found.", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                }
                // Add the actions
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                self.locationManager.stopUpdatingLocation()
            } else {
                
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    var adressString : String = ""
                    if place.postalCode != nil {
                        adressString = adressString + place.postalCode! //+ "\n"
                        self.zipcode = adressString
                        self.getNearByeventData()
                        self.locationManager.stopUpdatingLocation()
                    }else{
                        
                        let alertController = UIAlertController(title: "Error Occured!", message: "Postal Code not found.", preferredStyle: .alert)
                        // Create the actions
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            // NSLog("OK Pressed")
                        }
                        // Add the actions
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                        self.locationManager.stopUpdatingLocation()
                    }
                    self.locationManager.stopUpdatingLocation()
                    
                }
            }
        }
    }
    
}





extension String {
    func convertToTimeInterval() -> TimeInterval {
        guard self != "" else {
            return 0
        }
        
        var interval:Double = 0
        
        let parts = self.components(separatedBy: ":")
        for (index, part) in parts.reversed().enumerated() {
            interval += (Double(part) ?? 0) * pow(Double(60), Double(index))
        }
        
        return interval
    }
}
public extension String {
    
    func localiz(comment: String = "") -> String {
        guard let bundle = Bundle.main.path(forResource: LanguageManager.shared.currentLanguage.rawValue, ofType: "lproj") else {
            return NSLocalizedString(self, comment: comment)
        }
        
        let langBundle = Bundle(path: bundle)
        return NSLocalizedString(self, tableName: nil, bundle: langBundle!, comment: comment)
    }
    
}
