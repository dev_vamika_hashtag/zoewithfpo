//
//  VolunteerShifts.swift
//  ZoeBlue//print
//
//  Created by HashTag Labs on 12/11/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit

class VolunteerShifts: UIViewController,UITableViewDataSource,UITableViewDelegate,WaiverAgreementViewControllerDelegate,ChooseProfileTypeViewControllerDelegate {
    
    
    
    
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var buttonView: UIButton!
    
    //    @IBOutlet weak var lblHeadingName: UILabel!
    @IBOutlet weak var profilepic: UIImageView!
    var data:Array<Any>?
    var eventID:String?
    var notiMapID:String?
    var isFromType:String?
    var dataDetails:Dictionary<String,Any>?
    
    var volunteerShiftsList : [[String:Any]]!
    var applyShiftUserType:String?
    
    @IBOutlet weak var sideMenuTapped: UIButton!
    
    @IBOutlet weak var Table1: UITableView!
    
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var lblShift: UILabel!
    
    @IBOutlet weak var StackView: UIView!
    @IBOutlet weak var ShiftLabel: UILabel!
    @IBOutlet var OuterView: UIView!
    
    @IBOutlet weak var imgViewCoverPic: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.Table1.tableFooterView = UIView(frame: .zero)
        let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        self.OuterView.addGestureRecognizer(mytapGestureRecognizer)
        self.StackView.isUserInteractionEnabled = true
        OuterView.isHidden = true               // Change Status
        StackView.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        
        OuterView.isHidden = true
        StackView.isHidden = true
        
        self.call_for_Table_data()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Global.setUpViewWithTheme(ViewController: self)
        self.getCoverImageForRank()
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profilepic.image = image
                self.profilepic.layer.borderWidth = 1
                self.profilepic.layer.masksToBounds = false
                self.profilepic.layer.borderColor = UIColor.black.cgColor
                self.profilepic.layer.cornerRadius = self.profilepic.frame.height/2
                self.profilepic.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
        
    }
    
    func getCoverImageForRank(){
        
        var strImageNameCover = "cover_cloud.jpg"
        
        if let decoded  = UserDefaults.standard.object(forKey: "VolData") as? Data, let volData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as?  Dictionary<String, Any>, let userAvgRank = volData["user_avg_rank"] as? String {
            
            
            let floatUserAverageRank = Float(userAvgRank)!
            
            
            if ((floatUserAverageRank >= 0) && (floatUserAverageRank <= 20)){
                strImageNameCover = "cover_riseandshine.jpg"
            }else if ((floatUserAverageRank > 20) && (floatUserAverageRank <= 40)){
                strImageNameCover = "cover_cake.jpg"
            }else if ((floatUserAverageRank > 40) && (floatUserAverageRank <= 60)){
                strImageNameCover = "cover_cool.jpg"
            }else if ((floatUserAverageRank > 60) && (floatUserAverageRank <= 80)){
                strImageNameCover = "cover_truck.jpg"
            }else if (floatUserAverageRank > 80 ){
                strImageNameCover = "cover_cloud.jpg"
            }
            
            
        }
        self.imgViewCoverPic.image = UIImage(named:strImageNameCover)
        
        
    }
    
    
    @IBAction func notificationBellTapped(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
        //          let sb = UIStoryboard(name: "Main", bundle: nil)
        //           let obj = sb.instantiateViewController(withIdentifier: "noti") as! ProjectNotificationViewController
        //             present(obj,animated: true)
    }
    @objc func handleTap(_ sender:UITapGestureRecognizer){
        
        self.OuterView.isHidden = true
        self.StackView.isHidden = true
        
        
    }
    func call_for_Table_data()  {
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userID = userIDData["user_id"] as! String
        ////print(userID)
        
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.VolunteerShiftsDetails(user_id: userID , event_id:self.eventID!) { (responce,isSuccess) in
            if isSuccess {
                let resData = responce as! [[String: Any]]
                let dataDic = NSMutableArray()
                
                if self.isFromType == "Notification"{
                    if resData.count > 1{
                        for i in 0..<resData.count{
                            let mapData = resData[i] as NSDictionary
                            if mapData.count != 0{
                                if mapData["map_id"] as? String == self.notiMapID{
                                    dataDic.add(mapData)
                                }
                            }
                        }
                        self.data = dataDic as? Array<Any>
                    }
                }else{
                    self.data  = resData
                }
                print(self.data)
                
                
                self.Table1.reloadData()
                
            }else{
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Data not found", comment: ""), preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { _ in
                    //Cancel Actionse
                    self.performSegueToReturnBack()
                    // self.dismiss(animated: true, completion: nil)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (data != nil)
        {
            return data!.count
        }
        //                }else{
        return 0
        //                }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Table1.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VolunteerShiftTableViewCell
        
        let b = data![indexPath.row] as! Dictionary<String,Any>
        //var a = b[indexPath.row]
        
        cell.selectionStyle = .none
        
        cell.TitleLabel.text = b["shift_task_name"] as? String
        
        if b["shift_all_day"] as? String == "0"{
            let start_time = b["shift_start_time"] as! String
            let end_time = b["shift_end_time"] as! String
            let Time = "\(start_time) - \(end_time)"
            //.////print(Time)
            cell.TimeLabel.text = Time
        }else{
            cell.TimeLabel.text = "All Day"
        }
        
        
        let dateString = b["shift_date"] as! String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        let dateObj = dateFormatter.date(from: dateString)    //date is changing into string
        
        dateFormatter.dateFormat = "dd"
        ////print("Dateobj: \(dateFormatter.string(from: dateObj!))")  // the date data is coming now to again change from from string to date
        let dated = dateFormatter.string(from: dateObj!)
        ////print(dated)
        cell.DateLabel.text = dated 
        
        dateFormatter.dateFormat = "MM"
        ////print("Monthobj: \(dateFormatter.string(from: dateObj!))")
        let month = dateFormatter.string(from: dateObj!)
        let mon = Int(month)
        let mont = dateFormatter.monthSymbols[mon! - 1]
        let Month = String(mont.prefix(3))
        ////print(Month)
        cell.MonthLabel.text = Month
        
        dateFormatter.dateFormat = "EEE"
        ////print("Week: \(dateFormatter.string(from: dateObj!))")
        let weekday = Calendar.current.component(.weekday, from: dateObj!)
        let week:String = dateFormatter.weekdaySymbols![weekday - 1]
        let Week = String(week.prefix(3))
        ////print(Week)
        cell.WeekLabel.text = Week
        
        
        cell.StatusNameTapped.tag = indexPath.row
        //  //print(b["volunteer_apply"] as! String)
        
        
        ////print(b)
        
        let accepted = b["volunteer_req_accepted"] as! String
        ////print(accepted)
        
        let vol_request = b["shift_vol_req"] as! String
        ////print(vol_request)
        
        let changeRate = b["volunteer_apply"] as! String
        ////print(changeRate)
        let available = b["available"] as! Int
        ////print(available)
        if changeRate != "0"{
            
            cell.StatusNameTapped.setImage(UIImage(named: "complete-verified.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
            cell.StatusName.text = "Applied"
            cell.StatusName.textColor = UIColor(red: 39/255.0, green: 174/255.0, blue: 96/255.0, alpha: 1.0)
        }
        else
        if changeRate == "0" {
            
            if available == 0 {
                
                cell.StatusNameTapped.setImage(UIImage(named: "not-available.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
                cell.StatusName.text = "Not Available"
                cell.StatusName.textColor = UIColor.gray
                
            }else if available == 1 {
                
                cell.StatusNameTapped.setImage(UIImage(named: "csoavailable.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
                cell.StatusName.text = "Available"
                cell.StatusName.textColor =  UIColor(red: 39/255.0, green: 174/255.0, blue: 96/255.0, alpha: 1.0)
            }else{
                cell.StatusNameTapped.setImage(UIImage(named: "not-available.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
                cell.StatusName.text = "Not Available"
                cell.StatusName.textColor = UIColor.gray
            }
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        OuterView.isHidden = false
        StackView.isHidden = false
        
        let b = data![indexPath.row] as! Dictionary<String,Any>
        ////print(b)
        self.dataDetails = b
        
    }
    
    
    func applyForShiftWithAgreement(shiftData: Dictionary<String,Any>,index : Int){
        self.applyForShift(b : shiftData, indexSelected : index,isWithAgreement :true)
    }
    func showChooseProfilrTypeView(indexNum : Int){
        var chooseProfileType = ChooseProfileTypeViewController()
        chooseProfileType = ChooseProfileTypeViewController.init(nibName: "ChooseProfileTypeViewController", bundle: nil)
        chooseProfileType.delegate = self
        chooseProfileType.indexNum = indexNum
        chooseProfileType.view.frame = self.view.frame
        self.view.addSubview(chooseProfileType.view)
        self.addChild(chooseProfileType)
    }
    
    func chooseProfileWith(type: String, index: Int){
        self.applyShiftUserType = type
        let b = data![index] as! Dictionary<String,Any>
        
        if  b["event_waiver_req"] as! String == "1" && b["read_waiver_doc"] as! String == "0"{
        var attendaceView = WaiverAgreementViewController()
        attendaceView = WaiverAgreementViewController.init(nibName: "WaiverAgreementViewController", bundle: nil)
            attendaceView.delegate = self
        attendaceView.view.frame = self.view.frame
        attendaceView.shiftData = b
        attendaceView.shiftSelectedIndex = index
        attendaceView.eventID = self.eventID
        self.view.addSubview(attendaceView.view)
        self.addChild(attendaceView)
            return
        }
       
        self.applyForShift(b : b, indexSelected : index, isWithAgreement : false)
    }
    @objc func methodOfReceivedNotification(notification: Notification){
        let indexSelected = notification.userInfo!["selectedIndex"] as! Int
        let b = data![indexSelected] as! Dictionary<String,Any>
        
        let shiftAvailable = b["available"] as! Int
        ////print(changeRate)
        if shiftAvailable == 0{
        return
        }
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userType = userIDData["user_type"] as! String
        if userType == "EMP" {
            showChooseProfilrTypeView(indexNum : notification.userInfo!["selectedIndex"] as! Int)
        }else{
       
        
        if  b["event_waiver_req"] as! String == "1" && b["read_waiver_doc"] as! String == "0"{
        var attendaceView = WaiverAgreementViewController()
        attendaceView = WaiverAgreementViewController.init(nibName: "WaiverAgreementViewController", bundle: nil)
            attendaceView.delegate = self
        attendaceView.view.frame = self.view.frame
        attendaceView.shiftData = b
        attendaceView.shiftSelectedIndex = indexSelected
        attendaceView.eventID = self.eventID
        self.view.addSubview(attendaceView.view)
        self.addChild(attendaceView)
            return
        }
       
        self.applyForShift(b : b, indexSelected : indexSelected, isWithAgreement : false)
        }
//        let Rank = b["shift_rank"] as! String
//        ////print(Rank)
//
//
//        _ = b["volunteer_apply"] as! String
//        // //print(Apply)
//
//
//        let returnValue = (UserDefaults.standard.object(forKey: "user_avg_rate") as! String)
//        ////print(returnValue)
//
//        let changeRate = b["volunteer_apply"] as! String
//        ////print(changeRate)
//        if changeRate == "0"{
//
//            if Rank <= returnValue {
//
//                let cso_id = b["csoa_id"] as! String
//                ////print(cso_id)
//
//                let shift_id = b["shift_id"] as! String
//
//
//                let serviceHanlder = ServiceHandlers()
//                serviceHanlder.EventSendRequest(cso_id: cso_id, shift_id: shift_id, event_id: self.eventID!) { (responce,isSuccess) in
//                    if isSuccess {
//                        let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Volunteer Request Sent successfully", comment: ""), preferredStyle: UIAlertController.Style.alert)
//                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
//                        self.call_for_Table_data()
//                    }else{
//                        let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Error Occured!", comment: ""), preferredStyle: UIAlertController.Style.alert)
//                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
//                    }
//                }
//            }else{
//                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("You are not eligible for this shift", comment: ""), preferredStyle: UIAlertController.Style.alert)
//
//                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//
//            }
//        }else{
//            //            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("You are not eligible for this shift", comment: ""), preferredStyle: UIAlertController.Style.alert)
//            //            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
//            //            self.present(alert, animated: true, completion: nil)
//            OuterView.isHidden = false
//            StackView.isHidden = false
//
//            let b = data![indexSelected] as! Dictionary<String,Any>
//            ////print(b)
//            self.dataDetails = b
//        }
    }
    
    func applyForShift(b : Dictionary<String,Any>, indexSelected :Int, isWithAgreement :Bool){
        let Rank = b["shift_rank"] as! String
        ////print(Rank)
        
        
        _ = b["volunteer_apply"] as! String
        // //print(Apply)
        
        
        let returnValue = (UserDefaults.standard.object(forKey: "user_avg_rate") as! String)
        ////print(returnValue)
        
        let changeRate = b["volunteer_apply"] as! String
        let shiftAvailable = b["available"] as! Int
        ////print(changeRate)
        if changeRate == "0" && shiftAvailable == 1{
            
            if Rank <= returnValue {
                
                let cso_id = b["csoa_id"] as! String
                ////print(cso_id)
                
                let shift_id = b["shift_id"] as! String
                
                
                let serviceHanlder = ServiceHandlers()
                serviceHanlder.EventSendRequest(cso_id: cso_id, shift_id: shift_id, event_id: self.eventID!, waiver_doc: isWithAgreement ? "1" : "" ,userTypeString:self.applyShiftUserType ?? "VOL") { (responce,isSuccess) in
                    if isSuccess {
                        let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Volunteer request sent successfully", comment: ""), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.call_for_Table_data()
                    }else{
                        let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Error Occured!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }else{
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("You are not eligible for this shift", comment: ""), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
        }else{
            OuterView.isHidden = false
            StackView.isHidden = false
            
            let b = data![indexSelected] as! Dictionary<String,Any>
            ////print(b)
            self.dataDetails = b
        }
    }
    @IBAction func backbutton(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
        
    }
    @IBAction func btnSideMenu(_ sender: Any) {
    }
    
    @IBAction func ViewButtonTapped(_ sender: Any) {
        
        OuterView.isHidden = true
        StackView.isHidden = true
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        if let selectedEventVC =  mainSB.instantiateViewController(withIdentifier: "Cell") as? ShiftRank{
            selectedEventVC.data1 = self.dataDetails
            selectedEventVC.eventID = self.eventID
            self.navigationController?.pushViewController(selectedEventVC, animated: true)
        }
        
        
        
    }
    
    
    @IBAction func backButtonFunction(_ sender: Any) {
        //        self.navigationController?.popViewController(animated: true)
        performSegueToReturnBack()
    }
    
    
}
