//
//  ShiftRank.swift
//  ZoeBlue//print
//
//  Created by HashTag Labs on 13/11/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit

class ShiftRank: UIViewController,WaiverAgreementViewControllerDelegate,ChooseProfileTypeViewControllerDelegate {
    
    var data1: Dictionary<String,Any>!
    var eventID:String?
    
    
    
    @IBOutlet weak var whiteStar: FloatRatingView!
    
    @IBOutlet weak var applyShiftBtn: UIButton!
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var btnbackTap: UIButton!
    @IBOutlet weak var profilepic: UIImageView!
    @IBOutlet weak var StatusName: UILabel!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var VolunteerRequestedLabel: UILabel!
    @IBOutlet weak var ShiftDateLabel: UILabel!
    @IBOutlet weak var ShiftTimeLabel: UILabel!
    
    @IBOutlet weak var RatingRank: FloatRatingView!
    @IBOutlet weak var shiftRank: UILabel!
    @IBOutlet weak var ImageView: UIImageView!
    var applyShiftUserType:String?
    
    @IBOutlet weak var coverpic: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setValues()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Global.setUpViewWithTheme(ViewController: self)
        self.getCoverImageForRank()
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profilepic.image = image
                self.profilepic.layer.borderWidth = 1
                self.profilepic.layer.masksToBounds = false
                self.profilepic.layer.borderColor = UIColor.black.cgColor
                self.profilepic.layer.cornerRadius = self.profilepic.frame.height/2
                self.profilepic.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
        
    }
    
    
    
    
    @IBAction func ListButtonTapped(_ sender: Any) {
        
        
    }
    func applyForShiftWithAgreement(shiftData: Dictionary<String,Any>,index : Int){
        self.applyForShift(data1 : shiftData)
    }
    @IBAction func onApplyForShift(_ sender: Any){
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userType = userIDData["user_type"] as! String
        if userType == "EMP" {
            showChooseProfileTypeView()
        }else{
        if  data1["event_waiver_req"] as! String == "1" && data1["read_waiver_doc"] as! String == "0"{
        var attendaceView = WaiverAgreementViewController()
        attendaceView = WaiverAgreementViewController.init(nibName: "WaiverAgreementViewController", bundle: nil)
            attendaceView.delegate = self
        attendaceView.view.frame = self.view.frame
        attendaceView.shiftData = data1
            attendaceView.eventID = self.eventID
        self.view.addSubview(attendaceView.view)
        self.addChild(attendaceView)
            return
        }
        self.applyForShift(data1 : self.data1)
        }
    }
    func showChooseProfileTypeView(){
        var chooseProfileType = ChooseProfileTypeViewController()
        chooseProfileType = ChooseProfileTypeViewController.init(nibName: "ChooseProfileTypeViewController", bundle: nil)
        chooseProfileType.delegate = self
        chooseProfileType.view.frame = self.view.frame
        self.view.addSubview(chooseProfileType.view)
        self.addChild(chooseProfileType)
    }
    func chooseProfileWith(type: String, index: Int){
        self.applyShiftUserType = type
        
        if  data1["event_waiver_req"] as! String == "1" && data1["read_waiver_doc"] as! String == "0"{
        var attendaceView = WaiverAgreementViewController()
        attendaceView = WaiverAgreementViewController.init(nibName: "WaiverAgreementViewController", bundle: nil)
            attendaceView.delegate = self
        attendaceView.view.frame = self.view.frame
        attendaceView.shiftData = data1
            attendaceView.eventID = self.eventID
        self.view.addSubview(attendaceView.view)
        self.addChild(attendaceView)
            return
        }
        self.applyForShift(data1 : self.data1)
    }
    func applyForShift(data1 : Dictionary<String,Any>){
        
        let cso_id = data1["csoa_id"] as! String
        ////print(cso_id)
        let shift_id = data1["shift_id"] as! String
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.EventSendRequest(cso_id: cso_id, shift_id: shift_id, event_id: self.eventID!, waiver_doc: "" , userTypeString:self.applyShiftUserType ?? "VOL") { (responce,isSuccess) in
            if isSuccess {
                let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Volunteer request sent successfully", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.ImageView.image = UIImage(named: "complete-verified.png")
                
                self.StatusName.text = "Applied"
                self.StatusName.textColor = APP_GREEN_COLOR
            }else{
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Error Occured!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    func getCoverImageForRank(){
        
        var strImageNameCover = "cover_cloud.jpg"
        
        if let decoded  = UserDefaults.standard.object(forKey: "VolData") as? Data, let volData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as?  Dictionary<String, Any>, let userAvgRank = volData["user_avg_rank"] as? String {
            
            
            let floatUserAverageRank = Float(userAvgRank)!
            
            
            if ((floatUserAverageRank >= 0) && (floatUserAverageRank <= 20)){
                strImageNameCover = "cover_riseandshine.jpg"
            }else if ((floatUserAverageRank > 20) && (floatUserAverageRank <= 40)){
                strImageNameCover = "cover_cake.jpg"
            }else if ((floatUserAverageRank > 40) && (floatUserAverageRank <= 60)){
                strImageNameCover = "cover_cool.jpg"
            }else if ((floatUserAverageRank > 60) && (floatUserAverageRank <= 80)){
                strImageNameCover = "cover_truck.jpg"
            }else if (floatUserAverageRank > 80 ){
                strImageNameCover = "cover_cloud.jpg"
            }
            
            
        }
        self.coverpic.image = UIImage(named:strImageNameCover)
        
        
    }
    
    
    @IBAction func backButto(_ sender: Any) {
        
        performSegueToReturnBack()
    }
    
    
    
    
    
    func setValues(){
        
        self.RatingRank.rating = Double(data1!["shift_rank"] as! String) ?? 0.0
        self.whiteStar.isHidden = true
        self.RatingRank.isHidden = false
        RatingRank.isUserInteractionEnabled = false
        whiteStar.isUserInteractionEnabled = false
        
        self.TitleLabel.text = (data1["shift_task_name"] as? String)?.uppercased()
        self.VolunteerRequestedLabel.text = data1["shift_vol_req"] as? String
        self.ShiftDateLabel.text = data1["shift_date"] as? String
        if data1["shift_all_day"] as? String == "0"{
            let Start_time = data1["shift_start_time"] as! String
            let End_time = data1["shift_end_time"] as! String
            let Time = "\(Start_time) - \(End_time)"
            //print(Time)
            self.ShiftTimeLabel.text = Time
        }else{
            self.ShiftTimeLabel.text = "All Day"
        }
        
        
        
        let accepted = data1["volunteer_req_accepted"] as! String
        ////print(accepted)
        
        let vol_request = data1["shift_vol_req"] as! String
        ////print(vol_request)
        
        let changeRate = data1["volunteer_apply"] as! String
        ////print(changeRate)
        let available = data1["available"] as! Int
        ////print(available)
        if changeRate != "0"{
            
            self.ImageView.image = UIImage(named: "complete-verified.png")
            self.StatusName.text = "Applied"
            self.StatusName.textColor = APP_GREEN_COLOR
            self.applyShiftBtn.isHidden = true
        }
        else
        if changeRate == "0" {
            
            if vol_request == accepted {
                
                self.ImageView.image = UIImage(named: "not-available.png")
                self.StatusName.text = "Not Available"
                self.StatusName.textColor = UIColor.gray
                self.applyShiftBtn.isHidden = true
                
            }else if available == 0 {
                
                self.ImageView.image = UIImage(named: "not-available.png")
                self.StatusName.text = "Not Available"
                self.StatusName.textColor = UIColor.gray
                self.applyShiftBtn.isHidden = true
                
            }else {
                
                self.ImageView.image = UIImage(named: "csoavailable.png")
                self.StatusName.text = "Available"
                self.StatusName.textColor = UIColor(red: 39/255.0, green: 174/255.0, blue: 96/255.0, alpha: 1.0)
                self.applyShiftBtn.isHidden = false
            }
            
        }
    }
    @IBAction func BellButton(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
        
    }
    
    
}
