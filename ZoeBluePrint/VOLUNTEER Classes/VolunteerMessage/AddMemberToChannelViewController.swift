//
//  AddMemberToChannelViewController.swift
//  ZoeBluePrint
//
//  Created by Reetesh Bajpai on 19/05/20.
//  Copyright © 2020 Reetesh Bajpai. All rights reserved.
//

import UIKit
import SendBirdSDK
class AddMemberToChannelViewController: UIViewController,delegateNewMemberSelectionlistCell,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var csoProfilePic: UIImageView!
    @IBOutlet weak var volProfilePic: UIImageView!
    @IBOutlet weak var imgViewCsoCover: UIImageView!
    @IBOutlet weak var csoHeaderView:UIView!
    @IBOutlet weak var volHeaderView:UIView!
    @IBOutlet weak var headerViewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var imgCoverPic: UIImageView!
    @IBOutlet weak var tblCnnctduserList: UITableView!
    var connectedUserList = [[String:Any]]()
    var channel: SBDGroupChannel!
    var arrExistingUser : Array<String> = []
    var dataArray = [[String:Any]]()
    var selectedData : Array<String> = []
    var strChannelType : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.viewCreateChannel.isHidden = true
        // Do any additional setup after loading the view.
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userid = userIDData["user_id"] as! String
        let usertype = userIDData["user_type"] as! String
        if (usertype == "CSO"){
            //time to handle Header acording to Cso
        }else{
            //time to handle Header acording to VOL
        }
        self.callforConnecteduser()
    }
    
    func setProfilePic()  {
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let usertype = userIDData["user_type"] as! String
        if (usertype == "CSO" || usertype == "FPO"){
            //time to handle Header acording to Cso
            imgCoverPic.isHidden = true
            volHeaderView.isHidden = true
            
            
            let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
            let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
            let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
            if let dirPath          = paths.first
            {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
                if let image    = UIImage(contentsOfFile: imageURL.path){
                    self.csoProfilePic.image = image
                    self.csoProfilePic.layer.borderWidth = 1
                    self.csoProfilePic.layer.masksToBounds = false
                    self.csoProfilePic.layer.borderColor = UIColor.black.cgColor
                    self.csoProfilePic.layer.cornerRadius = self.csoProfilePic.frame.height/2
                    self.csoProfilePic.clipsToBounds = true
                }
                // Do whatever you want with the image
            }
            
        } else {
            //time to handle Header acording to VOL
            self.getCoverImageForRank()
            csoHeaderView.isHidden = true
            
            
            let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
            let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
            let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
            if let dirPath          = paths.first
            {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
                if let image    = UIImage(contentsOfFile: imageURL.path){
                    self.volProfilePic.image = image
                    self.volProfilePic.layer.borderWidth = 1
                    self.volProfilePic.layer.masksToBounds = false
                    self.volProfilePic.layer.borderColor = UIColor.black.cgColor
                    self.volProfilePic.layer.cornerRadius = self.volProfilePic.frame.height/2
                    self.volProfilePic.clipsToBounds = true
                }
                // Do whatever you want with the image
            }
            
        }
        
        self.view.layoutIfNeeded()
    }
    
    @IBAction func notificationBellTapped(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Global.setUpViewWithTheme(ViewController: self)
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let usertype = userIDData["user_type"] as! String
        if (usertype == "CSO" || usertype == "FPO"){
            self.imgViewCsoCover.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
        }
        
        self.getCoverImageForRank()
        setProfilePic()
        
    }
    func getCoverImageForRank(){
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userid = userIDData["user_id"] as! String
        let usertype = userIDData["user_type"] as! String
        if (usertype == "VOL" || usertype == "EMP"){
            var strImageNameCover = "cover_cloud.jpg"
            if let decoded  = UserDefaults.standard.object(forKey: "VolData") as? Data,let volData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as?  Dictionary<String, Any>, (volData["user_avg_rank"] != nil){
                if let userAvgRank = volData["user_avg_rank"] as? String {
                    
                    let floatUserAverageRank = Float(userAvgRank)!
                    
                    
                    if ((floatUserAverageRank >= 0) && (floatUserAverageRank <= 20)){
                        strImageNameCover = "cover_riseandshine.jpg"
                    }else if ((floatUserAverageRank > 20) && (floatUserAverageRank <= 40)){
                        strImageNameCover = "cover_cake.jpg"
                    }else if ((floatUserAverageRank > 40) && (floatUserAverageRank <= 60)){
                        strImageNameCover = "cover_cool.jpg"
                    }else if ((floatUserAverageRank > 60) && (floatUserAverageRank <= 80)){
                        strImageNameCover = "cover_truck.jpg"
                    }else if (floatUserAverageRank > 80 ){
                        strImageNameCover = "cover_cloud.jpg"
                    }
                    
                }
            }
            self.imgCoverPic.image = UIImage(named:strImageNameCover)
            
        }
    }
    
    func callforConnecteduser(){
        ActivityLoaderView.startAnimating()
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userid = userIDData["user_id"] as! String
        let usertype = userIDData["user_type"] as! String
        //user_email
        //user_type
        let servicehandler = ServiceHandlers()
        servicehandler.getConnectedUser(user_id:userid,user_type: usertype){(responce,isSuccess) in
            if isSuccess{
                self.connectedUserList = responce as! [[String : Any]]
                
                print(self.connectedUserList)
                self.fillterArray()
                
            }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblCnnctduserList.dequeueReusableCell(withIdentifier: "memberselectioncell") as! AddChannelMemberSelectionTableViewCell
        //user_f_name
        
        cell.lblUserName.text = "\(self.dataArray[indexPath.row]["user_f_name"] ?? "") \( self.dataArray[indexPath.row]["user_l_name"] ?? "")"
        cell.lblEmail.text = self.dataArray[indexPath.row]["user_email"] as? String
        cell.delegate = self
        cell.btnCheckMark.tag = indexPath.row
        cell.imgUserImage.image = UIImage(named: "user_email.png")
        
        if selectedData.contains(self.dataArray[indexPath.row]["user_id"] as! String) {
            // cell.accessoryType = .checkmark
            //newtickbox
            cell.btnCheckMark.setImage(UIImage(named: "newtickbox.png"), for: .normal)
        }else{
            //cell.accessoryType = .none
            cell.btnCheckMark.setImage(UIImage(named: "black-square-png"), for: .normal)
            //black-square-png
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
        
    }
    
    func checkMarkClicked(selectedRow:Int){
        
        let myIndexPath = IndexPath(row: selectedRow, section: 0)
        let cell = self.tblCnnctduserList.cellForRow(at: myIndexPath) as! AddChannelMemberSelectionTableViewCell
        if selectedData.contains(self.dataArray[myIndexPath.row]["user_email"] as! String)  {
            if let index = selectedData.firstIndex(of: self.dataArray[myIndexPath.row]["user_email"] as! String) {
                selectedData.remove(at: index)
            }
            cell.btnCheckMark.setImage(UIImage(named: "black-square-png"), for: .normal)
            print(self.selectedData)
        }
        
        else{
            
            let applicationUserListQueryByIds = SBDMain.createApplicationUserListQuery()
            applicationUserListQueryByIds?.userIdsFilter = [self.dataArray[myIndexPath.row]["user_email"] as! String]
            applicationUserListQueryByIds?.loadNextPage(completionHandler: { (users, error) in
                guard error == nil else {// Error.
                    return
                }
                //SBDUserConnectionStatus //SBDUserConnectionStatusNonAvailable = 0,
                if (users!.count > 0){
                    if Int(users![0].connectionStatus.rawValue) == 0{
                        let name = self.dataArray[myIndexPath.row]["user_f_name"] as! String
                        let alert = UIAlertController(title: "Alert!", message: " \(name) not registered in messenger", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        self.selectedData.append(self.dataArray[myIndexPath.row]["user_email"] as! String)
                        cell.btnCheckMark.setImage(UIImage(named: "newtickbox.png"), for: .normal)
                    }
                    
                }else{
                    let name = self.dataArray[myIndexPath.row]["user_f_name"] as! String
                    let alert = UIAlertController(title: "Alert!", message: " \(name) not registered in messenger", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                print(self.selectedData)
            })
        }
        
        
    }
    
    func checkforConnectionStatus(userEmail:String)-> Bool{
        var status = false
        let applicationUserListQueryByIds = SBDMain.createApplicationUserListQuery()
        applicationUserListQueryByIds?.userIdsFilter = [userEmail]
        applicationUserListQueryByIds?.loadNextPage(completionHandler: { (users, error) in
            guard error == nil else {// Error.
                return
            }
            print(users![0].userId)
            print(users![0].connectionStatus)
            //SBDUserConnectionStatus //SBDUserConnectionStatusNonAvailable = 0,
            if Int(users![0].connectionStatus.rawValue) == 0{
                status = false
            }else{
                status = true
            }
        })
        return status
    }
    
    
    func fillterArray(){
        print(self.connectedUserList)
        var indextoDelete : Array<Int> = []
        for index in 0...self.channel.memberCount-1 {
            //print("\(index) times 5 is \(index * 5)")
            var user: SBDMember!
            user =  self.channel.members!.object(at: Int(index)) as? SBDMember
            print(user.userId)
            let index = self.connectedUserList.firstIndex(where: { dictionary in
                guard let value = dictionary["user_email"] as? String
                else { return false }
                return value == user.userId
            })
            if let index = index {
                self.connectedUserList.remove(at: index)
            }
        }
        
        print(self.connectedUserList)
        if self.connectedUserList.count>0 {
            self.dataArray = self.connectedUserList
            self.tblCnnctduserList.delegate = self
            self.tblCnnctduserList.dataSource = self
            self.tblCnnctduserList.reloadData()
            
        }else{
            let alert = UIAlertController(title: "Alert!", message: "No user found!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
        ActivityLoaderView.stopAnimating()
    }
    
    
    
    @IBAction func createChannel(_ sender: Any) {
        if self.selectedData.count>0{
            if self.selectedData.count > 1 {
                self.strChannelType = "Channel"
            }else{
                self.strChannelType = "Individual"
            }
            // self.viewCreateChannel.isHidden = false
        }else{
            
            let alert = UIAlertController(title: "Alert!", message: "Select users to create channel", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func doneCreateChannel(_ sender: Any) {
        
    }
    @IBAction func cancelCreateChannelView(_ sender: Any) {
    }
    
    func hitAPIToSyncChannelToServer(channel: SBDGroupChannel){
        
        
    }
    
    
    @IBAction func addMemberClick(_ sender: Any) {
        
        if self.selectedData.count > 0{
            
            
            self.channel.inviteUserIds(selectedData, completionHandler: { (error) in
                guard error == nil else {   // Error.
                    return
                }
                let alert = UIAlertController(title: "Success!", message: "User added successfully!", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })
            
        }else{
            
            let alert = UIAlertController(title: "Alert!", message: "No user selected!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        self.view.removeFromSuperview()
    }
    
}


//memberselectioncell

