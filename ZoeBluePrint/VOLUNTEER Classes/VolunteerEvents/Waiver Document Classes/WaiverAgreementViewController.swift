//
//  WaiverAgreementViewController.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 05/04/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit
import Alamofire
import MobileCoreServices
protocol WaiverAgreementViewControllerDelegate: class {
    func applyForShiftWithAgreement(shiftData: Dictionary<String,Any>,index : Int)
}
class WaiverAgreementViewController: UIViewController, UINavigationControllerDelegate {
    var delegate :  WaiverAgreementViewControllerDelegate!
    @IBOutlet var uploadDocViewHeight: NSLayoutConstraint!
    @IBOutlet var backViewHeight: NSLayoutConstraint!
    @IBOutlet var checkAgreementBtn: UIButton!
    @IBOutlet var downloadAgreementDocBtn: UILabel!
    @IBOutlet var uploadDocButton: UILabel!
    @IBOutlet var uploadFileName: UILabel!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var backgroungCheckView: UIView!
    @IBOutlet var checkBgBtn: UIButton!
    @IBOutlet var bgTitle: UILabel!
    
    var fileUrl : URL!
    var choosedImageData : Data?
    var shiftData : Dictionary<String,Any>?
    var shiftSelectedIndex : Int!
    var eventID:String?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.shiftData!["background_check"] as! String == "1"{
            bgTitle.text = String(format: "Background check required for this event, please confirm if you are agreeing to do so %@", getStringForBgAgreement())
            
            self.backViewHeight.constant = 440
            bgTitle.isHidden = false
            checkBgBtn.isHidden = false
        }else{
            self.backViewHeight.constant = 340
            bgTitle.isHidden = true
            checkBgBtn.isHidden = true
        }
        if self.shiftData!["want_to_upload_by_volunteer"] as! String == "no"{
            self.backViewHeight.constant = self.backViewHeight.constant - 150
            self.uploadDocViewHeight.constant = 0.0
        }
//        let myString = "Background check required for this event, please confirm if you are agreeing to do so [ Name, Email Id, Phone Number, Parent/Gaurdian Name, Date of birth, Address ]"
//        let myAttribute = [ NSAttributedString.Key.foregroundColor: UIColor.black ]
        

        
       

        // set attributed text on a UILabel
        //Background check required for this event, please confirm if you are agreeing to do so [ Name, Email Id, Phone Number, Parent/Gaurdian Name, Date of birth, Address ].
    }
    func getStringForBgAgreement()-> String{
        if let dic = self.shiftData!["checked_fields"]{
            let bdCheckDic = self.convertToDictionary(text: dic as! String) as? [String : String]
            let componentArray = Array(bdCheckDic!.keys)
            var finalString = ""
            if componentArray.contains("email_check"){
                finalString.append(" Email")
            }
            if componentArray.contains("address_check"){
                finalString.append(" Address")
            }
            if componentArray.contains("dob_check"){
                finalString.append(" Date Of Birth")
            }
            if componentArray.contains("name_check"){
                finalString.append(" Name")
            }
            if componentArray.contains("parent_check"){
                finalString.append(" Parent/Gaurdian")
            }
            if componentArray.contains("phone_check"){
                finalString.append(" Phone Number")
            }
            return finalString
        }
        
        return ""
    }
    @IBAction func closeView(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    @IBAction func onClickDownloadAgreementDocBtn(_ sender: Any) {
        let alert = UIAlertController(title: NSLocalizedString("DOWNLOAD DOCUMENT?", comment: ""), message: NSLocalizedString("Do you want to download the document?", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {(_ action: UIAlertAction) -> Void in
            ActivityLoaderView.startAnimating()
            let document_file_url = self.shiftData!["event_waiver_doc"] as! String
            
            let fullName    = document_file_url
            let fullNameArr = fullName.components(separatedBy: "/")
            let fileName = fullNameArr.last
            let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)


//            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
//                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//                documentsURL.appendPathComponent(fileName!)
//                return (documentsURL, [.removePreviousFile])
//            }
            let docUrlString = document_file_url.replacingOccurrences(of: " ", with: "%20")
            AF.download(docUrlString, to: destination).responseData { response in
                if let destinationUrl = response.fileURL {
                    self.fileUrl = destinationUrl as URL
                    ActivityLoaderView.stopAnimating()
                    self.saveDataToPhone(fileName: fileName!)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func saveDataToPhone(fileName:String)
    {
        
        do {
            let fileData = try Data(contentsOf: self.fileUrl as URL)
            let activityController = UIActivityViewController(activityItems: [fileData], applicationActivities: nil)
            self.present(activityController, animated: true, completion: nil)
            
        } catch {
            //print("Unable to load data: \(error)")
        }
        
    }
    
    @IBAction func onClickbgAgreementBtn(_ sender: Any) {
        self.checkBgBtn.isSelected = !self.checkBgBtn.isSelected

    }
    @IBAction func onClickCheckAgreementBtn(_ sender: Any) {
        self.checkAgreementBtn.isSelected = !self.checkAgreementBtn.isSelected
//        if self.checkAgreementBtn.isSelected{
//
//        }else{
//            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Please accept agreement", comment: ""), preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
    }
    
    @IBAction func onClickUploadDocBtn(_ sender: Any) {
        
        let alert = UIAlertController(title: NSLocalizedString("UPLOAD FILES FROM", comment: ""), message: "", preferredStyle: .alert)
        let gallery = UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerController.SourceType.photoLibrary
            image.allowsEditing = false
            self.present(image, animated: true)
            {
                
            }
            // call method whatever u need
        })
        let camera = UIAlertAction(title: NSLocalizedString("Camera", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerController.SourceType.camera
            image.allowsEditing = false
            self.present(image, animated: true)
            {
//                self.mainView.isHidden = false
//
//                self.backgroundView.isHidden = false
            }
            // call method whatever u need
        })
        let drive = UIAlertAction(title: NSLocalizedString("Files", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            let documentPicker = UIDocumentPickerViewController(documentTypes: ["com.apple.iwork.pages.pages", "com.apple.iwork.numbers.numbers", "com.apple.iwork.keynote.key","public.image", "com.apple.application", "public.item","public.data", "public.content", "public.audiovisual-content", "public.movie", "public.audiovisual-content", "public.video", "public.audio", "public.text", "public.data", "public.zip-archive", "com.pkware.zip-archive", "public.composite-content", "public.text"], in: .import)
            
            documentPicker.delegate = self
            documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            
            self.present(documentPicker, animated: true, completion: nil)
            // call method whatever u need
        })
        let noButton = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil)
        alert.addAction(gallery)
        alert.addAction(camera)
        alert.addAction(drive)
        alert.addAction(noButton)
        present(alert, animated: true)
        
    }
}
extension WaiverAgreementViewController: UIDocumentPickerDelegate,UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            self.choosedImageData =  (image as? UIImage)!.jpegData(compressionQuality: 0.5)!
        }else{
            //print("error")
        }
        guard let fileURL = info[UIImagePickerController.InfoKey.imageURL] as? URL
        else {
            self.uploadFileName.text = "image1.png"
            return
        }
        
        self.uploadFileName.text = fileURL.lastPathComponent
        
        self.dismiss(animated: true, completion: nil)
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        if controller.documentPickerMode == UIDocumentPickerMode.import {
        }
        
        let cico = url as URL
        do{
            self.choosedImageData = try Data(contentsOf: url)
            self.uploadFileName.text = url.lastPathComponent
        } catch {
            //print(error)
        }
        
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        let newStr = text.replacingOccurrences(of: "\'", with: "\"")
        if let data = newStr.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
    }
    @IBAction func onClickSubmitBtn(_ sender: Any) {
        if !self.checkAgreementBtn.isSelected{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please select agreement", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        if !self.checkBgBtn.isSelected && self.shiftData!["background_check"] as! String == "1"{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please select background check", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        if self.shiftData!["want_to_upload_by_volunteer"] as! String == "no"{
            self.delegate.applyForShiftWithAgreement(shiftData: self.shiftData!,index : self.shiftSelectedIndex)
            self.view.removeFromSuperview()
            return
        }
        if  (self.choosedImageData != nil)
        {
            //Submit Button Functionality
            let currentTime = "waiver_" + "\(Date().timeIntervalSince1970)"
            let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
            let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
            let params = userIDData["user_id"] as! String
            let apiKey = "1234"
            let Action = "doc_locker_file_upload"
            let user_device = UIDevice.current.identifierForVendor!.uuidString
            let data2:[String:Any] = ["user_id":params,"event_id":self.eventID!,"api_key":apiKey,"action":Action,"document_name":currentTime + ".png","user_device":user_device,"user_type":userIDData["user_type"] as! String,"document_type":"0"]
            let imageSize: Int = choosedImageData!.count
            let limit:Double = 5000.0
            if(Double(imageSize/1000) <= limit)
            {
                // //print(data2)
                let serviceHanlder = ServiceHandlers()
                serviceHanlder.uploadLockerFiles(data_details:data2,file:choosedImageData!) { (responce, isSuccess) in
                    if isSuccess {
                        
                        let name = (responce as! [String : Any])["res_message"]
                        let alert = UIAlertController(title: "Success!", message: "Document uploaded successfully", preferredStyle: UIAlertController.Style.alert)
                        
                        alert.addAction(UIAlertAction(title: NSLocalizedString(NSLocalizedString("OK", comment: ""), comment: ""), style: UIAlertAction.Style.default, handler: { (action) in
                            self.delegate.applyForShiftWithAgreement(shiftData: self.shiftData!,index : self.shiftSelectedIndex == nil ? 0 : self.shiftSelectedIndex)
                            self.view.removeFromSuperview()
                        }))
                        self.present(alert, animated: true, completion: nil)
                       
                    }
                }
                
            }else{
                
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Image must be less then 2 MB.", comment: ""), preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        }
        else{
            
            
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please select document", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            
            
        }
        
    }
}
