//
//  ChooseProfileTypeViewController.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 14/06/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit
protocol ChooseProfileTypeViewControllerDelegate: class {
    func chooseProfileWith(type: String , index : Int)
}
class ChooseProfileTypeViewController: UIViewController {
    var delegate :  ChooseProfileTypeViewControllerDelegate!
    @IBOutlet var chooseVolunteerButton: UIButton!
    @IBOutlet var chooseEmpButton: UIButton!
    var indexNum : Int!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func chooseTypeView(_ sender: Any) {
        if self.chooseEmpButton.isSelected {
            self.chooseEmpButton.isSelected = false
            self.chooseVolunteerButton.isSelected = true
        }else{
            self.chooseEmpButton.isSelected = true
            self.chooseVolunteerButton.isSelected = false
        }
    }
    @IBAction func submitButton(_ sender: Any) {
        self.delegate.chooseProfileWith(type: self.chooseEmpButton.isSelected ? "EMP" : "VOL", index: self.indexNum)
        self.view.removeFromSuperview()
    }
    @IBAction func closeButton(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
}
