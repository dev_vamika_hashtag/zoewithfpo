//
//  Volunteer Targets.swift
//  ZoeBluePrint
//
//  Created by iOS Training on 02/03/20.
//  Copyright © 2020 Reetesh Bajpai. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class Volunteer_Targets: UIViewController {
    
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var lblVolHours:UILabel!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblHeadingName: UILabel!
    @IBOutlet weak var profilepic: UIImageView!
    @IBOutlet weak var lblTotalHours: UILabel!
    
    @IBOutlet weak var lblBlankLine: UILabel!
    @IBOutlet weak var lblTargetRequired: UILabel!
    @IBOutlet weak var lblCurrentHours: UILabel!
    
    @IBOutlet weak var imgViewCoverPic: UIImageView!
    @IBOutlet weak var CircluarProgress: CircularProgressView!
    @IBOutlet weak var whiteStarView: FloatRatingView!
    @IBOutlet weak var rankImage: UIImageView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var Completedhours: UILabel!
    @IBOutlet weak var rateView: UIView!
    @IBOutlet weak var myRateBottomLabel: UILabel!
    @IBOutlet weak var myTargetBottomLabel: UILabel!
    @IBOutlet weak var myTargetButton: UIButton!
    @IBOutlet weak var myRatingButton: UIButton!
    var Target : [[String:Any]]!
    var TargetPercentage:Float!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        // For CircularView:
        CircluarProgress.progressColor = UIColor(red: 208/255.0 , green:25/255.0 , blue: 89/255.0, alpha: 1.0)
        CircluarProgress.trackColor = UIColor(red: 238/255.0, green: 185/255.0, blue: 203/255.0, alpha: 1.0)
        CircluarProgress.tag = 101
        // self.perform(#selector(progressAnimate),with: nil, afterDelay: 2.0)
        
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let param = userIDData["user_id"] as! String
        print(param)
        
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.VolTargets(user_id : param) { (responce, isSuccess) in
            if isSuccess {
                
                var TargetData = responce as! Array<Any>
                self.Target = TargetData as? [[String:Any]]
                print(self.Target)
                
                for data in self.Target {
                    
                    let dataTarget = data 
                    
                    var current_hours = dataTarget["vol_hours"] as! String
                    print(current_hours)
                    //                    let cHours = current_hours
                    //                    print(cHours)
                    var strtoPrint = NSLocalizedString("Volunteers Hours Current", comment: "") + " \(dataTarget["vol_hours"] as! String) | " + NSLocalizedString("Target", comment: "") + " \(dataTarget["vol_hours_req"] as! String)"
                    self.lblCurrentHours.text = strtoPrint
                    //Volunteers HRS Current | Target 100
                    var vol_hours_req = dataTarget["vol_hours_req"] as! String
                    //print(vol_hours_req)
                    //var Target = "Target \(vol_hours_req)"
                    //print(Target)
                    //self.lblTargetRequired.text = Target
                    
                    self.TargetPercentage = (Float(current_hours)!)/(Float(vol_hours_req)!) * 100
                    print(self.TargetPercentage)
                    
                    var Percentage:Int = Int(self.TargetPercentage)
                    print(Percentage)
                    
                    var totalPercentage:String = String(Percentage)
                    print(totalPercentage)
                    
                    self.lblTotalHours.text = totalPercentage + "%"
                    
                    self.progressAnimate()
                    
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        self.getCoverImageForRank()
        Global.setUpViewWithTheme(ViewController: self)
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profilepic.image = image
                self.profilepic.layer.borderWidth = 1
                self.profilepic.layer.masksToBounds = false
                self.profilepic.layer.borderColor = UIColor.black.cgColor
                self.profilepic.layer.cornerRadius = self.profilepic.frame.height/2
                self.profilepic.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
        self.rateView.isHidden = true
        self.myTargetButton.setTitleColor(APP_BLACK_COLOR, for: UIControl.State.normal)
        self.myRatingButton.setTitleColor(UIColor.gray, for: UIControl.State.normal)
        myRateBottomLabel.isHidden = true
        myTargetBottomLabel.isHidden = false
    }
    func getCoverImageForRank(){
        
        var strImageNameCover = "cover_cloud.jpg"
        
        if let decoded  = UserDefaults.standard.object(forKey: "VolData") as? Data, let volData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as?  Dictionary<String, Any>, let userAvgRank = volData["user_avg_rank"] as? String {
            
            let floatUserAverageRank = Float(userAvgRank)!
            
            
            if ((floatUserAverageRank >= 0) && (floatUserAverageRank <= 20)){
                strImageNameCover = "cover_riseandshine.jpg"
            }else if ((floatUserAverageRank > 20) && (floatUserAverageRank <= 40)){
                strImageNameCover = "cover_cake.jpg"
            }else if ((floatUserAverageRank > 40) && (floatUserAverageRank <= 60)){
                strImageNameCover = "cover_cool.jpg"
            }else if ((floatUserAverageRank > 60) && (floatUserAverageRank <= 80)){
                strImageNameCover = "cover_truck.jpg"
            }else if (floatUserAverageRank > 80 ){
                strImageNameCover = "cover_cloud.jpg"
            }
            
        }
        
        self.imgViewCoverPic.image = UIImage(named:strImageNameCover)
        
        
    }
    
    
    
    
    
    
    @objc func progressAnimate() {
        
        let viewTag  = self.view.viewWithTag(101) as! CircularProgressView
        print(self.TargetPercentage/100)
        viewTag.setProfileWithAnimation(duration: 2.0, Value: self.TargetPercentage/100)
        
        
    }
    @IBAction func myRatingTapped(_ sender: Any) {
        self.myRatingButton.setTitleColor(APP_BLACK_COLOR, for: UIControl.State.normal)
        self.myTargetButton.setTitleColor(UIColor.gray, for: UIControl.State.normal)
        myRateBottomLabel.isHidden = false
        myTargetBottomLabel.isHidden = true
        self.rateView.isHidden = false
        if  let decoded  = UserDefaults.standard.object(forKey: "VolData") as? Data,let volData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as?  Dictionary<String, Any>{
            
            if let userAvgRate = volData["user_avg_rate"] as? String {
                self.ratingView.rating = Double(userAvgRate) ?? 0.0
                self.ratingView.isHidden = false
            }
            let resdata = volData["res_data"] as! Dictionary<String, Any>
            var hoursDone = resdata["vol_hours"] as! String
            hoursDone = hoursDone + NSLocalizedString(" VOLUNTEER HOURS COMPLETED ", comment: "")
            //print(hoursDone)
            
            whiteStarView.isHidden = true
            self.Completedhours.text = hoursDone
            self.rankImage.image = UIImage(named: findingAverageRank(user_hours: volData["user_avg_rank"] as! String))
        } else {
            var hoursDone = "0"
            hoursDone = hoursDone + NSLocalizedString(" VOLUNTEER HOURS COMPLETED ", comment: "")
            //print(hoursDone)
            self.ratingView.isHidden = false
            whiteStarView.isHidden = true
            self.Completedhours.text = hoursDone
            self.rankImage.image = UIImage(named: findingAverageRank(user_hours: "0"))
            
        }
        
        self.ratingView.isUserInteractionEnabled = false
        self.whiteStarView.isUserInteractionEnabled = false
        
    }
    func findingAverageRank(user_hours:String)->String{
        
        
        var percent = (user_hours as NSString).integerValue
        //print(percent)
        
        
        if percent >= 0 && percent <= 20 {
            
            //          var Image1 = UIImage(named:"rank_five.png")
            //            //print(Image1)
            return "risenshine.png"    //rank_five.png
        }else
        if percent >= 21 && percent <= 40 {
            
            return "cacke.png"     //rank_four.png
        }else
        if percent >= 41     && percent <= 60  {
            
            return "coll.png"    //rank_three.png
        }else
        if percent >= 61 && percent <= 80{
            
            return "truck.png"   //rank_two.png
            
        }else
        if percent >= 81 && percent <= 100{
            //var Image5 = UIImage(named:"rank_one.png")
            //         //print(Image5)
            
            return "cloud.png"     //rank_one.png
        }else
        if percent >= 100 {
            
            return "cloud.png"  //rank_one.png
        }
        return "risenshine.png" //rank_five.png
    }
    @IBAction func myTargetTapped(_ sender: Any) {
        self.myTargetButton.setTitleColor(APP_BLACK_COLOR, for: UIControl.State.normal)
        self.myRatingButton.setTitleColor(UIColor.gray, for: UIControl.State.normal)
        myRateBottomLabel.isHidden = true
        myTargetBottomLabel.isHidden = false
        self.rateView.isHidden = true
        
    }
    @IBAction func notificationBellTapped(_ sender: Any) { Utility.showNotificationScreen(navController: self.navigationController)
    }
}
