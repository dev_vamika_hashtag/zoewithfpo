//
//  FPORegistrationViewController.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 03/08/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit
import SideMenu
class FPORegistrationViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet var userProfilePic: UIImageView!
    @IBOutlet var companyNameTF: UITextField!
    @IBOutlet var fpoEmailTF: UITextField!
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var phoneTF: UITextField!
    @IBOutlet var streetTF: UITextField!
    @IBOutlet var zipCodeTF: UITextField!
    @IBOutlet var selectPlanBtn: UIButton!
    @IBOutlet var numOfEmpBtn: UIButton!
    @IBOutlet var cvvStackView: UIStackView!
    @IBOutlet var passwordStackView: UIStackView!
    @IBOutlet var cPasswordStackView: UIStackView!
    @IBOutlet var confirmPasswordTF: UITextField!
    @IBOutlet var passwordTF: UITextField!
    @IBOutlet var taxTF: UITextField!
    @IBOutlet var countryBtn: UIButton!
    @IBOutlet var stateBtn: UIButton!
    @IBOutlet var suiteTF: UITextField!
    @IBOutlet var cityTf: UITextField!
    @IBOutlet var cPhoneNumTF: UITextField!
    @IBOutlet var cFpoEmailTF: UITextField!
    @IBOutlet var userType: UIButton!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var bannerImage: UIImageView!
    @IBOutlet var screenTitle: UILabel!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var passEye: UIButton!
    @IBOutlet var cPassEye: UIButton!
    @IBOutlet var cardHolderName: UITextField!
    @IBOutlet var cardNumber: UITextField!
    @IBOutlet var cvvNum: UITextField!
    @IBOutlet var expiryDateTF: UITextField!
    @IBOutlet var menuButton: UIButton!
    @IBOutlet var notiButton: UIButton!
    var editProfileDetails:Dictionary<String,Any>!
    
    var user_countryID:String?
    var user_stateID:String?
    var screen:String?
    var boolShowBackAlert :Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshViewInViewdidload()
        addUnderLineToField(color:APP_BLACK_COLOR)
        zipCodeTF.delegate = self
        phoneTF.delegate = self
        cPhoneNumTF.delegate = self
        self.passwordTF.isSecureTextEntry = true
        self.confirmPasswordTF.isSecureTextEntry = true
        self.user_countryID = "1"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
        if(self.screen == "EDIT VIEW"){
            self.notiButton.isHidden = false
            self.menuButton.isHidden = false
        }else{
            self.notiButton.isHidden = true
            self.menuButton.isHidden = true
        }
    }
    func loadData(){
        
        if(self.screen == "EDIT VIEW"){
            self.cPassEye.isHidden = true
            self.passEye.isHidden = true
            self.backButton.isHidden = true
            self.userProfilePic.isHidden = false
            self.cPasswordStackView.isHidden = true
            self.passwordStackView.isHidden = true
            boolShowBackAlert = true
            let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
            let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
            let params = userIDData["user_id"] as! String
            let obj = ServiceHandlers()
            obj.editProfile(user_id: params){(responce,isSuccess) in
                if isSuccess{
                    self.editProfileDetails = responce as! Dictionary<String,Any>
                    print(self.editProfileDetails)
                    self.fpoEmailTF.text = self.editProfileDetails!["user_email"] as! String
                    self.taxTF.text = self.editProfileDetails!["org_taxid"] as! String
                    if let suiteNum = self.editProfileDetails!["user_site_number"]{
                        self.suiteTF.text =  suiteNum as? String
                    }
                    self.companyNameTF.text = self.editProfileDetails!["org_name"] as! String
                    self.cFpoEmailTF.text = self.editProfileDetails!["user_email"] as! String
                    self.cPhoneNumTF.text = self.formattedNumber(number:(self.editProfileDetails!["user_phone"] as? String)!)
                    self.firstName.text =  self.editProfileDetails!["user_f_name"] as! String
                    self.lastName.text = self.editProfileDetails!["user_l_name"] as! String
                    self.numOfEmpBtn.setTitle(String(format: "<%@", self.editProfileDetails!["fpo_emp_num"] as! String), for: .normal)
                    
                    if self.editProfileDetails!["fpo_emp_num"] as! String == "150"{
                        self.numOfEmpBtn.setTitle("150+", for: .normal)
                    }
                    self.phoneTF.text = self.formattedNumber(number:(self.editProfileDetails!["user_phone"] as? String)!)
                    self.streetTF.text = self.editProfileDetails!["user_address"] as! String
                    self.cityTf.text = self.editProfileDetails!["user_city"] as! String
                    self.user_stateID = self.editProfileDetails!["user_state"] as! String
                    
                    self.stateBtn.setTitle(self.editProfileDetails!["user_state_name"] as! String, for: .normal)
                    self.countryBtn.setTitle(self.editProfileDetails!["user_country_name"] as! String, for: .normal)
                    
                    self.user_countryID = self.editProfileDetails!["user_country"] as! String
                  
                    self.zipCodeTF.text = self.editProfileDetails!["user_zipcode"] as! String
                    self.submitButton.setTitle(NSLocalizedString("Update", comment: ""), for: UIControl.State.normal)
                   
                    
                }
            }
        }else{
            self.backButton.isHidden = false
            self.userProfilePic.isHidden = true
        }
    }
    func refreshViewInViewdidload(){
            self.bannerImage.image =  UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
        if(self.screen == "EDIT VIEW"){
            profile_pic()
            boolShowBackAlert = true
        }else{
            boolShowBackAlert = false
        }
        }
    
    func profile_pic()  {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.userProfilePic.image = image
                self.userProfilePic.layer.borderWidth = 1
                self.userProfilePic.layer.masksToBounds = false
                self.userProfilePic.layer.borderColor = APP_BLACK_COLOR.cgColor
                self.userProfilePic.layer.cornerRadius = self.userProfilePic.frame.height/2
                self.userProfilePic.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
        
    }
    func addUnderLineToField(color:UIColor)  {
        companyNameTF.setUnderLineOfColor(color: color)
        fpoEmailTF.setUnderLineOfColor(color: color)
        firstName.setUnderLineOfColor(color: color)
        lastName.setUnderLineOfColor(color: color)
        phoneTF.setUnderLineOfColor(color: color)
        cPhoneNumTF.setUnderLineOfColor(color: color)
        taxTF.setUnderLineOfColor(color: color)
        passwordTF.setUnderLineOfColor(color: color)
        confirmPasswordTF.setUnderLineOfColor(color: color)
        suiteTF.setUnderLineOfColor(color: color)
        cityTf.setUnderLineOfColor(color: color)
        cFpoEmailTF.setUnderLineOfColor(color: color)
        streetTF.setUnderLineOfColor(color: color)
        zipCodeTF.setUnderLineOfColor(color: color)
        cardHolderName.setUnderLineOfColor(color: color)
        cardNumber.setUnderLineOfColor(color: color)
        cvvNum.setUnderLineOfColor(color: color)
        expiryDateTF.setUnderLineOfColor(color: color)
    }
    @IBAction func onClickUserType(_ sender: Any) {
        let contents = ["FPO"]
        showPopoverForView(view: sender, contents: contents)
    }
    @IBAction func onClickNotiication(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    @IBAction func onClickMenu(_ sender: Any) {
        if let menu = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "volsidemenu") as? SideMenuNavigationController{
//            self.navigationController?.present(menu, animated: true, completion: nil)
        present(menu, animated: true, completion: nil)
        }
    }
    @IBAction func onBack(_ sender: Any) {
        if boolShowBackAlert ?? false{
            let alert = UIAlertController(title: NSLocalizedString("CONFIRM EXIT?", comment: ""), message: NSLocalizedString("Do you want to discard changes?", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel, handler: { _ in
                //Cancel Action
            }))
            alert.addAction(UIAlertAction(title: "YES",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            //Sign out action
                                            self.performSegueToReturnBack()
                                          }))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.performSegueToReturnBack()
        }
        
    }
    @IBAction func showConfirmPassword(_ sender: UIButton) {
        if !sender.isSelected{
            self.confirmPasswordTF.isSecureTextEntry = false
        }else{
            self.confirmPasswordTF.isSecureTextEntry = true
        }
    }
    @IBAction func onReset(_ sender: Any) {
    }
    @IBAction func onRegistration(_ sender: Any) {
        callRegistrationApi()
    }
    @IBAction func onClickState(_ sender: Any) {
        let utility = Utility()
        utility.fetchStateList{ (eventTypeList, isValueFetched) in
            if let list = eventTypeList {
                self.showPopoverForView(view: sender, contents: list)
            }
        }
    }
    
    @IBAction func numOfEmpCLick(_ sender: Any) {
        let contents = ["<10","<25","<75","150+"]
        showPopoverForView(view: sender, contents: contents)
    }
    @IBAction func selectPlan(_ sender: Any) {
        let contents = ["Trial[7 Days]","Paid[$995 Annually]"]
        showPopoverForView(view: sender, contents: contents)
    }
    @IBAction func onClickCountry(_ sender: Any) {
        let utility = Utility()
        utility.fetchCountryList{ (eventTypeList, isValueFetched) in
            if let list = eventTypeList {
                self.showPopoverForView(view: sender, contents: list)
            }
        }
    }
    @IBAction func showPassword(_ sender: UIButton) {
        if !sender.isSelected{
            self.passwordTF.isSecureTextEntry = false
        }else{
            self.passwordTF.isSecureTextEntry = true
        }
    }
    func generateStripeTokenFromCardDeatail(){
        let param = [
                     "card_name":"Vamika",
                     "card_number":"4242424242424242",
                     "month":"12",
                     "year":"23",
                     "cvc_number":"424"]
        let serviceHandler = ServiceHandlers()
        serviceHandler.getStripeTokenWith(params: param){(responce,isSuccess) in
            if isSuccess{
//                self.callRegistrationApi(stipe_token : "")
            }else{
               
            }
        }
    }
    func callRegistrationApi(){
        
        if (self.screen == "EDIT VIEW"){
            if(validateStage1Edit()){
                let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
                let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
                let user_id = userIDData["user_id"] as! String
                
                var params2 = [
                    "user_id":user_id,
                    "user_type":"FPO",
                    "user_device":UIDevice.current.identifierForVendor!.uuidString,
                    "user_f_name":self.firstName.text as? String,
                    "user_l_name":self.lastName.text as? String,
                    "user_country":self.user_countryID as? String,
                    "user_state":self.user_stateID as? String,
                    "user_city":self.cityTf.text as? String,
                    "user_zipcode":self.zipCodeTF.text as? String,
                    "user_address": self.streetTF.text as? String,
                    "user_email":self.fpoEmailTF.text as? String,
                    "user_phone":self.phoneTF.text as? String,
                    "fpo_employee_num":(self.numOfEmpBtn.titleLabel?.text as? String)?.replacingOccurrences(of: "<", with: "").replacingOccurrences(of: "+", with: ""),
                    "user_suite_number":self.suiteTF.text as? String,
                    "user_company":self.companyNameTF.text as? String,
                    "org_taxid":self.taxTF.text as? String
                ]
                
                let servicehandler = ServiceHandlers()
                servicehandler.updateFpoProfile(data: params2){(responce,isSuccess) in
                    if isSuccess{
                        self.user_stateID = ""
                        self.user_countryID = ""
                        
                            let alert = UIAlertController(title: "Success!", message: "Update Successful", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (alert_action) in
                                self.performSegueToReturnBack()
                            }))
                            self.present(alert, animated: true)
                        
                        
                    }else{
                        if responce == nil{
                        let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Error occured! Please try again!", comment: ""), preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true)
                        }else{
                            let data = responce as! Dictionary<String,Any>
                            if (((data["res_message"] as? String)?.contains("EMAILEXIST")) != nil){
                            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Email id already exits!", comment: ""), preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true)
                            }else if (((data["res_message"] as? String)?.contains("PHONEEXIST")) != nil){
                                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Phone is already exits!", comment: ""), preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                                self.present(alert, animated: true)
                                }
                            else{
                        //
                            }
                        }
                    }
                }
            }
        }
        else{
        if ( validate()){
        var params =            ["user_type":"FPO",
                                 "user_device": UIDevice.current.identifierForVendor!.uuidString,
                                 "user_f_name": self.firstName.text as? String,
                                 "user_l_name": self.lastName.text as? String,
                                 "user_email": self.fpoEmailTF.text as? String,
                                 "user_phone":  self.phoneTF.text as? String,
                                 "user_country": self.user_countryID as? String,
                                 "user_state": self.user_stateID as? String,
                                 "user_city" : self.cityTf.text as? String,
                                 "user_zipcode": self.zipCodeTF.text as? String,
                                 "user_address": self.streetTF.text as? String,
                                 "user_pass": self.passwordTF.text as? String,
                                 "fpo_employee_num":(self.numOfEmpBtn.titleLabel?.text as? String)?.replacingOccurrences(of: "<", with: "").replacingOccurrences(of: "+", with: ""),
                                 "user_suite_number":self.suiteTF.text as? String,
                                 "user_company":self.companyNameTF.text as? String,
                                 "org_taxid":self.taxTF.text as? String
        ]
        
        print(params)
        let servicehandler = ServiceHandlers()
        servicehandler.csoRegistrationStage1(data: params){(responce,isSuccess) in
            if isSuccess{
                
                var data = responce as! Dictionary<String,Any>
               let phone_otp = data["phone_otp"] as! String
                print(phone_otp)
                if self.screen == "EDIT VIEW"
                {
                    let alert = UIAlertController(title: "", message: "Update Successful", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (alert_action) in
                        self.performSegueToReturnBack()
                    }))
                    self.present(alert, animated: true)
                }else{
                    let sb = UIStoryboard(name: "Main", bundle: nil)
                    let obj1 = sb.instantiateViewController(withIdentifier: "phoneotp") as! PhoneOtp
                    obj1.phoneotp = phone_otp
                    obj1.user_id = (data["user_id"] as! String)
                    obj1.user_type = "FPO"
                    self.present(obj1, animated: true)
                }
            }else{
                let msg = responce as! String
                let alert = UIAlertController(title: "Alert!", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }
        }
        }
    }
    
    func validate()->Bool{
        if(self.companyNameTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Company Name is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if(self.fpoEmailTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Email is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else if !(self.isValidUserName(text: self.fpoEmailTF.text!)){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Not a valid email", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }  else if (self.fpoEmailTF.text != self.cFpoEmailTF.text ){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Please enter correct confirmation email", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        
        
        
        
    }else if(self.firstName.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("First name cannot be blank", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else if(self.lastName.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Last name cannot be blank", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else if(self.phoneTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Direct Dial cannot be blank", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if !(self.isValidPhoneNumber(text:self.phoneTF.text!)){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Not a valid Direct Dial.", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
       
        else if !(self.phoneTF.text == self.cPhoneNumTF.text ){
                let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Please enter correct confirmation Direct Dial", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true)
                return false
            
            
            
            
        }
       
        else if(self.streetTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please enter street", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if(self.cityTf.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please enter city", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else if(self.user_stateID == nil){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("State not selected", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if(self.zipCodeTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please enter zipcode", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        
        else if(self.user_countryID == nil){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Country not selected", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if self.numOfEmpBtn.titleLabel?.text == "Number of Employee's"{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Number of employees not selected", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if(self.passwordTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Password is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else  if !(self.validatePassword(password: self.passwordTF.text!) )
        {
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Password must be at least 8 characters 1 uppercase 1 lowercase and 1 number", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }else if(self.confirmPasswordTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Confirm password is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else if !(self.passwordTF.text == self.confirmPasswordTF.text){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Password and confirm password not same", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        
        
        return true
    }
    func validateStage1Edit()->Bool{
        if(self.companyNameTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Company Name is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if(self.fpoEmailTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Email is empty", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else if !(self.isValidUserName(text: self.fpoEmailTF.text!)){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Not a valid email", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }  else if (self.fpoEmailTF.text != self.cFpoEmailTF.text ){
            let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Please enter correct confirmation email", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert, animated: true)
            return false
        
        
        
        
    }else if(self.firstName.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("First name cannot be blank", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else if(self.lastName.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Last name cannot be blank", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else if(self.phoneTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Direct Dial cannot be blank", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if !(self.isValidPhoneNumber(text:self.phoneTF.text!)){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Not a valid Direct Dial.", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
       
        else if !(self.phoneTF.text == self.cPhoneNumTF.text ){
                let alert = UIAlertController(title: "Alert!", message:NSLocalizedString("Please enter correct confirmation Direct Dial", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true)
                return false
            
            
            
            
        }
       
        else if(self.streetTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please enter street", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if(self.cityTf.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please enter city", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }else if(self.user_stateID == nil){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("State not selected", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if(self.zipCodeTF.text == ""){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please enter zipcode", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        
        else if(self.user_countryID == nil){
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Country not selected", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        else if self.numOfEmpBtn.titleLabel?.text == "Number of Employee's"{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Number of employees not selected", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
            return false
        }
        
        
        
        return true
    }
    
    
    fileprivate func showPopoverForView(view:Any, contents:Any) {
        let controller = DropDownItemsTable(contents)
        let senderButton = view as! UIButton
        controller.showPopoverInDestinationVC(destination: self, sourceView: view as! UIView) { (selectedValue) in
            if let selectVal = selectedValue as? String {
                senderButton.setTitle(selectVal, for: .normal)
                senderButton.setImage(nil, for: .normal)
            } else if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetCountryServiceStrings.keyCountryName] as? String {
                self.user_countryID = selectVal[GetCountryServiceStrings.keyCountryId] as! String
                senderButton.setTitle(title, for: .normal)
                senderButton.setImage(nil, for: .normal)
            }  else if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetStateServiceStrings.keyStateName] as? String {
                self.user_stateID = selectVal[GetStateServiceStrings.keyStateId] as! String
                senderButton.setTitle(title, for: .normal)
                senderButton.setImage(nil, for: .normal)
            }
        }
    }
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XXX)XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == self.phoneTF || textField == self.cPhoneNumTF){
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = newString.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne {
                formattedString.append("1 ")
                index += 1
            }
            if (length - index) > 3 {
                let areaCode = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("(%@)", areaCode)
                index += 3
            }
            if length - index > 3 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", prefix)
                index += 3
            }
            
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
            
        }
        
        if(textField == zipCodeTF){
            
            guard let textFieldText = textField.text,
                  let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 5
        }
        
        return true
    }
    func isValidUserName(text:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        print(emailTest.evaluate(with: text))
        return emailTest.evaluate(with: text)
    }
    func isValidPhoneNumber(text: String)-> Bool{
        
        if text.count == 13
        {
            return true
            
        }else{
            return false
        }
        return true
    }
    func validatePassword(password: String) -> Bool
    {
        let regularExpression = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}"
        
        let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        
        return passwordValidation.evaluate(with: password)
    }
}
