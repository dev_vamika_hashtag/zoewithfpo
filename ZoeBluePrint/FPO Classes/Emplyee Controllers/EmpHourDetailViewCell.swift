//
//  EmpHourDetailViewCell.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 11/05/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit

class EmpHourDetailViewCell: UITableViewCell {
    @IBOutlet weak var shiftName: UILabel!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var hoursLbl: UILabel!
    @IBOutlet weak var shiftDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code 120
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
