//
//  MyEmployeeViewController.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 05/05/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit
import SendBirdSDK

class MyEmployeeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var employeeListBtn: UIButton!
    @IBOutlet weak var requestedEmployeeBtn: UIButton!
    @IBOutlet weak var archivedEmployeeBtn: UIButton!
    @IBOutlet weak var employeeListBottomLbl: UILabel!
    @IBOutlet weak var requestedBottomLbl: UILabel!
    @IBOutlet weak var archivedBottomLbl: UILabel!
    @IBOutlet weak var profilePicView: UIImageView!
    @IBOutlet weak var bannerImgView: UIImageView!
    @IBOutlet weak var employeeTableView: UITableView!
    @IBOutlet weak var hourDeatilView: UIView!
    @IBOutlet weak var hourDeatilTableView: UITableView!
    @IBOutlet var hoursBreakUpViewHeight: NSLayoutConstraint!
    var empInfoData =  NSArray()
    var empHoursInfoData =  NSArray()
    var empInfoDataToShow =  NSArray()
    var selectedOptionIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hourDeatilView.isHidden = true
        self.employeeTableView.tableFooterView = UIView()
        self.employeeTableView.register(UINib.init(nibName: "EmployeeInfoViewCell", bundle: nil), forCellReuseIdentifier: "EmployeeInfoViewCell")
        self.hourDeatilTableView.register(UINib.init(nibName: "EmpHourDetailViewCell", bundle: nil), forCellReuseIdentifier: "EmpHourDetailViewCell")
        self.hourDeatilTableView.tableFooterView = UIView()
        self.hourDeatilTableView.tableFooterView = UIView()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        hourDeatilView.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.profile_pic()
        self.bannerImgView.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
        hideAllBottomViews()
        self.employeeListBottomLbl.isHidden = false
        self.employeeListBtn.setTitleColor(.black, for: .normal)
        
        getEmployeeData()
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.hourDeatilView.isHidden = true
    }
    func setHoursBreakUPViewHeight(){
        //(self.empHoursInfoData.count )
        self.hoursBreakUpViewHeight.constant = CGFloat((5 * 120 ) + 50)
        if self.hoursBreakUpViewHeight.constant > self.view.frame.height * 0.8{
            self.hoursBreakUpViewHeight.constant = self.view.frame.height * 0.8
        }
    }
    func hideAllBottomViews(){
        self.requestedBottomLbl.isHidden = true
        self.employeeListBottomLbl.isHidden = true
        self.archivedBottomLbl.isHidden = true
        self.archivedEmployeeBtn.setTitleColor(.lightGray, for: .normal)
        self.employeeListBtn.setTitleColor(.lightGray, for: .normal)
        self.requestedEmployeeBtn.setTitleColor(.lightGray, for: .normal)
    }
    @IBAction func employeeListBtnPressed(_ sender: Any) {
        hideAllBottomViews()
        self.employeeListBottomLbl.isHidden = false
        self.employeeListBtn.setTitleColor(.black, for: .normal)
        selectedOptionIndex = 0
        if empInfoData.count != 0{
            let resultPredicate = NSPredicate(format: "status contains[c] %@", "10")
            self.empInfoDataToShow = self.empInfoData.filtered(using: resultPredicate) as NSArray
        }else{
            self.getEmployeeData()
        }
        self.employeeTableView.reloadData()
    }
    @IBAction func requestedEmployeeBtnPressed(_ sender: Any) {
        hideAllBottomViews()
        self.requestedBottomLbl.isHidden = false
        self.requestedEmployeeBtn.setTitleColor(.black, for: .normal)
        selectedOptionIndex = 1
        if empInfoData.count != 0{
            let resultPredicate = NSPredicate(format: "status contains[c] %@", "20")
            self.empInfoDataToShow = self.empInfoData.filtered(using: resultPredicate) as NSArray
        }else{
            self.getEmployeeData()
        }
        self.employeeTableView.reloadData()
    }
    @IBAction func archivedEmployeeBtnPressed(_ sender: Any) {
        hideAllBottomViews()
        self.archivedBottomLbl.isHidden = false
        self.archivedEmployeeBtn.setTitleColor(.black, for: .normal)
        selectedOptionIndex = 2
        if empInfoData.count != 0{
            let resultPredicate = NSPredicate(format: "status contains[c] %@", "30")
            self.empInfoDataToShow = self.empInfoData.filtered(using: resultPredicate) as NSArray
        }else{
            self.getEmployeeData()
        }
        self.employeeTableView.reloadData()
    }
    @IBAction func notificationPressed(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    
    func profile_pic()  {
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let params = userIDData["user_id"] as! String
        let serivehandler = ServiceHandlers()
        serivehandler.editProfile(user_id: params){(responce,isSuccess) in
            if isSuccess{
                let data = responce as! Dictionary<String,Any>
                let string_url = data["user_profile_pic"] as! String
                if let url = URL(string: string_url){
                    
                    do {
                        DispatchQueue.global().async {
                            if let imageData = try? Data(contentsOf: url)  {
                                
                                self.saveImageInDocsDir(dataImage: imageData)
                                DispatchQueue.main.async {
                                    self.profilePicView.image = UIImage(data: imageData)
                                    self.profilePicView.layer.borderWidth = 1
                                    self.profilePicView.layer.masksToBounds = false
                                    self.profilePicView.layer.borderColor = APP_BLACK_COLOR.cgColor
                                    self.profilePicView.layer.cornerRadius = self.profilePicView.frame.height/2
                                    self.profilePicView.clipsToBounds = true
                                }
                            }//make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                            
                        }
                    }
                }
            }
        }
        
    }
    
    func saveImageInDocsDir(dataImage: Data ) {
        
        if (dataImage != nil) {
            // get the documents directory url
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            // choose a name for your image
            let fileName = "profilepic.jpg"
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            do {
                // writes the image data to disk
                try dataImage.write(to: fileURL, options: Data.WritingOptions.atomic)
                print("file saved")
                print(fileURL)
            } catch {
                print("error saving file:", error)
            }
            
        }
        
        
    }
    func getEmployeeData(){
        self.empInfoData = NSArray()
        self.empInfoDataToShow = NSArray()
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let org_id = userIDData["user_id"] as! String
        let param = ["user_id":org_id]
        
        let serviceHandler = ServiceHandlers()
        serviceHandler.getAllEmployeeData(params: param){(responce,isSuccess) in
            if isSuccess{
                self.hideAllBottomViews()
                self.employeeListBottomLbl.isHidden = false
                self.employeeListBtn.setTitleColor(.black, for: .normal)
                
                self.empInfoData = responce as! NSArray
                let resultPredicate = NSPredicate(format: "status contains[c] %@", "10")
                self.empInfoDataToShow = self.empInfoData.filtered(using: resultPredicate) as NSArray
                self.employeeTableView.delegate = self
                self.employeeTableView.dataSource = self
                self.employeeTableView.reloadData()
            }else{
                self.employeeTableView.reloadData()
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.hourDeatilTableView {
            return 5//self.empHoursInfoData.count
        }
        return self.empInfoDataToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.hourDeatilTableView{
            let cell:EmpHourDetailViewCell = self.hourDeatilTableView.dequeueReusableCell(withIdentifier: "EmpHourDetailViewCell") as! EmpHourDetailViewCell
//            let dic =  self.empHoursInfoData[indexPath.row] as! NSDictionary
//            cell.shiftName.text = dic["shift_name"] as? String
//            cell.eventName.text = dic["event_email"] as? String
//            cell.shiftDate.text = dic["shift_date"] as? String
//            cell.hoursLbl.text = dic["total_hours"] as? String
            return cell
        }
        let cell:EmployeeInfoViewCell = self.employeeTableView.dequeueReusableCell(withIdentifier: "EmployeeInfoViewCell") as! EmployeeInfoViewCell
        let dic =  self.empInfoDataToShow[indexPath.row] as! NSDictionary
        cell.empName.text = dic["user_name"] as? String
        cell.empEmail.text = dic["login_email"] as? String
        cell.empPhoneNum.text = self.formattedNumber(number:(dic["login_phone"] as? String)!)
        cell.empHours.text = dic["emp_total_hours"] as? String
        cell.empAddress.text = String(format: "%@ %@%@ %@", (dic["vol_address"] as? String)?.isEmpty == nil ? "" : dic["vol_address"] as! CVarArg,(dic["vol_city"] as? String)?.isEmpty == nil ? "" : String(format: "%@ ", dic["vol_city"] as! CVarArg),(dic["state_name"] as? String)?.isEmpty == nil ? "" : dic["state_name"] as! CVarArg, (dic["vol_zipcode"] as? String)?.isEmpty == nil ? "" : dic["vol_zipcode"] as! CVarArg)//String(format: "%@ %@\n%@ %@", (dic["vol_state"] as? String)?.isEmpty == nil ? "" : dic["vol_state"] as! CVarArg ,(dic["vol_city"] as? String)?.isEmpty == nil ? "" : dic["vol_city"] as! CVarArg ,(dic["state_code"] as? String)?.isEmpty == nil ? "" : dic["state_code"] as! CVarArg, (dic["vol_zipcode"] as? String)?.isEmpty == nil ? "" : dic["vol_zipcode"] as! CVarArg)
        cell.empAddress.text = (cell.empAddress.text == " \n " || cell.empAddress.text == " \n") ? "N/A" : cell.empAddress.text
        if cell.empHours.text != "0"{
            cell.empHourBtn.isUserInteractionEnabled = true
        }else{
            cell.empHourBtn.isUserInteractionEnabled = false
        }
        if dic["status"] as? String == "10"{
            cell.acceptBtn.isHidden = false
            cell.rejectButton.isHidden = false
            cell.acceptBtn.setTitle("Chat", for: .normal)
            cell.rejectButton.setTitle("Reject", for: .normal)
        }else  if dic["status"] as? String == "20"{
            cell.acceptBtn.isHidden = false
            cell.rejectButton.isHidden = false
            cell.acceptBtn.setTitle("Accept", for: .normal)
            cell.rejectButton.setTitle("Reject", for: .normal)
        }else{
            cell.acceptBtn.isHidden = false
            cell.acceptBtn.setTitle("Accept", for: .normal)
            cell.rejectButton.isHidden = true
        }
        cell.empHourBtn.tag = indexPath.row
        cell.acceptBtn.tag = indexPath.row
        cell.rejectButton.tag = indexPath.row
        cell.empHourBtn.addTarget(self, action: #selector(empHourBtnClicked(sender:)), for: .touchUpInside)
        cell.acceptBtn.addTarget(self, action: #selector(acceptBtnClicked(sender:)), for: .touchUpInside)
        cell.rejectButton.addTarget(self, action: #selector(rejectBtnClicked(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.hourDeatilTableView{
            return 120.0
        }
        return 215.0
    }
    @objc func empHourBtnClicked(sender:UIButton)
    {
        let dic =  self.empInfoDataToShow[sender.tag] as! NSDictionary
        let empId = dic["emp_id"] as? String
        self.getEmployeeHoursBreakUpData(employeeId : empId!)
    }
    @objc func acceptBtnClicked(sender:UIButton)
    {
        let dic =  self.empInfoDataToShow[sender.tag] as! NSDictionary
        let status = dic["status"] as? String
        if status == "10"{
            let userDic = NSMutableDictionary()
            userDic["user_email"] = dic["login_email"] as? String
            userDic["user_name"] = dic["user_name"] as? String
            self.createChatChannelReqWith(userDic : userDic)
        }else{
        self.aceeptOrRejectReqWith(status : "10", rowId : (dic["id"] as? String)!)
        }
    }
    @objc func rejectBtnClicked(sender:UIButton)
    {
        let dic =  self.empInfoDataToShow[sender.tag] as! NSDictionary
//        let status = dic["status"] as? String
        self.aceeptOrRejectReqWith(status : "30", rowId : (dic["id"] as? String)!)
    }
    
    func createChatChannelReqWith(userDic : NSMutableDictionary){
        
        let userEmail = userDic["user_email"] as! String
        let userFullName = userDic["user_name"] as! String
        
        ActivityLoaderView.startAnimating()
        SBDMain.connect(withUserId: userEmail) { (user, error) in
            guard error == nil else {   // Error.
                return
                    ActivityLoaderView.stopAnimating()
            }
            ActivityLoaderView.stopAnimating()
            SBDGroupChannel.createChannel(withName: userFullName, isDistinct: true, userIds: [ userEmail ], coverUrl: nil, data: userEmail, customType: nil, completionHandler: { (groupChannel, error) in
                guard error == nil else {   // Error.
                    return
                }
                let vc = GroupChannelChatViewController.init(nibName: "GroupChannelChatViewController", bundle: nil)
                vc.channel = groupChannel
                self.navigationController?.pushViewController(vc, animated: true)
                
            })
        }
        
    }
    func aceeptOrRejectReqWith(status : String, rowId : String){
        let param = ["status":status, "row_id":rowId]
        let serviceHandler = ServiceHandlers()
        serviceHandler.changeEmployeeRequestStatus(params: param){(responce,isSuccess) in
            if isSuccess{
                self.getEmployeeData()
            }else{
               
            }
        }
    }
    func getEmployeeHoursBreakUpData(employeeId : String){
        
        self.empInfoData = NSArray()
        let param = ["user_id":employeeId]
        
        let serviceHandler = ServiceHandlers()
        serviceHandler.getEmployeeHoursData(params: param){(responce,isSuccess) in
            if isSuccess{
                self.empHoursInfoData = responce as! NSArray
                self.setHoursBreakUPViewHeight()
//                if self.empHoursInfoData.count != 0{
                    self.hourDeatilView.isHidden = false
                    self.hourDeatilTableView.delegate = self
                    self.hourDeatilTableView.dataSource = self
                    self.hourDeatilTableView.reloadData()
//                }else{
//                    self.hourDeatilView.isHidden = true
//                    self.hourDeatilTableView.reloadData()
//                }
            }else{
                self.setHoursBreakUPViewHeight()
                self.hourDeatilView.isHidden = false
                self.hourDeatilTableView.delegate = self
                self.hourDeatilTableView.dataSource = self
                self.hourDeatilTableView.reloadData()
            }
        }
    }
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XXX)XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
}
