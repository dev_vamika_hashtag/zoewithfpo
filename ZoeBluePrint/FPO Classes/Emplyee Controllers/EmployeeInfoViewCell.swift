//
//  EmployeeInfoViewCell.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 05/05/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit

class EmployeeInfoViewCell: UITableViewCell {
    @IBOutlet var acceptBtn: UIButton!
    @IBOutlet var rejectButton: UIButton!
    @IBOutlet weak var empName: UILabel!
    @IBOutlet weak var empEmail: UILabel!
    @IBOutlet weak var empPhoneNum: UILabel!
    @IBOutlet weak var empAddress: UILabel!
    @IBOutlet weak var empHours: UILabel!
    @IBOutlet weak var empHourBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
