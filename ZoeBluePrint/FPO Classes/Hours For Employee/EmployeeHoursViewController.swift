//
//  EmployeeHoursViewController.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 10/05/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit

class EmployeeHoursViewController: UIViewController {
    @IBOutlet weak var profilePicView: UIImageView!
    @IBOutlet weak var bannerImgView: UIImageView!
    @IBOutlet weak var requireHours: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.requireHours.setLeftPaddingPoints(5)
        self.getHoursData()
        self.profile_pic()
        self.bannerImgView.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
    }

    @IBAction func notificationPressed(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    @IBAction func savePressed(_ sender: Any) {
        if self.requireHours.text == ""{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please Enter Hours!", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.saveHoursData()
        }
    }
    func getHoursData(){
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userId = userIDData["user_id"] as! String
        let param = ["user_id":userId]
        
        let serviceHandler = ServiceHandlers()
        serviceHandler.getHoursForEmployee(params: param){(responce,isSuccess) in
            if isSuccess{
                let referralData = responce as! NSDictionary
                self.requireHours.text = referralData["required_hrs"] as? String
            }else{
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    
    func saveHoursData(){
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userId = userIDData["user_id"] as! String
        let param = ["user_id":userId,"employee_required_hrs":self.requireHours.text!]
        
        let serviceHandler = ServiceHandlers()
        serviceHandler.saveHoursForEmployee(params: param){(responce,isSuccess) in
            if isSuccess{
                let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Hours Updated Successfully!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { (action) in
//                    self.getHoursData()
                }))
                self.present(alert, animated: true, completion: nil)
                
            }else{
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    func profile_pic()  {
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let params = userIDData["user_id"] as! String
        let serivehandler = ServiceHandlers()
        serivehandler.editProfile(user_id: params){(responce,isSuccess) in
            if isSuccess{
                let data = responce as! Dictionary<String,Any>
                let string_url = data["user_profile_pic"] as! String
                if let url = URL(string: string_url){
                    
                    do {
                        DispatchQueue.global().async {
                            if let imageData = try? Data(contentsOf: url)  {
                                
                                self.saveImageInDocsDir(dataImage: imageData)
                                DispatchQueue.main.async {
                                    self.profilePicView.image = UIImage(data: imageData)
                                    self.profilePicView.layer.borderWidth = 1
                                    self.profilePicView.layer.masksToBounds = false
                                    self.profilePicView.layer.borderColor = APP_BLACK_COLOR.cgColor
                                    self.profilePicView.layer.cornerRadius = self.profilePicView.frame.height/2
                                    self.profilePicView.clipsToBounds = true
                                }
                            }//make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                            
                        }
                    }
                }
            }
        }
        
    }
    
    func saveImageInDocsDir(dataImage: Data ) {
        
        if (dataImage != nil) {
            // get the documents directory url
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            // choose a name for your image
            let fileName = "profilepic.jpg"
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            do {
                // writes the image data to disk
                try dataImage.write(to: fileURL, options: Data.WritingOptions.atomic)
                print("file saved")
                print(fileURL)
            } catch {
                print("error saving file:", error)
            }
            
        }
        
        
    }
}
