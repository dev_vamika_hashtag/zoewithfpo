//
//  BaseViewController.swift
//  ZoeBlue//print
//
//  Created by Reetesh Bajpai on 05/06/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Global.setUpViewWithTheme(ViewController: self)
    }


    func setNavigationBarHidden(toHide:Bool)  {
        self.navigationController?.navigationBar.isHidden = toHide
    }
}
