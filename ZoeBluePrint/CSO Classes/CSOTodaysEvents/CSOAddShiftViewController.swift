//
//  CSOAddShiftViewController.swift
//  ZoeBlue//print
//
//  Created by Rishi Chaurasia on 17/08/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit
import SendBirdSDK

class CSOAddShiftViewController: UIViewController,UITextFieldDelegate{
    
    @IBOutlet var weekHeight: NSLayoutConstraint!
    @IBOutlet var dateWidth: NSLayoutConstraint!
    @IBOutlet weak var lblMinimumRating: UILabel!
    @IBOutlet weak var lblShiftTask: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var HeadingLabel: UILabel!
    @IBOutlet weak var imgViewCsoCover: UIImageView!
    @IBOutlet weak var endDateCalendarView: UIImageView!
    @IBOutlet weak var lblShiftDate: UILabel!
    @IBOutlet weak var RankShiftSelectedLists: UIButton!
    
    @IBOutlet var hoursShiftBtn: UIButton!
    @IBOutlet weak var imageShiftDate: UIImageView!
    
    @IBOutlet var taskName: UITextField!
    
    
    
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var lblVolunteerRequired: UILabel!
    
    @IBOutlet weak var lblStartTime: UILabel!
    
    @IBOutlet weak var btnResetPressed: UIButton!
    
    @IBOutlet weak var shiftDateButton: UIButton!
    @IBOutlet weak var txtfldVolReq: UITextField!
    @IBOutlet weak var endDateButton: UIButton!
    @IBOutlet weak var floatingRatingView: FloatRatingView!
    @IBOutlet weak var startDateButton: UIButton!
    
    @IBOutlet weak var shiftTaskButton: UIButton!
    
    @IBOutlet weak var sundayButton: UIButton!
    @IBOutlet weak var mondayButton: UIButton!
    @IBOutlet weak var tuesdayButton: UIButton!
    @IBOutlet weak var wednesdayButton: UIButton!
    @IBOutlet weak var thrusdayButton: UIButton!
    @IBOutlet weak var fridayButton: UIButton!
    @IBOutlet weak var saturdayButton: UIButton!
    
    @IBOutlet weak var sundayView: UIView!
    @IBOutlet weak var mondayView: UIView!
    @IBOutlet weak var tuesdayView: UIView!
    @IBOutlet weak var wednesdayView: UIView!
    @IBOutlet weak var thrusdayView: UIView!
    @IBOutlet weak var fridayView: UIView!
    @IBOutlet weak var saturdayView: UIView!
    
    
    @IBOutlet var shiftEndDate: UIButton!
    
    @IBOutlet weak var addShiftButton: UIButton!
    
    @IBOutlet var endDateLbl: UILabel!
    var selectedDaysDatesArr = [String]()
    var daysNameArray = NSMutableDictionary()
    var shiftList = [[String:Any]]()
    var eventId = String ()
    var eventDetail = [String : Any]()
    var selectedShiftTaskId = String ()
    var selectedShiftRankId = String ()
    var sDate = String ()
    var endDate = String ()
    var sVolReq = String ()
    var shiftRank = String ()
    var shiftStartTime = String ()
    var shiftEndTime = String ()
    var data_for_update:Dictionary<String,Any>?
    var screen:String?
    var shiftName: String?
    private var groupChannelListQuery: SBDGroupChannelListQuery?
    
    let RankList = [["rank":"0",
                     "shift_rank":"0"],["rank":"1",
                                        "shift_rank":"1"],["rank":"2",
                                                           "shift_rank":"2"],["rank":"3",
                                                                              "shift_rank":"3"],["rank":"4",
                                                                                                 "shift_rank":"4"],["rank":"5",
                                                                                                                    "shift_rank":"5"]]
    
    
    @IBAction func onSelectDays(_ sender: UIButton) {
        self.txtfldVolReq.resignFirstResponder()
        if sender.isSelected{
            sender.isSelected = false
        }else{
            sender.isSelected = true
        }
        switch sender.tag {
        case 1:
            print("SUN")
            self.createFinalDateArray(day: "sunday")
            break
        case 2:
            print("MON")
            self.createFinalDateArray(day: "monday")
            break
        case 3:
            print("TUES")
            self.createFinalDateArray(day: "tuesday")
            break
        case 4:
            print("WED")
            self.createFinalDateArray(day: "wednesday")
            break
        case 5:
            print("THRUS")
            self.createFinalDateArray(day: "thrusday")
            break
        case 6:
            print("FRI")
            self.createFinalDateArray(day: "friday")
            break
        default:
            print("SAT")
            self.createFinalDateArray(day: "saturday")
        }
    }
    func addDataToView(detail : [String : Any]){
        self.eventDetail = detail
    }
    func createFinalDateArray(day: String){
        for i in (0..<daysNameArray.count)
        {
            let data = daysNameArray[i] as? NSDictionary
            if data!["day_name"] as! String == day{
                let dateStringSape = (data!["date"] as! String).components(separatedBy: " ").first
                let fomattedStr = changeShiftDateFormate(date : dateStringSape!)
                if selectedDaysDatesArr.contains(fomattedStr) {
                    if selectedDaysDatesArr.count > 1{
                    selectedDaysDatesArr.remove(at: selectedDaysDatesArr.firstIndex(of: dateStringSape!)!)
                    }else{
                        selectedDaysDatesArr.removeAll()
                    }
                }else{
                    selectedDaysDatesArr.append(fomattedStr)
                }
            }
        }
        //.componentsSeparatedByString(" ")
        print("===================\(selectedDaysDatesArr)================")
    }
    func changeShiftDateFormate(date : String)-> String{
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd" //2021-03-21
        let showDate = inputFormatter.date(from: date)
        inputFormatter.dateFormat = "MM-dd-yyyy" //03-07-2021
        let resultString = inputFormatter.string(from: showDate!)
        print(resultString)
        return resultString
    }
    @IBOutlet weak var backButton: UIButton!
    
    @IBAction func onClickHoursShiftBtn(_ sender: UIButton) {
        self.txtfldVolReq.resignFirstResponder()
        if sender.isSelected{
            sender.isSelected = false
            self.startDateButton.isEnabled = true
            self.startDateButton.alpha = 1.0
            self.startDateButton.setTitle("Start Time", for: .normal)
            self.startDateButton.setTitleColor(.lightGray, for: .normal)
            self.shiftStartTime = ""
            
            self.endDateButton.isEnabled = true
            self.endDateButton.alpha = 1.0
            self.endDateButton.setTitle("End Time", for: .normal)
            self.endDateButton.setTitleColor(.lightGray, for: .normal)
            self.shiftEndTime = ""
        }else{
            sender.isSelected = true
            self.startDateButton.isEnabled = false
            self.startDateButton.alpha = 0.5
            self.startDateButton.setTitle("12:00 AM", for: .normal)
            self.startDateButton.setTitleColor(APP_BLACK_COLOR, for: .normal)
            self.shiftStartTime = "12:00 AM"
            
            self.endDateButton.isEnabled = false
            self.endDateButton.alpha = 0.5
            self.endDateButton.setTitle("11:59 PM", for: .normal)
            self.endDateButton.setTitleColor(APP_BLACK_COLOR, for: .normal)
            self.shiftEndTime = "11:59 PM"
        }
    }
    
    @IBAction func back_Button(_ sender: Any) {
        performSegueToReturnBack()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        taskName.delegate = self
        txtfldVolReq.delegate = self
        txtfldVolReq.keyboardType = UIKeyboardType.numberPad
        taskName.isHidden = true
        
        let keyboard = UIToolbar()
        keyboard.sizeToFit()
        
        let DoneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: .done, target: self, action: #selector(doneClick))
        keyboard.items = [DoneButton]
        txtfldVolReq.inputAccessoryView = keyboard
        
        
        
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.getShiftList() { (responce, isSuccess) in
            if isSuccess {
                self.shiftList = responce as! [[String : Any]]
                // //print(self.shiftList)
            }
        }
        
        
        
        configureRatingView()
        
    }
    @IBOutlet weak var profilePicture: UIImageView!
    
    @IBAction func notificationButton(_ sender: Any) {
        
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        taskName.setUnderLineOfColor(color: APP_BLACK_COLOR)
        Global.setUpViewWithTheme(ViewController: self)
        disableDaysButtons()
        self.taskName.backgroundColor = APP_WHITE_COLOR
        self.imgViewCsoCover.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
        self.profile_pic()
        self.backButton.isHidden = true
        weekHeight.constant = 0
        if(self.screen == "EDIT SCREEN") || (self.screen == "DUPLICATE SCREEN"){
            //self.backButton.isHidden = false
            
            self.endDateCalendarView.isHidden = true
            self.shiftEndDate.isHidden = true
            self.endDateLbl.isHidden = true
            self.dateWidth.constant = self.view.frame.width * 0.52
            self.HeadingLabel.text = NSLocalizedString("UPDATE SHIFT", comment: "")
            
            self.profile_pic()
            //print("hello sir")
            // //print(self.data_for_update as Any)
            self.shiftDateButton.setTitle(self.data_for_update!["shift_date"] as! String, for: .normal)
            self.sDate = self.data_for_update!["shift_date"] as! String
            self.txtfldVolReq.text = self.data_for_update!["shift_vol_req"] as! String
            self.startDateButton.setTitle(self.data_for_update!["shift_start_time"] as! String, for: .normal)
            self.shiftStartTime = self.data_for_update!["shift_start_time"] as! String
            self.endDateButton.setTitle(self.data_for_update!["shift_end_time"] as! String, for: .normal)
            self.shiftEndTime = self.data_for_update!["shift_end_time"] as! String
            
            self.shiftTaskButton.setTitle(self.data_for_update!["shift_task_name"] as! String, for: .normal)
            self.selectedShiftTaskId = self.data_for_update!["shift_task"] as! String
            
            self.selectedShiftRankId = self.data_for_update!["shift_rank"] as! String // "rank_value" 13 Jan Reetesh
            self.RankShiftSelectedLists.setTitle(self.data_for_update!["shift_rank"] as! String ,for: .normal)
            
            
            self.addShiftButton.setTitle(NSLocalizedString("Update", comment: ""), for: .normal)
            if (self.screen == "DUPLICATE SCREEN"){
//                taskName.isHidden = false
//                taskName.text = self.data_for_update!["shift_task_name"] as? String
                self.addShiftButton.setTitle(NSLocalizedString("Duplicate", comment: ""), for: .normal)
            }else{
//                taskName.isHidden = true
            }
            
        }else{
            self.startDateButton.setTitle("Start Time", for: .normal)
            self.startDateButton.setTitleColor(.lightGray, for: .normal)
            
            self.endDateButton.setTitle("End Time", for: .normal)
            self.endDateButton.setTitleColor(.lightGray, for: .normal)
            
            self.shiftDateButton.setTitle("Start Date", for: .normal)
            self.shiftDateButton.setTitleColor(.lightGray, for: .normal)
            
            self.shiftEndDate.setTitle("End Date", for: .normal)
            self.shiftEndDate.setTitleColor(.lightGray, for: .normal)
            
            btnResetPressed.setTitle("Cancel", for: .normal)
        }
    }
    
    
    @IBAction func btnReset(_ sender: Any) {
        self.txtfldVolReq.resignFirstResponder()
        self.disableDaysButtons()
        hoursShiftBtn.isSelected = false
        
        self.startDateButton.setTitle("Start Time", for: .normal)
        self.startDateButton.setTitleColor(.lightGray, for: .normal)
        
        self.endDateButton.setTitle("End Time", for: .normal)
        self.endDateButton.setTitleColor(.lightGray, for: .normal)
        
        self.shiftDateButton.setTitle("Start Date", for: .normal)
        self.shiftDateButton.setTitleColor(.lightGray, for: .normal)
        
        self.shiftEndDate.setTitle("End Date", for: .normal)
        self.shiftEndDate.setTitleColor(.lightGray, for: .normal)
        
        self.shiftTaskButton.setTitle("Select Shift Task", for: .normal)
        self.RankShiftSelectedLists.setTitle("Select Minimum Rating", for: .normal)
        self.txtfldVolReq.text = ""
        
        selectedDaysDatesArr = [String]()
        sDate = String ()
        endDate = String ()
        sVolReq = String ()
        shiftRank = String ()
        shiftStartTime = String ()
        shiftEndTime = String ()
    }
    
    
    
    
    @objc func doneClick() {
        
        self.view.endEditing(true)
        
    }
    @IBAction func sideMenu(_ sender: Any) {
    }
    func addViewController(viewController:UIViewController)  {
        viewController.willMove(toParent: self)
        self.view.addSubview(viewController.view)
        self.addChild(viewController)
        viewController.didMove(toParent: self)
    }
    
    func configureRatingView()  {
        
        self.selectedShiftTaskId = ""
        self.selectedShiftRankId = ""
    }
    
    
    @IBAction func onSelectShiftEndDate(_ sender: UIButton) {
        self.txtfldVolReq.resignFirstResponder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        //print(eventDetail["event_register_start_date"]!)
        // //print(eventDetail["event_register_end_date"]!)
        let startDate = dateFormatter.date(from: eventDetail["event_register_start_date"]! as! String )
        //  //print(startDate)
        let endDate = dateFormatter.date(from: eventDetail["event_register_end_date"]! as! String )
        //  //print(endDate)
        
        let dateSelectionPicker = DateSelectionViewController(startDate: startDate!, endDate:  endDate!)
        dateSelectionPicker.view.frame = self.view.frame
        dateSelectionPicker.view.layoutIfNeeded()
        dateSelectionPicker.captureSelectDateValue(sender, inMode: .date) { (selectedDate) in
            let formatter = DateFormatter()
            // formatter.dateFormat = "dd-MMM-yyyy"
            formatter.dateFormat = "MM-dd-yyyy"
            let dateString = formatter.string(from: selectedDate)
            self.endDate = dateString
            (sender as AnyObject).setTitle(dateString, for:.normal)
            (sender as AnyObject).setImage(nil, for: .normal)
            (sender as AnyObject).setTitleColor(APP_BLACK_COLOR, for: .normal)
            if self.sDate != "" && self.endDate != ""{
                self.setupUIforDaysSelection()
            }
        }
        addViewController(viewController: dateSelectionPicker)
        
    }
    @IBAction func shiftDateButton(_ sender: Any) {
        self.txtfldVolReq.resignFirstResponder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        //print(eventDetail["event_register_start_date"]!)
        // //print(eventDetail["event_register_end_date"]!)
        
        let startDate = dateFormatter.date(from: eventDetail["event_register_start_date"]! as! String )
        //  //print(startDate)
        let endDate = dateFormatter.date(from: eventDetail["event_register_end_date"]! as! String )
        //  //print(endDate)
        
        let dateSelectionPicker = DateSelectionViewController(startDate: startDate!, endDate:  endDate!)
        dateSelectionPicker.view.frame = self.view.frame
        dateSelectionPicker.view.layoutIfNeeded()
        dateSelectionPicker.captureSelectDateValue(sender, inMode: .date) { (selectedDate) in
            let formatter = DateFormatter()
            // formatter.dateFormat = "dd-MMM-yyyy"
            formatter.dateFormat = "MM-dd-yyyy"
            //08-22-2019
            let dateString = formatter.string(from: selectedDate)
            self.sDate = dateString
            (sender as AnyObject).setTitle(dateString, for:.normal)
            (sender as AnyObject).setImage(nil, for: .normal)
            (sender as AnyObject).setTitleColor(APP_BLACK_COLOR, for: .normal)
            if self.endDate != "" && self.sDate != ""{
                self.setupUIforDaysSelection()
            }
        }
        addViewController(viewController: dateSelectionPicker)
        
    }
    func disableDaysButtons(){
        self.sundayButton.isSelected = false
        self.sundayButton.isEnabled = false
        self.sundayButton.alpha = 0.5
        
        self.mondayButton.isSelected = false
        self.mondayButton.isEnabled = false
        self.mondayButton.alpha = 0.5
        
        self.tuesdayButton.isSelected = false
        self.tuesdayButton.isEnabled = false
        self.tuesdayButton.alpha = 0.5
        
        self.wednesdayButton.isSelected = false
        self.wednesdayButton.isEnabled = false
        self.wednesdayButton.alpha = 0.5
        
        self.thrusdayButton.isSelected = false
        self.thrusdayButton.isEnabled = false
        self.thrusdayButton.alpha = 0.5
        
        self.fridayButton.isSelected = false
        self.fridayButton.isEnabled = false
        self.fridayButton.alpha = 0.5
        
        self.saturdayButton.isSelected = false
        self.saturdayButton.isEnabled = false
        self.saturdayButton.alpha = 0.5
        hideDaysViews()
    }
    func hideDaysViews(){
        
        self.mondayView.isHidden = true
        self.tuesdayView.isHidden = true
        self.sundayView.isHidden = true
        self.wednesdayView.isHidden = true
        self.thrusdayView.isHidden = true
        self.fridayView.isHidden = true
        self.saturdayView.isHidden = true
        
        weekHeight.constant = 0
        
    }
    func setupUIforDaysSelection(){
        let dates = Global.getDatesBetweenDates(startDate: self.sDate, endDate: self.endDate)
        daysNameArray = NSMutableDictionary()
        var dic = NSMutableDictionary()
        self.hideDaysViews()
        weekHeight.constant = 30
        for date in dates{
            let count = Global.getWeekDays(startDate: date)
            if count == 7 {
                dic = NSMutableDictionary()
                dic["day_name"] = "saturday"
                dic["date"] = "\(date)"
                daysNameArray[daysNameArray.count] = dic
                //                daysNameArray.append("saturday")
                self.saturdayView.isHidden = false
                self.saturdayButton.isEnabled = true
                self.saturdayButton.alpha = 1.0
            }else if count == 1{
                dic = NSMutableDictionary()
                dic["day_name"] = "sunday"
                dic["date"] = "\(date)"
                daysNameArray[daysNameArray.count] = dic
                //                daysNameArray.append("sunday")
                self.sundayView.isHidden = false
                self.sundayButton.isEnabled = true
                self.sundayButton.alpha = 1.0
            }else if count == 2{
                dic = NSMutableDictionary()
                dic["day_name"] = "monday"
                dic["date"] = "\(date)"
                daysNameArray[daysNameArray.count] = dic
                //                daysNameArray.append("monday")
                self.mondayView.isHidden = false
                self.mondayButton.isEnabled = true
                self.mondayButton.alpha = 1.0
            }else if count == 3{
                dic = NSMutableDictionary()
                dic["day_name"] = "tuesday"
                dic["date"] = "\(date)"
                daysNameArray[daysNameArray.count] = dic
                //                daysNameArray.append("tuesday")
                self.tuesdayView.isHidden = false
                self.tuesdayButton.isEnabled = true
                self.tuesdayButton.alpha = 1.0
            }else if count == 4{
                dic = NSMutableDictionary()
                dic["day_name"] = "wednesday"
                dic["date"] = "\(date)"
                daysNameArray[daysNameArray.count] = dic
                //                daysNameArray.append("wednesday")
                self.wednesdayView.isHidden = false
                self.wednesdayButton.isEnabled = true
                self.wednesdayButton.alpha = 1.0
            }else if count == 5{
                dic = NSMutableDictionary()
                dic["day_name"] = "thrusday"
                dic["date"] = "\(date)"
                daysNameArray[daysNameArray.count] = dic
                //                daysNameArray.append("thrusday")
                self.thrusdayView.isHidden = false
                self.thrusdayButton.isEnabled = true
                self.thrusdayButton.alpha = 1.0
            }else if count == 6{
                dic = NSMutableDictionary()
                dic["day_name"] = "friday"
                dic["date"] = "\(date)"
                daysNameArray[daysNameArray.count] = dic
                //                daysNameArray.append("friday")
                self.fridayView.isHidden = false
                self.fridayButton.isEnabled = true
                self.fridayButton.alpha = 1.0
            }
        }
        print("\(daysNameArray)")
        
        
    }
    @IBAction func addShift(_ sender: Any) {
        self.txtfldVolReq.resignFirstResponder()
        if(validate())
        {
            let strShiftStartTime = self.shiftStartTime
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            let datestart = dateFormatter.date(from: strShiftStartTime)
            dateFormatter.dateFormat = "HH:mm"
            let date24StartTime = dateFormatter.string(from: datestart!)
            let strShiftEndTime = self.shiftEndTime
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "h:mm a"
            let dateend = dateFormatter2.date(from: strShiftEndTime)
            dateFormatter2.dateFormat = "HH:mm"
            let date24EndTime = dateFormatter2.string(from: dateend!)
            let dateArrString = self.selectedDaysDatesArr.joined(separator: ",")
            if (self.screen == "EDIT SCREEN")
            {
                var dict = ["shift_id" : self.data_for_update!["shift_id"] as! String,
                            "shift_date" : self.sDate,
                            "shift_vol_req" : self.txtfldVolReq.text!,
                            "shift_start_time" : date24StartTime ,
                            "shift_end_time" : date24EndTime ,
                            "shift_rank" : self.selectedShiftRankId,
                            "shift_task" : self.selectedShiftTaskId
                ]
                let serviceHanlder = ServiceHandlers()
                serviceHanlder.updateShift(data_details: dict) { (responce, isSuccess) in
                    if isSuccess {
                        let alert = UIAlertController(title: "Alert!", message: "Update successful", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert , animated: true)
                    }
                }
                let objToBeSent = "Test Message from Notification"
                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: objToBeSent);
                performSegueToReturnBack()
            }
            if (self.screen == "DUPLICATE SCREEN"){
                let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
                let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
                let userEmail = userIDData["user_email"] as! String
                let dict = ["shift_id" : self.data_for_update!["shift_id"] as! String,
                            "shift_date" : self.sDate,
                            "shift_vol_req" : self.txtfldVolReq.text!,
                            "shift_start_time" : date24StartTime ,
                            "shift_end_time" : date24EndTime ,
                            "shift_rank" : self.selectedShiftRankId,
                            "shift_task" : self.selectedShiftTaskId,
                            "cso_id" : userIDData["user_id"] as! String,
                            "user_id" : userIDData["user_id"] as! String,
                            "user_device" : UIDevice.current.identifierForVendor!.uuidString,
                            "user_type" :  userIDData["user_type"] as! String,
                            "event_id" :  eventDetail["event_id"]! as! String
                            
                ] as [String : Any]
                let serviceHanlder = ServiceHandlers()
                serviceHanlder.duplicateShift(data_details: dict) { (responce, isSuccess) in
                    if isSuccess {
                        let addShiftResponce = responce as? [String: Any]
                        if let shiftDetail = addShiftResponce?["res_data"] as? [String: Any] {
                            if let shiftId = shiftDetail["event_shift_id"]{
                                self.createChannel(email: userEmail, shiftId: shiftId as! String)
                            }
                        }
                        let alert = UIAlertController(title: "Success!", message: "Shift created successfully", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (success) in
                            let objToBeSent = "Test Message from Notification"
                            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: objToBeSent);
                            self.performSegueToReturnBack()
                        }))
                        self.present(alert , animated: true)
                    }else{
                        let alert = UIAlertController(title: "Alert!", message: "Something went wrong!", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (success) in
                        }))
                        self.present(alert , animated: true)
                    }
                }
                
                
                
//                performSegueToReturnBack()
//                self.navigationController?.popViewController(animated: false)
            }
            
            else{
                if self.daysNameArray.count > 1 && selectedDaysDatesArr.count > 0{
                    let serviceHanlder = ServiceHandlers()
                    serviceHanlder.addShiftwithMultipleDates(event_id: eventId, shift_date: dateArrString, shift_vol_req: self.txtfldVolReq.text!, shift_start_time: date24StartTime, shift_end_time: date24EndTime, shift_rank: self.selectedShiftRankId, shift_task:  self.selectedShiftTaskId) { (responce, isSuccess) in
                        if isSuccess {
                            let addShiftResponce = responce as? [String: Any]
                            // //print(addShiftResponce)
                            let message = addShiftResponce!["res_status"] as! String
                            // //print(message)
                            if(message == "200"){
                                let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
                                let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
                                let userEmail = userIDData["user_email"] as! String
                                ActivityLoaderView.startAnimating()
                                
                                if let shiftDetail = addShiftResponce?["res_data"] as? [String: Any] {
                                    if let shiftId = shiftDetail["event_shift_id"]{
                                        self.createChannel(email: userEmail, shiftId: shiftId as! String)
                                    }
                                }
                                
                                let alert = UIAlertController(title: NSLocalizedString("Success!", comment: ""), message: NSLocalizedString("Shift added succesfully!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { (action) in
                                    self.navigationController?.popViewController(animated: false)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                }else{
                    let serviceHanlder = ServiceHandlers()
                    serviceHanlder.addShift(event_id: eventId, shift_date: self.sDate, shift_vol_req: self.txtfldVolReq.text!, shift_start_time: date24StartTime, shift_end_time: date24EndTime, shift_rank: self.selectedShiftRankId, shift_task:  self.selectedShiftTaskId) { (responce, isSuccess) in
                        if isSuccess {
                            let addShiftResponce = responce as? [String: Any]
                            // //print(addShiftResponce)
                            let message = addShiftResponce!["res_status"] as! String
                            // //print(message)
                            if(message == "200"){
                                let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
                                let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
                                let userEmail = userIDData["user_email"] as! String
                                ActivityLoaderView.startAnimating()
                                
                                if let shiftDetail = addShiftResponce?["res_data"] as? [String: Any] {
                                    if let shiftId = shiftDetail["event_shift_id"]{
                                        self.createChannel(email: userEmail, shiftId: shiftId as! String)
                                    }
                                }
                                
                                let alert = UIAlertController(title: NSLocalizedString("Success!", comment: ""), message: NSLocalizedString("Shift added succesfully!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { (action) in
                                    self.navigationController?.popViewController(animated: false)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                }
                
               
            }
        }
    }
    
    func createChannel(email: String, shiftId: String) {
        ActivityLoaderView.startAnimating()
        let eventName = "\(eventDetail["event_heading"]! as! String) (\(shiftName ?? ""))"
        SBDMain.connect(withUserId: email) { [self] (user, error) in
            guard error == nil else {   // Error.
                print("USER NOT CONNECTED")
                ActivityLoaderView.stopAnimating()
                return
            }
            self.groupChannelListQuery = SBDGroupChannel.createMyGroupChannelListQuery()
            self.groupChannelListQuery?.limit = 100
            self.groupChannelListQuery?.includeEmptyChannel = true
            self.groupChannelListQuery?.channelNameFilter = eventName
            if self.groupChannelListQuery?.hasNext == false {
                print("GROUP CHANNEL LIST QUERY NOT CREATED")
                ActivityLoaderView.stopAnimating()
                return
            }
            self.groupChannelListQuery?.loadNextPage(completionHandler: { (channels, error) in
                if error != nil {
                    print ("error")
                    ActivityLoaderView.stopAnimating()
                    print("CHANNELS NOT FOUND")
                    return
                }
                if channels?.count == 0{
                    var strCoverUrl = ""
                    if let url = eventDetail["event_image"] as? String{
                        strCoverUrl = url
                    }else{
                        strCoverUrl = "https://zbp.progocrm.com/uploads/events/"
                    }
                    // let eventName = "\(eventDetail["event_heading"]! as! String) (\(shiftName ?? ""))"
                    SBDGroupChannel.createChannel(withName: eventName, isDistinct: false, userIds: [ email ], coverUrl: strCoverUrl , data: email, customType: "Channel", completionHandler: { (groupChannel, error) in
                        guard error == nil else {   // Error.
                            ActivityLoaderView.stopAnimating()
                            print("CHANNEL NOT CREATED")
                            return
                        }
                        print("CHANNEL CREATED")
                        self.hitAPIToSyncChannelToServerForAddedShift(shiftID: shiftId, channel: groupChannel!)
                    })
                    
                }else{
                    print("CHANNEL with SAME NAME FOUND")
                }
            })
        }
        
    }
    
    func hitAPIToSyncChannelToServerForAddedShift(shiftID: String,channel: SBDGroupChannel){
        
        let servicehandler = ServiceHandlers()
        servicehandler.syncChannelToServerForShift(shift_id: shiftID, shift_channel_url: channel.channelUrl){(responce,isSuccess) in
            if isSuccess{
                let resData = responce as! String
                print(resData)
                print("server updated for add shift and Sendbird URL")
                ActivityLoaderView.stopAnimating()
            }else{
                print("error Occured for add shft sendbird update to server")
                ActivityLoaderView.stopAnimating()
            }
        }
    }
    
    func profile_pic()  {
        
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profilePicture.image = image
                self.profilePicture.layer.borderWidth = 1
                self.profilePicture.layer.masksToBounds = false
                self.profilePicture.layer.borderColor = APP_BLACK_COLOR.cgColor
                self.profilePicture.layer.cornerRadius = self.profilePicture.frame.height/2
                self.profilePicture.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
    }
    @IBAction func startTimeButtonClicked(_ sender: Any) {
        self.txtfldVolReq.resignFirstResponder()
        let dateSelectionPicker = DateSelectionViewController(startDate: nil, endDate:  nil)
        dateSelectionPicker.view.frame = self.view.frame
        dateSelectionPicker.view.layoutIfNeeded()
        dateSelectionPicker.captureSelectDateValue(sender, inMode: .time) { (selectedDate) in
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            let dateString = formatter.string(from: selectedDate)
            self.shiftStartTime = dateString
            (sender as AnyObject).setTitle(dateString, for:.normal)
            (sender as AnyObject).setImage(nil, for: .normal)
            (sender as AnyObject).setTitleColor(APP_BLACK_COLOR, for: .normal)
        }
        addViewController(viewController: dateSelectionPicker)
    }
    
    @IBAction func endDateButtonClicked(_ sender: Any) {
        self.txtfldVolReq.resignFirstResponder()
        let dateSelectionPicker = DateSelectionViewController(startDate:  nil, endDate:  nil)
        dateSelectionPicker.view.frame = self.view.frame
        dateSelectionPicker.view.layoutIfNeeded()
        dateSelectionPicker.captureSelectDateValue(sender, inMode: .time) { (selectedDate) in
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            let dateString = formatter.string(from: selectedDate)
            self.shiftEndTime = dateString
            (sender as AnyObject).setTitle(dateString, for:.normal)
            (sender as AnyObject).setImage(nil, for: .normal)
            (sender as AnyObject).setTitleColor(APP_BLACK_COLOR, for: .normal)
        }
        addViewController(viewController: dateSelectionPicker)
    }
    
    @IBAction func taskShiftSelection(_ sender: Any) {
        self.txtfldVolReq.resignFirstResponder()
        let controller = DropDownItemsTable(shiftList)
        controller.showPopoverInDestinationVC(destination: self, sourceView: sender as! UIView) { [self] (selectedValue) in
            // //print(selectedValue)
            
            if let selectShiftData = selectedValue as? [String:Any],
               let selectedShiftTaskId1 = selectShiftData[GetAddShiftSelectShiftStrings.keyShiftTaskId] as? String {
                self.selectedShiftTaskId = selectedShiftTaskId1
            }
            
            if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetAddShiftSelectShiftStrings.keyShiftTaskName] as? String {
                (sender as AnyObject).setTitle(title, for: .normal)
                shiftName = title
            }
        }
    }
    @IBAction func ShiftRankListButton(_ sender: Any) {
        self.txtfldVolReq.resignFirstResponder()
        
        let controller = DropDownItemsTable(RankList)
        //  //print(RankList)
        controller.showPopoverInDestinationVC(destination: self, sourceView: sender as! UIView) { (selectedRankValue) in
            //   //print(selectedRankValue)
            
            if let selectShiftData = selectedRankValue as? [String:Any],
               let selectedShiftRankId1 = selectShiftData[GetShiftRankListStrings.keyRank] as? String {
                self.selectedShiftRankId = selectedShiftRankId1
                // //print(self.selectedShiftRankId)
            }
            
            if let selectVal = selectedRankValue as? [String:Any], let title = selectVal[GetShiftRankListStrings.keyRankValue] as? String {
                (sender as AnyObject).setTitle(title, for: .normal)
            }
        }
        //
        
        
    }
    // Mark
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
              let rangeOfTextToReplace = Range(range, in: textFieldText) else {
            return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 5
    }
    
    func validate() -> Bool {
        
        
        let editedDatestartTime = self.shiftStartTime.replacingOccurrences(of: ":", with: "")
        // //print(editedDatestartTime)
        
        let editedDateEndTime = self.shiftEndTime.replacingOccurrences(of: ":", with: "")
        // //print(editedDateEndTime)
        
        let formatter = DateFormatter()
        // formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        let firstTime = formatter.date(from: shiftStartTime)
        let secondTime = formatter.date(from: shiftEndTime)
        
        
        
        if (self.sDate == "")
        {
            
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Shift date not selected.", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false;
            
        }else if(self.endDate == ""){
            if !(self.screen == "DUPLICATE SCREEN") && !(self.screen == "EDIT SCREEN"){
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Please select shift end date", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false;
            }
            
        }else if self.daysNameArray.count > 1 && selectedDaysDatesArr.count == 0{
            
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Please select shift days", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false;
            
        }
        else if(self.txtfldVolReq.text! == ""){
            
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Voluteer required data not provided.", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false;
            
        }else if(self.shiftStartTime == ""){
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Shift start time not selected", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return false;
        }else if(self.shiftEndTime == ""){
            
            
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Shift end time not selected", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return false;
            
        }else if firstTime?.compare(secondTime!) == .orderedDescending {
            
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("End time can't be before start time", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return false;
            
        }else if(self.selectedShiftRankId == ""){
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Shift rank not selected", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false;
        }else if(self.txtfldVolReq.text == "0"){
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Volunteer request can't be zero", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false;
            
        }else if(self.selectedShiftTaskId == ""){
            
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Shift task not selected", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false;
        }
        else if let limit = Int(editedDateEndTime), Int(editedDatestartTime)! >= limit {
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Shift end time should be greater than shift start time", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
    
}

extension CSOAddShiftViewController: FloatRatingViewDelegate {
    
    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        
        
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        
        self.shiftRank = String(Int(rating))
    }
    
}
extension Array where Element: Equatable {
    func indexes(of element: Element) -> [Int] {
        return self.enumerated().filter({ element == $0.element }).map({ $0.offset })
    }
}
