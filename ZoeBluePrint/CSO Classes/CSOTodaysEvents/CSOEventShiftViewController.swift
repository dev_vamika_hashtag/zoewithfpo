//
//  CSOEventShiftViewController.swift
//  ZoeBlue//print
//
//  Created by Rishi Chaurasia on 12/07/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit

class CSOEventShiftViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,ViewGroupsViewControllerDelegate {
    
    
    
    
    @IBOutlet weak var sideMenu: UIButton!
//    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnAttendance: UIButton!
    @IBOutlet weak var btnGroup: UIButton!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageDelete: UIImageView!
    @IBOutlet weak var imageEdit: UIImageView!
    @IBOutlet weak var imgViewCsoCover: UIImageView!
    
    @IBOutlet weak var lightStarRating: FloatRatingView!
    
    
    @IBOutlet weak var btnView: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    
    
    @IBOutlet weak var shiftRank: UILabel!
    
    @IBOutlet weak var starView: FloatRatingView!
    
    
    @IBOutlet weak var getShiftDetailsBackgroundView: UIView!
    
    
    @IBOutlet weak var RequestLabel: UILabel!
    
    @IBOutlet weak var ShiftDateLAbel: UILabel!
    
    @IBOutlet weak var ShiftTimeLabel: UILabel!
    
    @IBOutlet weak var getShiftDetailsView: UIView!
    
    @IBOutlet weak var tableGetAllShift: UITableView!
    @IBOutlet weak var task_name: UILabel!
    
    @IBOutlet weak var vol_requested: UILabel!
    @IBOutlet weak var shift_date: UILabel!
    @IBOutlet weak var shift_time: UILabel!
    var calenderData:Dictionary<String,Any>?
    var shiftdata = [[String:Any]]()
    var shiftDetails = [String:Any]()
    var index:Int?
    var event_id:String?
    var screen:String?
    var shift_Rank:Int?
    var attendaceView = AttendaceForumVC()
//    var volListView = AttendeesVolunteerViewController()
    @IBOutlet weak var profilePicture: UIImageView!
    
    
    
    @IBAction func notificationButton(_ sender: Any) {
        
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Global.setUpViewWithTheme(ViewController: self)
        self.imgViewCsoCover.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
        
        self.backButton.setImage(UIImage(named: "iphoneBackButton"), for: UIControl.State.normal)
        
        self.lightStarRating.isHidden = true
        // self.backButton.isHidden = false
        self.starView.isHidden = true
        self.starView.isUserInteractionEnabled = false
        self.lightStarRating.isUserInteractionEnabled = false
        shiftRank.isHidden = true
        RequestLabel.isHidden = true
        ShiftDateLAbel.isHidden = true
        ShiftTimeLabel.isHidden = true
        vol_requested.isHidden = true
        shift_date.isHidden = true
        shift_time.isHidden = true
        
        
        if screen == "calender"{
            
            let shiftdata = self.calenderData!["shiftdata"] as! Dictionary<String,Any>
            
            let servicehandler = ServiceHandlers()
            servicehandler.getShiftDetails(shiftId: shiftdata["shift_id"] as! String){
                (responce,isSuccess) in
                if isSuccess{
                    let data = responce as! Dictionary<String,Any>
                    self.setValues(shiftdata1: data)
                    
                    
                }
            }
            
        }
        self.profile_pic()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableGetAllShift.delegate = self
        self.tableGetAllShift.dataSource = self
        
        self.tableGetAllShift.isHidden = false
        
        
        let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        self.getShiftDetailsBackgroundView.addGestureRecognizer(mytapGestureRecognizer)
        self.getShiftDetailsView.isUserInteractionEnabled = true
        
        getShiftDetailsBackgroundView.isHidden = true               // Change Status
        getShiftDetailsView.isHidden = true
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        if screen != "calender"{
            self.getAllData()
        }
        
        tableGetAllShift.tableFooterView = UIView()
    }
    @objc func handleTap(_ sender:UITapGestureRecognizer) {
        
        self.getShiftDetailsBackgroundView.isHidden = true
        self.getShiftDetailsView.isHidden = true
        
        
    }
    func profile_pic()  {
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profilePicture.image = image
                self.profilePicture.layer.borderWidth = 1
                self.profilePicture.layer.masksToBounds = false
                self.profilePicture.layer.borderColor = APP_BLACK_COLOR.cgColor
                self.profilePicture.layer.cornerRadius = self.profilePicture.frame.height/2
                self.profilePicture.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
    }
    
    @IBAction func backButtonFunction(_ sender: Any) {
        performSegueToReturnBack()
    }
    @IBAction func addGroupToShiftButtonFunction(_ sender: Any) {
        self.getShiftDetailsBackgroundView.isHidden = true
        self.getShiftDetailsView.isHidden = true
        
        var data = shiftdata[index!]
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let selectedEventVC =  mainSB.instantiateViewController(withIdentifier: "ViewGroupsViewController") as? ViewGroupsViewController
        selectedEventVC?.delegate = self
        selectedEventVC?.processType = "selection"
        selectedEventVC?.shiftData = data
        self.navigationController?.pushViewController(selectedEventVC!, animated: true)
    }
    func sendSelectedGroupIds(ids: [String]) {
        let data = self.shiftdata[self.index!]
        let shift_id = data["shift_id"] as! String
        // //print(eventID)
        let param = ["shift_id":shift_id,
                     "group_id":ids[0]]
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.associateGroupToShift(params: param) { (responce, isSuccess) in
            if isSuccess {
                self.getGroupMemWith(groupId: ids)
                
                
            }else{
                
            }
        }
        
        
       
    }
    func getGroupMemWith(groupId: [String]){
        var volunteerIDs = [String]()
//        var count = 0
//        if groupId.count > 1{
//        for i in 1...groupId.count{
////            count = count + 1
//                let param = ["group_id":groupId[i-1]]
//                let serviceHandler = ServiceHandlers()
//                serviceHandler.getVolunteerGroupMembers(params: param){(responce,isSuccess) in
//                    if isSuccess{
//                       let data = responce as! Array<Any>
//                        for member in data {
//                            let dic = member as! NSDictionary
////                            for i in 1...dic.count-1{
//                                volunteerIDs.append(dic["id"] as! String)
//                                if i == groupId.count{
//                                    self.sendNotificatioToVolunteer(volIds: volunteerIDs)
//                                }
////                            }
//                        }
//                    }
//                }
//        }
//        }else{
            let param = ["group_id":groupId[0]]
            let serviceHandler = ServiceHandlers()
            serviceHandler.getVolunteerGroupMembers(params: param){(responce,isSuccess) in
                if isSuccess{
                   let data = responce as! Array<Any>
                    for member in data {
                        let dic = member as! NSDictionary
                            volunteerIDs.append(dic["volunteer_id"] as! String)
                                self.sendNotificatioToVolunteer(volIds: volunteerIDs)
                    }
                }
            }
//        }
    }
    func sendNotificatioToVolunteer(volIds: [String]){
        var data = shiftdata[index!]
        let shift_id = data["shift_id"] as! String
        let eventID = shiftDetails["event_id"] as! String
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let user_id = volIds.joined(separator: ",")
        let user_type = userIDData["user_type"] as! String
        let cso_id = userIDData["user_id"] as! String
        let param = ["shift_id":shift_id,
                     "user_id":user_id,
                     "user_type":user_type,
                     "user_device":UIDevice.current.identifierForVendor!.uuidString,
                     "event_id":eventID,
                     "cso_id":cso_id] as [String : Any]
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.sendEventNotificationToGroupMembers(params: param) { (responce, isSuccess) in
            if isSuccess {
                self.getShiftDetailsBackgroundView.isHidden = true
                self.getShiftDetailsView.isHidden = true
                self.getAllData()
            }else{
                
            }
        }
    }
    @IBAction func viewVolunteerList(_ sender: Any) {
        self.getShiftDetailsBackgroundView.isHidden = true
        self.getShiftDetailsView.isHidden = true
            let data = shiftdata[index!]
            let volListView = AttendeesVolunteerViewController.init(nibName: "AttendeesVolunteerViewController", bundle: nil)
            volListView.view.frame = self.view.frame
            volListView.shiftData = data
            volListView.eventDetailsData = shiftDetails
        self.navigationController!.pushViewController(volListView, animated: true)
//            self.view.addSubview(volListView.view)
//            self.addChild(volListView)
        
    }
    @IBAction func takeShiftAttendaceFunction(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(150)) {
            let data = self.shiftdata[self.index!]
            self.getShiftDetailsBackgroundView.isHidden = true
            self.getShiftDetailsView.isHidden = true
            if (self.shiftDetails["event_status"] as! String) == "20"{
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please Publish event first", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true)
                return
            }
            if self.attendaceView.view == nil{
                
            }else{

                self.attendaceView = AttendaceForumVC.init(nibName: "AttendaceForumVC", bundle: nil)
               
                self.attendaceView.view.frame = self.view.frame
                self.attendaceView.shiftData = data
                self.attendaceView.shiftDetailsData = self.shiftDetails
                self.attendaceView.cancelButton.addTarget(self, action: #selector(self.onCloseAttendaceView), for: .touchUpInside)
                self.attendaceView.submitButton.addTarget(self, action: #selector(self.onSubmitAttendaceView), for: .touchUpInside)
               
//                self.view.addSubview(self.attendaceView.view)
                self.navigationController!.pushViewController(self.attendaceView, animated: true)
                
            }
            }
       
    }
    @objc func onSubmitAttendaceView() {
        if attendaceView.view == nil{
            return
        }
        attendaceView.ResgisterVolunteer()
    }
    @objc func onCloseAttendaceView() {
        if attendaceView.view == nil{
            return
        }
        attendaceView.navigationController?.popViewController(animated: true)
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        //  //print("Value of notification : ", notification.object ?? "")
        self.getAllData()
    }
    func getAllData(){
        self.shiftdata = [[String:Any]]()
        // //print(shiftDetails)
        let eventID = shiftDetails["event_id"] as! String
        // //print(eventID)
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.getAllShift(eventId: eventID as? String ?? "") { (responce, isSuccess) in
            if isSuccess {
                self.shiftdata = responce as! [[String: Any]]
                // //print(self.shiftdata)
                self.tableGetAllShift.reloadData()
                
            }else{
                self.shiftdata = [[String:Any]]()
                self.tableGetAllShift.reloadData()
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Data not found", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert,animated: true)
            }
        }
        
    }
    func setValues(shiftdata1:Dictionary<String,Any>){
        
        
        self.view.backgroundColor = APP_WHITE_COLOR
        
        self.shift_time.textColor = APP_BLACK_COLOR
        self.vol_requested.textColor = APP_BLACK_COLOR
        self.shift_date.textColor = APP_BLACK_COLOR
        
        self.lightStarRating.isHidden = false
        self.lightStarRating.rating = Double(shiftdata1["shift_rank"] as! String) ?? 0.0
        self.lightStarRating.isUserInteractionEnabled = false
        starView.isHidden = true
        
        //    }
        
        self.tableGetAllShift.isHidden = true
        self.getShiftDetailsBackgroundView.isHidden = true
        // self.backButton.isHidden = false
        RequestLabel.isHidden = false
        ShiftDateLAbel.isHidden = false
        ShiftTimeLabel.isHidden = false
        shiftRank.isHidden = false
        vol_requested.isHidden = false
        shift_date.isHidden = false
        shift_time.isHidden = false
        
        
        task_name.text = (shiftdata1["shift_task_name"] as! String).uppercased()
        if shiftdata1["shift_all_day"] as? String == "0"{
            var start_time = shiftdata1["shift_start_time_format"] as! String
            
            var end_time = shiftdata1["shift_end_time_format"] as! String
            
            var Time = "\(start_time) - \(end_time)"
            
            shift_time.text = Time
        }else{
            shift_time.text = "All Day"
        }
        
        
        
        vol_requested.text = shiftdata1["shift_vol_req"] as? String
        
        shift_date.text = shiftdata1["shift_date"] as? String
        
        var status = shiftdata1["shift_status"] as! String
        
        let intVolunteerApply = Int(shiftdata1["volunteer_apply"]as! String)
        let intVolReq = Int(shiftdata1["shift_vol_req"]as! String)
        let intVolunteerreqAccepted = Int(shiftdata1["volunteer_req_accepted"]as! String)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shiftdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableGetAllShift.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! getShiftDataCell
        var dateString = shiftdata[indexPath.row]["shift_date"] as! String
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        let dateObj = dateFormatter.date(from: dateString)
        
        dateFormatter.dateFormat = "dd"
        // //print("Dateobj: \(dateFormatter.string(from: dateObj!))")
        let dated = dateFormatter.string(from: dateObj!)
        cell.lblMemberCount.layer.cornerRadius = cell.lblMemberCount.frame.width/2
        cell.lblMemberCount.layer.masksToBounds = true
        cell.lblMemberCount.text = shiftdata[indexPath.row]["shift_vol_req"] as! String
        cell.shiftDate.text = dated
        cell.shiftMonth.text = shiftdata[indexPath.row]["shift_month"] as! String
        var day = shiftdata[indexPath.row]["shift_day"] as! String
        // //print(day)
        cell.shiftDay.text = String(day.prefix(3))
        cell.lbnEventName.text = shiftdata[indexPath.row]["shift_task_name"] as! String
        if shiftdata[indexPath.row]["shift_all_day"] as? String == "0"{
            var start_time = shiftdata[indexPath.row]["shift_start_time"] as! String
            
            // //print(start_time)
            var end_time = shiftdata[indexPath.row]["shift_end_time"] as! String
            // //print(end_time)
            cell.lbnEventTime.text = "\(start_time) - \(end_time)"
        }else{
            cell.lbnEventTime.text = "All Day"
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // //print(indexPath.row)
        self.getShiftDetailsBackgroundView.isHidden = false
        self.getShiftDetailsView.isHidden = false
        index = indexPath.row
        
    }
    
    @IBAction func getDetailEditButton(_ sender: Any) {
        var data = shiftdata[index!]
        self.getShiftDetailsBackgroundView.isHidden = true
        //self.getShiftDetailsView.isHidden = true
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "CSOAddShiftViewController") as! CSOAddShiftViewController
        secondViewController.data_for_update = data
        secondViewController.screen = "EDIT SCREEN"
        secondViewController.eventDetail = shiftDetails
        navigationController?.pushViewController(secondViewController, animated: true)
        
    }
    
    
    @IBAction func getDetailDeleteButton(_ sender: Any) {
        let data = shiftdata[index!]["shift_id"]
        self.getShiftDetailsBackgroundView.isHidden = true
        //        self.getShiftDetailsBackgroundView.isHidden = true
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.deleteShiftForEventCSO(shift_id: data as? String ?? "") { (responce, isSuccess) in
            if isSuccess {
                let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Shift deleted successfully.", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(_ action: UIAlertAction) -> Void in
                    ActivityLoaderView.startAnimating()
                    self.getAllData()
                    self.tableGetAllShift.reloadData()
                }))
                self.present(alert, animated: true, completion: nil)
                
            }else{
                self.getAllData()
                self.tableGetAllShift.reloadData()
                let data = responce as! Dictionary<String,Any>
                let msg = data["res_message"] as! String
                let alert = UIAlertController(title: "Alert!", message: msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert,animated: true)
            }
        }
    }
    
    
    @IBAction func duplicateEventButton(_ sender: Any) {
        let data = shiftdata[index!]
        self.getShiftDetailsBackgroundView.isHidden = true
        //self.getShiftDetailsView.isHidden = true
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "CSOAddShiftViewController") as! CSOAddShiftViewController
        secondViewController.data_for_update = data
        secondViewController.screen = "DUPLICATE SCREEN"
        secondViewController.eventDetail = shiftDetails
        navigationController?.pushViewController(secondViewController, animated: true)
        
    }
    
    @IBAction func getDetailViewButton(_ sender: Any) {
        self.tableGetAllShift.isHidden = true
        self.getShiftDetailsBackgroundView.isHidden = true
        self.getShiftDetailsView.isHidden = true
        
        self.view.backgroundColor = .white
        //self.RequestLabel.textColor = .blue
        //self.ShiftDateLAbel.textColor = .blue
        //self.ShiftTimeLabel.textColor = .blue
        //self.shiftRank.textColor = .blue
        self.task_name.textColor = APP_BLACK_COLOR
        self.shift_time.textColor = APP_BLACK_COLOR
        self.vol_requested.textColor = APP_BLACK_COLOR
        self.shift_date.textColor = APP_BLACK_COLOR
        
        self.lightStarRating.rating = Double(shiftdata[index!]["shift_rank"] as! String) ?? 0.0
        self.lightStarRating.isUserInteractionEnabled = false
        starView.isHidden = true
        
        //   }
        
        self.task_name.text = (shiftdata[index!]["shift_task_name"] as! String).uppercased()
        if shiftdata[index!]["shift_all_day"] as? String == "0"{
            let startTime = shiftdata[index!]["shift_start_time"] as! String
            let endTime = shiftdata[index!]["shift_end_time"] as! String
            let Time = "\(startTime) - \(endTime)"
            // //print(Time)
            self.shift_time.text = Time
        }else{
            self.shift_time.text = "All Day"
        }
        
        
        RequestLabel.isHidden = false
        ShiftDateLAbel.isHidden = false
        ShiftTimeLabel.isHidden = false
        shiftRank.isHidden = false
        starView.isHidden = false
        lightStarRating.isHidden = false
        vol_requested.isHidden = false
        shift_date.isHidden = false
        shift_time.isHidden = false
        //        StatusLabel.isHidden = false
        //        StatusLabel.isHidden = false
        
        
        self.starView.rating = Double(shiftdata[index!]["shift_rank"]as! String ) ?? 0.0
        
        self.lightStarRating.rating = Double(shiftdata[index!]["shift_rank"]as! String ) ?? 0.0
        
        let data = shiftdata[index!]
        //        self.setValues(shiftdata1:data)
        let servicehandler = ServiceHandlers()
        servicehandler.getShiftDetails(shiftId: data["shift_id"] as! String){
            (responce,isSuccess) in
            if isSuccess{
                let data = responce as! Dictionary<String,Any>
                self.setValues(shiftdata1: data)
                
                
            }
        }
    }
}
