//
//  CSOTodaysEventDetailsViewController.swift
//  ZoeBlue//print
//
//  Created by Rishi Chaurasia on 11/07/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit

protocol CSOTodaysEventDetailsViewControllerDelegate: class {
    func shiftDetailsTapped(_ shiftDetail: [String: Any]?)
}

class CSOTodaysEventDetailsViewController: UIViewController {
    weak var delegate: CSOTodaysEventDetailsViewControllerDelegate?
    enum CellType:Int {
        case Description = 0
        case Address = 1
        case Phone_Number = 2
        case Email = 3
        case Start_Date = 4
        case Start_Time = 5
        
    }
    let eventDetailsCellIdentifier = "EventDetailTableViewCell"
    
    @IBOutlet weak var RatingLabel: UILabel!
    @IBOutlet weak var sideMenu: UIButton!
    @IBOutlet weak var imgViewCsoCover: UIImageView!
    @IBOutlet weak var lightStarView: FloatRatingView!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventDescription: UILabel!
    @IBOutlet weak var eventAddress: UILabel!
    
    @IBOutlet weak var whiteStar: FloatRatingView!
    @IBOutlet weak var shiftViewImage: UIImageView!
    
    @IBOutlet weak var mainView: UIView!
    var imgName:String = ""
    @IBOutlet weak var starRatingView: FloatRatingView!
    
    @IBOutlet weak var pubunpubLabel: UILabel!
    @IBOutlet weak var pubunpubImage: UIImageView!
    @IBOutlet weak var back_button: UIButton!
    
    @IBOutlet weak var buttonShift: UIButton!
    
    
    @IBOutlet weak var contentTableView: UITableView!
    var screen:String?
    var selectedEvent = [String: Any]()
    var event_id:String?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.profile_pic()
        Global.setUpViewWithTheme(ViewController: self)
        
        self.back_button.isHidden = true
        self.buttonShift.isHidden = true
        
        if screen == "DASHBOARD"{
            pubunpubLabel.isHidden = true
            pubunpubImage.isHidden = true
            
            
            let servicehandler = ServiceHandlers()
            servicehandler.getSelectedEventDetails(eventId: event_id!){(responce,isSuccess) in
                if isSuccess{
                    self.selectedEvent = responce as! [String: Any]
                    self.setEventData()
                    self.contentTableView.reloadData()
                    
                }
                
            }
        }else{
            self.back_button.isHidden = true
            pubunpubImage.isHidden = false
            pubunpubLabel.isHidden = false
            // print(selectedEvent["event_heading"] as! String)
            self.eventTitle.text = (selectedEvent["event_heading"] as! String).uppercased()
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                let string_url = self.selectedEvent["event_image"] as! String
                let replacedStr = string_url.replacingOccurrences(of: " ", with: "%20")
                
                let imageUrl = URL(string: replacedStr)!
                if let data = NSData(contentsOf: imageUrl) {
                    
                    do {
                        let imageData = try Data(contentsOf: imageUrl as URL)
                        self.eventImage.image = UIImage(data: imageData)
                    } catch {
                        print("Unable to load data: \(error)")
                    }
                }else{
                    do {
                        let imageUrlBlank = URL(string: "https://zbp.progocrm.com/assets/static/images/thumbnail-logo.png")!
                        let imageData = try Data(contentsOf: imageUrlBlank as URL)
                        self.eventImage.image = UIImage(data: imageData)
                    } catch {
                        print("Unable to load data: \(error)")
                    }
                }
            })
            
            
            
            var status = selectedEvent["event_status"] as? String
            //print(status)
            if status == "10" {
                
                self.pubunpubImage.image = UIImage(named: "tick.png")
                self.pubunpubLabel.text = " Published "
            }else
            if status != "10"{
                
                self.pubunpubImage.image = UIImage(named: "close.png")
                self.pubunpubLabel.text = " Unpublished "
            }
        }
        
        if (screen  == "calender" ){
            
            self.buttonShift.isHidden = false
            pubunpubLabel.isHidden = true
            pubunpubImage.isHidden = true
            self.setEventData()
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lightStarView.isHidden = false
        self.starRatingView.isHidden = true
        
        contentTableView.estimatedRowHeight = 78.0
        contentTableView.rowHeight = UITableView.automaticDimension
        self.imgViewCsoCover.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
    }
    
    
    
    
    @IBAction func notificationButton(_ sender: Any) {
        
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    
    @IBAction func sideButton(_ sender: Any) {
    }
    
    @IBOutlet weak var profilePicture: UIImageView!
    
    @IBAction func showShiftTapped(_ sender: Any) {
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        
        if let shiftVC = mainSB.instantiateViewController(withIdentifier: "CSOEventShiftViewController") as? CSOEventShiftViewController {
            NSLog("%@",selectedEvent)
            shiftVC.shiftDetails = selectedEvent
            self.view.addSubview(shiftVC.view)
        }
    }
    func profile_pic()  {
        
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profilePicture.image = image
                self.profilePicture.layer.borderWidth = 1
                self.profilePicture.layer.masksToBounds = false
                self.profilePicture.layer.borderColor = UIColor.black.cgColor
                self.profilePicture.layer.cornerRadius = self.profilePicture.frame.height/2
                self.profilePicture.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
    }
    @IBAction func shiftButtonFunction(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let obj = sb.instantiateViewController(withIdentifier: "CSOEventShiftViewController") as! CSOEventShiftViewController
        obj.calenderData = selectedEvent
        obj.screen = "calender" //reetesh jan8
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    
    func configureTableView() {
        let nibName = UINib(nibName: "EventDetailTableViewCell", bundle:nil)
        contentTableView!.register(nibName, forCellReuseIdentifier: eventDetailsCellIdentifier)
        contentTableView.tableFooterView = UIView()
    }
    
    
    func getAbsoluteStringFromSeparator(rawString:String, separator:String) -> String {
        let strComponents = rawString.components(separatedBy: separator)
        var absString = String()
        for str in strComponents {
            absString = absString + " " + str
        }
        return absString
        
    }
    @IBAction func backButton(_ sender: Any) {
        //        self.dismiss(animated: true, completion: nil)
        performSegueToReturnBack()
    }
    
    func setEventData()  {
        eventTitle.text = (selectedEvent["event_heading"] as? String)?.uppercased()
        let addr = selectedEvent["event_address"] as? String ?? ""
        let city = selectedEvent["event_city"] as? String ?? ""
        let state = selectedEvent["event_state_name"] as? String ?? ""
        let country = selectedEvent["event_country_name"] as? String ?? ""
        let zipcode = selectedEvent["event_postcode"] as? String ?? ""
        let description = selectedEvent["event_details"] as? String ?? ""
        
        var EventRate = selectedEvent["average_rating"] as! String
        
        let defaults = UserDefaults.standard.string(forKey: APP_THEME)
        if EventRate > "0" {
            
            var rating = Double(selectedEvent["average_rating"] as! String) ?? 0.0
            self.starRatingView.rating = rating
            self.starRatingView.isUserInteractionEnabled = false
            var Rate = String(format: "%.1f", rating)
            var countRate = selectedEvent["event_count_rating"] as! String
            self.RatingLabel.text = "Average Star Rating is \(Rate) from the Total \(countRate) Rating"
            
            
        }else if  EventRate < "0" {
            
            self.RatingLabel.text = "No Ratings"
            
        }
        var status = selectedEvent["event_status"] as? String
        //print(status)
        if status == "10" {
            
            self.pubunpubImage.image = UIImage(named: "tick.png")
            self.pubunpubLabel.text = " Published "
        }else
        if status != "10"{
            
            self.pubunpubImage.image = UIImage(named: "close.png")
            self.pubunpubLabel.text = " Unpublished "
        }
        let string_url = selectedEvent["event_image"] as! String
        
        let replacedStr = string_url.replacingOccurrences(of: " ", with: "%20")
        let imageUrl = URL(string: replacedStr)!
        do {
            let imageData = try Data(contentsOf: imageUrl as URL)
            self.eventImage.image = UIImage(data: imageData)
        } catch {
            // //print("Unable to load data: \(error)")
        }
    }
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XXX) XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    
}


extension  CSOTodaysEventDetailsViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        var heading = String()
//        var desc = String()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: eventDetailsCellIdentifier, for: indexPath) as! EventDetailTableViewCell
        
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        let descriptionKey = "event_details"
        if let eventdescription = selectedEvent[descriptionKey]{
//                heading = "Description"
//                desc = eventdescription as? String ?? ""
            cell.descriptionLabel.text = eventdescription as? String ?? ""
        }
        let eventPhoneNum = "event_phone"
        if let eventPhone = selectedEvent[eventPhoneNum]
        {
            
//                heading = "Phone Number"
//                desc =  self.formattedNumber(number:(selectedEvent[phoneKey] as! String))
            cell.phoneNumberLabel.text = self.formattedNumber(number:(eventPhone as! String))
        }
        let registerDate = "event_register_start_date"
        let phoneKey2 = "event_register_end_date"
        if let eventPhone = selectedEvent[registerDate] {
            if let eventPhone2 = selectedEvent[phoneKey2]{
//                    heading = "Date"
                
                cell.dateLabel.text = (eventPhone as? String ?? "") + " - " + (eventPhone2 as? String ?? "")
            }
        }
        let eventEmail = "event_email"
        if let eventPhone = selectedEvent[eventEmail] {
//                heading = "Email"
            cell.emailLabel.text = eventPhone as? String ?? ""
            
            
            
        }
        
            let eventAddress = "event_address"
            let eventCity = "event_city"
            let phoneKey3 = "event_state_name"
            let phoneKey4 = "event_country_name"
            let phoneKey5 = "event_postcode"
            if let eventPhone = selectedEvent[eventAddress] {
                if let eventPhone2 = selectedEvent[eventCity]{
                    if let eventPhone3 = selectedEvent[phoneKey3]{
                        if let eventPhone4 = selectedEvent[phoneKey4]{
                            if let eventPhone5 = selectedEvent[phoneKey5]{
//                                heading = "Address"
                                
                                let desc1 = (eventPhone as? String ?? "") + ", " + (eventPhone2 as? String ?? "")
                                let desc2 = (eventPhone3 as? String ?? "") + ", " + (eventPhone4 as? String ?? "")
                                let desc3 = (eventPhone5 as? String ?? "")
                                cell.addressLabel.text = desc1 + ", " + desc2 + ", "  + desc3
                            }
                        }
                        
                    }
                }
            }
        let startTime = "event_start_time_format"
        let endTime = "event_end_time_format"
        
        if let eventPhone = selectedEvent[startTime] {
            if let eventPhone2 = selectedEvent[endTime]{
//                    heading = "Time"
                
                cell.dateLabel.text = String(format: "%@\n%@", cell.dateLabel.text!,(eventPhone as? String ?? "") + " - " + (eventPhone2 as? String ?? ""))
            }
        }
//        cell.titleLabel.text = heading
//        cell.detailLabel.text  = desc
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260//UITableView.automaticDimension
    }
    
    
    
}

