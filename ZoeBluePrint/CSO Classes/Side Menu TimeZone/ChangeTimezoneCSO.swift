////
//  ChangeTimezoneCSO.swift
//  ZoeBlue//print
//
//  Created by Reetesh Bajpai on 06/11/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit

class ChangeTimezoneCSO: UIViewController {
    
    @IBOutlet weak var btnTimeZone: UIButton!
    @IBOutlet weak var imgViewCsoCover: UIImageView!
    @IBOutlet weak var imageTime: UIImageView!
    
    @IBOutlet weak var imgCoverPic: UIImageView!
    @IBOutlet weak var imageDay: UIImageView!
    
    
    @IBOutlet weak var imgProfilepic: UIImageView!
    @IBOutlet weak var lblSelectTimeZone: UILabel!
    @IBOutlet weak var lblSelectDayLight: UILabel!
    @IBOutlet weak var lblTimezone: UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnMenuCso: UIButton!
    @IBOutlet weak var btnMenuVol: UIButton!
    var screen:String!
    var timezoneID:String?
    var dayLightID:String?
    var listTimeZone: [[String:Any]]!
    @IBOutlet weak var dayLightButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Reetesh 16 jan
        btnSubmit.isEnabled = false
        let utility = Utility()
        utility.fetchTimeZone { (eventTypeList, isValueFetched) in
            if let list = eventTypeList {
                self.listTimeZone = list
                self.setDataToView()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Global.setUpViewWithTheme(ViewController: self)
        self.profile_pic()
        self.getCoverImageForRank()
        
    }
    
    func profile_pic()  {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.imgProfilepic.image = image
                self.imgProfilepic.layer.borderWidth = 1
                self.imgProfilepic.layer.masksToBounds = false
                self.imgProfilepic.layer.borderColor = APP_BLACK_COLOR.cgColor
                self.imgProfilepic.layer.cornerRadius = self.imgProfilepic.frame.height/2
                self.imgProfilepic.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
        
    }
    
    
    
    
    
    func getCoverImageForRank(){
        
        if let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as? Data, let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as?  Dictionary<String, Any>, let usertype = userIDData["user_type"] as? String, (usertype == "CSO" || usertype == "FPO") {
            self.btnMenuCso.isHidden = false
            self.btnMenuVol.isHidden = true
            self.imgCoverPic.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
            
        } else {
            self.btnMenuCso.isHidden = true
            self.btnMenuVol.isHidden = false
            var strImageNameCover = "cover_cloud.jpg"
            
            if let decoded  = UserDefaults.standard.object(forKey: "VolData") as? Data, let volData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? Dictionary<String, Any>, let userAvgRank = volData["user_avg_rank"] as? String {
                
                
                let floatUserAverageRank = Float(userAvgRank)!
                if ((floatUserAverageRank >= 0) && (floatUserAverageRank <= 20)){
                    strImageNameCover = "cover_riseandshine.jpg"
                }else if ((floatUserAverageRank > 20) && (floatUserAverageRank <= 40)){
                    strImageNameCover = "cover_cake.jpg"
                }else if ((floatUserAverageRank > 40) && (floatUserAverageRank <= 60)){
                    strImageNameCover = "cover_cool.jpg"
                }else if ((floatUserAverageRank > 60) && (floatUserAverageRank <= 80)){
                    strImageNameCover = "cover_truck.jpg"
                }else if (floatUserAverageRank > 80 ){
                    strImageNameCover = "cover_cloud.jpg"
                }
                
                
            }
            self.imgCoverPic.image = UIImage(named:strImageNameCover)
        }
        
        
    }
    @IBAction func notifBellIconEvent(_ sender: Any) {
        
        Utility.showNotificationScreen(navController: self.navigationController)
        
    }
    
    func setDataToView(){
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let params = userIDData["user_id"] as! String
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.getUserTimeZone(user_id: params) { [self] (responce, isSuccess) in
            if isSuccess {
                let data = responce as! Array<Any>
                let dict:Dictionary<String,Any> = data[0] as! Dictionary<String,Any>
                // Reetesh 16 jan
                
                self.dayLightID = dict["login_daylight"] as? String
                // self.timezoneID = dict["login_daylight"] as! String
                for zoneName in self.listTimeZone{
                    
                    let strZoneCode = zoneName["timezone_code"] as! String
                    let strZoneName = zoneName["timezone_name"] as! String
                    let ZoneChanged = "\(strZoneName) [\(strZoneCode)]"
                    
                    
                    if strZoneCode == dict["login_timezone"] as! String {
                        self.btnTimeZone.setTitle(ZoneChanged, for: .normal)
                        self.timezoneID = zoneName["timezone_code"] as? String //7th may
                    }
                }
                
                // Reetesh 16 jan
                if ((dict["login_daylight"] as! String) == "1"){
                    self.dayLightButton.setTitle(NSLocalizedString("ON", comment: ""), for: .normal) // Reetesh 16 jan
                }else{
                    self.dayLightButton.setTitle(NSLocalizedString("OFF", comment: ""), for: .normal) // Reetesh 16 jan
                }
                //print(dict)
                self.btnSubmit.isEnabled = true
            }
            
        }
    }
    @IBAction func back_button(_ sender: Any) {
        performSegueToReturnBack()
        //self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func selectTimeZoneDropDown(_ sender: Any) {
        let utility = Utility()
        utility.fetchTimeZone { (eventTypeList, isValueFetched) in
            if let list = eventTypeList {
                self.showPopoverForView(view: sender, contents: list)
            }
        }
    }
    
    
    @IBAction func selectDayLightDropDown(_ sender: Any) {
        let list = [["day_status":NSLocalizedString("ON", comment: ""),"day_id":"1"],["day_status":NSLocalizedString("OFF", comment: ""),"day_id":"0"]]// Reetesh 16 jan
        self.showPopoverForView1(view: sender, contents: list)
        
    }
    
    
    
    @IBAction func btnSubmit(_ sender: Any) {
        if(validate()){
            let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
            let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
            let userID = userIDData["user_id"] as! String
            
            let params = ["user_id":userID,
                          "login_timezone":self.timezoneID!,
                          "login_daylight":self.dayLightID]
            print(params)
            let serviceHanlder = ServiceHandlers()
            serviceHanlder.updateTimeZone(data:params as Dictionary<String, Any>) { (responce, isSuccess) in    
                if isSuccess {
                    UserDefaults.standard.set(self.timezoneID, forKey: UserDefaultKeys.key_userTimeZone)
                    let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Time zone update successfully", comment: ""), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { action in
                        //run your function here
                        // self.dism()
                    }))
                    self.present(alert, animated: true)
                    self.setDataToView()
                }
                
                
            }
            
        }
        
    }
    
    
    
    
    func dism(){
        performSegueToReturnBack()
        //self.dismiss(animated: true, completion: nil)
    }
    func validate() -> Bool {
        if(self.timezoneID == ""){
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Select time zone", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }else if (self.dayLightID == ""){
            let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Select day light", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
    fileprivate func showPopoverForView(view:Any, contents:Any) {
        let controller = DropDownItemsTable(contents)
        let senderButton = view as! UIButton
        controller.showPopoverInDestinationVC(destination: self, sourceView: view as! UIView) { (selectedValue) in
            if let selectVal = selectedValue as? String {
                senderButton.setTitle(selectVal, for: .normal)
                // senderButton.setImage(nil, for: .normal)
            } else if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetTimeZone.timeZoneName] as? String {
                self.timezoneID = selectVal[GetTimeZone.timeZoneCode] as? String
                let title2 = selectVal[GetTimeZone.timeZoneCode]
                let title3 =  "\(title) [\(title2 ?? "")]"
                print(title3)
                senderButton.setTitle(title3, for: .normal)
                //  senderButton.setImage(nil, for: .normal)
            }
        }
    }
    fileprivate func showPopoverForView1(view:Any, contents:Any) {
        let controller = DropDownItemsTable(contents)
        let senderButton = view as! UIButton
        controller.showPopoverInDestinationVC(destination: self, sourceView: view as! UIView) { (selectedValue) in
            if let selectVal = selectedValue as? String {
                senderButton.setTitle(selectVal, for: .normal)
                // senderButton.setImage(nil, for: .normal)
            } else if let selectVal = selectedValue as? [String:Any], let title = selectVal[DayLight.day_status] as? String {
                self.dayLightID = selectVal[DayLight.day_id] as? String
                
                senderButton.setTitle(title, for: .normal)
                // senderButton.setImage(nil, for: .normal)
            }
        }
    }
}

