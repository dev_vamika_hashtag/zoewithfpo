//
//  volunteerSeeFollowers.swift
//  ZoeBlue//print
//
//  Created by iOS Training on 27/11/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit
import SendBirdSDK

class volunteerSeeFollowers: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate,delegateSeeFollowers {
    
    
    @IBOutlet weak var groupPopUpBackground: UIView!
    @IBOutlet weak var gropNameTF: UITextField!
    @IBOutlet weak var cancelGroupName: UIButton!
    @IBOutlet weak var submitGroupName: UIButton!
    
    @IBOutlet weak var viewGroupButton: UIButton!
    @IBOutlet weak var createGroupButton: UIButton!
    @IBOutlet weak var checkAllVolunteerBtn: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var seeFollowersLabels: UILabel!
    
    @IBOutlet weak var VolunteerLabel: UILabel!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var imgViewCsoCover: UIImageView!
    @IBOutlet weak var SearchKey: UITextField!
    
    @IBOutlet weak var tblView: UITableView!
    
    var listDetails:Array<Any> = Array()
    var checkVolunteerDic =  [Bool?]()
    var selectedVolunteerIds = [String]()
    var processTypes = String()
    var updateEventName = String()
    var editGroupMemberDetail = [String]()
    @IBOutlet weak var button2view: UIView!
    
    @IBOutlet weak var button1vol: UIButton!
    
    @IBOutlet weak var button2vol: UIButton!
    var filteredData:Array<Any> = Array()
    
    
    @IBOutlet weak var btnUnlinkTapped: UIButton!
    
    let defaults = UserDefaults.standard.string(forKey: APP_THEME)
    
    @IBAction func NotificationButton(_ sender: Any) {
        
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.groupPopUpBackground.isHidden = true
        self.tblView.tableFooterView = UIView()
        tblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom:150, right: 0)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Global.setUpViewWithTheme(ViewController: self)
        self.profile_pic()
        self.imgViewCsoCover.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
        self.searchBar.delegate = self
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tabBarController?.delegate = self
        
        view.sendSubviewToBack(button2view)
        self.getList()
        VolunteerLabel.isHidden = true // reetesh
        seeFollowersLabels.isHidden = false
        button1vol.setTitleColor(APP_BLACK_COLOR, for: .normal)
        button2vol.setTitleColor(.gray, for: .normal)
        self.tblView.reloadData()
        if processTypes == "UPDATE GROUP"{
            self.viewGroupButton.isHidden = true
            self.createGroupButton.setTitle("Update Group", for: .normal)
        }else{
            self.viewGroupButton.isHidden = false
            self.createGroupButton.setTitle("Create Group", for: .normal)
        }
    }
  
    @IBAction func onCancelGroupNameButton(_ sender: Any) {
        self.groupPopUpBackground.isHidden = true
    }
    @IBAction func onSubmitGroupNameButton(_ sender: Any) {
        if self.gropNameTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces) != ""{
            
            let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
            let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
            let org_id = userIDData["org_id"] as! String
            let param = ["org_id":org_id,
                         "volunteer_list":self.selectedVolunteerIds.joined(separator: ","),
                         "group_name":self.gropNameTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces) ] as [String : Any]
            
            let serviceHandler = ServiceHandlers()
            serviceHandler.createVolunteerGroup(params: param){(responce,isSuccess) in
                if isSuccess{
                    self.groupPopUpBackground.isHidden = true
                    self.selectedVolunteerIds = [String]()
                    self.checkAllVolunteerBtn.isSelected = false
                    for i in 0..<self.checkVolunteerDic.count {
                        self.checkVolunteerDic[i] = false
                    }
                    self.tblView.reloadData()
                    let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Group created successfully", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }else{
                    if let msg = responce{
                        let alert = UIAlertController(title: "Alert!", message: msg as! String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                    let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Group not created", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }else{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please enter group name", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert,animated: true)
        }
    }
    @IBAction func onViewGroupButton(_ sender: Any) {
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let selectedEventVC =  mainSB.instantiateViewController(withIdentifier: "ViewGroupsViewController") as? ViewGroupsViewController
        self.navigationController?.pushViewController(selectedEventVC!, animated: true)
    }
    @IBAction func onCreateGroupButton(_ sender: Any) {
        if processTypes == "UPDATE GROUP"{
            let param = ["volunteer_list":self.selectedVolunteerIds.joined(separator: ","),
                         "group_id":updateEventName]
            
            let serviceHandler = ServiceHandlers()
            serviceHandler.updateVolunteerGroup(params: param){(responce,isSuccess) in
                if isSuccess{
                    self.processTypes = ""
                    self.selectedVolunteerIds = [String]()
                    let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Group updated successfully", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { (action) in
                        let mainSB = UIStoryboard(name: "Main", bundle: nil)
                        let selectedEventVC =  mainSB.instantiateViewController(withIdentifier: "ViewGroupsViewController") as? ViewGroupsViewController
                        self.navigationController?.pushViewController(selectedEventVC!, animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else{
                    let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Group not updated", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
        }else{
        if self.selectedVolunteerIds.count == 0{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please select volunteer", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alert,animated: true)
        }else{
            self.groupPopUpBackground.isHidden = false
        }
        }
    }
    @IBAction func onCheckAllVolunteerBtn(_ sender: Any) {
        checkAllVolunteerBtn.isSelected = !checkAllVolunteerBtn.isSelected
        if checkAllVolunteerBtn.isSelected{
        for i in 0..<filteredData.count {
            if (filteredData.count != 0) {
                let data = filteredData[i] as? Dictionary<String,Any>
                self.selectedVolunteerIds.append((data!["user_id"] as? String)!)
            }
        }
        }else{
            self.selectedVolunteerIds = [String]()
        }
        for i in 0..<checkVolunteerDic.count {
            checkVolunteerDic[i] = checkAllVolunteerBtn.isSelected ? true : false
        }
        self.selectedVolunteerIds = Array(Set(self.selectedVolunteerIds))
        self.tblView.reloadData()
    }
    @objc func checkButtonClicked(sender:UIButton)
    {
        print(sender.tag)
        if sender.isSelected{
            sender.isSelected = false
            if (filteredData.count != 0) {
                let data = filteredData[sender.tag] as? Dictionary<String,Any>
                if self.selectedVolunteerIds.contains((data!["user_id"] as? String)!){
                    self.selectedVolunteerIds.removeAll(where: { $0 == data!["user_id"] as? String })
                }
            }
        }else{
            sender.isSelected = true
            if (filteredData.count != 0) {
                let data = filteredData[sender.tag] as? Dictionary<String,Any>
                if !(self.selectedVolunteerIds.contains((data!["user_id"] as? String)!)){
                    self.selectedVolunteerIds.append((data!["user_id"] as? String)!)
                }
            }
        }
        self.selectedVolunteerIds = Array(Set(self.selectedVolunteerIds))
//        if self.selectedVolunteerIds.count == filteredData.count{
//            checkAllVolunteerBtn.isSelected = true
//        }
    }
    func profile_pic()  {
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profilePicture.image = image
                self.profilePicture.layer.borderWidth = 1
                self.profilePicture.layer.masksToBounds = false
                self.profilePicture.layer.borderColor = APP_BLACK_COLOR.cgColor
                self.profilePicture.layer.cornerRadius = self.profilePicture.frame.height/2
                self.profilePicture.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
    }
    func getList() {
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let params = userIDData["user_id"] as! String
        
        // Do any additional setup after loading the view.
        
        let servicehandler = ServiceHandlers()
        servicehandler.searchEvent(data: params){(responce,isSuccess)in
            if isSuccess{
                
                if(responce == nil){
                    
                    let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found", comment: ""), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                    self.present(alert,animated: true)
                    self.tblView.delegate = nil
                    self.tblView.dataSource = nil
                    //self.tblView.isHidden = true
                    self.filteredData.removeAll()
                    self.tblView.setContentOffset(.zero, animated: true)
                }else{
                    
                    self.listDetails = responce as! Array<Any>
                    for i in 0...self.listDetails.count{
                        self.checkVolunteerDic.insert(false, at: i)
                    }
                    print(self.listDetails)
                    print(self.checkVolunteerDic)
                    self.filteredData.removeAll()
                    if self.listDetails.count > 0 {
                        self.filteredData = self.listDetails
                        self.tblView.delegate = self
                        self.tblView.dataSource = self
                        //self.tblView.isHidden = false
                        self.tblView.reloadData()
                        self.tblView.setContentOffset(.zero, animated: true)
                        
                    }else{
                        self.tblView.delegate = nil
                        self.tblView.dataSource = nil
                        self.filteredData.removeAll()
                        self.tblView.setContentOffset(.zero, animated: true)
                        // self.tblView.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found", comment: ""), preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert,animated: true)
                        
                    }
                }
            }else{
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Error occured!", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert,animated: true)
            }
            
        }
        
    }
    func chatClicked(selectedRow:Int){
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userEmail = userIDData["user_email"] as! String
        var userFinalName = String()
        if let userFName = userIDData["user_f_name"] as? NSString {
            userFinalName = userFName as String
            if let userLName = userIDData["user_l_name"] as? NSString {
                userFinalName = userFinalName + " " + (userLName as String)
            }
        }
        
        
        ActivityLoaderView.startAnimating()
        SBDMain.connect(withUserId: userEmail) { (user, error) in
            guard error == nil else {   // Error.
                return
                    ActivityLoaderView.stopAnimating()
            }
            
            ActivityLoaderView.stopAnimating()
            SBDGroupChannel.createChannel(withName: userFinalName, isDistinct: true, userIds: [ userEmail ], coverUrl: nil, data: userEmail, customType: nil, completionHandler: { (groupChannel, error) in
                guard error == nil else {   // Error.
                    return
                }
                let vc = GroupChannelChatViewController.init(nibName: "GroupChannelChatViewController", bundle: nil)
                vc.channel = groupChannel
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            })
        }
    }
    func unlinkClicked(selectedRow:Int){
        
        
        
        let unlinkFollowersAlert = UIAlertController(title: "Alert!", message: NSLocalizedString("Do you want to unlink?", comment: ""), preferredStyle: UIAlertController.Style.alert)
        
        unlinkFollowersAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action: UIAlertAction!) in
            self.unlinkFollowers(dataToUnlink: self.filteredData[selectedRow] as! [String : Any])
        }))
        
        unlinkFollowersAlert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { (action: UIAlertAction!) in
            //print("Handle Cancel Logic here")
        }))
        
        present(unlinkFollowersAlert, animated: true, completion: nil)
        
    }
    func unlinkFollowers(dataToUnlink:[String:Any]){
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        
        
        
        let params = ["cso_id":userIDData["user_id"],"vol_id":dataToUnlink["user_id"] as! String,
                      "user_device":UIDevice.current.identifierForVendor!.uuidString]
        
        let servicehandler = ServiceHandlers()
        servicehandler.UnlinkFollowers(params: params) { (responce, isSuccess) in
            if isSuccess{
                self.getList()
                
                
            }else{
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Error occured!", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.tblView.delegate = nil
                self.tblView.dataSource = nil
                self.present(alert, animated: true)
            }
            
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.filteredData.count == 0{
            return 0
        }else{
            return filteredData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "cell") as! SeeFollowersTableViewCell
        if (filteredData.count != 0) {
            let data = filteredData[indexPath.row] as? Dictionary<String,Any>
            
            
            cell.backgroundColor =  APP_WHITE_COLOR
            cell.StudentName.textColor = APP_BLACK_COLOR
            cell.emailLAbel.textColor = APP_BLACK_COLOR
            cell.NumberLabel.textColor = APP_BLACK_COLOR
            cell.AttendedHrsLbn.textColor = APP_BLACK_COLOR
            cell.AverageRateView.backgroundColor = .clear
            cell.lightStarView.isHidden = false
            
            
            cell.name.textColor = APP_BLACK_COLOR
            cell.email.textColor = APP_BLACK_COLOR
            cell.rankAverage.textColor = APP_BLACK_COLOR
            cell.attendRating.textColor = APP_BLACK_COLOR
            cell.phone.textColor = APP_BLACK_COLOR
            cell.attendhour.textColor = APP_BLACK_COLOR
            if processTypes == "UPDATE GROUP"{
                if self.editGroupMemberDetail.contains((data!["user_email"] as? String)!){
                    checkVolunteerDic.insert(true, at: indexPath.row)
                    self.selectedVolunteerIds.append((data!["user_id"] as? String)!)
                }
            }
            self.selectedVolunteerIds = Array(Set(self.selectedVolunteerIds))
            
            if let fullRatingString = data!["user_avg_rating"] as? String {
                let yourInt1 = Float(fullRatingString)                        // converting String to Int
                var rates = String(format: "%.0f", yourInt1!)             // rounding off the rate
                let fullRatingArr = rates.components(separatedBy: ".")
                var firstRating: String = fullRatingArr[0]
                cell.lightStarView.rating = Double(firstRating)!
            }
            // }
            let student_f_name = data!["user_f_name"] as! String
            let student_l_name = data!["user_l_name"] as! String
            let std_name:String = "\(student_f_name)  \(student_l_name)"
            cell.StudentName.text = std_name as String
            var Phone = self.formattedNumber(number:(data!["user_phone"] as? String)!)
            //print(Phone)
            cell.NumberLabel.text = Phone
            cell.NumberLabel.isEnabled = true
            cell.delegate = self
            
            cell.AverageRateView.isUserInteractionEnabled = false
            cell.lightStarView.isUserInteractionEnabled = false
            
            
            cell.RankImage.image = UIImage(named: findingAverageRank(user_hours: data!["user_hours"] as! String , user_hours_req: data!["user_hours_req"] as! String))
            
            cell.emailLAbel.text = data!["user_email"] as? String
            cell.AttendedHrsLbn.text = (data!["user_hours"] as! String + " Hrs")
            let rank_img = self.findRankImages(rank: data!["user_rank"] as! String)
            
            cell.checkButton.isSelected = checkVolunteerDic[indexPath.row]!
            cell.checkButton.tag = indexPath.row
            cell.checkButton.addTarget(self, action: #selector(checkButtonClicked(sender:)), for: .touchUpInside)
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 277.0
        
    }
    
    func findingAverageRank(user_hours:String , user_hours_req:String)->String{
        
        
        
        var percent = (Int(user_hours)!) * (Int(user_hours_req)!)/100
        //print(percent)
        
        
        if percent >= 0 && percent <= 20 {
            
            return "rank_five.png"    //risenshine
        }else
        if percent >= 21 && percent <= 40 {
            
            return "rank_four.png"    //cacke.png
        }else
        if percent >= 41     && percent <= 60  {
            
            return "rank_three.png"   //coll.png
        }else
        if percent >= 61 && percent <= 80{
            
            return "rank_two.png"    //truck.png
        }else
        if percent >= 81 && percent <= 100{
            
            return "rank_one.png"    // cloud.png
        }else
        if percent >= 100 {
            
            return "rank_one.png"   // cloud.png
        }
        return "rank_five.png"
    }
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XXX) XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    @IBAction func button1(_ sender: Any) {
        processTypes = ""
        for i in 0..<self.checkVolunteerDic.count {
            self.checkVolunteerDic[i] = false
        }
        self.selectedVolunteerIds = [String]()
        self.tblView.reloadData()
        
        self.viewGroupButton.isHidden = false
        self.createGroupButton.setTitle("Create Group", for: .normal)
        
        
        VolunteerLabel.isHidden = true//reetesh
        seeFollowersLabels.isHidden = false
        view.sendSubviewToBack(button2view)
        
        button1vol.setTitleColor(APP_BLACK_COLOR, for: .normal)
        button2vol.setTitleColor(.gray, for: .normal)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        filteredData = searchText.isEmpty ? listDetails : listDetails.filter { (($0 as AnyObject)["user_f_name"] as! String).localizedCaseInsensitiveContains(searchText) }
        tblView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    @IBAction func button2(_ sender: Any) {
        
        self.processTypes = ""
        VolunteerLabel.isHidden = false
        seeFollowersLabels.isHidden = true
        
        view.bringSubviewToFront(button2view)
        
        button1vol.setTitleColor(.gray, for: .normal)
        button2vol.setTitleColor(APP_BLACK_COLOR, for: .normal)
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let obj = sb.instantiateViewController(withIdentifier: "request") as! CSORequest
        obj.strShowClose = "NO"
        obj.view.frame.size.height = button2view.frame.size.height
        obj.screen = "VolunteerSeeFollowers"
        obj.willMove(toParent: self)
        button2view.addSubview(obj.view)
        self.addChild(obj)
        obj.didMove(toParent: self)
        
        self.view.layoutIfNeeded()
    }
    
    func findRankImages(rank:String)->String{
        var r = Int(rank)
        var srank:String = ""
        switch r {
        case 1:
            srank = "cloud.png"   //cloud,rank_one.png
            break
        case 2:
            srank = "truck.png"    //truck,rank_two.png
            break
        case 3:
            srank = "coll.png"    //cucumber,rank_three.png
            break
        case 4:
            srank = "cacke.png"    //cake,rank_four.png
            break
        case 5:
            srank = "risenshine.png"    // sun rise,rank_five.png
            break
        default:
            srank = "risenshine.png"     //sun rise,rank_five.png
        }
        return srank
    }
    
}


extension volunteerSeeFollowers:UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let csoDasboardVC = viewController as? CSODashboardViewController {
            removeAllOtherViewsOfVC(viewcontroller: csoDasboardVC)
            return true
        }
        if let csoEventVC = viewController as? CSOEventsViewController {
            
            removeAllOtherViewsOfVC(viewcontroller: csoEventVC)
            
            
            return true
        }
        if let csoEventVC = viewController as? volunteerSeeFollowers {
            button1vol.setTitleColor(APP_BLACK_COLOR, for: .normal)
            button2vol.setTitleColor(.gray, for: .normal)
            view.sendSubviewToBack(button2view)
            removeAllOtherViewsOfVC(viewcontroller: csoEventVC)
            return true
        }
        if let csoEventVC = viewController as? CSOMessagingViewController {
            removeAllOtherViewsOfVC(viewcontroller: csoEventVC)
            return true
        }
        if let csoEventVC = viewController as? LockerViewController {
            removeAllOtherViewsOfVC(viewcontroller: csoEventVC)
            return true
        }
        return true
    }
    
    
    func removeAllOtherViewsOfVC(viewcontroller:UIViewController)  {
        
        for vc in viewcontroller.children {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
    }
}
