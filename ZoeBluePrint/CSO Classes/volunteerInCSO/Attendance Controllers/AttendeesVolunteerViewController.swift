//
//  AttendeesVolunteerViewController.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 02/04/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit
import SideMenu
class AttendeesVolunteerViewController: UIViewController {
    @IBOutlet var volTableView: UITableView!
    @IBOutlet var screenTitle: UILabel!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var bannerImageView: UIImageView!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var tableHeight: NSLayoutConstraint!
    var volInfoData = NSArray()
    var shiftData:Dictionary<String,Any>?
    var eventDetailsData = [String:Any]()
    var shiftID = String()
    var attendaceView = AttendaceForumVC()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.volTableView.register(UINib.init(nibName: "GroupMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "GroupMemberTableViewCell")
        self.volTableView.delegate = self
        self.volTableView.dataSource = self
        self.volTableView.tableFooterView = UIView()
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.bannerImageView.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
        profile_pic()
        self.getVolunteerData()
    }
    @IBAction func closeView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func profile_pic()  {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profileImageView.image = image
                self.profileImageView.layer.borderWidth = 1
                self.profileImageView.layer.masksToBounds = false
                self.profileImageView.layer.borderColor = APP_BLACK_COLOR.cgColor
                self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
                self.profileImageView.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
        
    }
    func getVolunteerData(){
        self.volInfoData =  NSArray()
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let user_id = userIDData["user_id"] as! String
        let param = ["user_id":user_id]
        let serviceHandler = ServiceHandlers()
        serviceHandler.getShiftAttendeesVolunteerList(params: param){(responce,isSuccess) in
            if isSuccess{
                self.shiftID = self.shiftData!["shift_id"] as! String
                self.volInfoData = responce as? NSArray ?? []
                let resultPredicate = NSPredicate(format: "shift_id contains[c] %@", self.shiftID)
                self.volInfoData = self.volInfoData.filtered(using: resultPredicate) as NSArray
                
                self.setTableViewHeight()
            }else{
                self.volTableView.reloadData()
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    @IBAction func onClickNotification(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    @IBAction func onClickMenuButton(_ sender: Any){
        if let menu = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "volsidemenu") as? SideMenuNavigationController{
//            self.navigationController?.present(menu, animated: true, completion: nil)
        present(menu, animated: true, completion: nil)
        }
    }
    func setTableViewHeight(){
        
        if self.volInfoData.count == 0{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found!", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { (action) in
//                self.view.removeFromSuperview()
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
//        self.tableHeight.constant = CGFloat(self.volInfoData.count * 66)
//        if (self.tableHeight.constant + 50) > (self.view.frame.height*0.8) {
//            self.tableHeight.constant = (self.view.frame.height*0.8) - 50.0
//        }
        self.volTableView.reloadData()
    }

}

extension AttendeesVolunteerViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.volInfoData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:GroupMemberTableViewCell = self.volTableView.dequeueReusableCell(withIdentifier: "GroupMemberTableViewCell") as! GroupMemberTableViewCell
        let dic = self.volInfoData[indexPath.row] as! NSDictionary
        cell.nameLbl.text = (dic["user_f_name"] as? String)! + " " + (dic["user_l_name"] as? String)!
        cell.memberCountLbl.text = (dic["user_email"] as? String)!
        cell.deleteButton.isHidden = true
        cell.checkButton.isSelected = (dic["map_status"] as? String)! == "70" ? true : false
        cell.horizentalStack.isHidden = true
        cell.checkButton.tag = indexPath.row
        cell.checkButton.addTarget(self, action: #selector(takeGroupMemberAttendance(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66.0
    }
    
    @objc func takeGroupMemberAttendance(sender:UIButton)
    {
        if !sender.isSelected{
            sender.isSelected = true
            self.takeShiftAttendaceFunction(index: sender.tag)
        }
    }
    
    func takeShiftAttendaceFunction(index: Int) {
       
        if (self.eventDetailsData["event_status"] as! String) == "20"{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Please publish event first", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true)
            return
        }
        if attendaceView.view == nil{
            
        }else{
            let dic = self.volInfoData[index] as! NSDictionary
            attendaceView = AttendaceForumVC.init(nibName: "AttendaceForumVC", bundle: nil)
            attendaceView.view.frame = self.view.frame
            attendaceView.shiftData = self.shiftData
            attendaceView.shiftDetailsData = self.eventDetailsData
            attendaceView.processType = "TAKE ATTENDANCE EDIT"
            attendaceView.editProcessData = NSMutableDictionary.init(dictionary: dic)
            attendaceView.cancelButton.addTarget(self, action: #selector(onCloseAttendaceView), for: .touchUpInside)
            attendaceView.submitButton.addTarget(self, action: #selector(onSubmitAttendaceView), for: .touchUpInside)
//            self.view.addSubview(attendaceView.view)
            self.navigationController!.pushViewController(self.attendaceView, animated: true)
        }
    }
    @objc func onSubmitAttendaceView() {
        if attendaceView.view == nil{
            return
        }
//        if attendaceView.startTimeBtn.titleLabel?.text != "Select Start Time" &&  attendaceView.endTimeBtn.titleLabel?.text != "Select End Time"{
        attendaceView.ResgisterVolunteer()
//        }else{
            ActivityLoaderView.startAnimating()
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
//                ActivityLoaderView.stopAnimating()
//                self.attendaceView.view.removeFromSuperview()
//            }
//        }
    }
    
    @objc func onCloseAttendaceView() {
        if attendaceView.view == nil{
            return
        }
        attendaceView.navigationController?.popViewController(animated: true)
        self.volTableView.reloadData()
        
    }
}
