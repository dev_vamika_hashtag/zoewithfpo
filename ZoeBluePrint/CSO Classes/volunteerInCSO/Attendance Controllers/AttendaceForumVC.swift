//
//  AttendaceForumVC.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 31/03/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit
import Toaster
import SideMenu
class AttendaceForumVC: UIViewController, UITextFieldDelegate,FloatRatingViewDelegate {
    @IBOutlet var segmentView: UISegmentedControl!
    @IBOutlet var addVolunteerDOBView: UIStackView!
    @IBOutlet var addVolunterrMobileNumberView: UIStackView!
    @IBOutlet var addVolunteerEmailView: UIStackView!
    @IBOutlet var addVolunteerNameView: UIStackView!
    @IBOutlet var searchVolunteerNameView: UIStackView!
    
    @IBOutlet var listTableView: UITableView!
    
    @IBOutlet var bannerImageView: UIImageView!
    @IBOutlet var profileImageView: UIImageView!
    
    @IBOutlet var segBottomView1: UIView!
    @IBOutlet var segBottomView2: UIView!
    
    @IBOutlet var searchVolunteerEmailView: UIStackView!
    @IBOutlet var volSearchName: UITextField!
    @IBOutlet var volSearchEmail: UITextField!
    @IBOutlet var volFirstName: UITextField!
    @IBOutlet var volLastName: UITextField!
    @IBOutlet var volEmail: UITextField!
    @IBOutlet var volPhoneNumber: UITextField!
    @IBOutlet var volComment: UITextField!
    @IBOutlet var volDobBtn: UIButton!
    @IBOutlet var startTimeBtn: UIButton!
    @IBOutlet var endTimeBtn: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var dobIcon: UIImageView!
    
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var volRateView: FloatRatingView!
    var volAge = String()
    var mapID = String()
    var volID = String()
    var processType = String()
    var shiftData:Dictionary<String,Any>?
    var shiftDetailsData = [String:Any]()
    var activeVolunteerList = NSArray()
    var filterVolunteerList = NSArray()
    var isSearching = false
    
    var editProcessData = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUnderLineToTF()
        let segAttributesNormal: NSDictionary = [
            NSAttributedString.Key.foregroundColor: UIColor.lightGray,
            NSAttributedString.Key.font: UIFont(name: (self.titleLbl.font.fontName), size: 16)!
        ]
        segmentView.setTitleTextAttributes(segAttributesNormal as? [NSAttributedString.Key : Any], for: .normal)
        
        let segAttributes: NSDictionary = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: (self.titleLbl.font.fontName), size: 16)!
        ]
        segmentView.setTitleTextAttributes(segAttributes as? [NSAttributedString.Key : Any], for: .selected)
        self.segmentView.setBackgroundImage(UIImage(named: "white_bg.jpg"), for: .normal, barMetrics: .default)
        self.segmentView.setBackgroundImage(UIImage(named: "white_bg.jpg"), for: .selected, barMetrics: .default)
        if #available(iOS 13.0, *) {
            self.segmentView.selectedSegmentTintColor = .clear
//            self.segmentView.layer.backgroundColor = UIColor.white.cgColor
        } else {
            // Fallback on earlier versions
        }
    }
   
    func setUnderLineToTF(){
        volEmail.setUnderLineOfColor(color: APP_BLACK_COLOR)
        volComment.setUnderLineOfColor(color: APP_BLACK_COLOR)
        volLastName.setUnderLineOfColor(color: APP_BLACK_COLOR)
        volFirstName.setUnderLineOfColor(color: APP_BLACK_COLOR)
//        volSearchName.setUnderLineOfColor(color: APP_BLACK_COLOR)
        volSearchEmail.setUnderLineOfColor(color: APP_BLACK_COLOR)
        volPhoneNumber.setUnderLineOfColor(color: APP_BLACK_COLOR)
        
        volDobBtn.setUnderLineForView(color: APP_BLACK_COLOR)
        startTimeBtn.setUnderLineForView(color: APP_BLACK_COLOR)
        endTimeBtn.setUnderLineForView(color: APP_BLACK_COLOR)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.bannerImageView.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
        profile_pic()
        if processType == "TAKE ATTENDANCE EDIT"{
            self.volID = editProcessData["user_id"] as! String
            self.getVolunteerDetailWithShift()
            
            segBottomView1.isHidden = true
            segBottomView2.isHidden = false
            self.segmentView.selectedSegmentIndex = 1
            self.listTableView.isHidden = true
            
            self.addVolunteerDOBView.isHidden = true
            self.addVolunteerNameView.isHidden = true
            self.addVolunteerEmailView.isHidden = true
            self.addVolunterrMobileNumberView.isHidden = true
            
            self.dobIcon.isHidden = true
            self.searchVolunteerNameView.isHidden = false
            self.searchVolunteerEmailView.isHidden = false
            
            
            self.volSearchName.isUserInteractionEnabled = false
            self.segmentView.isUserInteractionEnabled = false
            self.volSearchName.text = (editProcessData["user_f_name"] as? String)! + " " + (editProcessData["user_l_name"] as? String)!
            self.volSearchEmail.text = (editProcessData["user_email"] as? String)!
            
        }
        else
        {
        self.volSearchName.isUserInteractionEnabled = true
        self.segmentView.isUserInteractionEnabled = true
            
        isSearching = false
        
        segBottomView1.isHidden = true
        segBottomView2.isHidden = true
        
        listTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        listTableView.isHidden = true
        listTableView.delegate = self
        listTableView.dataSource = self
        
        volRateView.delegate = self
        self.volPhoneNumber.delegate = self
        self.volSearchName.delegate = self
        self.segmentView.selectedSegmentIndex = 0
        self.searchVolunteerNameView.isHidden = true
        self.searchVolunteerEmailView.isHidden = true
        segBottomView1.isHidden = false
        }

    }
    func profile_pic()  {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profileImageView.image = image
                self.profileImageView.layer.borderWidth = 1
                self.profileImageView.layer.masksToBounds = false
                self.profileImageView.layer.borderColor = APP_BLACK_COLOR.cgColor
                self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
                self.profileImageView.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
        
    }
    func getVolunteerDetailWithShift(){
//        getMapVolunteerShiftData
        let param = ["user_id" : self.volID,
                     "event_id":self.shiftDetailsData["event_id"],
                     "shift_id":self.shiftData!["shift_id"]]
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.getMapVolunteerShiftData(params: param) { (responce, isSuccess) in
            if isSuccess {
                    if let data = responce {
                        let arr = data  as! NSDictionary
                        let dic = arr["res_data"] as! NSDictionary
                        self.mapID = dic["map_id"] as! String
                        if dic["attend_in_time"] as? String != nil{
                            self.startTimeBtn.setTitle(Global.convertTimeIn12HoursFormate(strTime:(dic["attend_in_time"] as? String)!), for: .normal)
                            self.startTimeBtn.titleLabel?.textColor = APP_BLACK_COLOR
                        }
                        if  dic["attend_out_time"] as? String != nil{
                            self.startTimeBtn.setTitle(Global.convertTimeIn12HoursFormate(strTime:(dic["attend_out_time"] as? String)!), for: .normal)
                            self.endTimeBtn.titleLabel?.textColor = APP_BLACK_COLOR
                        }
                        if dic["map_rank_comment"] as? String != nil {
                            self.volComment.text = dic["map_rank_comment"] as? String
                        }
                        if dic["attend_rank"] as? String != nil{
                            self.volRateView.rating = Double(dic["attend_rank"] as! String) ?? 0.0
                        }
                        
                    }
            }else{
                
            }
        }
    }
    @IBAction func onClickNotification(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    @IBAction func onClickMenuButton(_ sender: Any){
        if let menu = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "volsidemenu") as? SideMenuNavigationController{
//            self.navigationController?.present(menu, animated: true, completion: nil)
        present(menu, animated: true, completion: nil)
        }
    }
    @IBAction func onClickCloseButton(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func selectDateOfBirth(_ sender: Any) {
        view.endEditing(true)
        let now = Date()
        let calendar = Calendar.current
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let date = Calendar.current.date(byAdding: .year, value: 1, to: Date())!
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let endDate = formatter.string(from: date)
        let edate = formatter.date(from: endDate)
        
        var dateComponent = DateComponents()
        let yearsToAdd = -200
        dateComponent.year = yearsToAdd
        let startDate = Calendar.current.date(byAdding: dateComponent, to: date)

        let dateSelectionPicker = DateSelectionViewController(startDate: startDate, endDate:  edate)
        dateSelectionPicker.view.frame = self.view.frame
        dateSelectionPicker.view.layoutIfNeeded()
        dateSelectionPicker.captureSelectDateValue(sender, inMode: .date) { (selectedDate) in
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            let dateString = formatter.string(from: selectedDate)
            (sender as AnyObject).setTitle(dateString, for:.normal)
            (sender as AnyObject).setImage(nil, for: .normal)
            (sender as AnyObject).setTitleColor(APP_BLACK_COLOR, for: .normal)
            let ageComponents = calendar.dateComponents([.year, .month, .day], from: selectedDate, to: now)
            self.volAge = "\(ageComponents.year!) year \(ageComponents.month!) month \(ageComponents.day!) days"
        }
        addViewController(viewController: dateSelectionPicker)
    }
    @IBAction func endTime(_ sender: Any) {
        let dateSelectionPicker = DateSelectionViewController(startDate: nil, endDate:  nil)
        dateSelectionPicker.view.frame = self.view.frame
        dateSelectionPicker.view.layoutIfNeeded()
        dateSelectionPicker.captureSelectDateValue(sender, inMode: .time) { (selectedDate) in
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            let dateString = formatter.string(from: selectedDate)
            (sender as AnyObject).setTitle(dateString, for:.normal)
            (sender as AnyObject).setImage(nil, for: .normal)
            (sender as AnyObject).setTitleColor(APP_BLACK_COLOR, for: .normal)
        }
        addViewController(viewController: dateSelectionPicker)
    }
    func addViewController(viewController:UIViewController)  {
        viewController.willMove(toParent: self)
        self.view.addSubview(viewController.view)
        self.addChild(viewController)
        viewController.didMove(toParent: self)
    }
    @IBAction func startTime(_ sender: Any) {
        let dateSelectionPicker = DateSelectionViewController(startDate: nil, endDate:  nil)
        dateSelectionPicker.view.frame = self.view.frame
        dateSelectionPicker.view.layoutIfNeeded()
        dateSelectionPicker.captureSelectDateValue(sender, inMode: .time) { (selectedDate) in
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            let dateString = formatter.string(from: selectedDate)
            (sender as AnyObject).setTitle(dateString, for:.normal)
            (sender as AnyObject).setImage(nil, for: .normal)
            (sender as AnyObject).setTitleColor(APP_BLACK_COLOR, for: .normal)
        }
        addViewController(viewController: dateSelectionPicker)
    }
    @IBAction func selectOption(_ sender: Any) {
        if self.segmentView.selectedSegmentIndex == 0{
            self.listTableView.isHidden = true
            segBottomView1.isHidden = false
            segBottomView2.isHidden = true
            
            self.searchVolunteerNameView.isHidden = true
            self.searchVolunteerEmailView.isHidden = true
            self.dobIcon.isHidden = false
            
            self.addVolunteerDOBView.isHidden = false
            self.addVolunteerNameView.isHidden = false
            self.addVolunteerEmailView.isHidden = false
            self.addVolunterrMobileNumberView.isHidden = false
        }else{
            segBottomView1.isHidden = true
            segBottomView2.isHidden = false
            
            self.addVolunteerDOBView.isHidden = true
            self.addVolunteerNameView.isHidden = true
            self.addVolunteerEmailView.isHidden = true
            self.addVolunterrMobileNumberView.isHidden = true
            
            self.dobIcon.isHidden = true
            self.searchVolunteerNameView.isHidden = false
            self.searchVolunteerEmailView.isHidden = false
            
            self.getAllVolunteerList()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.volPhoneNumber{
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = newString.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne {
                formattedString.append("1 ")
                index += 1
            }
            if (length - index) > 3 {
                let areaCode = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("(%@)", areaCode)
                index += 3
            }
            if length - index > 3 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", prefix)
                index += 3
            }
            
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
            
        }
        else if textField == self.volSearchName{
            self.listTableView.isHidden = false
            isSearching = true
            if string != ""{
                let resultPredicate = NSPredicate(format: "user_name contains[c] %@", textField.text! + string)
            self.filterVolunteerList = self.activeVolunteerList.filtered(using: resultPredicate) as NSArray
                if filterVolunteerList.count > 0{
                    self.listTableView.reloadData()
                }else{
                    self.listTableView.isHidden = true
                }
            }else{
                self.listTableView.isHidden = true
            }
        }
       
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == self.volSearchName{
            isSearching = false
            self.listTableView.isHidden = true
        }
        
        return true
    }
    
    func validation() -> Bool{
        if self.segmentView.selectedSegmentIndex == 0{
            if self.volFirstName.text == ""{
                let alert = UIAlertController(title: NSLocalizedString("Oops!", comment: ""), message: "Please enter first name", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
//                let toast = Toast(text: "Please Enter First Name", delay: Delay.short, duration: Delay.long)
//                toast.show()
                return false
            }
            else if self.volLastName.text == ""{
                let alert = UIAlertController(title: NSLocalizedString("Oops!", comment: ""), message: "Please enter last name", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
//                let toast = Toast(text: "Please Enter Last Name", delay: Delay.short, duration: Delay.long)
//                toast.show()
                return false
            }
            else if self.volEmail.text == ""{
                let alert = UIAlertController(title: NSLocalizedString("Oops!", comment: ""), message: "Please enter email address", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
//                let toast = Toast(text: "Please Enter Email Address", delay: Delay.short, duration: Delay.long)
//                toast.show()
                return false
            }
            else if self.volPhoneNumber.text == ""{
                let alert = UIAlertController(title: NSLocalizedString("Oops!", comment: ""), message: "Please enter phone number", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
//                let toast = Toast(text: "Please Enter Phone Number", delay: Delay.short, duration: Delay.long)
//                toast.show()
                return false
            }
            else if self.volDobBtn.titleLabel?.text == "Select Date of Birth"{
                let alert = UIAlertController(title: NSLocalizedString("Oops!", comment: ""), message: "Select date of birth", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
//                let toast = Toast(text: "Select Date of Birth", delay: Delay.short, duration: Delay.long)
//                toast.show()
                return false
            }
            else if !(Global.isValidUserName(text: self.volEmail.text!) )
            {
                let alert = UIAlertController(title: NSLocalizedString("Oops!", comment: ""), message: "Please enter correct email", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
//                let toast = Toast(text: "Please Enter Correct Email", delay: Delay.short, duration: Delay.long)
//                toast.show()
                return false
            }
            else if !(Global.isValidPhoneNumber(text: self.volPhoneNumber.text!) )
            {
                let alert = UIAlertController(title: NSLocalizedString("Oops!", comment: ""), message: "Please enter correct phone number", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
//                let toast = Toast(text: "Please Enter Correct Phone Number", delay: Delay.short, duration: Delay.long)
//                toast.show()
                return false
            }
            return true
        }else{
            if self.volSearchEmail.text == ""{
                let alert = UIAlertController(title: NSLocalizedString("Oops!", comment: ""), message: "Please enter first name", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
//                let toast = Toast(text: "Please Enter First Name", delay: Delay.short, duration: Delay.long)
//                toast.show()
                return false
            }
            else if self.volSearchName.text == ""{
                let alert = UIAlertController(title: NSLocalizedString("Oops!", comment: ""), message: "Please enter last name", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
//                let toast = Toast(text: "Please Enter Last Name", delay: Delay.short, duration: Delay.long)
//                toast.show()
                return false
            }
            return true
        }
    }
    @objc func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double){
     print("didUpdate")
     print(rating)
    }
    
    @objc func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double){
        print("isUpdating")
        print(rating)
    }
    func getAllVolunteerList(){
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.getSearchVolunteerList() { (responce, isSuccess) in
            if isSuccess {
                if let data = responce {
                    let arr = data  as! NSDictionary
                    self.activeVolunteerList = arr["res_data"]  as! NSArray
                    self.listTableView.reloadData()
                }
            }else{
                
            }
        }
    }
    fileprivate func showPopoverForView(view:UITextField, contents:Any) {
        let controller = DropDownItemsTable(contents)
        controller.showPopoverInDestinationVC(destination: self, sourceView: view as! UIView) { (selectedValue) in
            if let selectVal = selectedValue as? String {
               let index = controller.index(ofAccessibilityElement: view)
            }
        }
    }
    func ResgisterVolunteer(){
        if validation(){
            // //print(self.user_gender)
            if self.segmentView.selectedSegmentIndex == 0{
                let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
                let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
                let params = [
                    "user_type":"VOL",
                    "user_device":UIDevice.current.identifierForVendor!.uuidString,
                    "school_id":"",
                    "user_f_name":self.volFirstName.text!,
                    "user_l_name":self.volLastName.text!,
                    "user_email":self.volEmail.text!,
                    "user_phone":self.volPhoneNumber.text!,
                    "user_country":"",
                    "user_state":"",
                    "user_city":"",
                    "user_zipcode":"",
                    "user_address":"",
                    "user_dob":self.volDobBtn.titleLabel?.text,
                    "user_gender":"",
                    "parent_guardian_name": "",
                    "number_for_text": "",
                    "school_grade": "",
                    "age": self.volAge == "" ? "" : self.volAge,
                    "user_pass":userIDData["generic_password"] as? String == "" ? "Test@123" : userIDData["generic_password"] as! String]
                let serivehandlers = ServiceHandlers()
                serivehandlers.csoRegistrationStage1(data: params as Dictionary<String, Any>){(responce,isSuccess) in
                    if isSuccess {
                        let cso_id = userIDData["user_id"] as! String
                        
                        let data = responce as! Dictionary<String,Any>
                        self.volID = data["user_id"] as? String ?? ""
                        let params = ["shift_id":self.shiftData!["shift_id"],
                                      "user_id":data["user_id"] as? String,
                                      "user_type":"VOL",
                                      "user_device":UIDevice.current.identifierForVendor!.uuidString,
                                      "event_id":self.shiftDetailsData["event_id"],
                                      "cso_id":cso_id] as [String : Any]
                        self.callEventRequestApi(param: params, isNewUser: true)
                    }else{
                        let msg = responce as? String
                        let alert = UIAlertController(title: "Alert!", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true)
                    }
                    
                }
            }else{
                self.callSeachVolunteerSubmitForumAPI()
            }
        }
    }
    func callSeachVolunteerSubmitForumAPI(){
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let cso_id = userIDData["user_id"] as! String
        
        let params = ["shift_id":self.shiftData!["shift_id"],
                     "user_id":self.volID,
                     "user_type":"VOL",
                     "user_device":UIDevice.current.identifierForVendor!.uuidString,
                     "event_id":self.shiftDetailsData["event_id"],
                     "cso_id":cso_id] as [String : Any]
        self.callEventRequestApi(param: params, isNewUser: false)
    }
    func callEventRequestApi(param: [String: Any] , isNewUser: Bool){
        
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.sendEventNotificationToGroupMembers(params: param) { (responce, isSuccess) in
            if isSuccess {
                if isNewUser{
                    //sendWelcomeEmail
                    if let data = responce {
                        if self.processType != "TAKE ATTENDANCE EDIT"{
                            let arr = data  as! NSDictionary
                            let dic = arr["res_data"] as! NSDictionary
                            self.mapID = dic["map_id"] as! String
                        }
                        self.callWelcomeAPI(params: param)
                    }
                    
                }else{
                    
                    if self.processType != "TAKE ATTENDANCE EDIT"{
                        if let data = responce {
                        let arr = data  as! NSDictionary
                        let dic = arr["res_data"] as! NSDictionary
                            if (dic["map_id"] as? String)?.isEmpty != nil {
                                self.mapID = dic["map_id"] as! String
                            }else{
                                self.getVolunteerDetailWithShift()
                            }
                        
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(150)) {
                            if !(self.startTimeBtn.titleLabel?.text == "Select Start Time") && !(self.endTimeBtn.titleLabel?.text == "Select End Time"){
                                self.UpdateHours()
                            }else{
                                self.callRequestStatusAPI(mapStatus: "20")
                            }
                            }
                    
                    }else{
                        self.UpdateHours()
                    }
                }
            }else{
                
            }
        }
    }
    func callWelcomeAPI(params: [String: Any]){
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let gen_pswd = userIDData["generic_password"] as! String
        let parameter = ["user_id":params["user_id"],
                         "password":gen_pswd]
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.sendWelcomeEmail(params: parameter as [String : Any]) { (responce, isSuccess) in
            if isSuccess {
                if !(self.startTimeBtn.titleLabel?.text == "Select Start Time") && !(self.endTimeBtn.titleLabel?.text == "Select End Time"){
                    self.UpdateHours()
                }else{
                    self.callRequestStatusAPI(mapStatus: "20")
                }
            }else{
                
            }
        }
    }
    func UpdateHours(){
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let user_id = userIDData["user_id"] as! String
        let device = UIDevice.current.identifierForVendor!.uuidString
        let InTime = Global.convertTimeIn24HoursFormate(strTime:(self.startTimeBtn.titleLabel?.text!)!)
        let OutTime = Global.convertTimeIn24HoursFormate(strTime:(self.endTimeBtn.titleLabel?.text!)!)
       
        
        let params = ["user_id":user_id,
                      "user_type":"VOL",
                      "user_device":device,
                      "map_id":self.mapID,
                      "attend_in_time":InTime,
                      "attend_out_time":OutTime,
                      "map_status":"40"
        ]
        //print(params)
        let serviceHanlder = ServiceHandlers()
        
        serviceHanlder.TimeFilledData(params: params) { (responce, isSuccess) in
            if isSuccess {
                self.ChangingRank()
                
            }else{
                
                
            }
        }
    }
    func ChangingRank(){
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let userID = userIDData["user_id"] as! String
        
        let params = ["user_id":userID ,
                      "user_type":userIDData["user_type"] as! String,
                      "user_device":UIDevice.current.identifierForVendor!.uuidString ,
                      "vol_id": self.volID,
                      "attend_rank":Int(volRateView.rating),
                      "map_id":self.mapID,
                      "map_rank_comment":self.volComment.text == "" ? "" : self.volComment.text!
        ] as [String : Any?]
        print(params)
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.changeRankinVol(data:params) { (responce, isSuccess) in
            if isSuccess {
                self.changeHoursMethod()
            }
        }
    }
    
    func changeHoursMethod(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        var difference = String()
        if !(self.startTimeBtn.titleLabel?.text == "Select Start Time") && !(self.endTimeBtn.titleLabel?.text == "Select End Time"){
        let date1 = formatter.date(from: (self.startTimeBtn.titleLabel?.text)!)!
        let date2 = formatter.date(from: (self.endTimeBtn.titleLabel?.text)!)!
        
        let calendar = Calendar.current
            difference = Global.getTimeDifferenceBetweenTimes(sTime: self.startTimeBtn.titleLabel!.text!, eTime : self.endTimeBtn.titleLabel!.text!)
        }
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let user_id = userIDData["user_id"] as! String
//        let mapID = shiftDetailsData["map_id"] as! String
        let params = ["user_id":user_id,
                      "user_type":userIDData["user_type"] as! String,
                      "user_device":UIDevice.current.identifierForVendor!.uuidString,
                      "map_id":self.mapID,
                      "attend_hours": difference == nil ? "0" : difference,
                      "vol_id":self.volID
        ] as [String : Any]
        print(params)
        
        let servicehandler = ServiceHandlers()
        servicehandler.ChangeHours(data:params) { (responce, isSuccess) in
            if isSuccess{
             
                self.callRequestStatusAPI(mapStatus: "70")
                // call for change request
                // self.volunterData()
                
            }
        }
    }
    func callRequestStatusAPI(mapStatus: String){
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let user_id = userIDData["user_id"] as! String
        let user_device = UIDevice.current.identifierForVendor!.uuidString
        let data_dict = ["user_id":user_id,
                         "user_type":userIDData["user_type"] as! String,
                         "user_device":user_device,
                         "map_id":self.mapID,
                         "map_status":mapStatus,
                         "map_status_comment":self.volComment.text == "" ? "" : self.volComment.text!
        ] as [String : Any]
        let serviceHanlder = ServiceHandlers()
        serviceHanlder.csoChangeRequestStatus(data_dict:data_dict ) { (responce, isSuccess) in
            if isSuccess {
                if mapStatus == "20"{
                    let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Request marked successfully!", comment: ""), preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) in
//                            self.view.removeFromSuperview()
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else{
                let alert = UIAlertController(title: "Success!" , message: NSLocalizedString("Attendance marked successfully!", comment: ""), preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) in
//                        self.view.removeFromSuperview()
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
            }
        }
    }
}

extension AttendaceForumVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return self.filterVolunteerList.count
        }
            return self.activeVolunteerList.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell:UITableViewCell = (self.listTableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?)!
            var dic = NSDictionary()
            if isSearching{
                dic =  self.filterVolunteerList[indexPath.row] as! NSDictionary
            }else{
                dic =  self.activeVolunteerList[indexPath.row] as! NSDictionary
            }
            
            cell.textLabel?.text = dic["user_name"] as? String
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            var dic = NSDictionary()
            if isSearching{
                dic =  self.filterVolunteerList[indexPath.row] as! NSDictionary
            }else{
                dic =  self.activeVolunteerList[indexPath.row] as! NSDictionary
            }
            self.volSearchEmail.text = dic["user_email"] as? String
            self.volSearchName.text = dic["user_name"] as? String
            self.volID = dic["user_id"] as? String ?? ""
            self.listTableView.isHidden = true
            isSearching = false
        }
}
