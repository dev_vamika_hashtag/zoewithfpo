//
//  ViewGroupTableViewCell.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 25/03/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit

class ViewGroupTableViewCell: UITableViewCell {
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var memberCountLbl: UILabel!
    @IBOutlet var editButton: UIButton!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var viewButton: UIButton!
    
    @IBOutlet var checkBoxButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
