//
//  GroupMemberTableViewCell.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 25/03/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit

class GroupMemberTableViewCell: UITableViewCell {
    @IBOutlet var horizentalStack: UIStackView!
    @IBOutlet var headingStack: UIStackView!
    @IBOutlet var valueStack: UIStackView!
    @IBOutlet var grpNameLbl: UILabel!
    @IBOutlet var grpEmailLbl: UILabel!
    @IBOutlet var checkButton: UIButton!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var memberCountLbl: UILabel!
    @IBOutlet var deleteButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
