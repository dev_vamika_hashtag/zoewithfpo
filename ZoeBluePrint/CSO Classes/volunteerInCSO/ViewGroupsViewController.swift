//
//  ViewGroupsViewController.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 25/03/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit
protocol ViewGroupsViewControllerDelegate: class {
    func sendSelectedGroupIds(ids: [String])
}
class ViewGroupsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet var memBerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var groupTableView: UITableView!
    @IBOutlet weak var groupMemberTableView: UITableView!
    @IBOutlet weak var groupMemberBgView: UIView!
    @IBOutlet weak var groupMemberHeadingLbl: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var associateButton: UIButton!
    @IBOutlet var bottomBtnHeight: NSLayoutConstraint!
    var delegate :  ViewGroupsViewControllerDelegate!
    var gourpInfoData :  Array<Any>?
    var groupMemberInfoData :  Array<Any>?
    var selctedGroupIds =  [String]()
    var alreadySelctedGroupIds =  [String]()
    var processType = String()
    var shiftData:Dictionary<String,Any>?
    override func viewDidLoad() {
        super.viewDidLoad()
        getGroupData()
        self.groupMemberBgView.isHidden = true
        self.groupTableView.register(UINib.init(nibName: "ViewGroupTableViewCell", bundle: nil), forCellReuseIdentifier: "ViewGroupTableViewCell")
        self.groupMemberTableView.register(UINib.init(nibName: "GroupMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "GroupMemberTableViewCell")
        
        self.groupTableView.tableFooterView = UIView()
        self.groupMemberTableView.tableFooterView = UIView()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        groupMemberBgView.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        profile_pic()
        getBannerImage()
        if processType == "selection"{
            bottomBtnHeight.constant = 50
            let gId = shiftData!["associated_vol_group_ids"] as! String
            if gId != "0"{
                alreadySelctedGroupIds = gId.components(separatedBy: ",")
            }
        }else{
            bottomBtnHeight.constant = 0
        }
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.groupMemberBgView.isHidden = true
    }
    @IBAction func obMenuClick(_ sender: UIButton) {
        
    }
    @IBAction func onCalcelSelectionClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func onAssociateSelectionClick(_ sender: UIButton) {
        let set2 = Set(alreadySelctedGroupIds)
        selctedGroupIds = selctedGroupIds.filter { !set2.contains($0) }
        self.delegate.sendSelectedGroupIds(ids: self.selctedGroupIds)
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func onNotificatioClick(_ sender: UIButton) {
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    func getBannerImage(){
        self.bannerImageView.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
    }
    func profile_pic()  {
        
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let params = userIDData["user_id"] as! String
        let serivehandler = ServiceHandlers()
        serivehandler.editProfile(user_id: params){(responce,isSuccess) in
            if isSuccess{
                let data = responce as! Dictionary<String,Any>
                let string_url = data["user_profile_pic"] as! String
                if let url = URL(string: string_url){
                    
                    do {
                        DispatchQueue.global().async {
                            if let imageData = try? Data(contentsOf: url)  {
                                
                                self.saveImageInDocsDir(dataImage: imageData)
                                DispatchQueue.main.async {
                                    self.profileImage.image = UIImage(data: imageData)
                                    self.profileImage.layer.borderWidth = 1
                                    self.profileImage.layer.masksToBounds = false
                                    self.profileImage.layer.borderColor = APP_BLACK_COLOR.cgColor
                                    self.profileImage.layer.cornerRadius = self.profileImage.frame.height/2
                                    self.profileImage.clipsToBounds = true
                                }
                            }//make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                            
                        }
                    }
                }
            }
        }
        
    }
    func saveImageInDocsDir(dataImage: Data ) {
        
        if (dataImage != nil) {
            // get the documents directory url
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            // choose a name for your image
            let fileName = "profilepic.jpg"
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            do {
                // writes the image data to disk
                try dataImage.write(to: fileURL, options: Data.WritingOptions.atomic)
                print("file saved")
                print(fileURL)
            } catch {
                print("error saving file:", error)
            }
            
        }
        
        
    }
    func getGroupData(){
        self.gourpInfoData = Array<Any>()
        let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
        let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
        let org_id = userIDData["org_id"] as! String
        let param = ["user_id":org_id]
        
        let serviceHandler = ServiceHandlers()
        serviceHandler.getAllVolunteerGroups(params: param){(responce,isSuccess) in
            if isSuccess{
                self.gourpInfoData = responce as? Array<Any>
                self.groupTableView.delegate = self
                self.groupTableView.dataSource = self
                self.groupTableView.reloadData()
                self.associateButton.alpha = 1.0
                self.associateButton.isUserInteractionEnabled = true
            }else{
                self.groupTableView.reloadData()
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.associateButton.alpha = 0.5
                self.associateButton.isUserInteractionEnabled = false
            }
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == groupMemberTableView{
            return self.groupMemberInfoData?.count ?? 0
        }
        return self.gourpInfoData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == groupTableView{
            let cell:ViewGroupTableViewCell = self.groupTableView.dequeueReusableCell(withIdentifier: "ViewGroupTableViewCell") as! ViewGroupTableViewCell
            let dic =  self.gourpInfoData![indexPath.row] as! NSDictionary
            cell.nameLbl.text = dic["group_name"] as? String
            cell.memberCountLbl.text = dic["total_volunteer"] as? String
            if processType == "selection"{
                cell.editButton.isHidden = true
                cell.deleteButton.isHidden = true
                cell.viewButton.isHidden = true
                cell.checkBoxButton.isHidden = false
                if selctedGroupIds.contains((dic["id"] as? String)!){
                    cell.checkBoxButton.isSelected = true
                    cell.checkBoxButton.isUserInteractionEnabled = true
                    if alreadySelctedGroupIds.contains((dic["id"] as? String)!){
                        cell.checkBoxButton.isUserInteractionEnabled = false
                        cell.checkBoxButton.isSelected = true
                    }
                }else{
                    cell.checkBoxButton.isSelected = false
                    cell.checkBoxButton.isUserInteractionEnabled = true
                    if alreadySelctedGroupIds.contains((dic["id"] as? String)!){
                        cell.checkBoxButton.isSelected = true
                        cell.checkBoxButton.isUserInteractionEnabled = false
                    }
                }
            }else{
                cell.checkBoxButton.isHidden = true
                cell.editButton.isHidden = false
                cell.deleteButton.isHidden = false
                cell.viewButton.isHidden = false
            }
            cell.checkBoxButton.tag = indexPath.row
            cell.editButton.tag = indexPath.row
            cell.deleteButton.tag = indexPath.row
            cell.viewButton.tag = indexPath.row
            cell.editButton.addTarget(self, action: #selector(editButtonClicked(sender:)), for: .touchUpInside)
            cell.deleteButton.addTarget(self, action: #selector(deleteButtonClicked(sender:)), for: .touchUpInside)
            cell.viewButton.addTarget(self, action: #selector(viewDeatilButtonClicked(sender:)), for: .touchUpInside)
            cell.checkBoxButton.addTarget(self, action: #selector(selectGroups(sender:)), for: .touchUpInside)
            return cell
        }
        let cell:GroupMemberTableViewCell = self.groupMemberTableView.dequeueReusableCell(withIdentifier: "GroupMemberTableViewCell") as! GroupMemberTableViewCell
        let dic =  self.groupMemberInfoData![indexPath.row] as! NSDictionary
        cell.grpNameLbl.text = dic["user_name"] as? String
        cell.grpEmailLbl.text = dic["user_email"] as? String
        cell.checkButton.isHidden = true
        cell.valueStack.isHidden = true
        cell.headingStack.isHidden = true
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteGroupMember(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == groupMemberTableView{
            return 66.0
        }
        return 44.0
    }
    @objc func selectGroups(sender:UIButton)
    {
        //        alreadyAssociatedIds = [String]()
        if selctedGroupIds.count == 0{
            let dic =  self.gourpInfoData![sender.tag] as! NSDictionary
            if selctedGroupIds.contains(dic["id"] as! String){
                selctedGroupIds.removeAll(where: {$0 == dic["id"] as! String})
            }else{
                selctedGroupIds.append(dic["id"] as! String)
            }
            self.groupTableView.reloadData()
        }else{
            let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("You can select only one group at a time", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    @objc func deleteGroupMember(sender:UIButton)
    {
        let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Are you sure you want to delete the group member?", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertAction.Style.default, handler: { (action) in
            let dic =  self.groupMemberInfoData![sender.tag] as! NSDictionary
            let param = ["id":dic["id"]]            
            let serviceHandler = ServiceHandlers()
            serviceHandler.deleteVolunteerGroup(params: param as [String : Any]){(responce,isSuccess) in
                if isSuccess{
                    self.groupMemberInfoData?.remove(at: sender.tag)
                    self.groupMemberTableView.reloadData()
                    self.setMemberViewHeight()
                }else{
                    let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No data found!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @objc func editButtonClicked(sender:UIButton)
    {
        let dic =  self.gourpInfoData![sender.tag] as! NSDictionary
        let param = ["group_id":dic["id"]]
        let serviceHandler = ServiceHandlers()
        serviceHandler.getVolunteerGroupMembers(params: param){(responce,isSuccess) in
            if isSuccess{
                let data = responce as? Array<Any>
                var sendData = [String]()
                for i in 0...data!.count - 1{
                    let dic = data![i] as? Dictionary<String,Any>
                    let email = dic!["user_email"] as? String
                    sendData.append(email!)
                }
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                
                for aViewController in viewControllers {
                    if aViewController  is volunteerSeeFollowers {
                        let vc = aViewController  as? volunteerSeeFollowers
                        vc?.editGroupMemberDetail = sendData
                        vc?.processTypes = "UPDATE GROUP"
                        vc?.updateEventName = dic["id"] as! String
                        self.navigationController!.popToViewController(aViewController, animated: true)
                    }
                }
            }else{
                
            }
            
        }
    }
    @objc func deleteButtonClicked(sender:UIButton)
    {
        let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Are you sure you want to delete the group?", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertAction.Style.default, handler: { (action) in
            
            let dic =  self.gourpInfoData![sender.tag] as! NSDictionary
            let param = ["id":dic["id"] as? String]
            let serviceHandler = ServiceHandlers()
            serviceHandler.deleteVolunteerGroup(params: param){(responce,isSuccess) in
                if isSuccess{
                    self.getGroupData()
                }else{
                    let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Something went wrong!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    @objc func viewDeatilButtonClicked(sender:UIButton)
    {
        let dic =  self.gourpInfoData![sender.tag] as! NSDictionary
        let param = ["group_id":dic["id"]]
        let serviceHandler = ServiceHandlers()
        serviceHandler.getVolunteerGroupMembers(params: param){(responce,isSuccess) in
            if isSuccess{
                self.groupMemberInfoData = responce as? Array<Any>
                self.groupMemberTableView.delegate = self
                self.groupMemberTableView.dataSource = self
                self.groupMemberTableView.reloadData()
                self.groupMemberBgView.isHidden = false
                self.setMemberViewHeight()
            }else{
                self.groupMemberBgView.isHidden = true
                self.groupMemberInfoData =  Array<Any>()
                let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("No member found in this group!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    func setMemberViewHeight(){
        self.memBerViewHeight.constant = CGFloat(((self.groupMemberInfoData?.count ?? 0) * 66 ) + 100)
        if self.memBerViewHeight.constant > self.view.frame.height * 0.8{
            self.memBerViewHeight.constant = self.view.frame.height * 0.8
        }
    }
}
