//
//  GPSTableViewCell.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 10/03/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit

class GPSTableViewCell: UITableViewCell {
    @IBOutlet weak var inOrOutLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setValuesInCell(data: NSDictionary){
        inOrOutLabel.text = data[""] as? String
        dateLabel.text = data[""] as? String
        timeLabel.text = data[""] as? String
    }
}
