//
//  BackgroungCheckForEventVC.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 18/06/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import UIKit
protocol BackgroungCheckForEventVCDelegate: class {
    func chooseFieldsWith(dic: [String:String])
}
class BackgroungCheckForEventVC: UIViewController {
    var delegate :  BackgroungCheckForEventVCDelegate!
    @IBOutlet var nameBtn: UIButton!
    @IBOutlet var phoneNumBtn: UIButton!
    @IBOutlet var emailBtn: UIButton!
    @IBOutlet var dobBtn: UIButton!
    @IBOutlet var addressBtn: UIButton!
    @IBOutlet var parentBtn: UIButton!
    var fieldsDic = [String:String]()
    var viewDic : [String:String]!
    override func viewDidLoad() {
        super.viewDidLoad()
//        fieldsDic = {"name_check" : "1","email_check": "1","phone_check" : "1"}
//        fieldsDic["name_check"] = "0"
//        fieldsDic["email_check"] = "0"
//        fieldsDic["phone_check"] = "0"
//        fieldsDic["address_check"] = "0"
//        fieldsDic["dob_check"] = "0"
//        fieldsDic["parent_check"] = "0"
        if viewDic != nil{
        for (key, value) in viewDic {
            print("\(key) -> \(value)")
            if key == "name_check"{
                self.nameBtn.isSelected = true
                fieldsDic["name_check"] = "1"
            }
            if key == "email_check"{
                self.emailBtn.isSelected = true
                fieldsDic["email_check"] = "1"
            }
            if key == "phone_check"{
                self.phoneNumBtn.isSelected = true
                fieldsDic["phone_check"] = "1"
            }
            if key == "dob_check"{
                self.dobBtn.isSelected = true
                fieldsDic["dob_check"] = "1"
            }
            if key == "address_check"{
                self.addressBtn.isSelected = true
                fieldsDic["address_check"] = "1"
            }
            if key == "parent_check"{
                self.parentBtn.isSelected = true
                fieldsDic["parent_check"] = "1"
            }
        }
        }
    }

    @IBAction func onCheckFields(_ sender: UIButton) {
        if !sender.isSelected{
            sender.isSelected = true
        switch sender.tag {
        case 1:
            fieldsDic["name_check"] = "1"
            break
        case 2:
            fieldsDic["email_check"] = "1"
            break
        case 3:
            fieldsDic["phone_check"] = "1"
            break
        case 4:
            fieldsDic["dob_check"] = "1"
            break
        case 5:
            fieldsDic["address_check"] = "1"
            break
        case 6:
            fieldsDic["parent_check"] = "1"
            break
        default:
            break
        }
        }else{
            sender.isSelected = false
            switch sender.tag {
            case 1:
                fieldsDic["name_check"] = "0"
                break
            case 2:
                fieldsDic["email_check"] = "0"
                break
            case 3:
                fieldsDic["phone_check"] = "0"
                break
            case 4:
                fieldsDic["dob_check"] = "0"
                break
            case 5:
                fieldsDic["address_check"] = "0"
                break
            case 6:
                fieldsDic["parent_check"] = "0"
                break
            default:
                break
            }
        }
    }
    

    @IBAction func onSave(_ sender: Any) {
        if fieldsDic.count != 0 {
            self.delegate.chooseFieldsWith(dic: fieldsDic)
            self.view.removeFromSuperview()
        }
    }
    @IBAction func onClose(_ sender: Any) {
        self.view.removeFromSuperview()
    }
}
