//
//  CSOAddEventViewController.swift
//  ZoeBlue//print
//
//  Created by Rishi Chaurasia on 21/07/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit
import Alamofire
import CropViewController
import CoreLocation
import WebKit
public enum UIButtonBorderSide {
    case  Bottom
}
protocol refreshData {
    func refreshDataList(flagr:Bool)
}

class CSOAddEventViewController: UIViewController,UINavigationControllerDelegate,UIDocumentPickerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,LatLongdata,UIWebViewDelegate,XMLParserDelegate,BackgroungCheckForEventVCDelegate {
    
     
    @IBOutlet weak var closeButton_webView: UIButton!
    
    var listTimeZone: [[String:Any]]!
    var data_refresh:refreshData?
    @IBOutlet weak var webV: WKWebView!
    
    @IBOutlet weak var lblEventType: UILabel!
    
    
    
    @IBOutlet weak var startEndTimeView: UIView!
    @IBOutlet weak var startEndView: UIView!
    
    @IBOutlet weak var stateLbl: UILabel!
    
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var lblZoneTime: UILabel!
    @IBOutlet weak var backgroundView_webView: UIView!
    
    @IBOutlet weak var waiverViewHeight: NSLayoutConstraint!
    @IBOutlet weak var waiverViewTwoHeight: NSLayoutConstraint!
    @IBOutlet weak var waiverViewThreeHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewFieldBtn: UIButton!
    @IBOutlet weak var signDocYesButton: UIButton!
    @IBOutlet weak var signDocNoButton: UIButton!
    
    @IBOutlet weak var radioButtonYes: UIButton!
    @IBOutlet weak var radioButtonNo: UIButton!
    
    @IBOutlet weak var repeatCountTF: UITextField!
    @IBOutlet weak var daysOptionButton: UIButton!
    @IBOutlet weak var checkBackgroundVerification: UIButton!
    
    var myeventview:UIView?
    var typeList: [[String:Any]]?
    var stateCode:String?
    var bdCheckDic:[String:String]?
    var stateName = String()
    private var croppingStyle = CropViewCroppingStyle.default
    
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
   
    let imagePicker = UIImagePickerController()
    
    
@IBAction func showMap(_ sender: Any) {
    
    if(self.stateName != ""){
         let mainSB = UIStoryboard(name: "Main", bundle: nil)
        if let mapVC = mainSB.instantiateViewController(withIdentifier: "mapview") as? Map {
            UserDefaults.standard.set("open", forKey: "map")
            mapVC.state_code = self.stateCode
            mapVC.state_name = self.stateName
            mapVC.city = self.txtFldCity.text
            mapVC.country = self.btnCountrySel.titleLabel?.text
            mapVC.longitu = self.lang
             mapVC.latitu = self.lat
            mapVC.delegate = self
            self.present(mapVC, animated: true, completion: nil)
        
        }
        
    }else{
        UserDefaults.standard.set("close", forKey: "map")
        let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Select state", comment: ""), preferredStyle: UIAlertController.Style.alert)
                                             
                                                         // add an action (button)
                                                         alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                                             // show the alert
                                                         self.present(alert, animated: true, completion: nil)
    }
    
    }
    
    var sDate = String ()
    var eDate = String()
    
    
    @IBOutlet weak var lblEvent: UILabel!
    
    @IBOutlet weak var lblTimezone: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    
    @IBOutlet weak var lblStartDateBottomLine: UILabel!
    @IBOutlet weak var imageStartDate: UIImageView!
    @IBOutlet weak var lblEndDateBottomLine: UILabel!
    @IBOutlet weak var imageEndDate: UIImageView!
    
    @IBOutlet weak var startEndBG: UIView!
    
    @IBOutlet weak var imageDrop: UIImageView!
    
    @IBOutlet weak var imageDrop2: UIImageView!
    @IBOutlet weak var endStartTimeBG: UIView!
    
    @IBOutlet weak var imageDrop3: UIImageView!
    
    @IBOutlet weak var imageDrop4: UIImageView!
    
    @IBOutlet weak var lblWaiver: UILabel!
    
    
    @IBOutlet weak var imageStartTime: UIImageView!
    @IBOutlet weak var lblStartTimeBottom: UILabel!
    
    @IBOutlet weak var imageEndTime: UIImageView!
    @IBOutlet weak var lblEndtimeBottomLine: UILabel!
    
    
    
    @IBOutlet weak var lblHaveWaiverDocument: UILabel!
    
    @IBOutlet weak var btnStartDate: UIButton!
    @IBOutlet weak var btnEndDate: UIButton!
    @IBOutlet weak var btnStartTime: UIButton!
    @IBOutlet weak var btnEndTime: UIButton!
    @IBOutlet weak var btnAddUpdate: UIButton!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnViewOnMap: UIButton!
    @IBOutlet weak var btnEndDateSel: UIButton!
    @IBOutlet weak var btnStartDateSel: UIButton!
    @IBOutlet weak var txtFldEmail: UITextField!
    @IBOutlet weak var txtFldPhone: UITextField!
    @IBOutlet weak var txtFldAddress: UITextField!
    @IBOutlet weak var btnTimeZoneSel: UIButton!
    @IBOutlet weak var txtFldPostalcode: UITextField!
    @IBOutlet weak var txtFldCity: UITextField!
    @IBOutlet weak var btnStateSel: UIButton!
    @IBOutlet weak var btnCountrySel: UIButton!
    @IBOutlet weak var txtFldEventDescription: UITextField!
    @IBOutlet weak var txtfldEventName: UITextField!
    @IBOutlet weak var btnEventSelection: UIButton!
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mapview: UIImageView!
    var selectbutton:UIButton?
    var myeventbtn:UIButton?
    var myEventview:UIView?
    var  tabbarCnt :  UITabBarController!
    var waiver:String?
    var screenTitle = String ()
    var eventDetail = [String : Any] ()
    var StartTime:String = ""
    var EndTime:String = ""
    var check = false
    var neweventbutton:UIButton?
    var lang:String = ""
    var lat:String = ""
    
    var eventTypeId:String = ""
    var countryID:String = ""
    var stateID:String = ""
    var timeZoneID:String = ""
    var imgName:String = ""
    var fileName:String = ""
    var eventIdFromServer:String = ""
    var waiverfileupload:Bool?
    var req:String?
    var edit_event_data:Dictionary<String,Any>?
    
    @IBOutlet weak var waiverDocument: UIView!
    
    @IBOutlet weak var waiverText1: UILabel!
    
    @IBOutlet weak var waiverResetButtonOutlet: UIButton!
    @IBOutlet weak var waiverText2: UILabel!
    
    @IBOutlet weak var DetailLabel: UILabel!
    
    
    @IBOutlet weak var waiverFileButtonOutlet: UIButton!
    var file:Data?
    var img:Data?
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addUnderLineToField(color: APP_BLACK_COLOR)

        
    }
    
    func addUnderLineToField(color:UIColor)  {
        txtFldCity.setUnderLineOfColor(color: color)
        txtFldEmail.setUnderLineOfColor(color: color)
        txtFldPhone.setUnderLineOfColor(color: color)
        txtFldAddress.setUnderLineOfColor(color: color)
        txtfldEventName.setUnderLineOfColor(color: color)
        txtFldPostalcode.setUnderLineOfColor(color: color)
        txtFldEventDescription.setUnderLineOfColor(color: color)
        repeatCountTF.setLeftPaddingPoints(10)
    }
    
    @IBOutlet weak var checkButtonOutlet: UIButton!
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == self.txtFldPhone){
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = newString.components(separatedBy: NSCharacterSet.decimalDigits.inverted)

            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)

            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int

                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()

            if hasLeadingOne {
                formattedString.append("1 ")
                index += 1
            }
            if (length - index) > 3 {
                let areaCode = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("(%@)", areaCode)
                index += 3
            }
            if length - index > 3 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", prefix)
                index += 3
            }

            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
        }
        if(textField == txtFldPostalcode){     // 25_Feb Prachi
             
                 guard let textFieldText = textField.text,
                           let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                               return false
                       }
                       let substringToReplace = textFieldText[rangeOfTextToReplace]
                       let count = textFieldText.count - substringToReplace.count + string.count
                       return count <= 5
        }
            return true
        }
    
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {

            if controller.documentPickerMode == UIDocumentPickerMode.import {
                   
               }
        
        _ = url as URL //let cico
        
        self.fileName = url.lastPathComponent
        self.DetailLabel.text = url.lastPathComponent
            do{
            self.file = try Data(contentsOf: url)

            } catch {
                       //print(error)
                   }
        self.DetailLabel.text = url.lastPathComponent

    }
    @IBAction func waiverUploadButton(_ sender: Any) {
        
        let alert = UIAlertController(title: NSLocalizedString("UPLOAD FILES FROM", comment: ""), message: "", preferredStyle: .alert)
                      let gallery = UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
                          /** What we write here???????? **/
                        self.waiver = "waiverClassUpload"
                           let image = UIImagePickerController()
                                 image.delegate = self
                                 image.sourceType = UIImagePickerController.SourceType.photoLibrary
                                 image.allowsEditing = false
                         image.modalPresentationStyle = .overFullScreen
                                 self.present(image, animated: true)
                                 {
                                     
                                 }
                          // call method whatever u need
                      })
                      let camera = UIAlertAction(title: NSLocalizedString("Camera", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                 /** What we write here???????? **/
                                 self.waiver = "waiverClassUpload"
                                 let imagePicker = UIImagePickerController()
                                  imagePicker.delegate = self
                                 imagePicker.sourceType = UIImagePickerController.SourceType.camera
                                 imagePicker.allowsEditing = false
                        imagePicker.modalPresentationStyle = .overFullScreen
                                 self.present(imagePicker, animated: true)
                                 {
                                    
                                 }
                                 // call method whatever u need
                             })
               let drive = UIAlertAction(title: NSLocalizedString("Files", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                       /** What we write here???????? **/
                                      let documentPicker = UIDocumentPickerViewController(documentTypes: ["com.apple.iwork.pages.pages", "com.apple.iwork.numbers.numbers", "com.apple.iwork.keynote.key","public.image", "com.apple.application", "public.item","public.data", "public.content", "public.audiovisual-content", "public.movie", "public.audiovisual-content", "public.video", "public.audio", "public.text", "public.data", "public.zip-archive", "com.pkware.zip-archive", "public.composite-content", "public.text"], in: .import)
                                           
                                      documentPicker.delegate = self
                                    documentPicker.modalPresentationStyle = .overCurrentContext
                               self.present(documentPicker, animated: true, completion: nil)
                                       // call method whatever u need
                                   })
                      let noButton = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil)
                      alert.addAction(gallery)
                      alert.addAction(camera)
                      alert.addAction(drive)
                      alert.addAction(noButton)
                      present(alert, animated: true)

    }
    @IBAction func checkButton(_ sender: Any) {
        
        if check == false
        {
            check = true
            self.checkButtonOutlet.isSelected = true
            self.waiverDocument.isHidden = false
            self.waiverText1.isHidden = false
            self.waiverText2.isHidden = false
            self.waiverFileButtonOutlet.isHidden = false
            self.waiverResetButtonOutlet.isHidden = false
            self.waiverViewHeight.constant = 189.5
            self.waiverViewTwoHeight.constant = 60.0
            self.waiverViewThreeHeight.constant = 80.0
            
        }
        else
        {
            self.check = false
            self.checkButtonOutlet.isSelected = false
            self.waiverFileButtonOutlet.setImage(UIImage(named: "downloaddocument"), for: .normal)
            self.waiverDocument.isHidden = true
            self.waiverText1.isHidden = true
            self.waiverText2.isHidden = true
            self.waiverFileButtonOutlet.isHidden = true
            self.waiverResetButtonOutlet.isHidden = true
            self.waiverViewHeight.constant = 0
            self.waiverViewTwoHeight.constant = 0.0
            self.waiverViewThreeHeight.constant = 0.0
        }
        
        
    }
    
    func mapData(lang:String,lat:String,city:String,postal_code:String,address:String){
        self.lang = lang
        self.lat = lat
        self.txtFldPostalcode.text = postal_code
        self.txtFldAddress.text = address
        self.txtFldCity.text = city
       // UserDefaults.standard.set("close", forKey: "map")
    }
    
    @IBAction func selectFileButton(_ sender: Any) {
    }
    
    
    @IBAction func resetButton(_ sender: Any) {
        
        
        self.file = nil
        self.fileName = ""
        self.DetailLabel.text = ""
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//           picker.dismiss(animated: true)
        if (self.waiver == "waiverClassUpload"){
            picker.dismiss(animated: true)
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {

             self.file = (image as? UIImage)!.jpegData(compressionQuality: 0.5)!
             guard let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL
                 else {
                    self.fileName = "File"
                    self.DetailLabel.text = self.fileName
                    return }
             self.fileName = fileUrl.lastPathComponent
                self.DetailLabel.text = fileUrl.lastPathComponent
              // //print(fileName)
            }
        }else if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
           {
            picker.dismiss(animated: true)
            self.imgEvent.image = image
            self.imgEvent.contentMode = .scaleToFill
            self.img = (image).jpegData(compressionQuality: 0.5)!
            guard let fileURL = info[UIImagePickerController.InfoKey.imageURL] as? URL
            else {
                self.imgName = "image1.png"
                return
            }
            
            self.imgName = fileURL.lastPathComponent
      
        }else if let cameraImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            
            let cropViewController = CropViewController(image: cameraImage)
              cropViewController.delegate = self
            cropViewController.modalPresentationStyle = .overFullScreen
            if let fileURL = info[UIImagePickerController.InfoKey.imageURL] as? URL
             {
                self.imgName = fileURL.lastPathComponent
               
                //return
            }else{
                self.imgName = "image1.png"
            }
            
           
            imagePicker.pushViewController(cropViewController, animated: true)
//              present(cropViewController, animated: false, completion: nil)
        
        }else{
            // print("error")
    }

//           picker.dismiss(animated: true, completion: nil)
       }
    
    

    
    @IBAction func startEventTime(_ sender: Any) {
        let dateSelectionPicker = DateSelectionViewController(startDate: nil, endDate:  nil)
        dateSelectionPicker.view.frame = self.view.frame
        dateSelectionPicker.view.layoutIfNeeded()
        dateSelectionPicker.captureSelectDateValue(sender, inMode: .time) { (selectedDate) in
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            let dateString = formatter.string(from: selectedDate)
            self.StartTime = dateString
            (sender as AnyObject).setTitle(dateString, for:.normal)
            (sender as AnyObject).setImage(nil, for: .normal)
        }
        addViewController(viewController: dateSelectionPicker)
        
        
    }
    
    @IBAction func endEventTime(_ sender: Any) {
        let dateSelectionPicker = DateSelectionViewController(startDate: nil, endDate:  nil)
               dateSelectionPicker.view.frame = self.view.frame
               dateSelectionPicker.view.layoutIfNeeded()
               dateSelectionPicker.captureSelectDateValue(sender, inMode: .time) { (selectedDate) in
                   let formatter = DateFormatter()
                   formatter.dateFormat = "hh:mm a"
                   let dateString = formatter.string(from: selectedDate)
                   self.EndTime = dateString
                   (sender as AnyObject).setTitle(dateString, for:.normal)
                   (sender as AnyObject).setImage(nil, for: .normal)
               }
               addViewController(viewController: dateSelectionPicker)
    }
    @IBAction func startEventDate(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let startDate = dateFormatter.date(from: "06-01-2019" )
        let endDate = dateFormatter.date(from: "06-01-2050")
        
        
        let dateSelectionPicker = DateSelectionViewController(startDate: startDate!, endDate:  endDate!)
        dateSelectionPicker.view.frame = self.view.frame
        dateSelectionPicker.view.layoutIfNeeded()
        dateSelectionPicker.captureSelectDateValue(sender, inMode: .date) { (selectedDate) in
            let formatter = DateFormatter()
           // formatter.dateFormat = "dd-MMM-yyyy"
            formatter.dateFormat = "MM-dd-yyyy"
            //08-22-2019
            let dateString = formatter.string(from: selectedDate)
            self.sDate = dateString
            (sender as AnyObject).setTitle(dateString, for:.normal)
            (sender as AnyObject).setImage(nil, for: .normal)
        }
        addViewController(viewController: dateSelectionPicker)
    }
    func addViewController(viewController:UIViewController)  {
           viewController.willMove(toParent: self)
           self.view.addSubview(viewController.view)
           self.addChild(viewController)
           viewController.didMove(toParent: self)
       }
    @IBAction func endEventDate(_ sender: Any) {
        let dateFormatter = DateFormatter()
               dateFormatter.dateFormat = "MM-dd-yyyy"
               let startDate = dateFormatter.date(from: "06-01-2019" )
               let endDate = dateFormatter.date(from: "06-01-2050")
               
               
               let dateSelectionPicker = DateSelectionViewController(startDate: startDate!, endDate:  endDate!)
               dateSelectionPicker.view.frame = self.view.frame
               dateSelectionPicker.view.layoutIfNeeded()
               dateSelectionPicker.captureSelectDateValue(sender, inMode: .date) { (selectedDate) in
                   let formatter = DateFormatter()
                  // formatter.dateFormat = "dd-MMM-yyyy"
                   formatter.dateFormat = "MM-dd-yyyy"
                   //08-22-2019
                   let dateString = formatter.string(from: selectedDate)
                   self.eDate = dateString
                   (sender as AnyObject).setTitle(dateString, for:.normal)
                   (sender as AnyObject).setImage(nil, for: .normal)
               }
               addViewController(viewController: dateSelectionPicker)
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        _ = tapGestureRecognizer.view as! UIImageView//tappedImage

        let alert = UIAlertController(title: NSLocalizedString("Select image from", comment: ""), message: "", preferredStyle: .alert)
        let gallery = UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.imagePicker.allowsEditing = false
            self.imagePicker.modalPresentationStyle = .overFullScreen
            
            self.present(self.imagePicker, animated: true)
                   {
                       
                   }
            // call method whatever u need
        })
        let camera = UIAlertAction(title: NSLocalizedString("Camera", comment: ""), style: .default, handler: { [self](_ action: UIAlertAction) -> Void in
                   /** What we write here???????? **/
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.imagePicker.allowsEditing = false
                self.imagePicker.modalPresentationStyle = .overFullScreen
                self.present(self.imagePicker, animated: true)
                   // call method whatever u need
            }
               })
        let noButton = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil)
        alert.addAction(gallery)
        alert.addAction(camera)
        alert.addAction(noButton)
        present(alert, animated: true)
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //
        self.txtFldPhone.attributedPlaceholder = NSAttributedString(string: "Phone Number*", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        self.txtFldEmail.attributedPlaceholder = NSAttributedString(string: "Email*", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        Global.fixFontWithInWidth(button: btnStartDate)
        Global.fixFontWithInWidth(button: btnEndDate)
        Global.fixFontWithInWidth(button: btnStartTime)
        Global.fixFontWithInWidth(button: btnEndTime)
       let utility = Utility()
       utility.fetchTimeZone { (eventTypeList, isValueFetched) in
           if let list = eventTypeList {
               // self.showPopoverForView(view: sender, contents: list)
               //print(list)
               self.listTimeZone = list
               let decodedUserData  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
                                let userData = NSKeyedUnarchiver.unarchiveObject(with: decodedUserData) as!  Dictionary<String, Any>
               
               if let timeZone = userData["user_timezone"] {
                if ((timeZone as? String )?.isEmpty) == nil
                {
                    self.btnTimeZoneSel.setTitle("Eastern Standard Time", for: .normal)
                    self.timeZoneID = "6"
                }else{
                   var userTimeZone = ""
                   userTimeZone = timeZone as! String
                   for zoneName in self.listTimeZone{
                                          
                   let strZoneCode = zoneName["timezone_code"] as! String
                   let strZoneName = zoneName["timezone_name"] as! String
                   if strZoneCode == userTimeZone {
                   self.btnTimeZoneSel.setTitle(strZoneName, for: .normal)
                    self.timeZoneID = zoneName["timezone_id"] as! String
                                          }
                                      }
               }
               }
                      
                      
               
           }
       }
        self.hideKeyboardWhenTappedAround()

      self.waiverViewHeight.constant = 0
        self.waiverViewTwoHeight.constant = 0.0
        self.waiverViewThreeHeight.constant = 0.0
     /* Stop */
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
       imgEvent.isUserInteractionEnabled = true
       imgEvent.addGestureRecognizer(tapGestureRecognizer)
        
        
       // //print(screenTitle)

        self.txtFldEmail.delegate = self
        self.txtFldPhone.delegate = self
        self.txtFldAddress.delegate = self
        self.txtFldPostalcode.delegate = self
        self.txtFldEventDescription.delegate = self
        self.txtfldEventName.delegate = self
        self.backgroundView_webView.isHidden = true
        
        if screenTitle == "ADD EVENT DETAILS"{
            viewFieldBtn.isHidden = true
            self.lblScreenTitle.text = NSLocalizedString("ADD EVENT DETAILS", comment: "")

                 let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
                                 let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
                                 let user_id = userIDData["user_id"] as! String
                 
                 let serviceHanlder = ServiceHandlers()
                 serviceHanlder.getProfileData1(user_id: user_id) { (data, isSuccess) in
                             if isSuccess{
                              //  //print(data!)
                                let a = data as? Dictionary<String, Any>
                                // //print(a)
                                 
                                 self.txtFldEmail.text = a!["user_email"] as? String
                                 self.txtFldAddress.text = a!["user_address"] as? String
                   
                                 self.txtFldPhone.text =  self.formattedNumber(number:(a!["user_phone"] as? String)!)
                                 self.txtFldPostalcode.text = a!["user_zipcode"] as? String
                               
                                 self.txtFldCity.text = a!["user_city"] as? String
                                 self.btnStateSel.setTitle(a!["user_state_name"] as? String, for: .normal)
                                 self.btnCountrySel.setTitle(a!["user_country_name"] as? String, for: .normal)
                                 self.countryID = (a!["user_country"] as? String)!
                                 self.stateID = (a!["user_state"] as? String)!
                                
                                 let serviceHanlder = ServiceHandlers()
                                 serviceHanlder.getStateList(country_id: self.countryID){(responce,isSuccess) in
                                     if isSuccess{
                                         let state = responce as! Array<Any>
                                         for state_code in state{
                                             let state_code1 = state_code as! Dictionary<String,Any>
                                             if ((state_code1["state_id"] as! String) == (self.stateID )){
                                                 self.stateCode = (state_code1["state_code"] as! String)
                                                 self.stateName = (state_code1["state_name"] as! String)
                                             }
                                         }
                                     }
                                 }
                                self.countryID = (a!["user_country"] as? String)!
                                 self.stateID = (a!["user_state"] as? String)!
                                self.getLocation(from: String(format: "%@, %@, %@", self.txtFldAddress.text!,self.txtFldCity.text!,a!["user_country_name"] as! String))
                     }
                                   
                 }
        }else{
         
         self.lblScreenTitle.text = NSLocalizedString("UPDATE EVENT DETAILS", comment: "")
        }
                       
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XXX)XXX-XXXX"

        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= 100.0
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
//    func stringValueDic(_ str: String) -> [String : Any]?{
//            let data = str.data(using: String.Encoding.utf8)
//            if let dict = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : Any] {
//                return dict
//            }
//            return nil
//        }
    func convertToDictionary(text: String) -> [String: Any]? {
        let newStr = text.replacingOccurrences(of: "\'", with: "\"")
        if let data = newStr.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    override func viewDidAppear(_ animated : Bool) {
        super.viewDidAppear(animated)
        Global.setUpViewWithTheme(ViewController: self)
        if screenTitle == "UPDATE EVENT DETAILS"{
            self.lblScreenTitle.text = NSLocalizedString("UPDATE EVENT DETAILS", comment: "")
            
        }else{
            btnClear.setTitle("Cancel", for: .normal)
            self.lblScreenTitle.text = NSLocalizedString("ADD EVENT DETAILS", comment: "")
            
        }
            
            if(self.screenTitle.caseInsensitiveCompare(NSLocalizedString("UPDATE EVENT DETAILS", comment: "")) == .orderedSame){
                let servicehandler = ServiceHandlers()
                servicehandler.getSelectedEventDetails(eventId: self.eventDetail["event_id"] as! String){ (responce, isSuccess) in
                    if isSuccess {
                        DispatchQueue.main.asyncAfter(deadline: .now()) {
                            ActivityLoaderView.startAnimating()
                        
                        let data_of_event = responce as! Dictionary<String,Any>
                        self.edit_event_data = data_of_event
                        let waiver = data_of_event["event_waiver_req"]
                        self.btnAddUpdate.setTitle(NSLocalizedString("Update", comment: ""), for: .normal)
                        self.btnEndDateSel.setTitle(data_of_event["event_register_end_date"] as? String, for: .normal)
                        self.lat = data_of_event["event_latitude"] as! String
                        self.eventIdFromServer = self.eventDetail["event_id"] as! String
                        
                        self.lang = data_of_event["event_longitude"] as! String
                        
                        self.eDate = data_of_event["event_register_end_date"] as! String
                        self.sDate = data_of_event["event_register_start_date"] as! String
                        self.btnStartDateSel.setTitle(data_of_event["event_register_start_date"] as? String, for: .normal)
                        //            var Start
                        self.btnStartTime.setTitle(data_of_event["event_start_time_format"] as? String, for: .normal)
                        self.StartTime = data_of_event["event_start_time_format"] as! String
                        self.btnEndTime.setTitle(data_of_event["event_end_time_format"] as? String, for: .normal)
                        self.EndTime = data_of_event["event_end_time_format"] as! String
                        self.txtFldEmail.text = data_of_event["event_email"] as? String
                        
                        
                        print(data_of_event)
                        self.txtFldAddress.text = data_of_event["event_address"] as? String ?? "default value"
                        
                        self.txtFldPostalcode.text = data_of_event["event_postcode"] as? String ?? "default value"
                        
                        
                        self.txtFldPhone.text = self.formattedNumber(number:(data_of_event["event_phone"] as? String)!)
                        
                        self.timeZoneID = (data_of_event["event_timezone"] as? String)!
                        self.txtFldCity.text = (data_of_event["event_city"] as? String)
                        self.stateID = data_of_event["event_state"] as! String
                        self.stateName = data_of_event["event_state_name"] as! String
                        self.btnStateSel.setTitle(data_of_event["event_state_name"] as? String, for: .normal)
                        self.btnCountrySel.setTitle(data_of_event["event_country_name"] as? String, for: .normal)
                        self.countryID = (data_of_event["event_country"] as? String)!
                        self.txtFldEventDescription.text = data_of_event["event_details"] as? String
                        self.txtfldEventName.text = data_of_event["event_heading"] as? String
                        self.imgName = (data_of_event["event_image"] as? String)!
                        //getOptionSymbolRealName(optionString:String)
                        //repeat_every_type
                        //repeat_every
                        //want_to_upload_by_volunteer
                            if let isBgCheck = data_of_event["background_check"]{
                                self.checkBackgroundVerification.isSelected = isBgCheck as? String == "1" ? true : false
                            }
//                            if let arry = data_of_event["checked_fields_json"] as? [String:String]{
//                                self.bdCheckDic = arry
//                            }
                            if let dic = data_of_event["checked_fields"]{
                                self.bdCheckDic = [String:String]()
                                self.bdCheckDic = self.convertToDictionary(text: dic as! String) as? [String : String]

                            }
                            
                        if data_of_event["want_to_upload_by_volunteer"] as? String == "yes"{
                            self.radioButtonYes.isSelected = true
                        }else{
                            self.radioButtonNo.isSelected = true
                        }
                        self.repeatCountTF.text =  data_of_event["repeat_every"] as? String
                        self.daysOptionButton.setTitle(self.getOptionSymbolRealName(optionString:(data_of_event["repeat_every_type"] as? String)!), for: .normal)
                        if (self.listTimeZone != nil){
                            for zoneName in self.listTimeZone{
                                
                                let strZoneName = zoneName["timezone_name"] as! String
                                print(strZoneName)
                                
                                if strZoneName == data_of_event["event_timezone_name"]as! String
                                {
                                    self.btnTimeZoneSel.setTitle(strZoneName, for: .normal)
                                }
                                print(data_of_event["event_timezone_name"]as! String)
                                print(strZoneName)
                            }
                        }
                        
                        let fileUrl = URL(string: data_of_event["event_waiver_doc"] as! String)
                        if(fileUrl != nil)
                        {
                            if let imageData = NSData(contentsOf: (fileUrl!)){
                                self.file = imageData as Data
                            }
                            
                        }
                        
                        self.btnEventSelection.setTitle(data_of_event["event_type_name"] as? String, for: .normal)
                        if(waiver as! String == "1"){
                            
                            self.waiverViewHeight.constant = 189.5
                            self.waiverViewTwoHeight.constant = 60.0
                            self.waiverViewThreeHeight.constant = 80.0
                            self.DetailLabel.text = data_of_event["event_waiver_doc_name"] as? String
                            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.TapOnLabel))
                            
                            tapGesture.numberOfTapsRequired = 1
                            tapGesture.numberOfTouchesRequired = 1
                            
                            self.DetailLabel.addGestureRecognizer(tapGesture)
                            self.DetailLabel.isUserInteractionEnabled = true
                            self.DetailLabel.attributedText = NSAttributedString(string: self.DetailLabel.text ?? "", attributes:
                                                                                    [.underlineStyle: NSUnderlineStyle.single.rawValue])
                            
                            self.fileName = data_of_event["event_waiver_doc_name"] as! String
                            self.waiverDocument.isHidden = false
                            self.checkButtonOutlet.isSelected = true
                            self.waiverText1.isHidden = false
                            self.waiverText2.isHidden = false
                            self.waiverFileButtonOutlet.isHidden = false
                            self.waiverResetButtonOutlet.isHidden = false
                        }else{
                            self.waiverViewHeight.constant = 0
                            self.waiverViewTwoHeight.constant = 0.0
                            self.waiverViewThreeHeight.constant = 0.0
                            self.waiverDocument.isHidden = true
                            self.waiverText1.isHidden = true
                            self.waiverText2.isHidden = true
                            self.waiverFileButtonOutlet.isHidden = true
                            self.waiverResetButtonOutlet.isHidden = true
                            self.fileName = ""
                            self.file = nil
                            self.checkButtonOutlet.isSelected = false
                        }
                        
                        if let imageURLstr = data_of_event["event_image"] as? String,
                           let imageURL:URL = URL(string: imageURLstr.replacingOccurrences(of: " ", with: "%20")) {
                            let serviceHanlder = ServiceHandlers()
                            serviceHanlder.getImageFromURL(url: imageURL) { (data, isSuccess) in
                                if let imgData = data as? Data {
                                    DispatchQueue.main.async() { () -> Void in
                                        self.img = imgData
                                        if let image = UIImage(data: imgData) {
                                            self.imgEvent.image = image
                                            self.imgEvent.contentMode = .scaleToFill
                                        }
                                        
                                    }
                                }
                            }
                        }
                        
                        self.eventTypeId = data_of_event["event_type_id"] as! String
                        let servicehandler = ServiceHandlers()
                        servicehandler.getEventType(){(responce,isSuccess) in
                            if isSuccess{
                                let data_event = responce as! Array<Any>
                                for event in data_event{
                                    let d = event as! Dictionary<String,Any>
                                    if (d["event_type_id"] as! String) == self.eventTypeId {
                                        self.btnEventSelection.setTitle(d["event_type_name"] as? String, for: .normal)
                                    }
                                }
                            }
                        }
                        
                        let sv = ServiceHandlers()
                        sv.getStateList(country_id: data_of_event["event_country"] as! String){(responce,isSuccess) in
                            if isSuccess{
                                let data = responce as! Array<Any>
                                for state in data{
                                    ActivityLoaderView.stopAnimating()
                                    let state_n = state as! Dictionary<String,Any>
                                    if(state_n["state_name"] as! String == data_of_event["event_state_name"] as! String)
                                    {
                                        self.stateCode = state_n["state_code"] as? String
                                    }
                                }
                                ActivityLoaderView.stopAnimating()
                            }
                            
                        }
                    }
                    }
                    self.view.setNeedsLayout()
                    self.viewDidLayoutSubviews()
                    self.view.setNeedsUpdateConstraints()
                    self.view.layoutIfNeeded()
                }
                
                
            }
        
    }

    
    
    @objc func TapOnLabel(){
        let wav = self.edit_event_data!["event_waiver_doc"] as! String

        
        ActivityLoaderView.startAnimating()
         let destination = DownloadRequest.suggestedDownloadDestination()

         AF.download(wav, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
             print("Progress: \(progress.fractionCompleted)")
         } .validate().responseData { ( response ) in
             print(response.fileURL!.lastPathComponent)
            ActivityLoaderView.stopAnimating()
            let alert = UIAlertController(title: NSLocalizedString("Download completed!", comment: ""), message: "", preferredStyle: UIAlertController.Style.alert)
                                             
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
         }
       
    }
 
    //MARK:- FUNCTIONS
    @IBAction func selectEventClick(_ sender: Any) {
            // //print("Event Type Selected")
    
        
             let utility = Utility()
             utility.fetchEventTypeList { (eventTypeList, isValueFetched) in
                 if let list = eventTypeList {
                     self.showPopoverForView(view: sender, contents: list)
                 }
             }
         }
         
         
         @IBAction func btnSelectState(_ sender: Any) {
             let utility = Utility()
             utility.fetchStateList{ (eventTypeList, isValueFetched) in
                 if let list = eventTypeList {
                     self.showPopoverForView2(view: sender, contents: list)
                 }
             }
             
         }
       @IBAction func btnSelectCountry(_ sender: Any) {
           
            let utility = Utility()
            utility.fetchCountryList{ (eventTypeList, isValueFetched) in
                if let list = eventTypeList {
                    self.showPopoverForView1(view: sender, contents: list)
                }
            }
            
        }
        @IBAction func timeZoneSelected(_ sender: Any) {
                  let utility = Utility()
                  utility.fetchTimeZone{ (eventTypeList, isValueFetched) in
                      if let list = eventTypeList {
                          self.showPopoverForView3(view: sender, contents: list)
                      }
                  }
                  
              }
    
        
    @IBAction func CrossButton(_ sender: Any) {
    }
    
    @IBAction func TickButton(_ sender: Any) {
    }
   
    fileprivate func showPopoverForView(view:Any, contents:Any) {
            let controller = DropDownItemsTable(contents)
            let senderButton = view as! UIButton
            controller.showPopoverInDestinationVC(destination: self, sourceView: view as! UIView) { (selectedValue) in
                if let selectVal = selectedValue as? String {
                    senderButton.setTitle(selectVal, for: .normal)
                    senderButton.setImage(nil, for: .normal)
                } else if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetEventType.keyEventTypeName] as? String {
                    self.eventTypeId = selectVal[GetEventType.keyEventTypeId] as! String
                    
                    senderButton.setTitle(title, for: .normal)
                    senderButton.setImage(nil, for: .normal)
                }
            }
        }
        fileprivate func showPopoverForView1(view:Any, contents:Any) {
            let controller = DropDownItemsTable(contents)
            let senderButton = view as! UIButton
            controller.showPopoverInDestinationVC(destination: self, sourceView: view as! UIView) { (selectedValue) in
                if let selectVal = selectedValue as? String {
                    senderButton.setTitle(selectVal, for: .normal)
                    senderButton.setImage(nil, for: .normal)
                } else if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetCountryServiceStrings.keyCountryName] as? String {
                    self.countryID = selectVal[GetCountryServiceStrings.keyCountryId] as! String
                    senderButton.setTitle(title, for: .normal)
                    senderButton.setImage(nil, for: .normal)
                }  else if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetStateServiceStrings.keyStateName] as? String {
                    
                    senderButton.setTitle(title, for: .normal)
                    senderButton.setImage(nil, for: .normal)
                }
            }
        }
        fileprivate func showPopoverForView2(view:Any, contents:Any) {
            let controller = DropDownItemsTable(contents)
            let senderButton = view as! UIButton
            controller.showPopoverInDestinationVC(destination: self, sourceView: view as! UIView) { (selectedValue) in
                if let selectVal = selectedValue as? String {
                    senderButton.setTitle(selectVal, for: .normal)
                    senderButton.setImage(nil, for: .normal)
                } else if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetCountryServiceStrings.keyCountryName] as? String {
                    senderButton.setTitle(title, for: .normal)
                    senderButton.setImage(nil, for: .normal)
                }  else if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetStateServiceStrings.keyStateName] as? String {
                    self.stateID = selectVal[GetStateServiceStrings.keyStateId] as! String
                    self.stateCode = selectVal[GetStateServiceStrings.keyStateCode] as? String
                    self.stateName = selectVal[GetStateServiceStrings.keyStateName] as! String
                    senderButton.setTitle(title, for: .normal)
                    senderButton.setImage(nil, for: .normal)
                }
            }
        }
        fileprivate func showPopoverForView3(view:Any, contents:Any) {
            let controller = DropDownItemsTable(contents)
            let senderButton = view as! UIButton
            controller.showPopoverInDestinationVC(destination: self, sourceView: view as! UIView) { (selectedValue) in
                if let selectVal = selectedValue as? String {
                    senderButton.setTitle(selectVal, for: .normal)
                    senderButton.setImage(nil, for: .normal)
                } else if let selectVal = selectedValue as? [String:Any], let title = selectVal[GetTimeZone.timeZoneName] as? String {
                    self.timeZoneID = selectVal[GetTimeZone.timeZoneId] as! String
                    senderButton.setTitle(title, for: .normal)
                    senderButton.setImage(nil, for: .normal)
               }
          
            }
        }
      
    
    
    @IBAction func closeButton(_ sender: Any) {
        self.view.sendSubviewToBack(webV)
        self.backgroundView_webView.isHidden = true
    }
    
    func conver12HoursTo24(timeAsString : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"

        let date = dateFormatter.date(from: timeAsString)
        dateFormatter.dateFormat = "HH:mm"

        let Date24 = dateFormatter.string(from: date!)
        print("24 hour formatted Date:",Date24)
        return Date24
    }
    
    @IBAction func addUpdateEvent(_ sender: Any) {
        
        if lblScreenTitle == neweventbutton{
            
            self.lblScreenTitle.text = NSLocalizedString("ADD EVENT DETAILS", comment: "")
           // //print(self.lblScreenTitle)
        }else{
            self.lblScreenTitle.text = NSLocalizedString("UPDATE EVENT DETAILS", comment: "")
             //print(self.lblScreenTitle)
        }
        
        if(screenTitle.caseInsensitiveCompare("UPDATE EVENT DETAILS") == .orderedSame){
            if(validate()){
               
                if(fileName == ""){
                               req = "0"
                               self.waiverfileupload = false
                           }else{
                               req = "1"
                               self.waiverfileupload = true
                           }
            let serviceHanlder = ServiceHandlers()
               
                if(self.img != nil){
                }else{
                    self.imgName = ""
                }
                
                serviceHanlder.updateEvent(event_id:self.eventIdFromServer,event_type_id:self.eventTypeId ,event_heading:txtfldEventName.text!,event_details:txtFldEventDescription.text!,event_address:txtFldAddress.text!,event_country:self.countryID,event_state:self.stateID,event_city:txtFldCity.text!,event_postcode:txtFldPostalcode.text!,event_timezone:self.timeZoneID,event_latitude:self.lat,event_longitude:self.lang,event_email:txtFldEmail.text!,event_phone:txtFldPhone.text!,event_image:self.imgName,event_register_start_date:self.sDate, event_register_end_date:self.eDate, event_end_time: conver12HoursTo24(timeAsString : self.EndTime), event_start_time: conver12HoursTo24(timeAsString : self.StartTime), event_waiver_doc: self.fileName,event_waiver_req:req!,want_to_upload_by_volunteer:(self.radioButtonYes.isSelected ? "yes" : "No"),repeat_every: (self.repeatCountTF.text == "" ? "0" : self.repeatCountTF.text!) , repeat_every_type: (self.req == "1" ? self.getOptionSymbol(optionString: (self.daysOptionButton.titleLabel?.text!)!) : "" ),background_check: self.checkBackgroundVerification.isSelected ? "1" : "0",checked_fields: self.checkBackgroundVerification.isSelected ? self.bdCheckDic as! [String : String] : [:]) { (responce, isSuccess) in
                 if isSuccess {
                    _ = responce as! [String: Any]//resdata
                 //   //print(resdata)
                    if(self.img != nil){
                                     self.uploadImage()
                }else  if self.waiverfileupload! && (self.file != nil){
                            
                        self.uploadWaiverFile()
                        
                }else{
        self.txtFldEventDescription.text = ""
                    self.txtfldEventName.text = ""
                self.btnTimeZoneSel.setTitle(NSLocalizedString("Select Time Zone", comment: ""), for: .normal)
        self.btnEventSelection.setTitle(NSLocalizedString("Select Event Type", comment: ""), for: .normal)
                        self.timeZoneID = ""
                            self.eventTypeId = ""
            self.btnStartDate.setTitle("", for: .normal)
                    self.sDate = ""
    self.btnEndDate.setTitle("", for: .normal)
                    self.eDate = ""
        self.btnStartTime.setTitle("", for: .normal)
                    self.StartTime = ""
    self.btnEndTime.setTitle("", for: .normal)
        self.EndTime = ""
        self.file = nil
        self.fileName = ""
        self.img = nil
    self.lat = ""
                self.lang = ""
  let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Event added successfully", comment: ""), preferredStyle: UIAlertController.Style.alert)
                                         
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
                    }
                }
                   
            }
                // self.dismiss(animated: true, completion: nil)
            }
            }else if (validate()){
            
            if(fileName == ""){
                req = "0"
                self.waiverfileupload = false
            }else{
                req = "1"
                self.waiverfileupload = true
            }
               
            var phone = txtFldPhone.text!
                if self.lat == "" || self.lang == "" || self.lat == "(null)" || self.lang == "(null)"{
                    self.getLocation(from: String(format: "%@, %@, %@", self.txtFldAddress.text!,self.txtFldCity.text!,(self.btnCountrySel.titleLabel?.text)!))
                }
           // //print(phone)
            if phone.count >= 10{
            phone.insert("(", at: phone.startIndex)
            phone.insert(")", at: phone.index(phone.startIndex, offsetBy: 4))
            phone.insert("-", at: phone.index(phone.startIndex, offsetBy: 5))
            phone.insert("-", at: phone.index(phone.startIndex, offsetBy: 9))
            }else {
                let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Please insert correct phone number", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            let serviceHanlder = ServiceHandlers()
                if(self.img != nil){
                }else{
                    self.imgName = ""
                }
                serviceHanlder.addEvent(event_type_id: eventTypeId, event_heading: txtfldEventName.text!, event_details: txtFldEventDescription.text!, event_address: txtFldAddress.text!, event_country: countryID, event_state: stateID, event_city: txtFldCity.text!, event_postcode: txtFldPostalcode.text!, event_timezone: timeZoneID, event_latitude: lat, event_longitude: lang, event_email: txtFldEmail.text!, event_phone: txtFldPhone.text!, event_image: self.imgName, event_register_start_date: sDate, event_register_end_date: eDate, event_start_time: conver12HoursTo24(timeAsString : self.StartTime), event_end_time: conver12HoursTo24(timeAsString : self.EndTime), event_waiver_doc: fileName, event_waiver_req: req!,want_to_upload_by_volunteer:(self.radioButtonYes.isSelected ? "yes" : "No"),repeat_every: (self.repeatCountTF.text == "" ? "0" : self.repeatCountTF.text!) , repeat_every_type: (self.req == "1" ? self.getOptionSymbol(optionString: (self.daysOptionButton.titleLabel?.text!)!) : "" ), background_check: self.checkBackgroundVerification.isSelected ? "1" : "0",checked_fields: self.checkBackgroundVerification.isSelected ? self.bdCheckDic as! [String : String] : [:]){ (responce, isSuccess) in
                if isSuccess {
                    let resdata = responce as! [String: Any]
                    self.eventIdFromServer = resdata["event_id"] as! String
                   // //print(resdata)
                    if(self.img != nil){
                    self.uploadImage()
                    }else  if self.waiverfileupload! && (self.file != nil) {
                                               self.uploadWaiverFile()
                    }else{
                        self.txtFldEventDescription.text = ""
                                                  self.txtfldEventName.text = ""
                                                  self.btnTimeZoneSel.setTitle(NSLocalizedString("Select Time Zone", comment: ""), for: .normal)
                                                  self.btnEventSelection.setTitle(NSLocalizedString("Select Event Type", comment: ""), for: .normal)
                                                  self.timeZoneID = ""
                                                  self.eventTypeId = ""
                                                  self.btnStartDate.setTitle("", for: .normal)
                                                  self.sDate = ""
                                                  self.btnEndDate.setTitle("", for: .normal)
                                                  self.eDate = ""
                                                  self.btnStartTime.setTitle("", for: .normal)
                                                  self.StartTime = ""
                                                  self.btnEndTime.setTitle("", for: .normal)
                                                  self.EndTime = ""
                                                  self.file = nil
                                                  self.fileName = ""
                                                  self.img = nil
                                                  self.lat = ""
                                                  self.lang = ""
                        let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Event added successfully", comment: ""), preferredStyle: UIAlertController.Style.alert)
                        
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { (action) in
                            self.data_refresh?.refreshDataList(flagr: true)
                        }))
                           self.present(alert, animated: true, completion: nil)
                        
                    }
                }
            }
            
              view.removeFromSuperview()
            self.myeventview?.isHidden = false
          
        }}
    func getLocation(from address: String) {
        let geocoder = CLGeocoder()
//        let dic = [NSTextCheckingKey.zip: Int(yourZipCode)]
        geocoder.geocodeAddressString(address) { (placemarks, error) in
                guard let placemarks = placemarks,
                let location = placemarks.first?.location?.coordinate else {
                    return
                }
            let lat = location.latitude
            let long = location.longitude
            print("\(lat)===========\(long)")
            self.lat = "\(lat)"
            self.lang = "\(long)"
            }

    }

    //MARK:- UPLOAD IMAGE IN ADD EVENT
    
    func uploadImage() {
        
                let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
                let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
                let params = userIDData["user_id"] as! String
                let eventId = eventIdFromServer
                let apiKey = "1234"
                let Action = "cso_event_file_upload"
        _ = UIDevice.current.identifierForVendor!.uuidString//user_device
        let data2:[String:Any] = ["event_id":eventId,"user_id":params,"api_key":apiKey,"action":Action,"img_name":imgName]
        if(img != nil){
                let imageSize: Int = img!.count
              //  //print("actual size of image in KB: %f ", Double(imageSize)/1000.0 )
                let limit:Double = 5000.0
                if(Double(imageSize/1000) <= limit)
                {
                 //   //print(data2)
                    let serviceHanlder = ServiceHandlers()
                    serviceHanlder.addEventImageUpload(data_details:data2,file:img!) { (responce, isSuccess) in
                        if isSuccess {
                            //  //print(responce)
                            if self.waiverfileupload! && (self.file != nil) {
                                self.uploadWaiverFile()
                            }
                            else{
                                
                                self.txtFldEventDescription.text = ""
                                self.txtfldEventName.text = ""
                                self.btnTimeZoneSel.setTitle(NSLocalizedString("Select Time Zone", comment: ""), for: .normal)
                                self.btnEventSelection.setTitle(NSLocalizedString("Select Event Type", comment: ""), for: .normal)
                                self.timeZoneID = ""
                                self.eventTypeId = ""
                                self.btnStartDate.setTitle("", for: .normal)
                                self.imgEvent.image = UIImage.init(named: "no_image.jpg")
                                self.sDate = ""
                                self.btnEndDate.setTitle("", for: .normal)
                                self.eDate = ""
                                self.btnStartTime.setTitle("", for: .normal)
                                self.StartTime = ""
                                self.btnEndTime.setTitle("", for: .normal)
                                self.EndTime = ""
                                self.file = nil
                                self.fileName = ""
                                self.img = nil
                                self.lat = ""
                                self.lang = ""
                                let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Event updated successfully", comment: ""), preferredStyle: UIAlertController.Style.alert)
                                
                                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            
                        }
                        
                    }
                    self.data_refresh?.refreshDataList(flagr: true)
                     view.removeFromSuperview()
                 self.myeventview?.isHidden = false
                    
                    
                    if !(screenTitle.caseInsensitiveCompare(NSLocalizedString("UPDATE EVENT DETAILS", comment: "")) == .orderedSame){
                        selectbutton = myeventbtn
                               neweventbutton!.setTitleColor(.gray, for: .normal)
                               myeventbtn!.setTitleColor(APP_BLACK_COLOR, for: .normal)
                    }
                }else{
                    let alert = UIAlertController(title: "Alert!", message: NSLocalizedString("Image must be upto 5 MB.", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    
    
    //MARK:- UPLOAD waiver IN ADD EVENT
       
       func uploadWaiverFile() {
                   //Submit Button Functionality
                   let decoded  = UserDefaults.standard.object(forKey: UserDefaultKeys.key_LoggedInUserData) as! Data
                   let userIDData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as!  Dictionary<String, Any>
                   let params = userIDData["user_id"] as! String
                   let eventId = eventIdFromServer
                   let apiKey = "1234"
                   let Action = "event_doc_upload"
        _ = UIDevice.current.identifierForVendor!.uuidString//user_device
        let data2:[String:Any] = ["event_id":eventId,"user_id":params,"api_key":apiKey,"action":Action,"file_name":fileName]

                       let serviceHanlder = ServiceHandlers()
                       serviceHanlder.addEventFileUpload(data_details:data2,file:file!) { (responce, isSuccess) in
                           if isSuccess {
                               
                            self.txtFldEventDescription.text = ""
                            self.txtfldEventName.text = ""
                            self.btnTimeZoneSel.setTitle(NSLocalizedString("Select Time Zone", comment: ""), for: .normal)
                            self.btnEventSelection.setTitle(NSLocalizedString("Select Event Type", comment: ""), for: .normal)
                            self.timeZoneID = ""
                            self.eventTypeId = ""
                            self.btnStartDate.setTitle("", for: .normal)
                            self.sDate = ""
                            self.btnEndDate.setTitle("", for: .normal)
                            self.eDate = ""
                            self.btnStartTime.setTitle("", for: .normal)
                            self.StartTime = ""
                            self.btnEndTime.setTitle("", for: .normal)
                            self.EndTime = ""
                            self.file = nil
                            self.fileName = ""
                            self.imgEvent.image = UIImage.init(named: "no_image.jpg")
                            self.img = nil
                            self.lat = ""
                            self.lang = ""
    let alert = UIAlertController(title: "Success!", message: NSLocalizedString("Event updated successfully", comment: ""), preferredStyle: UIAlertController.Style.alert)
                               
                               // add an action (button)
                               alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                               // show the alert
                               self.present(alert, animated: true, completion: nil)
                           }
                           
                       }
                    self.data_refresh?.refreshDataList(flagr: true)
                     view.removeFromSuperview()
                 self.myeventview?.isHidden = false
    if !(screenTitle.caseInsensitiveCompare("UPDATE EVENT DETAILS") == .orderedSame){
                     selectbutton = myeventbtn
                   neweventbutton!.setTitleColor(.gray, for: .normal)
                 myeventbtn!.setTitleColor(APP_BLACK_COLOR, for: .normal)
                    }
       
       }
       
       //MARK:- VALIDATION IN ADD EVENT FORM
    
    func validate() -> Bool {

            if(file != nil){
             let fileSize: Int = file!.count
            // //print("actual size of image in KB: %f ", Double(fileSize)/1000.0 )
                let FileLimit:Double = 5000.0
                if(Double(fileSize/1000) >= FileLimit){
                        let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("File should be less then 2 MB.", comment: ""), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        return false
                }
            }
            if (self.sDate == "" )
                {
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Start date not selected.", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false
                }
            if (self.eDate == "" )
                {
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("End date not selected.", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false
                }
            if(self.txtfldEventName.text == ""){
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Event name not filled.", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
            }
            if(self.txtFldEventDescription.text == ""){
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Event description not filled", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    return false;
                }
            if(self.eventTypeId == ""){
                    
                    
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Event type not selected", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    return false;
                }
            if(self.timeZoneID == ""){
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Time zone not selected", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
            }
            if(self.txtFldAddress.text == ""){
                    
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Address not filled", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
                }
            if(self.txtFldCity.text == ""){
                    
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("City not filled", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
                }
            if(self.stateID == ""){
                    
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("State not selected", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
                }
            if(self.txtFldPostalcode.text == ""){
                    
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Zip Code not filled", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
                }
            if(self.countryID == ""){
                    
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Country not Selected", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
                }
            if(self.txtFldEmail.text == ""){
                    
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Email not filled", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
                }
        if(self.txtFldPhone.text == ""){
                
                let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Please enter phone number", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return false;
            }
            if !(self.txtFldPostalcode.text?.count == 5 )
                        {
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Invalid zip code!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                                   alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                                   self.present(alert, animated: true, completion: nil)
                                   return false
                
            }
            if !(self.isValidPhoneNumber(text:self.txtFldPhone.text!)){
                    
                let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Invalid phone number", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false
                
            }
            if !(self.isValidUserName(text:self.txtFldEmail.text!)){
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Email invalid!", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false
        }
            if(self.eDate == ""){
                    
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("End date not filled", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
                }
            if(self.StartTime == ""){
                    
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Start time not filled", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
                }
            if(self.EndTime == ""){
                    
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("End time not filled", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
                
            }
            if(self.txtFldPhone.text! == ""){
                    
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("Phone number not filled", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false;
            }
            if self.check {
                    if (self.fileName == ""){
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString("File not selected", comment: ""), preferredStyle: UIAlertController.Style.alert)
                                   alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                                   self.present(alert, animated: true, completion: nil)
                                   return false
                    }
                }
            if eDate < sDate {
                    let alert = UIAlertController(title: NSLocalizedString("Alert!", comment: ""), message: NSLocalizedString(" End date should be greater than start date", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false
                }
        
                return true
    }
    func isValidUserName(text:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
       // //print(emailRegEx)
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
      //  //print(emailTest)
        return emailTest.evaluate(with: text)
    }
    
    func isValidPhoneNumber(text: String)-> Bool{
     
        if text.count == 13
        {
            return true
            
        }
        else{
           return false
        }
//        return true
    }
    
    @IBAction func ResetbuttonPressed(_ sender: Any) {
        if screenTitle == "ADD EVENT DETAILS"{
       // Swift.//print("Reset button pressed")
        self.txtfldEventName.text = ""
        self.txtFldEmail.text = ""
        self.txtFldCity.text = ""
        self.txtFldPhone.text = ""
        self.txtFldEventDescription.text = ""
        self.txtFldAddress.text = ""
        self.txtFldPostalcode.text = ""
        self.img = nil
        self.imgName = ""
        self.file = nil
        self.fileName = ""
        self.imgEvent.image = UIImage.init(named: "no_image.jpg")
        self.lang = ""
        self.lat = ""
        
        self.btnStartDate.setTitle("Enter Start Date", for: .normal)
        self.btnEndDate.setTitle("Enter End Date", for: .normal)
        self.btnStartDateSel.setTitle("Enter Start Date", for: .normal)
        self.btnEndDateSel.setTitle("Enter End Date", for: .normal)
        self.btnStartTime.setTitle("Enter Start Time", for: .normal)
        self.btnEndTime.setTitle("Enter End Time", for: .normal)

        self.sDate = ""
        self.eDate = ""
        self.StartTime = ""
        self.EndTime = ""
        }else{
            let servicehandler = ServiceHandlers()
            servicehandler.getSelectedEventDetails(eventId: eventDetail["event_id"] as! String){ (responce, isSuccess) in
            if isSuccess {
                let data_of_event = responce as! Dictionary<String,Any>
                self.edit_event_data = data_of_event
                let waiver = data_of_event["event_waiver_req"]
                self.btnAddUpdate.setTitle(NSLocalizedString("Update", comment: ""), for: .normal)
                self.btnEndDateSel.setTitle(data_of_event["event_register_end_date"] as? String, for: .normal)
                    self.lat = data_of_event["event_latitude"] as! String
                self.eventIdFromServer = self.eventDetail["event_id"] as! String
               
                    self.lang = data_of_event["event_longitude"] as! String
                    
                    self.eDate = data_of_event["event_register_end_date"] as! String
                    self.sDate = data_of_event["event_register_start_date"] as! String
                self.btnStartDateSel.setTitle(data_of_event["event_register_start_date"] as? String, for: .normal)
    //            var Start
                    self.btnStartTime.setTitle(data_of_event["event_start_time_format"] as? String, for: .normal)
                    self.StartTime = data_of_event["event_start_time_format"] as! String
                    self.btnEndTime.setTitle(data_of_event["event_end_time_format"] as? String, for: .normal)
                    self.EndTime = data_of_event["event_end_time_format"] as! String
                self.txtFldEmail.text = data_of_event["event_email"] as? String
   
    
                print(data_of_event)
                self.txtFldAddress.text = data_of_event["event_address"] as? String ?? "default value"
                
                self.txtFldPostalcode.text = data_of_event["event_postcode"] as? String ?? "default value"

                
                self.txtFldPhone.text = self.formattedNumber(number:(data_of_event["event_phone"] as? String)!)

                self.timeZoneID = (data_of_event["event_timezone"] as? String)!
                 self.txtFldCity.text = (data_of_event["event_city"] as? String)
                self.stateID = data_of_event["event_state"] as! String
                 self.stateName = data_of_event["event_state_name"] as! String
                 self.btnStateSel.setTitle(data_of_event["event_state_name"] as? String, for: .normal)
                 self.btnCountrySel.setTitle(data_of_event["event_country_name"] as? String, for: .normal)
                self.countryID = (data_of_event["event_country"] as? String)!
                 self.txtFldEventDescription.text = data_of_event["event_details"] as? String
                 self.txtfldEventName.text = data_of_event["event_heading"] as? String
                self.imgName = (data_of_event["event_image"] as? String)!
                
                if (self.listTimeZone != nil){
                for zoneName in self.listTimeZone{
                        
                      let strZoneName = zoneName["timezone_name"] as! String
                        print(strZoneName)
                  
                        if strZoneName == data_of_event["event_timezone_name"]as! String
                    {
                   self.btnTimeZoneSel.setTitle(strZoneName, for: .normal)
                                                  }
                        print(data_of_event["event_timezone_name"]as! String)
                        print(strZoneName)
                                              }
                }
                 
                let fileUrl = URL(string: data_of_event["event_waiver_doc"] as! String)
                if(fileUrl != nil)
                {
                if let imageData = NSData(contentsOf: (fileUrl!)){
                    self.file = imageData as Data
                    }
                    
                }
               
                self.btnEventSelection.setTitle(data_of_event["event_type_name"] as? String, for: .normal)
                if(waiver as! String == "1"){
                    
                    self.waiverViewHeight.constant = 189.5
                    self.waiverViewTwoHeight.constant = 60.0
                    self.waiverViewThreeHeight.constant = 80.0
                    self.DetailLabel.text = data_of_event["event_waiver_doc_name"] as? String
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.TapOnLabel))
                   
                    tapGesture.numberOfTapsRequired = 1
                    tapGesture.numberOfTouchesRequired = 1
                    
                    self.DetailLabel.addGestureRecognizer(tapGesture)
                    self.DetailLabel.isUserInteractionEnabled = true
                    self.DetailLabel.attributedText = NSAttributedString(string: self.DetailLabel.text ?? "", attributes:
                    [.underlineStyle: NSUnderlineStyle.single.rawValue])
                        
                        self.fileName = data_of_event["event_waiver_doc_name"] as! String
                    self.waiverDocument.isHidden = false
                    self.checkButtonOutlet.isSelected = true
//                    self.checkButtonOutlet.setImage(UIImage(named: "tick.jpg"), for: .normal)
                    self.waiverText1.isHidden = false
                    self.waiverText2.isHidden = false
                    self.waiverFileButtonOutlet.isHidden = false
                    self.waiverResetButtonOutlet.isHidden = false
                }else{
                    self.waiverViewHeight.constant = 0
                    self.waiverViewTwoHeight.constant = 0.0
                    self.waiverViewThreeHeight.constant = 0.0
                    self.waiverDocument.isHidden = true
                    self.waiverText1.isHidden = true
                    self.waiverText2.isHidden = true
                    self.waiverFileButtonOutlet.isHidden = true
                    self.waiverResetButtonOutlet.isHidden = true
                    self.fileName = ""
                    self.file = nil
                    self.checkButtonOutlet.isSelected = false
                }
                
                if let imageURLstr = data_of_event["event_image"] as? String,
                    let imageURL:URL = URL(string: imageURLstr.replacingOccurrences(of: " ", with: "%20")) {
                     let serviceHanlder = ServiceHandlers()
                     serviceHanlder.getImageFromURL(url: imageURL) { (data, isSuccess) in
                         if let imgData = data as? Data {
                             DispatchQueue.main.async() { () -> Void in
                                 self.img = imgData
                                if let image = UIImage(data: imgData) {
                                    self.imgEvent.image = image
                                    self.imgEvent.contentMode = .scaleToFill
                                }
                                 
                             }
                         }
                     }
                }
                
                self.eventTypeId = data_of_event["event_type_id"] as! String
                           let servicehandler = ServiceHandlers()
                           servicehandler.getEventType(){(responce,isSuccess) in
                               if isSuccess{
                                   let data_event = responce as! Array<Any>
                                   for event in data_event{
                                       let d = event as! Dictionary<String,Any>
                                       if (d["event_type_id"] as! String) == self.eventTypeId {
                                        self.btnEventSelection.setTitle(d["event_type_name"] as? String, for: .normal)
                                       }
                                   }
                               }
                           }
                
                let sv = ServiceHandlers()
                sv.getStateList(country_id: data_of_event["event_country"] as! String){(responce,isSuccess) in
                    if isSuccess{
                        let data = responce as! Array<Any>
                        for state in data{
                            let state_n = state as! Dictionary<String,Any>
                            if(state_n["state_name"] as! String == data_of_event["event_state_name"] as! String)
                            {
                                self.stateCode = state_n["state_code"] as? String
                            }
                        }

                    }

                }
                
            }
                self.view.setNeedsLayout()
                self.viewDidLayoutSubviews()
                self.view.setNeedsUpdateConstraints()
                self.view.layoutIfNeeded()
            }
                
                
            }
        
    }
}

extension CSOEventsViewController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let csoDasboardVC = viewController as? CSODashboardViewController {
            removeAllOtherViewsOfVC(viewcontroller: csoDasboardVC)
            return true
        }
        if let csoEventVC = viewController as? CSOEventsViewController {
            selectedButton = myEventsButton
            tableViewEventList.isHidden = false
            newEventButton.setTitleColor(.gray, for: .normal)
            myEventsButton.setTitleColor(APP_BLACK_COLOR, for: .normal)
            newEventLabl.isHidden = true
            myEventLabl.isHidden = false
            myEventsView.isHidden = false
           removeAllOtherViewsOfVC(viewcontroller: csoEventVC)
           

            return true
        }
        if let csoEventVC = viewController as? volunteerSeeFollowers {
            removeAllOtherViewsOfVC(viewcontroller: csoEventVC)
            return true
        }
        if let csoEventVC = viewController as? CSOMessagingViewController {
            removeAllOtherViewsOfVC(viewcontroller: csoEventVC)
            return true
        }
        if let csoEventVC = viewController as? LockerViewController {
            removeAllOtherViewsOfVC(viewcontroller: csoEventVC)
            return true
        }
        return true
    }
    
    
 
    

func removeAllOtherViewsOfVC(viewcontroller:UIViewController)  {
    
    for vc in viewcontroller.children {
        vc.willMove(toParent: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParent()
    }
}
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


extension CSOAddEventViewController:CropViewControllerDelegate {
    func cropViewController(_ cropViewController: CropViewController, didCropImageToRect rect: CGRect, angle: Int) {
//        self.dismiss(animated: true, completion: nil)
        performSegueToReturnBack()
        self.imagePicker.dismiss(animated: true, completion: nil)
       
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.imgEvent.image = image
        self.imgEvent.contentMode = .scaleToFill
        self.img = (image).jpegData(compressionQuality: 0.5)!

        
        
    }
    
    //vamika
    @IBAction func showDaysOptions(_ sender: Any) {
        let contents = ["Day(s)","Week(s)","Month(s)","Year(s)"]
        showPopoverForView(view: self.daysOptionButton!, contents: contents)
    }
    func getOptionSymbol(optionString:String)-> String{
        if optionString == "Week(s)"{
            return "W"
        }
        else if optionString == "Month(s)"{
            return "M"
        }
        else if optionString == "Year(s)"{
            return "Y"
        }
        else{
            return "D"
        }
    }
    func getOptionSymbolRealName(optionString:String)-> String{
        if optionString == "W"{
            return "Week(s)"
        }
        else if optionString == "M"{
            return "Month(s)"
        }
        else if optionString == "Y"{
            return "Year(s)"
        }
        else{
            return "Day(s)"
        }
    }
    fileprivate func showPopoverForView(view:UIButton, contents:Any) {
        let controller = DropDownItemsTable(contents)
        controller.showPopoverInDestinationVC(destination: self, sourceView: view as! UIView) { (selectedValue) in
            if let selectVal = selectedValue as? String {
                view.setTitle(selectVal, for: .normal)
            }
        }
    }
    
    @IBAction func wantSignedDoc(_ sender: UIButton) {
        if self.radioButtonNo.isSelected{
            self.radioButtonNo.isSelected = false
            self.radioButtonYes.isSelected = true
        }else{
            self.radioButtonYes.isSelected = true
        }
    }
    
    @IBAction func wantUnsignedDoc(_ sender: UIButton) {
        if self.radioButtonYes.isSelected{
            self.radioButtonYes.isSelected = false
            self.radioButtonNo.isSelected = true
        }else{
            self.radioButtonNo.isSelected = true
        }
    }
    @IBAction func viewFields(_ sender: UIButton) {
        var bgCheck = BackgroungCheckForEventVC()
        bgCheck = BackgroungCheckForEventVC.init(nibName: "BackgroungCheckForEventVC", bundle: nil)
        bgCheck.delegate = self
        bgCheck.viewDic = self.bdCheckDic
        bgCheck.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.minY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
//        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        self.view.addSubview(bgCheck.view)
        self.addChild(bgCheck)
    }
    @IBAction func checkBgFields(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            self.bdCheckDic = [String:String]()
        }else{
            sender.isSelected = true
            self.showFieldsView()
        }
    }
    //viewDic
    func showFieldsView(){
        var bgCheck = BackgroungCheckForEventVC()
        bgCheck = BackgroungCheckForEventVC.init(nibName: "BackgroungCheckForEventVC", bundle: nil)
        bgCheck.delegate = self
        if screenTitle == "UPDATE EVENT DETAILS"{
            bgCheck.viewDic = self.bdCheckDic
        }
        bgCheck.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.minY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
//        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        self.view.addSubview(bgCheck.view)
        self.addChild(bgCheck)
    }
    func chooseFieldsWith(dic: [String : String]) {
        self.bdCheckDic = dic
        if dic != nil || dic.count > 0{
            self.checkBackgroundVerification.isSelected = true
        }
    }
}
