//
//  AppDelegate.swift
//  ZoeBlueprint
//
//  Created by Reetesh Bajpai on 03/06/19.
//  Copyright © 2019 Reetesh Bajpai. All rights reserved.
//

import UIKit
import SendBirdSDK // Sendbird
import IQKeyboardManagerSwift
import UserNotifications
import LanguageManager_iOS
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coverPicImagearray = Array<Any>()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        SBDMain.initWithApplicationId("F46FE267-AE82-45B2-9F44-0BD7266FCFDE")
         IQKeyboardManager.shared.enable = true
       
        UITabBar.appearance().barTintColor = .white
       UITabBar.appearance().tintColor = .black

        
         coverPicImagearray = ["cover_riseandshine.jpg", "cover_cake.jpg", "cover_cool.jpg", "cover_truck.jpg","cover_cloud.jpg"]
          UserDefaults.standard.set(coverPicImagearray.randomElement(), forKey: "csocoverpic")
        registerForPushNotifications()
//        let theme = UserDefaults.standard.value(forKey: APP_THEME) as? String
//        if theme == nil || theme == ""{
//        if #available(iOS 13.0, *) {
//            if UIScreen.main.traitCollection.userInterfaceStyle == .dark{
//                UserDefaults.standard.set("Dark Mode", forKey: APP_THEME)
//
//            }else{
//                UserDefaults.standard.set("Light Mode", forKey: APP_THEME)
//            }
//        }else {
            UserDefaults.standard.set("Light Mode", forKey: APP_THEME)
//        }
//        }else{
//            if theme == "Light Mode" {
//                UserDefaults.standard.set("Light Mode", forKey: APP_THEME)
//            }
//            else
//            {
//                UserDefaults.standard.set("Dark Mode", forKey: APP_THEME)
//            }
//        }
    
//
        Global.setUpViewWithTheme(ViewController: (self.window?.rootViewController)!)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print(coverPicImagearray.randomElement()!)
        UserDefaults.standard.set(coverPicImagearray.randomElement(), forKey: "csocoverpic") //setObject
        Global.setUpViewWithTheme(ViewController: (self.window?.rootViewController)!)
        NotificationCenter.default.post(Notification(name: Notification.Name("UPDATE SETTINGS"),object: nil))
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func registerForPushNotifications() {
        UNUserNotificationCenter.current()
          .requestAuthorization(
            options: [.alert, .sound, .badge]) { [weak self] granted, _ in
            print("Permission granted: \(granted)")
            guard granted else { return }
            self?.getNotificationSettings()
          }
    }
    func getNotificationSettings() {
      UNUserNotificationCenter.current().getNotificationSettings { settings in
        print("Notification settings: \(settings)")
      }
    }
    func application(
      _ application: UIApplication,
      didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
      let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
      let token = tokenParts.joined()
      print("Device Token: \(token)")
    }
    func application(
      _ application: UIApplication,
      didFailToRegisterForRemoteNotificationsWithError error: Error
    ) {
      print("Failed to register: \(error)")
    }

}


