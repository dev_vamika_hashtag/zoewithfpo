//
//  Language.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 20/03/20.
//  Copyright © 2020 Reetesh Bajpai. All rights reserved.
//

import UIKit
import LanguageManager_iOS
import CoreLocation
import SideMenu
class Language: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var languageSeg: UISegmentedControl!
    @IBOutlet weak var locationSeg: UISegmentedControl!
    @IBOutlet weak var themeSeg: UISegmentedControl!
    @IBOutlet weak var notificationSeg: UISegmentedControl!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var locationServicePressed: UISwitch!
    @IBOutlet weak var pushNotificationPressed: UISwitch!
    @IBOutlet weak var coverPhoto: UIButton!
    @IBOutlet weak var lblLocationService: UILabel!
    @IBOutlet weak var lblPushNotification: UILabel!
    @IBOutlet weak var lblTheme: UILabel!
    @IBOutlet weak var themePressed: UISwitch!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    var img:Data?
    var profilePhoto:Data?
    var imgName:String = ""
    var ImagePro:String = ""
    var profile:Bool?
    var proImage: String!
    var covImage: String!
    var isEnabled = false
    var type : String!
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        if (self.proImage == "ProfileImageSelected") {
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                
                self.profileImage.image = image
                self.profilePhoto = (image as? UIImage)!.jpegData(compressionQuality: 0.5)!
                guard let fileURL = info[UIImagePickerController.InfoKey.imageURL] as? URL
                else {
                    self.ImagePro = "image2"
                    //        Images()
                    return
                }
                self.ImagePro = fileURL.lastPathComponent
            }else{
                //print("error")
            }
        }else if (self.proImage != "ProfileImageSelected"){
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.coverImage.image = image
                self.img = (image as? UIImage)!.jpegData(compressionQuality: 0.5)!
                guard let fileURL = info[UIImagePickerController.InfoKey.imageURL] as? URL
                else {
                    self.imgName = "Cover_image_name"
                    return
                }
                
                self.imgName = fileURL.lastPathComponent
                //ChooseCoverImage()
                
            }else{
                //print("error")
            }
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func profile_pic()  {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("profilepic.jpg")
            if let image    = UIImage(contentsOfFile: imageURL.path){
                self.profileImage.image = image
                self.profileImage.layer.borderWidth = 1
                self.profileImage.layer.masksToBounds = false
                self.profileImage.layer.borderColor = APP_BLACK_COLOR.cgColor
                self.profileImage.layer.cornerRadius = self.profileImage.frame.height/2
                self.profileImage.clipsToBounds = true
            }
            // Do whatever you want with the image
        }
        
    }
    @IBAction func btnChooseCoverPic(_ sender: Any) {
        
        let alert = UIAlertController(title: NSLocalizedString("Upload cover image", comment: ""), message: "", preferredStyle: .alert)
        let gallery = UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            self.covImage = "CoverImageSelected"
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerController.SourceType.photoLibrary
            image.allowsEditing = false
            image.modalPresentationStyle = .overCurrentContext
            self.present(image, animated: true)
            {
                
            }
            //            self.Images()
            // call method whatever u need
        })
        let camera = UIAlertAction(title: NSLocalizedString("Camera", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            self.covImage = "CoverImageSelected"
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .overCurrentContext
            imagePicker.showsCameraControls = true
            imagePicker.modalPresentationStyle = .overCurrentContext
            self.present(imagePicker, animated: true, completion: nil)
            //            self.Images()
            // call method whatever u need
        })
        let noButton = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(gallery)
        alert.addAction(camera)
        alert.addAction(noButton)
        present(alert, animated: true)
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        performSegueToReturnBack()
        //dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnChooseProfilePic(_ sender: Any) {
        
        let alert = UIAlertController(title: "Upload profile image", message: "", preferredStyle: .alert)
        let gallery = UIAlertAction(title: "Gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            self.proImage = "ProfileImageSelected"
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerController.SourceType.photoLibrary
            //  image.allowsEditing = false
            image.modalPresentationStyle = .overCurrentContext
            self.present(image, animated: true)
            {
                
            }
            // call method whatever u need
            //            self.Images()
        })
        let camera = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            /** What we write here???????? **/
            self.proImage = "ProfileImageSelected"
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .overCurrentContext
            imagePicker.showsCameraControls = true
            imagePicker.modalPresentationStyle = .overCurrentContext
            self.present(imagePicker, animated: true, completion: nil)
            // call method whatever u need
            //            self.Images()
        })
        let noButton = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(gallery)
        alert.addAction(camera)
        alert.addAction(noButton)
        present(alert, animated: true)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let segAttributesNormal: NSDictionary = [
            NSAttributedString.Key.foregroundColor: UIColor.lightGray,
            NSAttributedString.Key.font: UIFont(name: (self.titleLbl.font.fontName), size: 14)!
        ]
        let segAttributes: NSDictionary = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: (self.titleLbl.font.fontName), size: 14)!
        ]
        notificationSeg.setTitleTextAttributes(segAttributesNormal as? [NSAttributedString.Key : Any], for: .normal)
        notificationSeg.setTitleTextAttributes(segAttributes as? [NSAttributedString.Key : Any], for: .selected)
        
        languageSeg.setTitleTextAttributes(segAttributesNormal as? [NSAttributedString.Key : Any], for: .normal)
        languageSeg.setTitleTextAttributes(segAttributes as? [NSAttributedString.Key : Any], for: .selected)
        
        locationSeg.setTitleTextAttributes(segAttributesNormal as? [NSAttributedString.Key : Any], for: .normal)
        locationSeg.setTitleTextAttributes(segAttributes as? [NSAttributedString.Key : Any], for: .selected)
//        if #available(iOS 13.0, *) {
//            self.locationSeg.selectedSegmentTintColor = .clear
//            self.languageSeg.selectedSegmentTintColor = .clear
//            self.notificationSeg.selectedSegmentTintColor = .clear
//        } else {
//            // Fallback on earlier versions
//        }
        // Do any additional setup after loading the view.
        coverImage.contentMode = .scaleAspectFill
        coverImage.clipsToBounds = true
        self.coverPhoto.setTitleColor(UIColor.black, for: UIControl.State.normal)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidBecomeActive),
                                               name:  Notification.Name("UPDATE SETTINGS"),
                                               object: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        permissionForPushNotification(sender: notificationSeg)
        checkLocationUpdate(sender: locationSeg)
        self.setUpLanguage()
        getCoverImageForRank()
//        self.setUpTheme()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        profile_pic()
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewsWithLang()
    }

    @objc func applicationDidBecomeActive() {
        permissionForPushNotification(sender: notificationSeg)
        checkLocationUpdate(sender: locationSeg)
        self.setUpLanguage()
    }
    
    func getCoverImageForRank(){
        if type == "VOL" || type == "EMP"{
            var strImageNameCover = "cover_cloud.jpg"
            
            if let decoded  = UserDefaults.standard.object(forKey: "VolData") as? Data, let volData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? Dictionary<String, Any>, let userAvgRank = volData["user_avg_rank"] as? String {
                
                
                let floatUserAverageRank = Float(userAvgRank)!
                
                
                if ((floatUserAverageRank >= 0) && (floatUserAverageRank <= 20)){
                    strImageNameCover = "cover_riseandshine.jpg"
                }else if ((floatUserAverageRank > 20) && (floatUserAverageRank <= 40)){
                    strImageNameCover = "cover_cake.jpg"
                }else if ((floatUserAverageRank > 40) && (floatUserAverageRank <= 60)){
                    strImageNameCover = "cover_cool.jpg"
                }else if ((floatUserAverageRank > 60) && (floatUserAverageRank <= 80)){
                    strImageNameCover = "cover_truck.jpg"
                }else if (floatUserAverageRank > 80 ){
                    strImageNameCover = "cover_cloud.jpg"
                }
                
                self.coverImage.image = UIImage(named:strImageNameCover)
            }
        }else{
            self.coverImage.image = UIImage(named:UserDefaults.standard.string(forKey: "csocoverpic")!)
        }
        
        
        
    }
    
    @IBAction func onClickNotificationSeg(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            notificationServicesEnabled()
            if isEnabled {
                sender.selectedSegmentIndex = 1
                print("its on!")
            } else {
                sender.selectedSegmentIndex = 0
                //                sender.setOn(false, animated: true)
                print("its off!")
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        } else {
            sender.selectedSegmentIndex = 0
            //            sender.setOn(false, animated: true)
            print("its off!")
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func onChangeAppTheme(_ sender: UISegmentedControl) {
        if (sender.selectedSegmentIndex == 1){
            
            UserDefaults.standard.set("Dark Mode", forKey: APP_THEME)
            self.SetTheme()
            
        }else{
            
            UserDefaults.standard.set("Light Mode", forKey: APP_THEME)
            self.SetTheme()
            
        }
    }
    
    @IBAction func onClickNotification(_ sender: Any) {
        Utility.showNotificationScreen(navController: self.navigationController)
    }
    @IBAction func onClickMenu(_ sender: Any) {
        if let menu = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: (type == "VOL" || type == "EMP") ? "csoSideMenu" : "volsidemenu") as? SideMenuNavigationController{
//            self.navigationController?.present(menu, animated: true, completion: nil)
        present(menu, animated: true, completion: nil)
        }
    }
    @IBAction func onClickLocationSeg(_ sender: UISegmentedControl) {
        StartupdateLocation(locationSeg)
    }
    
    func updateViewsWithLang(){
        
        
    }
    @IBAction func onPressLanguageSegment(_ sender: UISegmentedControl) {
        
        
        let alertController = UIAlertController(title: "Are you sure?", message: "Please change you iPhone language.", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            if let url = NSURL(string: "App-Prefs:root=General") {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Not Now!", style: UIAlertAction.Style.default, handler: {(cAlertAction) in
            //Redirect to Settings app
            self.setUpLanguage()
        })
        
        alertController.addAction(cancelAction)
        
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func setUpLanguage(){
        let selectedLang =  NSLocale.current.identifier
        if selectedLang.contains("en_"){
            languageSeg.selectedSegmentIndex = 0
        }else{
            languageSeg.selectedSegmentIndex = 1
        }
        
    }
    
    func setUpTheme() {
        
        let theme = UserDefaults.standard.value(forKey: APP_THEME)
        if theme as! String == "Light Mode"{
            self.themeSeg.selectedSegmentIndex = 0
            //            self.themePressed.setOn(false, animated: true)
        }else{
            self.themeSeg.selectedSegmentIndex = 1
            //            self.themePressed.setOn(true, animated: true)
            self.SetTheme()
            
        }
    }
    func SetTheme() {
        Global.setUpViewWithTheme(ViewController: self)
    }
    
    
    
    func permissionForPushNotification(sender: UISegmentedControl){
        self.notificationServicesEnabled()
        if(isEnabled){
            sender.selectedSegmentIndex  = 1
            //            sender.setOn(true, animated: true)
        }
        else{
            sender.selectedSegmentIndex  = 0
            //            sender.setOn(false , animated: false)
        }
    }
    
    func notificationServicesEnabled() -> Bool {
        isEnabled = false
        if UIApplication.shared.responds(to: #selector(getter: UIApplication.currentUserNotificationSettings)) {
            let notificationSettings = UIApplication.shared.currentUserNotificationSettings
            
            if notificationSettings == nil || (notificationSettings?.types == []) {
                isEnabled = false
            } else {
                isEnabled = true
            }
        } else {
            
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                isEnabled = true
            } else {
                isEnabled = false
            }
        }
        return isEnabled
    }
    //    @IBAction func btnLocationService(_ sender: UISwitch) {
    //        StartupdateLocation(locationServicePressed)
    //    }
    func isLocationAccessEnabled()-> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("location No access")
                
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                print("location Access")
                return true
            }
        } else {
            print("Location services not enabled")
            return false
        }
    }
    func checkLocationUpdate(sender: UISegmentedControl){
        let isEnable = isLocationAccessEnabled()
        if isEnable{
            sender.selectedSegmentIndex = 1
            //            sender.setOn(true, animated: true)
        }else{
            sender.selectedSegmentIndex = 0
            //            sender.setOn(false, animated: true)
        }
    }
    func StartupdateLocation(_ sender: UISegmentedControl) {
        let locationManager = CLLocationManager.init()
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startUpdatingLocation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if sender.selectedSegmentIndex == 1 {
                let isEnable = self.isLocationAccessEnabled()
                if isEnable {
                    sender.selectedSegmentIndex = 1
                    //                    sender.setOn(true, animated: true)
                    print("location  on!")
                } else {
                    //                    sender.setOn(false, animated: true)
                    self.showPermissionAlert(self.locationSeg)
                    print("location off!")
                }
            } else {
                //                sender.setOn(false, animated: true)
                self.showLocationDeniedAlert(self.locationSeg)
                print("location off!")
                
            }
        }
        
    }
    func showLocationDeniedAlert(_ sender: UISegmentedControl){
        let alertController = UIAlertController(title: NSLocalizedString("Are you Sure?", comment: ""), message: NSLocalizedString("Please disable location permissions in settings.", comment: ""), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.cancel, handler: {(cAlertAction) in
            //Redirect to Settings app
            sender.selectedSegmentIndex = 1
            //            sender.setOn(true, animated: true)
        })
        
        alertController.addAction(cancelAction)
        
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func showPermissionAlert(_ sender: UISegmentedControl){
        let alertController = UIAlertController(title: NSLocalizedString("Please enable location permissions in settings", comment: ""), message: "", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.cancel)
        alertController.addAction(cancelAction)
        
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        if controller.documentPickerMode == UIDocumentPickerMode.import {
            
        }
        
        _ = url as URL//let cico
        //print(cico)
        //print(url)
        
        // //print(url.lastPathComponent)
        do{
            self.img = try Data(contentsOf: url)
            
        } catch {
            //print(error)
        }
        
        
        
    }
    
    
    
}
