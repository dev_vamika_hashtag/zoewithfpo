//
//  String + Extension.swift
//  ZoeBluePrint
//
//  Created by HashTag Labs on 09/02/21.
//  Copyright © 2021 Reetesh Bajpai. All rights reserved.
//

import Foundation
import LanguageManager_iOS

public extension String {

 
  func localize(comment: String = "") -> String {
    guard let bundle = Bundle.main.path(forResource: LanguageManager.shared.currentLanguage.rawValue, ofType: "lproj") else {
      return NSLocalizedString(self, comment: comment)
    }

    let langBundle = Bundle(path: bundle)

    return NSLocalizedString(self, tableName: nil, bundle: langBundle!, comment: comment)
  }

}
